#*******************************
#
# Repast HPC Tutorial 
# ENVIRONMENT DEFINITIONS
#
#*******************************

MPICXX=mpicxx -std=c++17 -Wno-deprecated 

BOOST_LIBS=-lboost_mpi -lboost_serialization -lboost_system -lboost_filesystem -lboost_date_time -lboost_iostreams
BOOST_LIB_DIR=$(BOOST_ROOT)/lib
REPAST_HPC_LIB_DIR=$(REPASTHPC_ROOT)/lib
REPAST_HPC_INCLUDE=-I$(REPASTHPC_ROOT)/include

REPAST_HPC_LIBS=-lrepast_hpc-2.3.0
