#!/usr/bin/env bash

#SBATCH --ntasks=3
#SBATCH -o slurm_simulation/test/test-%j.out # STDOUT
#SBATCH -e slurm_simulation/error/err-%j.err # STDERR


# on s'assure d'avoir un environnement propre
module purge

# on charge les module necessaire au le script
module load gcc/9.2.0
module load openmpi/4.0.5
module load netcdf/4.7.3
module load boost/1.72.0
module load repast_hpc/2.3.0


nbOfSim=200

nbOfScenario=3

nbSupers=60

fileNameAnalyze1="byCatSC"
xtra1="complianceSC_PVS/"

sbatch -p common,dedicated --qos=fast --job-name=analysis ./modelInterventionsCP.sh $nbOfScenario $nbOfSim $fileNameAnalyze1 $xtra1 $nbSupers

fileNameAnalyze2="byCatSCVC"
xtra2="vaccinationSC_PVS/"

sbatch -p common,dedicated --qos=fast --job-name=analysis ./modelInterventionsVCSC.sh $nbOfScenario $nbOfSim $fileNameAnalyze2 $xtra2 $nbSupers
