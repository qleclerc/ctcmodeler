#!/bin/bash
#SBATCH -N 1
#SBATCH --mem=2000
#SBATCH -o slurm_simulation/test/test_%A-%a.out # STDOUT
#SBATCH -e slurm_simulation/error/err_%A-%a.err # STDERR

# Pour charger R dans l environnement
module load R/4.2.2
module load gcc/9.2.0

intervention=$1

if [ $intervention = "cohorting" ]
then
    echo "Cohorting analysis"
    Rscript cohorting_analysis.R
fi

if [ $intervention = "compare" ]
then
    echo "Comparison analysis"
    Rscript compare.R
fi

if [ $intervention = "process_60" ]
then
    echo "Comparison2 60 SC analysis"
    Rscript process_results_60.R
fi

if [ $intervention = "process_20" ]
then
    echo "Comparison2 20 SC analysis"
    Rscript process_results_20.R
fi

if [ $intervention = "process_100" ]
then
    echo "Comparison2 100 SC analysis"
    Rscript process_results_100.R
fi

if [ $intervention = "process_60_alt" ]
then
    echo "Comparison2 60 SC analysis"
    Rscript process_results_60_alt.R
fi

if [ $intervention = "process_20_alt" ]
then
    echo "Comparison2 20 SC analysis"
    Rscript process_results_20_alt.R
fi

if [ $intervention = "process_100_alt" ]
then
    echo "Comparison2 100 SC analysis"
    Rscript process_results_100_alt.R
fi

if [ $intervention = "sensi1" ]
then
    echo "Comparison2 sensi analysis 1"
    Rscript process_results_sensi_part1.R
fi

if [ $intervention = "sensi2" ]
then
    echo "Comparison2 sensi analysis 2"
    Rscript process_results_sensi_part2.R
fi

if [ $intervention = "PVS" ]
then
    echo "Comparison2 PVS analysis"
    Rscript process_results_PVS.R
fi

if [ $intervention = "validation" ]
then
    echo "validation analysis"
    Rscript process_validation.R
fi