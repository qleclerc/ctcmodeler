#!/usr/bin/env bash
#SBATCH -n 6
#SBATCH -o slurm_simulation/test/test-%j.out # STDOUT
#SBATCH -e slurm_simulation/error/err-%j.err # STDERR


# on s'assure d'avoir un environnement propre
module purge

# on charge les module necessaire au le script
module load gcc/9.2.0
module load openmpi/4.0.5
module load netcdf/4.7.3
module load boost/1.72.0
module load repast_hpc/2.3.0

FA="yes"
NOA=2
BTV="yes"
NCIV=0

nbOfNet=$1
nbOfSim=$2
addName=$3
fileNameAnalyze=$4
xtra=$5

# on va faire d'abord le real
Command="sbatch -p common,dedicated --qos=fast ./modelCluster.sh -o no -b no -m real -d none -f $FA -n $NOA"
Submit_Output="$($Command 2>&1)"
JobR=`echo $Submit_Output | grep 'Submitted batch job' | awk '{print $4}'`
echo "le job id du real est "
echo $JobR

# réseaux avec biais pour comparaison
#for i in `seq 1 $nbOfNet`;do
#	addName0="SimulatedCtcBias"$addName$i
#	echo "Construction du réseau "$i
#	echo $addName0
#	Command="sbatch -p common --qos=fast --job-name=simuBiasNetworks ./modelCluster.sh -o no -b no -d build -f $FA -n $NOA -t $BTV -z $NCIV -g testRecord -r $addName0 "
#	Submit_Output="$($Command 2>&1)"
#	JobIdSimCtc=`echo $Submit_Output | grep 'Submitted batch job' | awk '{print $4}'`
#done

# on va d'abord faire les réseaux
declare -A jobScenario
for i in `seq 1 $nbOfNet`;do
	filename1="SimulatedCtcValidation"$addName$i
	echo "Construction du réseau "$i
	echo $filename1
	Command="sbatch -p common --qos=fast --job-name=simuMultipleNetwork ./modelCluster.sh -o no -b no -d build -f $FA -n $NOA -t $BTV -z $NCIV -g $filename1 "
	Submit_Output="$($Command 2>&1)"
	JobIdSimCtc=`echo $Submit_Output | grep 'Submitted batch job' | awk '{print $4}'`
	jobScenario[$i]=$JobIdSimCtc
done

#puis les learnings
declare -A jobScenario2
for j in `seq 1 $nbOfNet`;do
	 echo "les jobs learnings sont: "
	 echo ${jobScenario[$j]}
	filename2="SimulatedCtcValidation"$addName$j
	echo $filename2
	learningF1="parameters/resultParameters_SimulatedCtcValidation"$addName$j".csv"
	Command="sbatch -p common --qos=fast --job-name=simuMultipleNetwork --dependency=afterany:${jobScenario[$j]} ./modelCluster.sh -o no -b yes -d use -f $FA -n $NOA -t $BTV -z $NCIV -l $learningF1 -g $filename2"
	Submit_Output="$($Command 2>&1)"
	JobIdSimCtc=`echo $Submit_Output | grep 'Submitted batch job' | awk '{print $4}'`
	echo "les jobs learning de minimumboucle sont: "
	echo $JobIdSimCtc
	jobScenario2[$j]=$JobIdSimCtc
done

declare -A jobSimulation
#puis les simulations
for k in `seq 1 $nbOfNet`;do
	filename3="SimulatedCtcValidation"$addName$k
	echo $filename3
	learningF2="parameters/resultParameters_SimulatedCtcValidation"$addName$k".csv"	
	Command="sbatch -p common,dedicated --qos=fast --array=1-$nbOfSim --job-name=simuMultipleNetwork --dependency=afterany:${jobScenario2[$k]} ./modelCluster.sh -o no -b no -m sim -d use -f $FA -n $NOA -t $BTV -z $NCIV -l $learningF2 -g $filename3 -x $xtra"
	Submit_Output="$($Command 2>&1)"
	JobIdSim=`echo $Submit_Output | grep 'Submitted batch job' | awk '{print $4}'`
	jobSimulation[$k]=$JobIdSim
	echo "les jobs simulations de minimumboucle sont: "
	echo $JobIdSim
done


