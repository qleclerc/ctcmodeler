/* ORGANISM CLASS
* this file gather all organism from the project
*
*	
*	Organism.cpp
*
*	Created on: march 09, 2018
*	Author: Audrey
*
*/
#include "Organism.h"


using namespace std;
using namespace repast;


Organism::Organism(){
	
}

Organism::Organism(repast::AgentId id): id_(id){
	
}

Organism::~Organism(){
	
}

OrganismPackage::OrganismPackage(){
	
}

OrganismPackage::OrganismPackage(int _id, int _rank, int _type, int _currentRank): id(_id), rank(_rank), type(_type), currentRank(_currentRank){ 

}

void Organism::affiche() const{
	
	cout<< "I'm alive! " <<endl;
}

void Organism::set(int _currentRank){
	id_.currentRank(_currentRank);

}