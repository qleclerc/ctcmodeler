/* Pathogen
* 
*
*	
*	Pathogen.h
*
*	Created on: Feb 22, 2018
*	Author: Audrey
*
*/

#ifndef PATHOGEN_H_
#define PATHOGEN_H_

#include "Individual.h"
#include "Organism.h"

#include "repast_hpc/TDataSource.h"
#include "repast_hpc/SVDataSet.h"
#include "repast_hpc/SharedContext.h"
#include "repast_hpc/Schedule.h"
#include "repast_hpc/SharedNetwork.h"
#include "repast_hpc/Edge.h"
#include "repast_hpc/Random.h"
#include "repast_hpc/Properties.h"
#include "repast_hpc/Utilities.h"
#include <ctime>
#include <map>

#include <boost/serialization/export.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/map.hpp> 
#include <boost/serialization/vector.hpp>
#include <boost/serialization/access.hpp>


class Individual;

class Phenotype{
	friend class boost::serialization::access;
	friend class Bacteria;
	
	protected:
	std::vector<std::string> listATB;
	std::vector<std::string> listPhenot;
	std::string atbString;
	
	public:
	Phenotype();
	Phenotype(std::string liste, std::string atb);
	Phenotype(Phenotype const& phenotypeCopy);
	Phenotype(std::vector<std::string> listATB_,std::vector<std::string> listPhenot_);
	~Phenotype();
	/*Getter*/
	std::vector<std::string> getListATB(){return listATB;}
	std::vector<std::string> getListPhenot() const {return listPhenot;}
	std::string getAtbString() const {return atbString;}
	

	template<class Archive>
        void serialize( Archive& ar, const unsigned int /*version*/ )
        {
			ar & listATB;
            ar & listPhenot;
			ar & atbString;

        }
};

class Pathogen: public Organism  {
	friend class boost::serialization::access;
	
	protected:
		std::string typePath;
		std::map <std::string, std::string> positiveIndividuals ; //attiention ici pour la dernière date. Quand on a l'info des prlvt, c'est la date de dernier prlvt. Quand on ne l'a pas comme dans les simulations c'est la premiere date de rencontre. Pour l'instant avoir cette info n'est pas encore utile mais à l'avenir il faudra faire un choix sur cette date
		std::vector <std::string> actualPositiveIndividuals ;
		std::map <std::pair<std::string, std::string>, std::vector <double> > probabilityOfTransmission;
	
	public:
		Pathogen();
		Pathogen(repast::AgentId id, std::string typePath_);
		Pathogen(repast::AgentId id, std::string typePath_,std::map <std::string, std::string> positiveIndividuals_, std::vector <std::string> actualPositiveIndividuals_,std::map <std::pair<std::string, std::string>, std::vector <double> > probabilityOfTransmission_);
		Pathogen(repast::AgentId id);
		Pathogen(std::string typePath_);
		
		Pathogen(Pathogen const& pathoCopy);
		
		virtual ~Pathogen();
		
		virtual void affiche() const;
		
		std::string getTypePath(){return typePath;}

		bool operator==(const Pathogen &b);
		bool isEqual(vector<string> tmp);
		std::map<std::string,std::string> getPositiveIndividuals(){return positiveIndividuals;}
		std::vector <std::string> getActualPositiveIndividuals(){return actualPositiveIndividuals;}
		std::map <std::pair<std::string, std::string>, std::vector <double> > getProbabilityOfTransmission(){return probabilityOfTransmission;}
		
		/*Setters*/
		void setPositiveIndividuals(std::map <std::string,std::string> p){positiveIndividuals=p;}
		void setActualPositiveIndividuals(std::vector <std::string> a){actualPositiveIndividuals=a;}
		void removeIndFromActualPositiveIndividuals(std::string i);
		void setProbabilityOfTransmission(std::map <std::pair<std::string, std::string>, std::vector <double> > m ){probabilityOfTransmission = m;}
		void addPositiveIndividuals(std::string p2, std::string s){positiveIndividuals[p2]=s;}
		void addActualPositiveIndividuals(std::string p3);
		void addNewProbabilityOfTransmission(std::string pa, std::string pe, std::vector<double> value){probabilityOfTransmission.insert(make_pair(make_pair(pa,pe), value));}
		void addProbabilityOfTransmission(std::string pa, std::string pe, double value){probabilityOfTransmission[make_pair(pa,pe)].push_back(value);}
		
		/*Setter*/
		void set(int _currentRank, std::string _typePath, std::map <std::string, std::string> _positiveIndividuals, std::vector <std::string> _actualPositiveIndividuals, std::map <std::pair<std::string, std::string>, std::vector <double> > _probabilityOfTransmission);
		
		void addIndividual(std::string individual, std::string dateAdd);
		
	template<class Archive>
        void serialize( Archive& ar, const unsigned int /*version*/ )
        {
			ar & boost::serialization::base_object<Organism>(*this);
            ar & typePath;
            ar & positiveIndividuals;
			ar & actualPositiveIndividuals;
			ar & probabilityOfTransmission;

        }
	
};


class Bacteria: public Pathogen{
	friend class boost::serialization::access;
	
	private:
	std::string stringId;
	std::string family;
	std::string genus;
	std::string species;
	std::string spatype;
	std::string BMR;
	Phenotype sequenceATB;
		
	public:
	Bacteria();
	Bacteria(Bacteria const& bactCopy);
	Bacteria(std::string typePath_, std::string family_, std::string genus_, std::string species_, std::string spatype_, std::string BMR_);
	Bacteria(std::string typePath_, std::string family_, std::string genus_, std::string species_,  std::string spatype_, std::string BMR_, Phenotype sequenceATB_);
	
	Bacteria(repast::AgentId id, std::string typePath_, std::string family_, std::string genus_, std::string species_,  std::string spatype_, std::string BMR_, Phenotype sequenceATB_);
	Bacteria(repast::AgentId id, std::vector<std::string> v, std::string listATB);
	Bacteria(repast::AgentId id, std::string typePath_, std::map <std::string, std::string> positiveIndividuals_, std::vector <std::string> actualPositiveIndividuals_, std::map <std::pair<std::string, std::string>, std::vector <double> > probabilityOfTransmission_, std::string family_, std::string genus_, std::string species_,  std::string spatype_, std::string BMR_,Phenotype sequenceATB_);
	virtual ~Bacteria();
	
	virtual void affiche() const;
	void affiche2() const;
	
	virtual bool isEqual(const Bacteria &b) const;
	// bool operator==(const Bacteria &b);
	
	/*Setter*/
	void set(int _currentRank, std::string typePath_, std::map <std::string, std::string> positiveIndividuals_, std::vector <std::string> actualPositiveIndividuals_, std::map <std::pair<std::string, std::string>, std::vector <double> > probabilityOfTransmission_, std::string _family, std::string _genus, std::string _species, std::string _spatype, std::string _BMR, Phenotype _sequenceATB);
	
	/*Getter*/
	std::string getFamily() const{return family;}
	std::string getGenus()const{return genus;}
	std::string getSpecies() const{return species;}
	std::string getSpatype() const{return spatype;}
	std::string getBMR() const{return BMR;}
	std::string getStringId() const{return stringId;}
	std::string getPhenotype() {return sequenceATB.getAtbString();}
	Phenotype getPhenotypeClass() const{return sequenceATB;}
	
	template<class Archive>
        void serialize( Archive& ar, const unsigned int /*version*/ )
        {
			ar & boost::serialization::base_object<Pathogen>(*this);
            ar & family;
            ar & genus;
			ar & species;
			ar & spatype;
			ar & BMR; 
			ar & sequenceATB;

        }
};


/* Serializable Agent Package */
struct PathogenPackage: public OrganismPackage{
	
public:
	// int    id;
    // int    rank;
    // int    type;
    // int    currentRank;
	std::string typePath;
	std::map <std::string, std::string> positiveIndividuals ;
	std::vector <std::string> actualPositiveIndividuals ;
	std::map <std::pair<std::string, std::string>, std::vector <double> > probabilityOfTransmission;
    

    /* Constructors */
    PathogenPackage(); // For serialization
    PathogenPackage(int _id, int _rank, int _type, int _currentRank, std::string _typePath, std::map <std::string, std::string> _positiveIndividuals, std::vector <std::string> _actualPositiveIndividuals, std::map <std::pair<std::string, std::string>, std::vector <double> > _probabilityOfTransmission);
	virtual ~PathogenPackage()=default;
	
    /* For archive packaging */
    template<class Archive>
    void serialize(Archive &ar, const unsigned int version){
		ar & boost::serialization::base_object<OrganismPackage>(*this);
        // ar & id;
        // ar & rank;
        // ar & type;
        // ar & currentRank;
        ar & typePath;
        ar & positiveIndividuals;
		ar & actualPositiveIndividuals;
		ar & probabilityOfTransmission;
    }

};

struct BacteriaPackage: public PathogenPackage{
	
public:
	// int    id;
    // int    rank;
    // int    type;
    // int    currentRank;
	// std::string typePath;
	// std::map <std::string, std::string> positiveIndividuals ;
	// std::vector <std::string> actualPositiveIndividuals ;
	// std::map <std::pair<std::string, std::string>, std::vector <int> > probabilityOfTransmission;
	std::string family;
	std::string genus;
	std::string species;
	std::string spatype;
	std::string BMR;
	Phenotype sequenceATB;
    

    /* Constructors */
    BacteriaPackage(); // For serialization
    BacteriaPackage(int _id, int _rank, int _type, int _currentRank, std::string _typePath, std::map <std::string, std::string> _positiveIndividuals, std::vector <std::string> _actualPositiveIndividuals, std::map <std::pair<std::string, std::string>, std::vector <double> > _probabilityOfTransmission,std::string _family, std::string _genus, std::string _species,  std::string _spatype,  std::string _BMR, Phenotype _sequenceATB);
	virtual ~BacteriaPackage()=default;
	
    /* For archive packaging */
    template<class Archive>
    void serialize(Archive &ar, const unsigned int version){
		ar & boost::serialization::base_object<PathogenPackage>(*this);
        // ar & id;
        // ar & rank;
        // ar & type;
        // ar & currentRank;
		// ar & typePath;
        // ar & positiveIndividuals;
		// ar & actualPositiveIndividuals;
		// ar & probabilityOfTransmission;
        ar & family;
        ar & genus;
		ar & species;
		ar & spatype;
		ar & BMR; 
		ar & sequenceATB;
    }

};

#endif












