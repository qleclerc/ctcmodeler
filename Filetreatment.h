/* Filetreatment
* this file gather all agent from the project
*
*	
*	Filetreatment.h
*
*	Created on: Nov 17, 2017
*	Author: Audrey
*
*/

#ifndef FILETREATMENT_H_
#define FILETREATMENT_H_

#include <boost/mpi.hpp>
#include "repast_hpc/Schedule.h"
#include "repast_hpc/Properties.h"
#include "repast_hpc/SharedContext.h"
#include "repast_hpc/SharedNetwork.h"
#include "repast_hpc/AgentRequest.h"
#include "repast_hpc/Properties.h"

#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <vector> 
#include <map>
#include <algorithm>
#include <boost/algorithm/string.hpp>
#include <numeric>

#include "Organism.h"

class Parameters {
	/* Classe qui permet de créer un objet avec tous les paramètres nécessaires à la simulation */
	
	public:
	//type - nombre d'admission par semaine
	std::map<std::string, double> mapAdmitted; // le nombre d'admission par semaine pour chaque catégorie
	//pathogen - type - moyenne - variance
	std::map<std::string, std::map<std::string, std::vector<std::pair<std::string, double>>>> mapDuration; //map (key: pathogen, val: une autre map --> avec en string le type PA ou PE et en val: vector de pair avec la moyenne et la variance des durées de colonisation)
	//pathogen - probabilité d'admission
	std::map<std::string, double> mapAdmission; //map des admissions, pour chaque categories donne la probabilité d'admission par semaine
	std::map<std::string, double> mapDischarge;//idem que pour les admissions
	std::map<std::string, std::map<std::string, double>> mapColoAtAdmission; //donne pour chaque pathogen et chaque cat la proba d'être admis colonisé par semaine
	std::map<std::string, std::map<std::string, double>> mapProbabilityOfTransmission;//la map des probabilité de transmission
	std::map<std::string, std::map<std::string, std::vector<double>>> mapProbabilityOfTransmission2;//la map des probabilité de transmission
	std::map<std::string, std::map<std::string, double>> mapLengthOfStay;//map qui donne pour chaque catégorie la durée de séjour moyen ainsi que la variance
	std::map<std::string, std::map<std::string, std::vector<double>>> mapDistributionDuration; // map qui donne pour les PE et les PA les durées de colonisation
	//jour de la semaine - type - moyenne - variance
	std::map<std::string, std::map<std::string, std::map<std::string, double>>> mapDaysOfWeek; //map qui donne pour chaque jour de la semaine et pour chaque type, la moyenne et la variance du nombre de prélèvement pour la période
	std::map<std::string, std::map<std::string, double>> mapAdmissionDistribution; //map qui donne pour chaque coupe type_motif/cat_service la moyenne et la variance des admissions sur la période 
	std::map<std::string, std::map<std::string, double>> mapDischargeDistribution;//map qui donne pour chaque coupe type_motif/cat_service la moyenne et la variance des discharges sur la période 
	std::map<std::string, double> mapNbOfInd; // map qui donne pour chaque couple type_motif/cat_service le nombre d'individus à l'initialisation déterminer à partir du fichier d'admission pour les prochaines simulations
	
	public:
	//constructeur par défaut
	Parameters(); 
	//destructeur
	virtual ~Parameters(); 
	//1er constructeur
	Parameters(std::map<std::string, std::map<std::string, std::vector<std::pair<std::string, double>>>> mapDuration_, std::map<std::string, double> mapAdmission_, std::map<std::string, double> mapDischarge_, std::map<std::string, std::map<std::string, double>> mapColoAtAdmission_, std::map<std::string, std::map<std::string, double>> mapProbabilityOfTransmission_);
	
	Parameters(std::map<std::string, std::map<std::string, std::vector<std::pair<std::string, double>>>> mapDuration_, std::map<std::string, double> mapAdmission_, std::map<std::string, double> mapDischarge_, std::map<std::string, std::map<std::string, double>> mapColoAtAdmission_, std::map<std::string, std::map<std::string, std::vector<double>>> mapProbabilityOfTransmission2_);
	
	//2eme constructeur
	Parameters(std::map<std::string, double> mapAdmission_, std::map<std::string, double> mapDischarge_);
	
	//3eme constructeur
	Parameters(std::map<std::string, std::map<std::string, std::vector<std::pair<std::string, double>>>>mapDuration_, std::map<std::string, std::map<std::string, double>> mapColoAtAdmission_, std::map<std::string, std::map<std::string, double>> mapProbabilityOfTransmission_);
	
	Parameters(std::map<std::string, std::map<std::string, std::vector<std::pair<std::string, double>>>>mapDuration_, std::map<std::string, std::map<std::string, double>> mapColoAtAdmission_, std::map<std::string, std::map<std::string, std::vector<double>>> mapProbabilityOfTransmission2_);
	
	//4eme constructeur 
	Parameters(std::map<std::string, std::map<std::string, std::vector<std::pair<std::string, double>>>>mapDuration_, std::map<std::string, double> mapAdmission_, std::map<std::string, double> mapDischarge_, std::map<std::string, std::map<std::string, double>> mapColoAtAdmission_, std::map<std::string, std::map<std::string, double>> mapProbabilityOfTransmission_, std::map<std::string, std::map<std::string, double>> mapLengthOfStay_, std::map<std::string, double> mapAdmitted_);
	
	Parameters(std::map<std::string, std::map<std::string, std::vector<std::pair<std::string, double>>>>mapDuration_, std::map<std::string, double> mapAdmission_, std::map<std::string, double> mapDischarge_, std::map<std::string, std::map<std::string, double>> mapColoAtAdmission_, std::map<std::string, std::map<std::string, std::vector<double>>> mapProbabilityOfTransmission2_, std::map<std::string, std::map<std::string, double>> mapLengthOfStay_, std::map<std::string, double> mapAdmitted_);
	
	//5eme constructeur
	Parameters(std::map<std::string, double> mapAdmission_, std::map<std::string, double> mapDischarge_, std::map<std::string, std::map<std::string, double>> mapLengthOfStay_, std::map<std::string, double> mapAdmitted_);
	
	//6eme constructeur
	Parameters(std::map<std::string, std::map<std::string, std::vector<std::pair<std::string, double>>>> mapDuration_, std::map<std::string, double> mapAdmission_, std::map<std::string, double> mapDischarge_, std::map<std::string, std::map<std::string, double>> mapColoAtAdmission_, std::map<std::string, std::map<std::string, double>> mapProbabilityOfTransmission_, std::map<std::string, std::map<std::string, std::map<std::string, double>>> mapDaysOfWeek_);
	
	Parameters(std::map<std::string, std::map<std::string, std::vector<std::pair<std::string, double>>>> mapDuration_, std::map<std::string, double> mapAdmission_, std::map<std::string, double> mapDischarge_, std::map<std::string, std::map<std::string, double>> mapColoAtAdmission_, std::map<std::string, std::map<std::string, std::vector<double>>> mapProbabilityOfTransmission2_, std::map<std::string, std::map<std::string, std::map<std::string, double>>> mapDaysOfWeek_);
	
	//7eme constructeur
	Parameters(std::map<std::string, std::map<std::string, std::vector<std::pair<std::string, double>>>>mapDuration_, std::map<std::string, std::map<std::string, std::vector<double>>> mapDistributionDuration_, std::map<std::string, std::map<std::string, double>> mapColoAtAdmission_, std::map<std::string, std::map<std::string, double>> mapProbabilityOfTransmission_, std::map<std::string, std::map<std::string, std::map<std::string, double>>> mapDaysOfWeek_);
	
	Parameters(std::map<std::string, std::map<std::string, std::vector<std::pair<std::string, double>>>>mapDuration_, std::map<std::string, std::map<std::string, std::vector<double>>> mapDistributionDuration_, std::map<std::string, std::map<std::string, double>> mapColoAtAdmission_, std::map<std::string, std::map<std::string, std::vector<double>>> mapProbabilityOfTransmission2_, std::map<std::string, std::map<std::string, std::map<std::string, double>>> mapDaysOfWeek_);
	
	//8eme constructeur
	Parameters(std::map<std::string, std::map<std::string, std::vector<std::pair<std::string, double>>>>mapDuration_, std::map<std::string, std::map<std::string, double>> mapColoAtAdmission_, std::map<std::string, std::map<std::string, double>> mapProbabilityOfTransmission_, std::map<std::string, std::map<std::string, std::map<std::string, double>>> mapDaysOfWeek_);
	
	Parameters(std::map<std::string, std::map<std::string, std::vector<std::pair<std::string, double>>>>mapDuration_, std::map<std::string, std::map<std::string, double>> mapColoAtAdmission_, std::map<std::string, std::map<std::string, std::vector<double>>> mapProbabilityOfTransmission2_, std::map<std::string, std::map<std::string, std::map<std::string, double>>> mapDaysOfWeek_);
	
	//constructeur admission1
	Parameters(std::map<std::string, double> mapAdmission_, std::map<std::string, double> mapDischarge_, std::map<std::string, std::map<std::string, double>> mapAdmissionDistribution_, std::map<std::string, std::map<std::string, double>> mapDischargeDistribution_, std::map<std::string, double> mapNbOfInd_, std::map<std::string, std::map<std::string, double>> mapLengthOfStay_);
	
	//constructeur admission2
	Parameters(std::map<std::string, double> mapAdmission_, std::map<std::string, std::map<std::string, double>> mapAdmissionDistribution_, std::map<std::string, double> mapNbOfInd_, std::map<std::string, std::map<std::string, double>> mapLengthOfStay_);
	
	/*Getters*/
	std::map<std::string, std::map<std::string, std::vector<std::pair<std::string, double>>>> getMapDuration(){return mapDuration;}
	std::map<std::string, double> getMapAdmission(){return mapAdmission;}
	std::map<std::string, double> getMapDischarge(){return mapDischarge;}
	std::map<std::string, std::map<std::string, double>> getMapColoAtAdmission(){return mapColoAtAdmission;}
	std::map<std::string, std::map<std::string, double>> getMapProbabilityOfTransmission(){return mapProbabilityOfTransmission;}
	std::map<std::string, std::map<std::string, std::vector<double>>> getMapProbabilityOfTransmission2(){return mapProbabilityOfTransmission2;}
	std::map<std::string, double> getMapAdmitted(){return mapAdmitted;}
	std::map<std::string, std::map<std::string, double>> getMapLengthOfStay(){return mapLengthOfStay;}
	std::map<std::string, std::map<std::string, std::vector<double>>> getMapDistributionDuration(){return mapDistributionDuration;}
	std::map<std::string, std::map<std::string, std::map<std::string, double>>> getMapDaysOfWeek(){return mapDaysOfWeek;}	
	std::map<std::string, std::map<std::string, double>> getMapAdmissionDistribution(){return mapAdmissionDistribution;}
	std::map<std::string, std::map<std::string, double>> getMapDischargeDistribution(){return mapDischargeDistribution;}
	std::map<std::string, double> getMapNbOfInd(){return mapNbOfInd;}
	

	/*Setters*/
	void setMapDuration(std::map<std::string, std::map<std::string, std::vector<std::pair<std::string, double>>>> mapDuration_){mapDuration = mapDuration_;}
	void setMapAdmission(std::map<std::string, double> mapAdmission_){ mapAdmission = mapAdmission_ ;}
	void setMapDsicharge(std::map<std::string, double> mapDischarge_){ mapDischarge = mapDischarge_ ;}
	void setMapColoAtAdmission(std::map<std::string, std::map<std::string, double>> mapColoAtAdmission_){ mapColoAtAdmission = mapColoAtAdmission_ ;}
	void setMapProbabilityOfTransmission(std::map<std::string, std::map<std::string, double>> mapProbabilityOfTransmission_){ mapProbabilityOfTransmission = mapProbabilityOfTransmission_ ;}
	void setMapProbabilityOfTransmission(std::map<std::string, std::map<std::string, std::vector<double>>> mapProbabilityOfTransmission2_){ mapProbabilityOfTransmission2 = mapProbabilityOfTransmission2_ ;}
	void setMapDistributionDuration(std::map<std::string, std::map<std::string, std::vector<double>>> input_map){mapDistributionDuration=input_map;}
	void setMapAdmissionDistribution(std::map<std::string, std::map<std::string, double>> mapAdmissionDistribution_){ mapAdmissionDistribution = mapAdmissionDistribution_ ;}
	void setMapDischargeDistribution(std::map<std::string, std::map<std::string, double>> mapDischargeDistribution_){ mapDischargeDistribution = mapDischargeDistribution_ ;}
	void setMapNbOfInd(std::map<std::string, double> mapNbOfInd_){ mapNbOfInd = mapNbOfInd_ ;}
	
};

struct Filetreatment {
	
	public:
	std::string file_;
	std::vector<std::vector<std::string>> tab;
	
	public:
	//constructeur par défaut
	Filetreatment(); 
	Filetreatment(std::string file);
	
	std::vector<std::vector<std::string>> tabInfo(const std::vector<std::string>& name_colomn); //fonction qui récupère les info d'admission dans un vector de vector
	std::vector<std::vector<std::string>> tabInfoPathogen();//permet de récupérer les info relative aux pathogènes que l'on souhaite étudiés
	std::multimap<std::string,std::vector<std::string>> tabPrlvt(const std::vector<std::string>& name_colomn);//récupère les info de prlvt dans une multimap

	static std::string checkDate(std::string d); //vérifie si la date est au bon format année-mois-jour
	static std::string dateToString(time_t tm);//renvoie un string à partir d'une date
	static time_t stringToDate(std::string st);//renoie une date à partir d'un string
	std::vector<std::string> buildMeAVectorOfPathogens(std::vector<std::vector<std::string>> Mt_path); //fonction qui construit un vecteur de pathogen à analyser sous la forme d'une séquence de string au lieu d'un vecteur de vecteur (plus pratique pour la suite)
	std::map<std::string, std::map<std::string, std::vector<std::string>>> buildMeAddDisMap(std::vector<std::vector<std::string>> Mt, int pos);//fonction qui retourne une map qui redonne deux maps: dans la première la clé est la date et la valeur est un vecteur avec les individus admis ce jour là (sous forme de string avec calc_ident). La seconde, la clé est la date et la valeur est un vecteur avec les individus qui sont discharge ce jour là (sous forme de string avec calc_ident)

	
	Parameters* findADParameters(); //fonction qui renvoie tous les paramètres nécessaire pour construire le fichier d'admission simulé à partir d'un autre fichier d'admission réel
	Parameters* findParameters(); //fonction qui renvoie tous les paramètres sous la forme de variable à partir du fichier de paramètre issu de learning
	
	/*Getters*/
	std::string getFile(){return file_;}
	
};

#endif