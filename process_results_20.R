
library(dplyr)
library(lubridate)
library(readr)
library(reshape2)

## FUNCTIONS #####


fetch_results = function(filenames, files_path){
  
  sim_id = 1
  summary = data.frame(acq_PA = rep(0, length(filenames)),
                       acq_PE = rep(0, length(filenames)),
                       acq_tot = rep(0, length(filenames)),
                       sim_id = rep(0, length(filenames)),
                       scenario = rep(0, length(filenames)))
  
  for(f in filenames){
    
    #extract scenario number
    scenario = parse_number(strsplit(f, "_")[[1]][4])
    
    data = read.csv(here::here(files_path, f), sep = ";") %>%
      setNames(c("date", "prop_all", "prop_PA", "prop_PE", "acq_PA", "test_PA", "acq_PE", "test_PE", "id", "empty")) %>%
      select(acq_PA, acq_PE) %>%
      summarise(across(everything(),sum)) %>%
      mutate(acq_tot = acq_PA + acq_PE,
             sim_id = sim_id,
             scenario = scenario)
    
    summary[sim_id,] = data
    sim_id = sim_id+1
  }
  
  return(summary)
  
}


fetch_results_supercontactors = function(filenames, files_path){
  
  sim_id = 1
  summary = data.frame(acq_PA = rep(0, length(filenames)),
                       acq_PE = rep(0, length(filenames)),
                       acq_tot = rep(0, length(filenames)),
                       sim_id = rep(0, length(filenames)),
                       cat = rep("a", length(filenames)),
                       type = rep("a", length(filenames)),
                       scenario = rep(0, length(filenames)))
  
  for(f in filenames){
    
    if(grepl("c_", f)) type = "Contact"
    else if(grepl("d_", f)) type = "Duration"
    else if(grepl("m_", f)) type = "Both"
    
    if(grepl("PA", f)) cat = "Patients"
    else if(grepl("PE", f)) cat = "Staff"
    else cat = "Both"
    
    #extract scenario number
    scenario = parse_number(f)
    
    data = read.csv(here::here(files_path, f), sep = ";") %>%
      setNames(c("date", "prop_all", "prop_PA", "prop_PE", "acq_PA", "test_PA", "acq_PE", "test_PE", "id", "empty")) %>%
      select(acq_PA, acq_PE) %>%
      summarise(across(everything(),sum)) %>%
      mutate(acq_tot = acq_PA + acq_PE,
             sim_id = sim_id,
             cat = cat,
             type = type,
             scenario = scenario)
    
    summary[sim_id,] = data
    sim_id = sim_id+1
  }
  
  return(summary)
  
}

calc_impact = function(summary, group, intervention, ncompare = 10){
  
  summary = summary %>%
    arrange(scenario)
  
  summary_0 = summary %>%
    filter(scenario == 0)
  summary = summary %>%
    filter(scenario != 0)
  
  summary_0_expand = data.frame(acq_tot = sample(summary_0$acq_tot,
                                                 nrow(summary)*ncompare,
                                                 replace = T))
  
  #note in vapply the "rep(1.1, ncompare)" argument just provides a template for the return value
  #it does NOT affect results, just tells the function to return a vector of doubles with size = ncompare
  summary_expand = data.frame(acq_tot = as.vector(vapply(summary$acq_tot,
                                                         function(x) rep(x,ncompare),
                                                         rep(1.1,ncompare))),
                              scenario = as.vector(vapply(summary$scenario,
                                                          function(x) rep(x,ncompare),
                                                          rep(1.1,ncompare))))
  
  summary_expand[,1] = (summary_0_expand[,1]-summary_expand[,1])/summary_0_expand[,1]
  
  summary_tt = data.frame(scenario = unique(summary$scenario),
                          val = 0, lower = 0, upper = 0, pval = 0)
  
  for(i in unique(summary_tt$scenario)){
    
    tt = wilcox.test(summary_expand %>%
                       filter(scenario==i) %>%
                       pull(acq_tot), conf.int = T, exact = F)
    
    summary_tt$val[i] = unname(tt$estimate)
    summary_tt$lower[i] = tt$conf.int[1]
    summary_tt$upper[i] = tt$conf.int[2]
    summary_tt$pval[i] = tt$p.value
    
  }
  
  summary_tt = summary_tt %>%
    arrange(scenario) %>%
    mutate(group = group) %>%
    mutate(scenario = (scenario%%5)) %>%
    mutate(scenario = replace(scenario, scenario==0, 5)) %>%
    mutate(scenario = 2*scenario) %>%
    mutate(inter = intervention)
  
  return(summary_tt)
}


calc_impact_supercontactors = function(summary, intervention, ncompare = 10){
  
  summary = summary %>%
    arrange(scenario, cat, type)
  
  summary_0 = summary %>%
    filter(scenario == 0)
  summary = summary %>%
    filter(scenario != 0)
  
  summary_0_expand = data.frame(acq_tot = sample(summary_0$acq_tot,
                                                 nrow(summary)*ncompare,
                                                 replace = T))
  
  #note in vapply the "rep(1.1, ncompare)" argument just provides a template for the return value
  #it does NOT affect results, just tells the function to return a vector of doubles with size = ncompare
  summary_expand = data.frame(acq_tot = as.vector(vapply(summary$acq_tot,
                                                         function(x) rep(x,ncompare),
                                                         rep(1.1,ncompare))),
                              scenario = as.vector(vapply(summary$scenario,
                                                          function(x) rep(x,ncompare),
                                                          rep(1.1,ncompare))),
                              cat = as.vector(vapply(summary$cat,
                                                     function(x) rep(x,ncompare),
                                                     rep("hello",ncompare))),
                              type = as.vector(vapply(summary$type,
                                                      function(x) rep(x,ncompare),
                                                      rep("hello",ncompare))))
  
  summary_expand[,1] = (summary_0_expand[,1]-summary_expand[,1])/summary_0_expand[,1]
  
  summary_tt = summary %>%
    select(cat, type, scenario) %>%
    distinct %>%
    mutate(val = 0, lower = 0, upper = 0, pval = 0)
  
  for(i in 1:nrow(summary_tt)){
    
    tt = wilcox.test(summary_expand %>%
                       filter(scenario==summary_tt$scenario[i],
                              cat==summary_tt$cat[i],
                              type==summary_tt$type[i]) %>%
                       pull(acq_tot), conf.int = T, exact = F)
    
    summary_tt$val[i] = unname(tt$estimate)
    summary_tt$lower[i] = tt$conf.int[1]
    summary_tt$upper[i] = tt$conf.int[2]
    summary_tt$pval[i] = tt$p.value
    
  }
  
  
  summary_tt = summary_tt %>%
    mutate(scenario = 2*scenario) %>%
    mutate(group = paste(cat,type,sep = "-"),
           inter = intervention) %>%
    select(scenario, val, lower, upper, pval, group, inter)
  
  
}



## RANDOM ALL 20 COMPLIANCE #####

SC_compliance_path = here::here("output", "acquisition", "complianceSC_20")
compliance_names = grep("acquisitionRandomAll", list.files(SC_compliance_path), value = T)

compliance_summary = fetch_results(compliance_names, SC_compliance_path)
compliance_summary %>% group_by(scenario) %>% summarise(mean = mean(acq_tot))

compliance_tt = calc_impact(compliance_summary, "All staff", "Contact precaution")

write.csv(compliance_tt, here::here("results", "CPRandAll_20_results.csv"), row.names = F)


## RANDOM PATIENTS 20 COMPLIANCE #####

SC_compliance_path = here::here("output", "acquisition", "complianceSC_20")

compliance_names = grep("acquisitionRandomPatient", list.files(SC_compliance_path), value = T)

compliance_summary = fetch_results(compliance_names, SC_compliance_path)
compliance_summary %>% group_by(scenario) %>% summarise(mean = mean(acq_tot))

compliance_tt = calc_impact(compliance_summary, "Patients", "Contact precaution")

write.csv(compliance_tt, here::here("results", "CPRandPat_20_results.csv"), row.names = F)


## RANDOM ALL 20 VACCINATION #####

SC_vaccination_path = here::here("output", "acquisition", "vaccinationSC_20")
vaccination_names = grep("acquisitionVCRandomAll", list.files(SC_vaccination_path), value = T)

vaccination_summary = fetch_results(vaccination_names, SC_vaccination_path)
vaccination_summary %>% group_by(scenario) %>% summarise(mean = mean(acq_tot))

vaccination_tt = calc_impact(vaccination_summary, "All staff", "Vaccination")

write.csv(vaccination_tt, here::here("results", "VCRandAll_20_results.csv"), row.names = F)


## RANDOM PATIENTS 20 VACCINATION #####

SC_vaccination_path = here::here("output", "acquisition", "vaccinationSC_20")

vaccination_names = grep("acquisitionVCRandomPatient", list.files(SC_vaccination_path), value = T)

vaccination_summary = fetch_results(vaccination_names, SC_vaccination_path)
vaccination_summary %>% group_by(scenario) %>% summarise(mean = mean(acq_tot))

vaccination_tt = calc_impact(vaccination_summary, "Patients", "Vaccination")

write.csv(vaccination_tt, here::here("results", "VCRandPat_20_results.csv"), row.names = F)


## COMPLIANCE 20 SUPERCONTACTORS #####

SC_compliance_path = here::here("output", "acquisition", "complianceSC_20")
supercontactors_names = grep("acquisitionbyCatSC", list.files(SC_compliance_path), value = T)

compliance_summary = fetch_results_supercontactors(supercontactors_names, SC_compliance_path)
compliance_summary %>% group_by(scenario) %>% summarise(mean = mean(acq_tot))

supercontactors_tt = calc_impact_supercontactors(compliance_summary, "Contact precaution")

write.csv(supercontactors_tt, here::here("results", "CPSC_20_results.csv"), row.names = F)


## VACCINATION 20 SUPERCONTACTORS #####

SC_vaccination_path = here::here("output", "acquisition", "vaccinationSC_20")
VCsupercontactors_names = grep("VCbyCatSC", list.files(SC_vaccination_path), value = T)

vaccination_summary = fetch_results_supercontactors(VCsupercontactors_names, SC_vaccination_path)
vaccination_summary %>% group_by(scenario) %>% summarise(mean = mean(acq_tot))

VCsupercontactors_tt = calc_impact_supercontactors(vaccination_summary, "Vaccination")

write.csv(VCsupercontactors_tt, here::here("results", "VCSC_20_results.csv"), row.names = F)

