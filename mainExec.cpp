/* MAIN
* main file of the project
*
*	
*	main.cpp
*
*	Created on: Nov 17, 2017
*	Last update: Jan 3, 2018
*	Author: Audrey
*
*/
#include <iostream>
#include <fstream>
#include <vector> 
#include <sstream>      
#include <string>
#include <regex>
#include <math.h>
#include <stdio.h>

#include <boost/lexical_cast.hpp>
#include <boost/mpi.hpp>
#include <boost/mpi/collectives.hpp>
#include <boost/algorithm/string.hpp>
#include<boost/math/distributions.hpp>
#include <map>
#include <algorithm>

using namespace std;


std::vector<std::string> split(const std::string& s, char delimiter)
{
   std::vector<std::string> tokens;
   std::string token;
   std::istringstream tokenStream(s);
   while (std::getline(tokenStream, token, delimiter))
   {
      tokens.push_back(token);
   }
   return tokens;
}

vector<string> returnPathogens(string fileN){
	string line;
	vector<vector<string>> table;
	ifstream ifs (fileN);
	vector<string> tempo;
	vector <string> first_line;
	getline(ifs,line);
	boost::trim_if(line, boost::is_any_of("\r\n|\r|\n|\t"));
	boost::split(first_line, line, boost::is_any_of(";"));
				
	if(!ifs.good()){
		cout<<"enable to open file"<<endl;
	}
	while(getline(ifs, line)){
		boost::trim_if(line, boost::is_any_of("\r\n|\r|\n|\t"));
		boost::split(tempo, line, boost::is_any_of(";"));
		vector<string> stock_tempo;
		for(int s(0); s<tempo.size();s++){
			stock_tempo.push_back(tempo[s]);
		}
		table.push_back(stock_tempo);
	}
	ifs.close();
	
	vector<string> res;
	for(int i(0); i<table.size(); i++){
		string pathogen;
		if(table[i][0]!=""){
			for(int j(0); j<table[i].size(); j++){
				if(j==table[i].size()-1){
					pathogen = pathogen + table[i][j];
				}else{
					pathogen = pathogen + table[i][j] + "_";
				}	
			}
			res.push_back(pathogen);
		}
	}
	
	return res;
	
}

map<string, map<string, vector<double>>> returnAcValue(string Filename){
	map<string, map<string, vector<double>>> res;	
	ifstream ifs (Filename);
	vector<string> tempo;
	vector <string> first_line;
	string line;
	getline(ifs,line);
	boost::trim_if(line, boost::is_any_of("\r\n|\r|\n|\t"));
	boost::split(first_line, line, boost::is_any_of(";"),boost::algorithm::token_compress_on);
	vector<string> listB;
	vector<double> indexALL;
	vector<double> indexPA;
	vector<double> indexPE;
	
	for(int column(0); column<first_line.size(); column++){

		if(first_line[column] != "Date" && first_line[column] !="individual" && first_line[column]!= ""){
			vector<string> bacteriaTYPE;
			boost::split(bacteriaTYPE, first_line[column], boost::is_any_of("-"));
			if(bacteriaTYPE[1]=="ALL" || bacteriaTYPE[1]=="PA" || bacteriaTYPE[1]=="PE"){
				vector<string>::iterator itListB = find(listB.begin(), listB.end(), bacteriaTYPE[0]);
				if(itListB==listB.end()){
					listB.push_back(bacteriaTYPE[0]);
					// map<> pairInterm;
					// res[bacteriaTYPE[0]] = pairInterm;
				}
				
				if(bacteriaTYPE[1]=="ALL"){
					indexALL.push_back(column);
				}else if(bacteriaTYPE[1]=="PA"){
					indexPA.push_back(column);
				}else if(bacteriaTYPE[1]=="PE"){
					indexPE.push_back(column);
				}
			}
			
		}
		
	}

	vector<double> allPA;
	vector<double> allPE;
	while(getline(ifs, line)){
		boost::trim_if(line, boost::is_any_of("\r\n|\r|\n|\t"));
		boost::split(tempo, line, boost::is_any_of(";"));
		for(int index(0); index < listB.size(); index++){
			
			// res[listB[index]].first.push_back(stod(tempo[indexPA[index]]));
			res[listB[index]]["ALL"].push_back(stod(tempo[indexALL[index]]));
			res[listB[index]]["PA"].push_back(stod(tempo[indexPA[index]]));
			res[listB[index]]["PE"].push_back(stod(tempo[indexPE[index]]));
			
			// if(tempo[indexPE[index]]=="inf"){
				// res[listB[index]].second.push_back(0.0);
				// cout << "inf " << Filename << endl;
				// cout <<  " tempo[indexPE[index]] " << tempo[indexPE[index]] << endl;
				
			// }else{
				// res[listB[index]].second.push_back(stod(tempo[indexPE[index]]));
			// }
			
			
		}

	}
	ifs.close();
	return res;
}

map<string, map<string, map<int, vector<double>>>> returnAcValueSim(string fav, string noav, string btv, string nciv, string lpapav, string lpapev, string lpepav, string lpepev, int testSimuNb){
	map<string, map<string, map<int, vector<double>>>> res;
	vector<string> listB;
	vector<double> indexALL;
	vector<double> indexPA;
	vector<double> indexPE;
	for(int nb(1); nb<=testSimuNb; nb++){
				
		string FileNameForAc = "output/acquisition/acquisition_simu_" + fav + "_" + noav +"_" + btv + "_" + nciv + "_"+ lpapav + "_" + lpapev + "_" + lpepav + "_" + lpepev + "_" + to_string(nb) + ".csv";
		// ifstream ifs (FileNameForAc);
		FILE* ifs;
		const char* filenameChar = FileNameForAc.c_str();
		ifs = fopen(filenameChar, "r");
		char* linechar;
		if(ifs!=NULL){
			// exit(EXIT_FAILURE);
			vector<string> tempo;
			vector <string> first_line;
			string line;
			
			size_t len = 0;
			getline(&linechar,&len,ifs);
			line = linechar;
			boost::trim_if(line, boost::is_any_of("\r\n|\r|\n|\t"));
			boost::split(first_line, line, boost::is_any_of(";"),boost::algorithm::token_compress_on);
			
			
			if(nb==1){
				for(int column(0); column<first_line.size(); column++){
					if(first_line[column] != "Date" && first_line[column] !="individual" && first_line[column]!= ""){
						vector<string> bacteriaTYPE;
						boost::split(bacteriaTYPE, first_line[column], boost::is_any_of("-"));
						if(bacteriaTYPE[1]=="ALL" || bacteriaTYPE[1]=="PA" || bacteriaTYPE[1]=="PE"){
							vector<string>::iterator itListB = find(listB.begin(), listB.end(), bacteriaTYPE[0]);
							if(itListB==listB.end()){
								listB.push_back(bacteriaTYPE[0]);
							}
							if(bacteriaTYPE[1]=="ALL"){
								indexALL.push_back(column);
							}else if(bacteriaTYPE[1]=="PA"){
								indexPA.push_back(column);
							}else if(bacteriaTYPE[1]=="PE"){
								indexPE.push_back(column);
							}
						}
						
					}
					
				}
			}
			
			vector<double> allPA;
			vector<double> allPE;
			int week = -1;

			while((getline(&linechar,&len,ifs))!=-1){
				week ++;

				line = linechar;
				boost::trim_if(line, boost::is_any_of("\r\n|\r|\n|\t"));
				boost::split(tempo, line, boost::is_any_of(";"));
				for(int index(0); index < listB.size(); index++){
					res[listB[index]]["ALL"][week].push_back(stod(tempo[indexALL[index]]));
					res[listB[index]]["PA"][week].push_back(stod(tempo[indexPA[index]]));
					res[listB[index]]["PE"][week].push_back(stod(tempo[indexPE[index]]));
				}

			}
			// ifs.close();
			fclose(ifs);
			if(linechar){
				free(linechar);
			}
		}else{
			cout << " FileNameForAc " << FileNameForAc << endl;
		}
		
		
	}
	return res;
}

bool checkSize(int nb, map<string, map<string, vector<double>>> input_map){
	bool res;
	for(auto const& [key, val]:input_map){
		map<string, vector<double>> valMap = val;
		if(valMap["ALL"].size()!=nb){
			res = false;
		}else{
			res = true;
		}
	}
	return res;
}

double computeMean(const std::vector<double>& numbers){
    if (numbers.empty())
        return std::numeric_limits<double>::quiet_NaN();
	
    return std::accumulate(numbers.begin(), numbers.end(), 0.0) / numbers.size();
}

double percentile(std::vector<double> &vectorIn, double percent)
{
    sort(vectorIn.begin(), vectorIn.end());
	auto nth = vectorIn.begin() + (percent*vectorIn.size())/100;
    std::nth_element(vectorIn.begin(), nth, vectorIn.end());
    return *nth;
}

pair<map<string, map<string, vector<double>>>, map<string, map<string, pair<vector<double>, vector<double>>>>> returnMeanSimAcValue(string fav, string noav, string btv, string nciv, string lpapav, string lpapev, string lpepav, string lpepev, int testSimuNb, vector<string> allBacteriaToAnalyzed, int nbOfWeek){
	//on va initialiser la map de résultat
	map<string, map<string, vector<double>>> res;
	map<string, map<string, pair<vector<double>, vector<double>>>> resQ;
	
	vector<string> type = {"ALL", "PA", "PE"};

	
	map<string, map<string, map<int, vector<double>>>> resQuantile = returnAcValueSim(fav ,noav , btv ,nciv ,lpapav , lpapev ,lpepav ,lpepev, testSimuNb);
	
	for(auto const& b:allBacteriaToAnalyzed){
		for(auto const& t:type){
			for(int nbOf(0); nbOf<nbOfWeek; nbOf++){
				auto it = resQuantile[b][t].find(nbOf);
				if(it!=resQuantile[b][t].end()){
					res[b][t].push_back(computeMean(resQuantile[b][t][nbOf]));
					double q1 = percentile(resQuantile[b][t][nbOf], 5);
					double q2 = percentile(resQuantile[b][t][nbOf], 95);
					resQ[b][t].first.push_back(q1);
					resQ[b][t].second.push_back(q2);
				}else{
					// cout << " ooops petit soucis il y a des semaines qui manquent! " << endl;
				}
			}
		}
	}
	return make_pair(res, resQ);
}

double checkOutside(double point, double bInf, double bSup){
	double poids = 0;
	if(point<bInf){
		poids = bInf - point;
	}else if (point>bSup){
		poids = point - bSup;
	}
	return poids;
}

map<string, map<string, double>> returnCriterion(string fav, string noav, string btv, string nciv, string lpapav, string lpapev, string lpepav, string lpepev, int testSimuNb, vector<string> allBacteriaToAnalyzed){
	map<string, map<string, double>> res;
	
	vector<string> type = {"ALL", "PA", "PE"};
	string FilenameRealAc = "output/acquisition/acquisition_real_"+ fav + "_"+ noav +".csv";
	map<string, map<string, vector<double>>> pairMapResReal = returnAcValue(FilenameRealAc);
	vector<string> listB;
	int nbOfWeek;
	for(auto const& [key, val]:pairMapResReal){
		listB.push_back(key);
		map<string, double> mapInterm;
		for(auto const& [k, v]:val){
			nbOfWeek = v.size();
			mapInterm[k] = 0.0;
			res[key] = mapInterm;
		}
	}
	pair<map<string, map<string, vector<double>>>, map<string, map<string, pair<vector<double>, vector<double>>>>> resParamSim = returnMeanSimAcValue(fav, noav, btv, nciv, lpapav, lpapev, lpepav, lpepev, testSimuNb, allBacteriaToAnalyzed, nbOfWeek);


	map<string,map<string, vector<double>>> pairMapResSim = resParamSim.first;
	map<string, map<string, pair<vector<double>, vector<double>>>> quantileSim = resParamSim.second;

	for(auto const& path:allBacteriaToAnalyzed){
		map<string, vector<double>> pairVectorReal = pairMapResReal[path];
		map<string, vector<double>> pairVectorSim = pairMapResSim[path];
		if(pairVectorReal.size()==pairVectorSim.size()){
			map<string, pair<vector<double>, vector<double>>> pairQuantile = quantileSim[path];
			for(auto const& t:type){
				double typeValue = 0.0;
				for(int w(0); w<nbOfWeek; w++){
					double weight = checkOutside(pairVectorReal[t][w], pairQuantile[t].first[w], pairQuantile[t].second[w]);
					int weightiNT=0;
					if(weight>0.0)weightiNT=1;
					double PAs = pow(pairVectorSim[t][w] - pairVectorReal[t][w], 2) + weightiNT;
					// double PAs = pow(pairVectorSim[t][w] - pairVectorReal[t][w], 2);

					typeValue = typeValue + PAs;
				}
				res[path][t] = typeValue;

			}
		}else{
			cout << " pb entre simu et real au niveau du nombre de semaine " <<endl; 
		}
		
	}

	return res;
}



map<string, vector<string>> findProbabilities(vector<string> pathogens, string filename){
	// string filename = "resultParameters.csv";
	ifstream ifs (filename);
	string line;
	vector<string> tempo;
	map<string, vector<string>> probabilities;
	for(auto const& p:pathogens){
		vector<string> vectInterm;
		probabilities[p] = vectInterm;
	}
	while(getline(ifs, line)){
		boost::trim_if(line, boost::is_any_of("\r\n|\r|\n|\t"));
		boost::split(tempo, line, boost::is_any_of(";"));
		if(tempo[0]=="probability of transmission"){
			string probaTitle = tempo[2] + ":" + tempo[3];
			probabilities[tempo[1]].push_back(probaTitle);
		}
	}
	return probabilities;
}

vector<vector<string>> variablesToPlay(string filename){
	vector<vector<string>> res;
	ifstream ifs (filename);
	string line;
	vector<string> tempo;
	vector<string> firstAcVector;
	vector<string> nbOfAcVector;
	vector<string> byTypeVector;
	vector<string> logPAPAVector;
	vector<string> logPAPEVector;
	vector<string> logPEPAVector;
	vector<string> logPEPEVector;
	vector<string> nbCtcInfVector;
	while(getline(ifs, line)){
		boost::trim_if(line, boost::is_any_of("\r\n|\r|\n|\t"));
		boost::split(tempo, line, boost::is_any_of(";"));
		if(tempo[0]!="")firstAcVector.push_back(tempo[0]);
		if(tempo[1]!="")nbOfAcVector.push_back(tempo[1]);
		if(tempo[2]!="")byTypeVector.push_back(tempo[2]);
		if(tempo[3]!="")logPAPAVector.push_back(tempo[3]);
		if(tempo[4]!="")logPAPEVector.push_back(tempo[4]);
		if(tempo[5]!="")logPEPAVector.push_back(tempo[5]);
		if(tempo[6]!="")logPEPEVector.push_back(tempo[6]);
		if(tempo[7]!="")nbCtcInfVector.push_back(tempo[7]);
	}
	res.push_back(firstAcVector);
	res.push_back(nbOfAcVector);
	res.push_back(byTypeVector);
	res.push_back(logPAPAVector);
	res.push_back(logPAPEVector);
	res.push_back(logPEPAVector);
	res.push_back(logPEPEVector);
	res.push_back(nbCtcInfVector);
	return res;
}

int main(int argc, char **argv) {
	
	cout << "------------- C'est partie pour la recherche du meilleur ------------"<<endl;
	
	string filnameToPlay = argv[1];
	string testSimu = argv[2];
	string nbOftab = argv[3];
	int testSimuNb = stoi(testSimu);
	
	cout << "les fichiers utilisés sont: " << endl;
	cout << "filnameToPlay : " << filnameToPlay << endl;
	cout << "testSimu :  " << testSimu << endl;
	cout << "nbOftab :  " << nbOftab <<endl;
	
	vector<vector<string>> vectToPlay = variablesToPlay(filnameToPlay);
	vector<string> firstAcVector = vectToPlay[0];
	vector<string> nbOfAcVector = vectToPlay[1];
	vector<string> byTypeVector = vectToPlay[2];
	vector<string> logPAPAVector = vectToPlay[3];
	vector<string> logPAPEVector = vectToPlay[4];
	vector<string> logPEPAVector = vectToPlay[5];
	vector<string> logPEPEVector = vectToPlay[6];
	vector<string> nbCtcInfVector = vectToPlay[7];
	
	std::ofstream myCSVsimulation;
	string fileR = "resultsSimulation"+nbOftab+".csv";
	myCSVsimulation.open(fileR);
	
	vector <string> allBacteriaToAnalyzed = returnPathogens("input/toy_bacteria_all.csv");
	
	//option à tester
	// vector<string> firstAcVector = {"yes", "no"};
	// vector<string> nbOfAcVector = {"1", "2"};
	// vector<string> byTypeVector = {"yes", "no"};
	// vector<string> logPAVector = {"yes", "no"};
	// vector<string> logPEVector = {"yes", "no"};
	// vector<string> nbCtcInfVector = {"0", "50", "100", "150", "200"};
	
	//première étape lancer une fois le real
	// std::string str = "mpirun -n 6 ./main.exe config.props model.props --mode real";
	// const char *command = str.c_str();
	// system(command);
	
	map<string, map<string, map<string, double>>> allCriteriaScenario;
	
	vector<string> headerCSV = {"firstAcq", "nbOfAcqAllowed", "byType", "nbOfCtcInf", "logPAPA", "logPAPE","logPEPA", "logPEPE"};
	for(auto const& b:allBacteriaToAnalyzed){
		string probHeader = "probabilities_" + b;
		string bPA = "criterion-" + b + "-PA";
		string bPE = "criterion-" + b + "-PE";
		string bALL = "criterion-" + b + "-ALL";
		headerCSV.push_back(probHeader);
		headerCSV.push_back(bPA);
		headerCSV.push_back(bPE);
		headerCSV.push_back(bALL);
	}
	headerCSV.push_back("FinalCriterion");
	
	for(auto const& elem:headerCSV){
		if(elem!="FinalCriterion"){
			myCSVsimulation << elem + ";";
		}else{
			myCSVsimulation << elem << endl;
		}
	}

	
	//seconde étape: boucles de simulation
	for(auto const& fav:firstAcVector){
		for(auto const& noav:nbOfAcVector){
			for(auto const& btv:byTypeVector){
				for(auto const& lpapav:logPAPAVector){
					for(auto const& lpapev:logPAPEVector){
						for(auto const& lpepav:logPEPAVector){
							for(auto const& lpepev:logPEPEVector){
								for(auto const& nciv:nbCtcInfVector){
									string scenario = fav + "_" + noav + "_" + btv + "_" + nciv + "_" + lpapav + "_" + lpapev + "_" + lpepav + "_" + lpepev; 
									cout << "Traitement de : " << scenario << endl;
									
									map<string, map<string, double>> mapCriterion = returnCriterion(fav, noav, btv, nciv, lpapav,lpapev, lpepav, lpepev, testSimuNb, allBacteriaToAnalyzed);
									
									if(!mapCriterion.empty()){
										allCriteriaScenario[scenario] = mapCriterion;
									
										
										string resP = "parameters/resultParameters_"+scenario+".csv";
										map<string, vector<string>> probabilities = findProbabilities(allBacteriaToAnalyzed, resP);
										myCSVsimulation << fav + ";" + noav + ";" + btv + ";" + nciv + ";" + lpapav + ";" + lpapev + ";" + lpepav + ";" + lpepev + ";"; 
										
										
										double lastCriterion = 0.0;
										for(auto const& b:allBacteriaToAnalyzed){

											vector<string> probaPathogen = probabilities[b];
											string str_prob;
											for(int probIndex(0); probIndex<probaPathogen.size(); probIndex++){
												if(probIndex!=(probaPathogen.size()-1)){
													str_prob = str_prob + probaPathogen[probIndex] + "_";
												}else{
													str_prob = str_prob + probaPathogen[probIndex];
												}
											}
											myCSVsimulation << str_prob + ";";
											map<string, double> criteria = mapCriterion[b];
											myCSVsimulation << to_string(criteria["PA"]) + ";" + to_string(criteria["PE"]) + ";" + to_string(criteria["ALL"]) + ";";
											lastCriterion = lastCriterion + (criteria["ALL"]);
											
										}
																				
										myCSVsimulation << lastCriterion;
										myCSVsimulation << endl;
									}else{
										cout << " petit soucis de taille avec " << scenario << endl;
									}
									
									
								}
							}
						}	
					}	
				}
			}
		}
	}
	
	myCSVsimulation.close();
	
	//3ème étape: trouver le meilleur critère
	cout << " compute the best criterion ..." << endl;
	vector<string> scenar;
	vector<double> resultCritAll;
	for (auto const& [scenario, criteria]:allCriteriaScenario){
		scenar.push_back(scenario);
		map<string, map<string, double>> criteriaVal = criteria;
		double c = 0.0;
		for(auto const& [path, crit]:criteriaVal){
			map<string, double> allmapC = crit;
			// c = c + (allmapC["PA"] + allmapC["PE"]);
			c = c + (allmapC["ALL"]);
		}
		resultCritAll.push_back(c);
	}
	vector<double>::iterator result = std::min_element(std::begin(resultCritAll), std::end(resultCritAll));
	int d = std::distance(std::begin(resultCritAll), result);
	cout << "The best scenario is " << scenar[d] << " with a value : " << *std::min_element(std::begin(resultCritAll), std::end(resultCritAll)) << endl;

}






