/* ORGANISM CLASS
* this file gather all organism from the project
*
*	
*	Organism.h
*
*	Created on: march 09, 2018
*	Author: Audrey
*
*/

#ifndef ORGANISM_H_
#define ORGANISM_H_

#include "repast_hpc/AgentId.h"


#include <boost/serialization/export.hpp>
#include <boost/serialization/access.hpp>

class Organism{
	friend class boost::serialization::access;
	
	protected:
		repast::AgentId id_;
		
	public:
		Organism(); //constructeur par défaut
		Organism(repast::AgentId id); 
		
		virtual repast::AgentId& getId(){return id_;}
		virtual const repast::AgentId& getId() const {return id_;}	
		
		virtual void affiche() const;
		
		/*Setter*/
		void set(int _currentRank);
		
		virtual ~Organism();
		
		/* For archive packaging */
		template<class Archive>
		void serialize(Archive &ar, const unsigned int version){
			ar & id_;
		}
	
};

/* Serializable Agent Package */
struct OrganismPackage {
	
public:
	int    id;
    int    rank;
    int    type;
    int    currentRank;
    

    /* Constructors */
    OrganismPackage(); // For serialization
    OrganismPackage(int _id, int _rank, int _type, int _currentRank);
	virtual ~OrganismPackage()= default;
	
	/* For archive packaging */
    template<class Archive>
    void serialize(Archive &ar, const unsigned int version){
        ar & id;
        ar & rank;
        ar & type;
        ar & currentRank;
    }

};


#endif /* ORGANISM_H_ */