#************************************************************************************************************
#
# makefile
#
# include ./ordi_boulot/env
# include ./ordi_perso/env
# include ./ordi_cnam/env
#************************************************************************************************************

MPICXX=mpicxx -std=c++17 -Wno-deprecated 

BOOST_LIBS=-lboost_mpi -lboost_serialization -lboost_system -lboost_filesystem -lboost_date_time -lboost_iostreams
REPASTHPC_LIBS=-lrepast_hpc-2.3.0


.PHONY: all
all : main 


.PHONY: clean_output_files
clean_output_files:
	rm -f *.csv *.txt
	rm -f ./output/*.csv ./output/*.txt
	rm -f ./logs/*.*

.PHONY: clean_compiled_files
clean_compiled_files:
	rm -f *.exe  ./bin/*.exe
	rm -f *.o rm -f ./object/*.o
.PHONY: clean
clean: clean_compiled_files clean_output_files 


DEPS = Filetreatment.h FindContact.h gnuplot-iostream.h Individual.h Learning.h Model.h Organism.h Pathogen.h Interventions.h
OBJ = Filetreatment.o FindContact.o Individual.o Learning.o Model.o Organism.o Pathogen.o Interventions.o main.o


%.o: %.cpp $(DEPS)
	echo "using $(MPICXX) with BOOST $(BOOST_ROOT) and REPAST $(REPASTHPC_INCLUDE)\n"
	$(MPICXX)  -I/$(BOOST_ROOT)/include -I$(REPASTHPC_ROOT)/include -g -c -o $@ $< 

main: 	$(OBJ)
	$(MPICXX) -L$(BOOST_ROOT)/lib -g \
	-L$(REPASTHPC_ROOT)/lib \
	-L$(REPASTHPC_ROOT)/lib \
	-o main.exe $(OBJ)  $(BOOST_LIBS) $(REPASTHPC_LIBS)
	$(MPICXX) mainExec.cpp -o mainExec
	



