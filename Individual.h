/* INDIVIDUAL CLASS
* this file gather all agent from the project
*
*	
*	Individual.h
*
*	Created on: Nov 17, 2017
*	Author: Audrey
*
*/

#ifndef INDIVIDUAL_H_
#define INDIVIDUAL_H_

#include "Organism.h"
#include "Filetreatment.h"

#include "repast_hpc/TDataSource.h"
#include "repast_hpc/SVDataSet.h"
#include "repast_hpc/SharedContext.h"
#include "repast_hpc/Schedule.h"
#include "repast_hpc/SharedNetwork.h"
#include "repast_hpc/Edge.h"
#include "repast_hpc/Random.h"
#include "repast_hpc/Properties.h"
#include "repast_hpc/Utilities.h"
#include <ctime>
# include <map>

#include <boost/serialization/export.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/map.hpp> 
#include <boost/serialization/vector.hpp>
#include <boost/serialization/access.hpp>

class Pathogen ;

class Contact {
	friend class boost::serialization::access;
	friend class Model;
	
	protected:
		std::string from;
		std::string to;
		std::string day;
		std::string length;
	
	public:
		Contact();
		Contact(std::vector<std::string> v);
		std::string getFrom(){return from;}
		std::string getTo(){return to;}
		std::string getDay(){return day;}
		std::string getLength(){return length;}
		
	template<class Archive>
        void serialize( Archive& ar, const unsigned int /*version*/ )
        {
            ar & from;
            ar & to;
			ar & day;
			ar & length;

        }
	
};


//on a une classe individual est le parent des classes staff et patients
class Individual: public Organism{
	// friend class boost::serialization::access;
	
	protected:
		std::string calc_ident_; //numero d'anonymat de l'individu
		std::string status_; //status: PE pour personnel ou PA pour patient
		std::string dt_in_; //date d'admission
		std::string dt_out_;//date de discharge
		std::string ward_;//service
		std::string gender_;//genre
		std::map <std::string,std::vector<std::string>> positivePathogens_ ;//map avec tous les pathogènes qu'à eu l'individu ainsi que la date d'acquisition de ce pathogène
		std::vector <std::string> actualPositivePathogens_ ;//vector avec les pathogènes qu'il porte en temps réel
		std::map<std::string, std::string> mapColonization_;//map avec le pathogène et la date où il doit se décoloniser
		std::map<std::string, std::pair<std::string, std::string>> pathogensStatus;
		std::string manuportage="";
	
	public:
	Individual(); //constructeur par défaut
	Individual(std::string calc_ident); //constructeur avec que le nom de l'individu
	Individual(repast::AgentId id, const std::vector <std::string>& v); //constructeur avec le vecteur des caractéristiques de l'individu
	Individual(const std::vector <std::string>& v);
	Individual(std::string calc_ident, std::string status,std::string dt_in, std::string dt_out, std::string ward, std::string gender); // le constructeur avec les caractéristiques un par un
	Individual(repast::AgentId id, std::string calc_ident, std::string status,std::string dt_in, std::string dt_out, std::string ward, std::string gender);// le constructeur avec en plus le AgentId utile pour repast
	Individual(repast::AgentId id, std::string calc_ident, std::string status,std::string dt_in, std::string dt_out, std::string ward, std::string gender, std::map <std::string,std::vector<std::string>> positivePathogens, std::vector <std::string> actualPositivePathogens, std::map<std::string, std::string> mapColonization, std::map<std::string, std::pair<std::string, std::string>> pathogensStatus, std::string manuportage);// le constructeur avec en plus le AgentId utile pour repast
	Individual(std::string calc_ident, std::string status,std::string dt_in, std::string dt_out, std::string ward, std::string gender, std::map <std::string,std::vector<std::string>> positivePathogens, std::vector <std::string> actualPositivePathogens, std::map<std::string, std::string> mapColonization, std::map<std::string, std::pair<std::string, std::string>> pathogensStatus, std::string manuportage);
	std::vector<std::string> getAllSwabDates();
	
	virtual ~Individual(); //destructeur
	
	/*Getter*/
	std::string getCalc_ident(){return calc_ident_;}
	std::string getStatus(){return status_;}
	std::string getDt_in(){return dt_in_;}
	std::string getDt_out(){return dt_out_;}
	std::string getWard(){return ward_;}
	std::string getGender(){return gender_;}
	std::map<std::string,std::vector<std::string>> getPositivePathogens(){return positivePathogens_;}
	std::map<std::string,std::pair<std::string, std::string>> getPathogensStatus(){return pathogensStatus;}
	std::string getManuportage(){return manuportage;}
	std::vector <std::string> getActualPositivePathogens(){return actualPositivePathogens_;}
	std::map<std::string, std::string> getMapColonization(){return mapColonization_;}
	
	/*Setter*/
	void set(int currentRank, std::string calc_ident, std::string status,std::string dt_in, std::string dt_out, std::string ward, std::string gender);
	void setDt_in(std::string newDt_in){dt_in_ = newDt_in;}
	void setDt_out(std::string newDt_out){dt_out_ = newDt_out;}
	void setWard(std::string newWard){ward_ = newWard;}
	void setPositivePathogens(std::map <std::string, std::vector<std::string>> p){positivePathogens_=p;}
	void setPathogensStatus(std::map <std::string, std::pair<std::string, std::string>> p){pathogensStatus=p;}
	void setManuportage(std::string p){manuportage=p;}
	void setActualPositivePathogens(std::vector <std::string> a){actualPositivePathogens_=a;}
	void removePositivePathogens();
	void removeActualPositivePathogens();
	void setColonization(std::string key, std::string val){mapColonization_[key]=val;}
	void clearAllColonization(){mapColonization_.clear();}
	void clearAllPathogensStatus(){pathogensStatus.clear();}
	
	void removePathFromActualPositivePathogens(std::string i);//fonction qui enlève un pathogène du vecteur des pathogènes actuellement porté
	void addPositivePathogens(std::string p2, std::vector<std::string> s){positivePathogens_.insert(pair<std::string, std::vector<std::string>>(p2,s));}//ajoute un pathogène à la map des pathogènes
	void addActualPositivePathogens(std::string p3){actualPositivePathogens_.push_back(p3);}//ajoute un pathogène au vector des pathogènes actuellement portés
	void addColonization(std::string key, std::string val){mapColonization_.insert(make_pair(key, val));}//ajoute un pathogène et une date de décolonisation
	void supColonization (std::string key);//supprime la colonisation de la map des colonisations
	void supStatusPathogen (std::string key);//supprime le status de la map des colonisations
	void addInsidePosPathogens(std::string pathogen, std::string dateAdd);//rajoute un pathogène dans la map en vérifiant qu'il n'est pas déjà présent (auquel cas il faut juste ajouter la date de prlvt)
	void addAPrlvt(std::string date);//utile pour la simulation, rajoute un prlvt même vide (ajoute une date de prlvt)
	
	void changePathogensStatus(std::string pathogen, std::string newStatus, std::string dateE);
	
	virtual void affiche() const; //une fonction affiche pour tester l'individu
	void afficheSuccessor(repast::SharedNetwork<Individual,repast::RepastEdge<Individual>, repast::RepastEdgeContent<Individual>, repast::RepastEdgeContentManager<Individual> > *network);
	void affichePredecessor(repast::SharedNetwork<Individual,repast::RepastEdge<Individual>, repast::RepastEdgeContent<Individual>, repast::RepastEdgeContentManager<Individual> > *network);
	
	std::vector<std::string> findIndividualInContactWith();
	int numberOfContactsWith(std::string name);
	void addPathogen(std::string pathogen, std::string dateAdd);//fonction qui ajoute un pathogene dans la map des positive path et dans le vector des actuals path
	
	/* For archive packaging */
    // template<class Archive>
    // void serialize(Archive &ar, const unsigned int version){
        // ar & boost::serialization::base_object<Organism>(*this);
        // ar & calc_ident_;
		// ar & status_;
        // ar & dt_in_;
		// ar & dt_out_;
		// ar & ward_;
		// ar & gender_;
		// ar & positivePathogens_;
		// ar & actualPositivePathogens_;
		// ar & mapColonization_;
    // }
	
	
};

// BOOST_SERIALIZATION_ASSUME_ABSTRACT(Individual)

class Patient: public Individual{
	// friend class boost::serialization::access;
	
	private:
		std::string hosp_flag_; //caractéristique supplémentaire du patient

	
	public:
	Patient();
	Patient(Patient const& patientCopy);
	Patient(std::string calc_ident, std::string status,std::string dt_in, std::string dt_out, std::string ward, std::string gender);
	Patient(repast::AgentId id, std::string calc_ident, std::string status,std::string dt_in, std::string dt_out, std::string ward, std::string gender, std::string hosp_flag);
	Patient(repast::AgentId id, std::string calc_ident, std::string status,std::string dt_in, std::string dt_out, std::string ward, std::string gender,std::map <std::string, std::vector<std::string>> positivePathogens, std::vector <std::string> actualPositivePathogens, std::map<std::string, std::string> mapColonization, std::map<std::string, std::pair<std::string, std::string>> pathogensStatus, std::string manuportage, std::string hosp_flag);
	Patient(repast::AgentId id, const std::vector <string>& v);
	Patient(const std::vector <string>& v);
	/*Getter*/
	std::string getHosp_flag(){return hosp_flag_;}
	/*Setter*/
	void setHosp_flag(std::string hosp_flag){ hosp_flag_ = hosp_flag;}
	void set(int currentRank, std::string calc_ident, std::string status,std::string dt_in, std::string dt_out, std::string ward, std::string gender, std::map <std::string, std::vector<std::string>> positivePathogens,std::vector <std::string> actualPositivePathogens, std::map<std::string, std::string> mapColonization, std::map<std::string, std::pair<std::string, std::string>> pathogensStatus, std::string manuportage,std::string hosp_flag_);
	
	virtual ~Patient();
	
	virtual void affiche() const;
	
	/* For archive packaging */
    // template<class Archive>
    // void serialize(Archive &ar, const unsigned int version){
        // ar & boost::serialization::base_object<Individual>(*this);
		// ar & hosp_flag_;
    // }
};

class Staff: public Individual{
	// friend class boost::serialization::access;
	
	private:

		std::string cat_; //caractéristique supplémentaire du staff

	
	public:
	Staff();
	Staff(Staff const& staffCopy);
	Staff(std::string calc_ident, std::string status,std::string dt_in, std::string dt_out, std::string ward, std::string gender);
	Staff(repast::AgentId id, std::string calc_ident, std::string status,std::string dt_in, std::string dt_out, std::string ward, std::string gender,std::string cat);
	Staff(repast::AgentId id, std::string calc_ident, std::string status,std::string dt_in, std::string dt_out, std::string ward, std::string gender,std::map <std::string, std::vector<std::string>> positivePathogens,std::vector <std::string> actualPositivePathogens,std::map<std::string, std::string> mapColonization, std::map<std::string, std::pair<std::string, std::string>> pathogensStatus, std::string manuportage,std::string cat);
	Staff(repast::AgentId id, const std::vector <string>& v);
	Staff(const std::vector <string>& v);
	/*Getter*/
	std::string getCat(){return cat_;}
	/*Setter*/
	void setCat(std::string cat){ cat_ = cat;}
	void set(int currentRank, std::string calc_ident, std::string status,std::string dt_in, std::string dt_out, std::string ward, std::string gender, std::map <std::string, std::vector<std::string>> positivePathogens,std::vector <std::string> actualPositivePathogens,std::map<std::string, std::string> mapColonization, std::map<std::string, std::pair<std::string, std::string>> pathogensStatus, std::string manuportage,std::string cat_);
	virtual ~Staff();
	
	virtual void affiche() const;
	
	/* For archive packaging */
    // template<class Archive>
    // void serialize(Archive &ar, const unsigned int version){
        // ar & boost::serialization::base_object<Individual>(*this);
		// ar & cat_;
    // }
	
};

/* Serializable Agent Package */
struct IndividualPackage: public OrganismPackage {
	
public:
	// int    id;
    // int    rank;
    // int    type;
    // int    currentRank;
	std::string calc_ident;
	std::string status;
	std::string dt_in;
	std::string dt_out;
	std::string ward;
	std::string gender;
	std::map <std::string, std::vector<std::string>> positivePathogens ;
	std::map<std::string, std::pair<std::string, std::string>> pathogensStatus;
	std::string manuportage;
	std::vector <std::string> actualPositivePathogens ;
	std::map<std::string, std::string> mapColonization;
	std::string supp;
    

    /* Constructors */
    IndividualPackage(); // For serialization
    IndividualPackage(int _id, int _rank, int _type, int _currentRank, std::string _calc_ident, std::string status,std::string _dt_in,std::string _dt_out,std::string _ward,std::string _gender, std::map <std::string, std::vector<std::string>> _positivePathogens, std::vector <std::string> _actualPositivePathogens, std::map<std::string, std::string> _mapColonization, std::map<std::string, std::pair<std::string, std::string>> _pathogensStatus, std::string _manuportage, std::string _supp);
	virtual ~IndividualPackage()=default;
	
    /* For archive packaging */
    template<class Archive>
    void serialize(Archive &ar, const unsigned int version){
		ar & boost::serialization::base_object<OrganismPackage>(*this);
        // ar & id;
        // ar & rank;
        // ar & type;
        // ar & currentRank;
        ar & calc_ident;
		ar & status;
        ar & dt_in;
		ar & dt_out;
		ar & ward;
		ar & gender;
		ar & positivePathogens;
		ar & pathogensStatus;
		ar & manuportage;
		ar & actualPositivePathogens;
		ar & mapColonization;
		ar & supp;
    }

};

#include "Pathogen.h"

#endif /* INDIVIDUAL_H_ */