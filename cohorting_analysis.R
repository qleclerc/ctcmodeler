
library(dplyr)
library(reshape2)
library(lubridate)
library(readr)


acquisition_path = here::here("output", "acquisition", "cohorting")
cohorting_names = list.files(acquisition_path)

adm_data = read.csv(here::here("input", "toy_admission.csv"), sep=";") %>%
  select(id, hospitalization, cat)
adm_data$cat[adm_data$cat == ""] = adm_data$hospitalization[adm_data$cat == ""]
adm_data = adm_data[,c(1,3)] %>%
  distinct()
eq_table = openxlsx::read.xlsx(here::here("input", "cat_groupings.xlsx")) %>%
  select(cat, cat_ag)

adm_data = adm_data %>%
  left_join(eq_table, by = "cat") %>%
  count(cat_ag) %>%
  rename(cat = cat_ag)


## COHORTING MATRIX #####

cohorting_matrix = data.frame(cat = c("Nurses", "Care assistants", "Reeducation", "Doctors", "Porters", "Other"),
                  s1 = c(1,0,0,0,0,0),
                  s2 = c(0,1,0,0,0,0),
                  s3 = c(0,0,1,0,0,0),
                  s4 = c(0,0,0,1,0,0),
                  s5 = c(0,0,0,0,1,0),
                  s6 = c(0,0,0,0,0,1),
                  
                  s7 = c(1,1,0,0,0,0),
                  s8 = c(1,0,1,0,0,0),
                  s9 = c(1,0,0,1,0,0),
                  s10 = c(1,0,0,0,1,0),
                  s11 = c(1,0,0,0,0,1),
                  
                  s12 = c(0,1,1,0,0,0),
                  s13 = c(0,1,0,1,0,0),
                  s14 = c(0,1,0,0,1,0),
                  s15 = c(0,1,0,0,0,1),
                  
                  s16 = c(0,0,1,1,0,0),
                  s17 = c(0,0,1,0,1,0),
                  s18 = c(0,0,1,0,0,1),
                  
                  s19 = c(0,0,0,1,1,0),
                  s20 = c(0,0,0,1,0,1),
                  
                  s21 = c(0,0,0,0,1,1),
                  
                  s22 = c(1,1,1,0,0,0),
                  s23 = c(1,1,0,1,0,0),
                  s24 = c(1,1,0,0,1,0),
                  s25 = c(1,1,0,0,0,1),
                  
                  s26 = c(1,0,1,1,0,0),
                  s27 = c(1,0,1,0,1,0),
                  s28 = c(1,0,1,0,0,1),
                  
                  s29 = c(1,0,0,1,1,0),
                  s30 = c(1,0,0,1,0,1),
                  
                  s31 = c(1,0,0,0,1,1),
                  
                  s32 = c(0,1,1,1,0,0),
                  s33 = c(0,1,1,0,1,0),
                  s34 = c(0,1,1,0,0,1),
                  
                  s35 = c(0,1,0,1,1,0),
                  s36 = c(0,1,0,1,0,1),
                  
                  s37 = c(0,1,0,0,1,1),
                  
                  s38 = c(0,0,1,1,1,0),
                  s39 = c(0,0,1,1,0,1),
                  
                  s40 = c(0,0,1,0,1,1),
                  
                  s41 = c(0,0,0,1,1,1),
                  
                  s42 = c(1,1,1,1,0,0),
                  s43 = c(1,1,1,0,1,0),
                  s44 = c(1,1,1,0,0,1),
                  
                  s45 = c(1,1,0,1,1,0),
                  s46 = c(1,1,0,1,0,1),
                  
                  s47 = c(1,1,0,0,1,1),
                  
                  s48 = c(1,0,1,1,1,0),
                  s49 = c(1,0,1,1,0,1),
                  
                  s50 = c(1,0,1,0,1,1),
                  s51 = c(1,0,0,1,1,1),
                  
                  s52 = c(0,1,1,1,1,0),
                  s53 = c(0,1,1,1,0,1),
                  s54 = c(0,1,1,0,1,1),
                  s55 = c(0,1,0,1,1,1),
                  s56 = c(0,0,1,1,1,1),
                  
                  s57 = c(1,1,1,1,1,0),
                  s58 = c(1,1,1,1,0,1),
                  s59 = c(1,1,1,0,1,1),
                  s60 = c(1,1,0,1,1,1),
                  s61 = c(1,0,1,1,1,1),
                  s62 = c(0,1,1,1,1,1),
                  
                  s63 = c(1,1,1,1,1,1),
                  
                  s64 = c(0,0,0,0,0,0)) %>%
  melt() %>%
  mutate(cat = factor(cat, levels = rev(c("Nurses", "Care assistants", "Reeducation", "Doctors", "Porters", "Other")))) %>%
  mutate(variable = gsub("s", "", variable)) %>%
  mutate(variable = as.numeric(variable))

cohorting_numbers = cohorting_matrix %>%
  left_join(adm_data, by = "cat") %>%
  mutate(n = n*value) %>%
  group_by(variable) %>%
  summarise(n=sum(n)) %>%
  rename(scenario = variable) %>%
  mutate(n = replace(n, n==0, max(n)))


## COHORTING #####

sim_id = 1
cohorting_summary = data.frame(acq_PA = rep(0, length(cohorting_names)),
                               acq_PE = rep(0, length(cohorting_names)),
                               acq_tot = rep(0, length(cohorting_names)),
                               sim_id = rep(0, length(cohorting_names)),
                               scenario = rep(-1, length(cohorting_names)))

#scenarios_to_test = c(0:23, 63, 64)

for(f in cohorting_names){
  
  #extract scenario number
  if(!grepl("scenario", f)){
    if(grepl("RandomGraph", f)){
      scenario = 64
    } else scenario = 0
  } else scenario = parse_number(f)
  
  #if(!(scenario %in% scenarios_to_test)) next
  
  data = read.csv(here::here(acquisition_path, f), sep = ";") %>%
    setNames(c("date", "prop_all", "prop_PA", "prop_PE", "acq_PA", "test_PA", "acq_PE", "test_PE", "id", "empty")) %>%
    select(acq_PA, acq_PE) %>%
    summarise(across(everything(),sum)) %>%
    mutate(acq_tot = acq_PA + acq_PE,
           sim_id = sim_id,
           scenario = scenario)
  
  cohorting_summary[sim_id,] = data
  sim_id = sim_id+1
}

#cohorting_summary = cohorting_summary %>%
#  filter(sim_id != 0)

table(cohorting_summary$scenario)
# cohorting_summary = rbind(cohorting_summary,
#                           c(20,10,30,0,25))


cohorting_summary = cohorting_summary%>%
  filter(scenario >= 0) %>%
  arrange(scenario)

#compare each with 50 randomly chosen 0s
ncompare = 2

cohorting_0 = cohorting_summary %>%
  filter(scenario == 0)
cohorting_summary = cohorting_summary %>%
  filter(scenario != 0)

cohorting_0_expand = data.frame(acq_tot = sample(cohorting_0$acq_tot,
                                                 nrow(cohorting_summary)*ncompare,
                                                 replace = T))

#note in vapply the "rep(1.1, ncompare)" argument just provides a template for the return value
#it does NOT affect results, just tells the function to return a vector of doubles with size = ncompare
cohorting_summary_expand = data.frame(acq_tot = as.vector(vapply(cohorting_summary$acq_tot,
                                                                 function(x) rep(x,ncompare),
                                                                 rep(1.1,ncompare))),
                                      scenario = as.vector(vapply(cohorting_summary$scenario,
                                                                  function(x) rep(x,ncompare),
                                                                  rep(1.1,ncompare))))

cohorting_summary_expand[,1] = (cohorting_0_expand[,1]-cohorting_summary_expand[,1])/cohorting_0_expand[,1]

cohorting_summary_expand = cohorting_summary_expand %>%
  left_join(cohorting_numbers, by = "scenario") %>%
  mutate(rel_acq_tot = acq_tot/n)

cohorting_tt = data.frame(scenario = unique(cohorting_summary$scenario),
                          val = 0, lower = 0, upper = 0, pval = 0,
                          rel_val = 0, rel_lower = 0, rel_upper = 0, rel_pval = 0)

for(i in unique(cohorting_tt$scenario)){
  
  tt = wilcox.test(cohorting_summary_expand %>%
                     filter(scenario==i) %>%
                     pull(acq_tot), conf.int = T, exact = F)
  
  cohorting_tt$val[i] = unname(tt$estimate)
  cohorting_tt$lower[i] = tt$conf.int[1]
  cohorting_tt$upper[i] = tt$conf.int[2]
  cohorting_tt$pval[i] = tt$p.value
  
  rel_tt = wilcox.test(cohorting_summary_expand %>%
                     filter(scenario==i) %>%
                     pull(rel_acq_tot), conf.int = T, exact = F)
  
  cohorting_tt$rel_val[i] = unname(rel_tt$estimate)
  cohorting_tt$rel_lower[i] = rel_tt$conf.int[1]
  cohorting_tt$rel_upper[i] = rel_tt$conf.int[2]
  cohorting_tt$rel_pval[i] = rel_tt$p.value
  
}


write.csv(cohorting_tt, here::here("results", "cohorting_results.csv"), row.names = F)
write.csv(cohorting_matrix, here::here("results", "cohorting_matrix.csv"), row.names = F)
