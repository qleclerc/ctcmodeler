/* FINDCONTACT CLASS
* this file gather all organism from the project
*
*	
*	FindContact.h
*
*	Created on: sept 04, 2018
*	Author: Audrey
*
*/

#ifndef FINDCONTACT_H_
#define FINDCONTACT_H_

#include <ctime>
#include <map>
#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <functional>
#include <numeric>
#include "repast_hpc/initialize_random.h"
#include <random>

#include <boost/algorithm/string.hpp>
#include <boost/random.hpp>
#include <boost/random/random_device.hpp>
#include <boost/math/distributions.hpp>
#include <boost/math/distributions/chi_squared.hpp>
#include <boost/random/gamma_distribution.hpp>

#include <stdio.h>
#include <time.h>
#include <vector>
#include <regex>
#include "Filetreatment.h"
#include "Learning.h"
#include "Individual.h"

class FindContact{

	protected:
	std::map<std::string, double> meetProb;
	bool catBool = false;
	bool withHosp = false;
	std::string test;
	std::map<std::string, std::vector<std::string>> catByWard;
	std::map<std::pair<std::string, std::string>, std::map<std::string, int>> coupleToCohort;
	bool cohorting;
	bool randomGraph = false;
	std::map<std::string, std::map<std::string, std::vector<std::string>>> admissionDischarge;
	std::map<std::string, std::map<std::string, std::vector<int>>> schedulePE;
	std::map<std::string, std::map<std::string, std::map<int, double>>> hourlyProb;
	vector<string> hospitalizationStatus;
	repast::Properties* mainProps;
	bool recordTest;
	map<std::string, std::map<std::string, std::vector<int>>> daysOfRecordByInd;
	map<Individual*, vector<string>> recordIndividuals;
	map<string, double> probRecord;
	std::map<std::string, int> nbOfIndRecord;
	std::vector<Individual*> allIndividuals;
	std::map<Individual*, vector<string>> allIndDates;
	std::map<time_t, std::vector<Contact*>> allContacts;
	std::map<time_t, std::vector<Contact*>> allRecordedContacts;
	std::map<std::string, std::pair<time_t, time_t>> mapLastCTCPeople; // pour chaque couple en contact le début et la fin du dernier contact
	std::map<std::string, int> mapNbCTCPeople; // pour chaque couple en contact le nombre de fois où ils ont été en contact
	std::map<std::string, std::map<std::string, std::vector<Individual*>>> alreadyMeetPeople;
	std::map<std::string, std::map<int, std::map<std::string, std::map<std::string, std::vector<Individual*>>>>> mapPeopleToMeet;
	boost::random::mt19937 eng;
	string filenameParameters;
	int nbOfSimuWeeks;
	std::string ctc;
	std::string admin;
	std::string schedule;
	std::map<std::string,vector<shared_ptr<Contact>>> mapCtc;
	std::vector<std::string> people;
	std::vector<std::vector<std::string>> tabAdmin;
	std::map<std::string, std::string> mapCat; 
	std::map<std::string, std::string> mapWard; 
	std::map<std::pair<std::string, std::string>, std::pair <std::string, std::string>> mapTabAdmin;
	std::string beginDate; //date de début de l'étude
	std::string endDate; //date de fin de l'étude
	std::vector<std::string> keyCat;
	std::vector<std::string> keyWard;
	std::map<std::string, std::map<std::string, std::map<std::string, double>>> resProba;
	std::map<std::string, std::map<std::string, std::map< std::string, std::pair< double, double>>>> mapDistributionNB;
	std::map<std::string, std::map<std::string, std::map< std::string, std::pair< double, double>>>> mapDistributionLength;
	std::string findWeekStatus(time_t date);
	int numberOfDaysByWeekStatus(std::vector<std::string> input_vector, std::string week);
	time_t beginDateSimuTime;
	time_t endDateSimuTime;
	std::map<std::string,std::vector<Individual*>> nbOfIndPerDay;
	std::vector<std::string> nonWorkingDays;
	
	public:
	//constructeur par défaut
	FindContact();
	//constructeur
	FindContact(repast::Properties* mainProps, std::string ctc_, std::string admin_, std::string schedule_,  std::string beginDate_,  std::string endDate_, bool recordTest_, bool catBool_);
	//destructeur
	~FindContact(); 
	
	std::vector<Individual*> checkTemporalCoherence(Individual* input_ind, std::vector<Individual*> input_vector, std::map<Individual*, std::vector<std::string>> allIndDates_, std::map<Individual*, std::vector<std::string>> allIndDatesTot);
	//cohorting
	std::map<std::string, std::vector<std::string>> mapCohort(std::vector<Individual*> input_individual, std::map<std::pair<std::string, std::string>, std::map<std::string, int>> couple, string beginDate, string endDate);
	std::map<std::pair<std::string, std::string>, std::map<std::string,int>> getCoupleToCohort(){return(coupleToCohort);}
	void setCoupleToCohort(std::map<std::pair<std::string, std::string>, std::map<std::string, int>> input_vector){coupleToCohort=input_vector;}
	bool getCohorting(){return(cohorting);}
	void setCohorting(bool input_bool){cohorting=input_bool;}
	//stat
	double randomNBinom(double size, double mean, boost::mt19937& rng);
	double randomGamma(double mean, double variance, boost::mt19937& rng);
	double randomExpo(double mean, boost::mt19937& rng);
	double randomPoisson(double mean, boost::mt19937& rng);
	double randomNormal(double mean, double variance, boost::mt19937& rng);
	double randomLogNormal(double mean, double variance, boost::mt19937& rng);
	void buildMapContact();
	void buildMeIndividuals();
	std::map<std::string,std::vector<Individual*>> computeNbOfIndPerDay(std::string fDate, std::string lDate);
	std::map<std::string,std::vector<Individual*>> computeNbOfIndPerDayRecord(std::string fDate, std::string lDate);
	std::map<std::string, std::map<int, std::vector<Individual*>>> computeNbOfIndPerDayWithHours(std::string fDate, std::string lDate, bool recorded=false, bool randomGraph_=false);
	std::vector<std::string> chooseRandomDate(std::vector<std::string> input_vector, double p);
	std::vector<std::string> keepPeriodDates(std::vector<std::string> input_vector, std::string fDate, std::string lDate);
	int nbOf(std::vector<std::string> individuals, std::string cat);
	std::map<std::string, int> nbOfCat(std::vector<std::string> individuals);
	std::tuple<std::map<std::string, std::string>, std::vector<std::string>, std::map<std::string, std::string>, std::vector<std::string>> findMapCat(std::vector<std::vector<std::string>> tabAdmin);
	std::map<std::string, std::vector<std::string>> InsideOutsideWard(std::vector<std::string> individuals, std::string ward);
	std::map<int, std::map<std::string, std::map<std::string, std::pair<double, double>>>> findMeanVarInAMap(std::map<int, std::map<std::string, std::map<std::string, std::vector<double>>>> input_map);
	void createMeNewContact(std::string fromStart, std::string filenameContact, bool randomGraph_in=false, bool cohorting_in=false, std::string test_in="");
	std::map<std::string, std::map<std::string, double>> fromToProbabilities(std::map <std::string, double> input_map, std::string cat);
	std::map<std::string, std::map<std::string, std::vector<Individual*>>> catInsideOutsideWard(std::vector<Individual*> individuals, std::string ward, Individual* ind);
	std::map<std::string, std::map<std::string, std::vector<Individual*>>> catInsideOutsideWardCohort(std::vector<Individual*> individuals, std::string ward, Individual* ind, std::map<std::string, std::vector<std::string>>map_cohort, std::map<std::pair<std::string, std::string>, std::map<std::string, int>> couple_vector);
	std::map<string, std::map<std::string, std::vector<Individual*>>> findMePeopleInContactWith(std::map<std::string, std::map<std::string, std::vector<Individual*>>> input_map, std::string cat, std::string hour_week, Individual* people, std::vector<time_t> pickHour, bool weekend, std::vector<Individual*> newPeopleHere_);
	std::map<string, std::vector<Individual*>> findMeFromToPeople(std::map<std::string, double> input_map, std::string stat, std::string cat, std::string hour_week, std::map<std::string, std::vector<Individual*>> peopleToMeetInside, Individual* people, std::vector<time_t> pickHour, bool weekend, std::vector<Individual*> newPeopleHere_);
	std::map<std::string, std::vector<Individual*>> catInsideVector(std::vector<Individual*> input_vector);
	std::vector<Individual*> chooseCandidatesInsideVector(std::vector<Individual*> input_vector, int nb, Individual* people);
	std::vector<Individual*> chooseCandidatesInsideVector(std::vector<Individual*> input_vector, int nb);
	std::vector<time_t> returnVectorOfTime(time_t day, time_t hour);	
	void fillContactFromOtherCat(std::vector<Individual*> peopleInCtc, std::string couple, Individual* people, std::string hour_week, std::vector<time_t> pickHour, std::map<std::string, std::pair<double, double>> mapLength, std::string statAR);
	void writeContacts(std::string filenameContact, std::map<time_t, std::vector<Contact*>> input_ctc);
	void findMeContactProbabilities(bool randomGraph_in = false, std::string test_in="");
	std::vector<std::string> returnVectorOfDay(string bday, string eday);
	std::map<std::string, std::vector<Individual*>> fillMapCatGeneral(std::vector<Individual*> input_vector);
	std::map<std::string, std::vector<Individual*>> fillMapTypeGeneral(std::vector<Individual*> input_vector);
	
	bool meetingStatus(std::string ind1, std::string ind2);
	std::vector<Individual*> checkContact(std::vector<Individual*> input_vector, Individual* ind, std::vector<time_t> pickHour);
	std::map<std::string, std::map<std::string, std::vector<Individual*>>> nbOfIndByCatByDay(std::string fDate, std::string lDate);
	std::pair<double, double> computeLogMeanAndVariance(std::vector<double> input_vector);
	std::map<int, std::map<std::string, int>> computeNbOfIndPerHour(std::string fDate, std::string lDate);
	static double computeMedian(std::vector<double>& numbers);
	std::map<std::string, std::map<std::string, std::vector<std::string>>> findRecordedDaysByWeekST();
	void buildSchedule();
	std::map<std::string, std::map<std::string, std::map<int, int>>> findRecordedHoursByWeekST(bool randomGraph_in=false);
	void buildHourlyProb(std::string date1, std::string date2, bool randomGraph_=false);
	std::map<std::string, std::vector<Individual*>> computeNbOfIndPerDayWithSchedule(std::string fDate, std::string lDate);
	std::vector<Individual*> checkSchedule(std::vector<Individual*> input_vector, std::string day, int hour);
	std::vector<Individual*> checkAdmissionDischarge(std::vector<Individual*> input_vector, std::string day, int hour);
	std::map<int, std::map<std::string, std::map<std::string, std::map< std::string, std::pair< double, double>>>>> orderedMap(std::map<std::string, std::map<std::string, std::map< std::string, std::pair< double, double>>>> input_map);
	void writeAllProbabilities();
	std::map<std::string, std::map<std::string, std::map<std::string, double>>> getResProba();
	void setResProba(std::map<std::string, std::map<std::string, std::map<std::string, double>>> input_map);
	bool getWithHosp(){return(withHosp);}
	void setWithHosp(bool withHosp_){withHosp=withHosp_;}
	std::map<std::string,vector<shared_ptr<Contact>>> getMapCtc(){return(mapCtc);}
	void setMapCtc(std::map<std::string,vector<shared_ptr<Contact>>> input_map){mapCtc = input_map;}
	std::map<std::string, std::string> getMapCat(){return(mapCat);}
	std::map<std::string, std::vector<std::string>> findPatientAndStaffFromVector(std::vector<std::string> input_vector);
	std::map<std::string, std::vector<std::string>> catInsideVectorString(std::vector<std::string> input_vector);
	std::map<std::string, std::vector<std::string>> findLinkedIndividualsAfterCohort(std::map<std::string, std::vector<std::string>> linked_map, std::map<std::string, std::vector<std::string>> cohorting_map);
	std::map<std::string, std::vector<std::string>> patientAndStaffFromDays(std::vector<std::string> dates, std::map<std::string, std::vector<Individual*>> input_indPerDay);
	std::map<std::pair<std::string, std::string>, std::pair<double, double>> sensorsBias(std::string fDate, std::string lDate);
	void writeContactParameters(std::string filenameContactParameters, std::string date1, std::string date2, bool randomGBool);
	void getContactParameters(std::string filename);
	vector<Individual*> getAllIndividuals(){return(allIndividuals);}
	std::string findMeCatGeneral(Individual* individual, bool patientHosp = false);
	
};

template<typename T, typename K>
std::map<int, std::map<T, K>> fillTheMap(std::vector<T> const& keyCat){
	
	std::map<int, std::map<T, K>> res;
	for(int i(0); i<24; i++){
		std::map<T, K> resInterm;
		for(auto const& k:keyCat){
			// cout<< k << endl;
			T insertK = k;
			K r = 0;
			resInterm.insert(make_pair(insertK, r));
		}
		res.insert(make_pair(i, resInterm));
	}
	return res;
}




#endif