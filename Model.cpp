/* Model
* this file allow the treatment of the different files
*
*	
*	Model.cpp
*
*	Created on: Jan 8, 2018
*	Author: Audrey
*
*/

#include <stdio.h>
#include <time.h>
#include <vector>
#include <regex>
#include <cmath>

#include "repast_hpc/AgentId.h"
#include "repast_hpc/RepastProcess.h"
#include "repast_hpc/Properties.h"
#include "repast_hpc/Utilities.h"
#include "repast_hpc/logger.h"
#include "repast_hpc/initialize_random.h"

#include <boost/lexical_cast.hpp>
#include <boost/mpi.hpp>
#include <boost/mpi/collectives.hpp>
#include <boost/algorithm/string.hpp>
#include "boost/date_time/posix_time/posix_time.hpp"
#include <boost/date_time/posix_time/posix_time_io.hpp>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>
#include <boost/math/distributions.hpp>
#include <boost/math/distributions/chi_squared.hpp>
#include <boost/iostreams/device/mapped_file.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/type_traits/integral_constant.hpp>
#include <boost/random.hpp>
#include <boost/random/random_device.hpp>
#include <boost/random/gamma_distribution.hpp>
#include <boost/tuple/tuple.hpp>
#include "gnuplot-iostream.h"

#include "repast_hpc/SVDataSetBuilder.h"

#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>

#include "Model.h"

#include "Individual.h"
#include "Pathogen.h"
#include "Organism.h"
#include "Learning.h"

#define DEBUG_RANK 6
using namespace repast;
using namespace std;
using namespace boost::accumulators;
using boost::math::chi_squared;
using boost::math::quantile;
using boost::math::complement;
using boost::iostreams::mapped_file_source;
using boost::iostreams::stream;

const string TICK_PATH = "tick.path";

/*the Boost library provides some very convenient functions relating to sending information back and forth across processes. This line tells Boost that there will be a class being sent back and forth that carries a specific kind of edge content, in this case the default RepastEdge with RepastHPCDemoAgents as nodes. With this, Boost performs some behind-the-scenes work to make sure that it knows how to package and send this content without too much other preparation needed. However, if you omit this line, the result will be Boost trying to send information for which it does not recognize the structure, and a segmentation fault will occur.*/
BOOST_CLASS_EXPORT_GUID(repast::SpecializedProjectionInfoPacket<repast::RepastEdgeContent<Organism> >, "SpecializedProjectionInfoPacket_EDGE");

OrganismPackageProvider::OrganismPackageProvider(repast::SharedContext<Organism>* organismPtr): organisms(organismPtr){
	
}

//Organism
void OrganismPackageProvider::providePackage(Organism* organism, std::vector<OrganismPackage>& out){
	repast::AgentId id = organism->getId();
	OrganismPackage package(id.id(), id.startingRank(), id.agentType(), id.currentRank());
	out.push_back(package);
	
}

void OrganismPackageProvider::provideContent(repast::AgentRequest req, std::vector<OrganismPackage>& out){
	std::vector<repast::AgentId> ids = req.requestedAgents();
	for(size_t i=0; i<ids.size(); i++){
		Organism* organism = dynamic_cast<Organism*>(organisms->getAgent(ids[i]));
		providePackage(organism,out);
	}
}

Organism* OrganismPackageReceiver::createAgent(OrganismPackage package){
	repast::AgentId id(package.id, package.rank,package.type, package.currentRank);
	return new Organism(id);
}

void OrganismPackageReceiver::updateAgent(OrganismPackage package){
	repast::AgentId id(package.id, package.rank,package.type);
	Organism* organism = dynamic_cast<Organism*>(organisms->getAgent(id));
	organism->set(package.currentRank);
}

//Individual
void OrganismPackageProvider::providePackage(Individual* individual, std::vector<IndividualPackage>& out){
	/*c'est une fonction qui va faire un package, c'est à dire qui stocke dans un vecteur toutes les infos de l'objet individual*/
	repast::AgentId id = individual->getId();
	string catInd = individual->getStatus();
	if(catInd=="PA"){
		Patient* patient = dynamic_cast<Patient*>(individual);
		IndividualPackage package(id.id(), id.startingRank(), id.agentType(), id.currentRank(),patient->getCalc_ident(), patient->getStatus(),patient->getDt_in(),patient->getDt_out(),patient->getWard(),patient->getGender(), patient->getPositivePathogens(), patient->getActualPositivePathogens(),  patient->getMapColonization(), patient->getPathogensStatus(), patient->getManuportage(), patient->getHosp_flag());
		out.push_back(package);
	}else if(catInd=="PE"){
		Staff* staff = dynamic_cast<Staff*>(individual);
		IndividualPackage package(id.id(), id.startingRank(), id.agentType(), id.currentRank(),staff->getCalc_ident(), staff->getStatus(),staff->getDt_in(),staff->getDt_out(),staff->getWard(),staff->getGender(), staff->getPositivePathogens(), staff->getActualPositivePathogens(), staff->getMapColonization(), staff->getPathogensStatus(), staff->getManuportage(), staff->getCat());
		out.push_back(package);
	}
}

void OrganismPackageProvider::provideContent(repast::AgentRequest req, std::vector<IndividualPackage>& out){
	/* c'est une fonction qui récupère le package quand l'individus est requis dans un autre process que son process home*/
	std::vector<repast::AgentId> ids=req.requestedAgents();
	for(size_t i = 0; i<ids.size();i++){
		Individual* ind = dynamic_cast<Individual*>(organisms->getAgent(ids[i]));
		providePackage(ind,out); 
	}
}

OrganismPackageReceiver::OrganismPackageReceiver(repast::SharedContext<Organism>* organismPtr): organisms(organismPtr){
	
}

Individual * OrganismPackageReceiver::createAgent(IndividualPackage package){
	/* c'est une fonction qui va créé un individus à partir d'un package dans un process qui n'est pas le process home de l'individus en question mais un process dans lequel il est requis */
	repast::AgentId id(package.id, package.rank, package.type,package.currentRank);
	if(package.status=="PA"){
		return new Patient(id, package.calc_ident, package.status,package.dt_in, package.dt_out, package.ward, package.gender,package.positivePathogens, package.actualPositivePathogens, package.mapColonization, package.pathogensStatus, package.manuportage, package.supp);
	}else if (package.status=="PE"){
		return new Staff(id, package.calc_ident, package.status,package.dt_in, package.dt_out, package.ward, package.gender, package.positivePathogens,package.actualPositivePathogens, package.mapColonization, package.pathogensStatus, package.manuportage, package.supp);
	}
}

void OrganismPackageReceiver::updateAgent(IndividualPackage package){
	/* c'est une fonction qui ne créé pas d'individus mais qui le met à jour dans le home en fonction de ce qui s'est pas passé dans les autres process */
	repast::AgentId id(package.id, package.rank, package.type,package.currentRank);
	Individual* individual = dynamic_cast<Individual*>(organisms->getAgent(id));
	string catInd = individual->getStatus();
	if(catInd=="PA"){
		Patient* patient = dynamic_cast<Patient*>(individual);
		patient->set(package.currentRank,package.calc_ident, package.status,package.dt_in, package.dt_out, package.ward, package.gender, package.positivePathogens, package.actualPositivePathogens,package.mapColonization, package.pathogensStatus, package.manuportage,package.supp);
	}else if(catInd=="PE"){
		Staff* staff = dynamic_cast<Staff*>(individual);
		staff->set(package.currentRank,package.calc_ident, package.status,package.dt_in, package.dt_out, package.ward, package.gender, package.positivePathogens, package.actualPositivePathogens, package.mapColonization,package.pathogensStatus, package.manuportage,package.supp);
	}
}

//pathogen
void OrganismPackageProvider::providePackage(Pathogen* pathogen, std::vector<PathogenPackage>& out){
	repast::AgentId id = pathogen->getId();
	PathogenPackage package(id.id(), id.startingRank(), id.agentType(), id.currentRank(), pathogen->getTypePath(), pathogen->getPositiveIndividuals(), pathogen->getActualPositiveIndividuals(), pathogen->getProbabilityOfTransmission());
	out.push_back(package);
	
}

void OrganismPackageProvider::provideContent(repast::AgentRequest req, std::vector<PathogenPackage>& out){
	std::vector<repast::AgentId> ids = req.requestedAgents();
	for(size_t i=0; i<ids.size(); i++){
		Pathogen* pathogen = dynamic_cast<Pathogen*>(organisms->getAgent(ids[i]));
		providePackage(pathogen,out);
	}
}

Pathogen* OrganismPackageReceiver::createAgent(PathogenPackage package){
	repast::AgentId id(package.id, package.rank,package.type, package.currentRank);
	return new Pathogen(id, package.typePath, package.positiveIndividuals, package.actualPositiveIndividuals, package.probabilityOfTransmission);
}

void OrganismPackageReceiver::updateAgent(PathogenPackage package){
	repast::AgentId id(package.id, package.rank,package.type);
	Pathogen* pathogen = dynamic_cast<Pathogen*>(organisms->getAgent(id));
	pathogen->set(package.currentRank,  package.typePath, package.positiveIndividuals, package.actualPositiveIndividuals, package.probabilityOfTransmission);
}

//bacterie
void OrganismPackageProvider::providePackage(Bacteria* bacteria, std::vector<BacteriaPackage>& out){
	repast::AgentId id = bacteria->getId();
	BacteriaPackage package =  BacteriaPackage(id.id(), id.startingRank(), id.agentType(), id.currentRank(), bacteria->getTypePath(), bacteria->getPositiveIndividuals(), bacteria->getActualPositiveIndividuals(), bacteria->getProbabilityOfTransmission(), bacteria->getFamily(),bacteria->getGenus(), bacteria->getSpecies(), bacteria->getSpatype(), bacteria->getBMR(),bacteria->getPhenotypeClass());
	out.push_back(package);
}

void OrganismPackageProvider::provideContent(repast::AgentRequest req, std::vector<BacteriaPackage>& out){
	std::vector<repast::AgentId> ids = req.requestedAgents();
	for(size_t i=0; i<ids.size(); i++){
		Bacteria* bacteria = dynamic_cast<Bacteria*>(organisms->getAgent(ids[i]));
		providePackage(bacteria,out);
	}
}

Bacteria* OrganismPackageReceiver::createAgent(BacteriaPackage package){
	repast::AgentId id(package.id, package.rank,package.type, package.currentRank);
	return new Bacteria(id, package.typePath, package.positiveIndividuals, package.actualPositiveIndividuals, package.probabilityOfTransmission, package.family, package.genus, package.species, package.spatype, package.BMR, package.sequenceATB);
}

void OrganismPackageReceiver::updateAgent(BacteriaPackage package){
	repast::AgentId id(package.id, package.rank,package.type);
	Bacteria* bacteria = dynamic_cast<Bacteria*>(organisms->getAgent(id));
	bacteria->set(package.currentRank,  package.typePath, package.positiveIndividuals, package.actualPositiveIndividuals, package.probabilityOfTransmission, package.family, package.genus, package.species, package.spatype, package.BMR, package.sequenceATB);
}

Model::Model(repast::Properties* mainProps, int argc, char** argv, boost::mpi::communicator* comm, string startD, string stopD, string gameStat): context(comm), startDate(startD), stopDate(stopD), gameStatus(gameStat){
	//---- fonction qui crée le Model et permet de tout mettre en place ----//
	props = mainProps;
	initializeRandom(*props, comm);
	eng = Random::instance()->engine();
	string debugModeString = props->getProperty("debugMode");
	debugMode = debugModeString=="yes"? true:false;	
	startDate = startDate + " 00:00:00";
	stopDate = stopDate + " 23:59:30";
	string tickPathString = props->getProperty(TICK_PATH);
	tickPath = strToInt(tickPathString);
	rank = RepastProcess::instance()->rank();
	struct tm tmStop;
	const char *stopDateChar = stopDate.c_str();
	strptime(stopDateChar, "%Y-%m-%d%t%T", &tmStop);
	tmStop.tm_isdst = 0;
	time_t timStop = mktime(&tmStop);
	struct tm tmStart;
	const char *startDateChar = startDate.c_str();
	strptime(startDateChar, "%Y-%m-%d%t%T", &tmStart);
	tmStart.tm_isdst = 0;
	timStart = mktime(&tmStart);
	stopAt = (timStop - timStart)/(time_t)tickPath;
	provider = new OrganismPackageProvider(&context);
	receiver = new OrganismPackageReceiver(&context);
	organismNetwork = new repast::SharedNetwork<Organism,repast::RepastEdge<Organism>, repast::RepastEdgeContent<Organism>, repast::RepastEdgeContentManager<Organism> >("organismNetwork",false,&edgeContentManager);
	context.addProjection(organismNetwork);
	enteroATB = props->getProperty("enterobacteria.atb.list");
	staphATB = props->getProperty("staphylococcus.atb.list");
	probaByType = props->getProperty("probaByType") == "yes" ? true : false;
	durationByType = props->getProperty("durationByType") == "yes" ? true : false;
	prevalenceByWard = props->getProperty("prevalenceByWard") == "yes" ? true : false;
	string infoB = props->getProperty("pathogen.file");
	Filetreatment *f = new Filetreatment(infoB);
	Mt_path=f->tabInfoPathogen(); 
	allBacteriaToAnalyzed = f->buildMeAVectorOfPathogens(Mt_path);
	if(rank==0)cout << " Les bactéries à analyser sont: ";
	for(auto const& b:allBacteriaToAnalyzed){
		if(rank==0)cout << b << "\n";
	}
	if(rank==0)cout << endl;
	delete f;
	if(debugMode){
		cout<< rank << " Fonction: Model --> début du modèle \n";
		cout<< " Fonction: Model --> startDate " << startDate << " stopDate " << stopDate << " tickPath " << tickPath << "\n";
		cout<< " Fonction: Model --> probaByType " << probaByType << " durationByType " << durationByType << " prevalenceByWard " << prevalenceByWard << "\n";
		cout<< " Fonction: Model --> allBacteriaToAnalyzed " << allBacteriaToAnalyzed.size() << endl;
	}
}

Model::~Model(){
	/* fonction qui détruit le modèle et d'autres élèments à la fin */
	delete provider;
	delete receiver;
	delete props;
	delete parameters;
	if(ifs_ctc.is_open()){
		ifs_ctc.close();
	}
	if(ifs_prlvt.is_open()){
		ifs_prlvt.close();
	}
	if(myCSVacquisition.is_open()){
		myCSVacquisition.close();
	}

	if(myCSVprevalence.is_open()){
		myCSVprevalence.close();
	}
	if(myCSVswab.is_open()){
		myCSVswab.close();
	}
	remove("myDAT.dat");
}

vector<string> Model::stringToVectorProps(string propsName, string delim){
	/* fonction qui prend en argument un string séparé par un delim et retourne un vector de string comprenant chaque string délimité par delim */ 
	vector<string> res;
	string propsString = props->getProperty(propsName);
	boost::trim_if(propsString, boost::is_any_of("\r\n|\r|\n|\t"));
	boost::split(res, propsString, boost::is_any_of(delim));
	return res;
}

void Model::init(string admin,string ctc,string prlvt, string fileNameResult_, string optionSimu_, string numberOfSim_){
	/* Ici on intialise le modèle et du coup on initialise les individus qui vont être mis dans le context, on initialise aussi la mapContact qui contient tous les contacts mais comme on le fait qu'une fois on l'a dispatch dans les autres process via broadcast qui transmet la mapContact dans les autres process */
	
	optionSimu = optionSimu_;
	numberOfSim = numberOfSim_;
	fileNameResult = fileNameResult_;
	
	boost::mpi::communicator* comm = repast::RepastProcess::instance()->getCommunicator();
	string stringWards = props->getProperty("ward.name");
	
	//récuperation des paramètres et option indispensable au fonctionnement
	boost::split(wards , stringWards , boost::is_any_of("-"),boost::token_compress_on);
	manualDuration = strToInt(props->getProperty("manualDuration"));
	if(manualDuration!=0){
		boolManDuration = true;
	}
	// logPAPA = props->getProperty("withLogPAPA") == "yes" ? true : false;
	// logPAPE = props->getProperty("withLogPAPE") == "yes" ? true : false;
	// logPEPA = props->getProperty("withLogPEPA") == "yes" ? true : false;
	// logPEPE = props->getProperty("withLogPEPE") == "yes" ? true : false;
	distributionProb = props->getProperty("distributionProb");
	string testParam = props->getProperty("findParameters");
	if(testParam=="yes"){
		string filenameParameters;
		if(fileNameResult==""){
			filenameParameters = props->getProperty("filenameParameters");
		}else{
			filenameParameters = fileNameResult;
		}
		Filetreatment *f_param = new Filetreatment(filenameParameters);
		parameters = f_param->findParameters();
		delete f_param;
	}
	
	map<string, map<string, vector<double>>> mapProbSim = parameters->getMapProbabilityOfTransmission2();
	string borne = props->getProperty("borne");
	
	// map<string, map<string, double>> mapProbSim = parameters->getMapProbabilityOfTransmission();
	
	if(rank==0)cout << " les proba sont : " << endl;
	// for(auto const& [key, val]:mapProbSim){
		// for(auto const& [k,v]:val){
			// DoubleUniformGenerator chooseProbaContener = Random::instance()->createUniDoubleGenerator(v[0],v[1]);
			// double proba = chooseProbaContener.next();
			// mapProb[key][k] = proba;
			// if(rank==0) cout << key << " --> " << k << " --> " << proba << endl;
		// }
	// }
	
	for(auto const& [key, val]:mapProbSim){
		for(auto const& [k,v]:val){
			// double valueBorne = 0.0;
			double valueBorne = v[0];
			// if(borne=="inf"){
				// valueBorne = v[0];
			// }else if(borne=="sup"){
				// valueBorne = v[1];
			// }
			mapProb[key][k] = valueBorne;
			if(rank==0) cout << key << " --> " << k << " --> " << valueBorne << endl;
		}
	}
	// cout << " proba ok " << endl;
	
	// mapProb = mapProbSim;
	
	// if(rank==0)cout << " les proba sont : " << endl;
	// for(auto const& [key, val]:mapProbSim){
		// for(auto const& [k,v]:val){
			// if(rank==0) cout << key << " --> " << k << " --> " << v << endl;
		// }
	// }
	
	aleaLevel = props->getProperty("aleaLevel") == "yes" ? true : false;
	
	findMapDayRandom();
	
	//Fichier de contact à initialiser
	Filetreatment *f_ctc = new Filetreatment(ctc);
	string line_ctc;
	ifs_ctc =  ifstream(f_ctc->getFile());
	delete f_ctc;	
	getline(ifs_ctc,line_ctc); /* header */
	getline(ifs_ctc, line_ctc); /* première ligne */
	boost::trim_if(line_ctc, boost::is_any_of("\r\n|\r|\n|\t"));
	boost::split(tempo_ctc, line_ctc, boost::is_any_of(";"));
	struct tm tm_ctc;
	const char *tempo_ctcChar = tempo_ctc[2].c_str();
	strptime(tempo_ctcChar, "%Y-%m-%d%t%T", &tm_ctc);
	time_t timCtc = mktime(&tm_ctc);	
	/* ensuite il faut avancer dans le fichier */
	if(timCtc <= (timStart+tickPath)){
		while(timCtc < (timStart+tickPath)){
			getline(ifs_ctc, line_ctc); /* première ligne */
			boost::trim_if(line_ctc, boost::is_any_of("\r\n|\r|\n|\t"));
			boost::split(tempo_ctc, line_ctc, boost::is_any_of(";"));
			tempo_ctcChar = tempo_ctc[2].c_str();
			strptime(tempo_ctcChar, "%Y-%m-%d%t%T", &tm_ctc);
			timCtc = mktime(&tm_ctc);
		}
	}else{
		if(debugMode && rank==0)cout << " pas de contact avant la date de début " << endl;
	} 
	try{
		if(stringToDate(stopDate) < timCtc){
			throw runtime_error(" no contact available during the simulation period ");
		}
	}catch(std::exception const& exc){
		cerr << exc.what() << "\n";
		exit(1);
	}
	
	useOfPreviousPrlvt = props->getProperty("usePreviousPrlvt");
	
	if(useOfPreviousPrlvt=="yes"){
		//Fichier des prélèvements à initialiser
		Filetreatment *f_prlvt = new Filetreatment(prlvt);
		string line_prlvt;
		ifs_prlvt =  ifstream(f_prlvt->getFile());
		delete f_prlvt;
		getline(ifs_prlvt,line_prlvt); /* header */
		getline(ifs_prlvt, line_prlvt); /* première ligne */
		boost::trim_if(line_prlvt, boost::is_any_of("\r\n|\r|\n|\t"));
		boost::split(tempo_prlvt, line_prlvt, boost::is_any_of(";"));
		struct tm tm_prlvt;
		string tempo_prlvtString = Filetreatment::checkDate(tempo_prlvt[1]) + " 00:00:00";
		const char *tempo_prlvtChar = tempo_prlvtString.c_str();
		strptime(tempo_prlvtChar, "%Y-%m-%d%t%T", &tm_prlvt);
		time_t timPrlvt = mktime(&tm_prlvt);	
		/* ensuite il faut avancer dans le fichier */
		if(timPrlvt <= timStart){
			while(timPrlvt < timStart){
				previousPrlvt.insert(pair<string,vector<string>>(tempo_prlvt[0],tempo_prlvt));
				getline(ifs_prlvt, line_prlvt); /* première ligne */
				boost::trim_if(line_prlvt, boost::is_any_of("\r\n|\r|\n|\t"));
				boost::split(tempo_prlvt, line_prlvt, boost::is_any_of(";"));
				timPrlvt = stringToDateWithHour(tempo_prlvt[1]);
			}
		}else{
			if(debugMode && rank==0)cout << " pas de prlvt avant la date de début " << endl;
		} 
	}
	
	
	string nbOfInitializedPatientString = props->getProperty("nbOfInitializedPatient");
	string nbOfInitializedStaffString = props->getProperty("nbOfInitializedStaff");
	string nbOfInitializedIndividualString = props->getProperty("nbOfInitializedIndividual");
	int nbOfInitializedPatient = strToInt(nbOfInitializedPatientString);
	int nbOfInitializedStaff = strToInt(nbOfInitializedStaffString);
	int nbOfInitializedIndividual = strToInt(nbOfInitializedIndividualString);
	InitializedPatientAtAdmission = props->getProperty("InitializedPatientAtAdmission") == "yes" ? true : false;
	InitializedStaffAtAdmission = props->getProperty("InitializedStaffAtAdmission") == "yes" ? true : false;
		
	/* Ici on va initialiser les premiers pathogènes dans le context au niveau du rank 0 */
	int idP = -1;
	if(rank == 0){
		if(gameStatus=="real"){
			if(!previousPrlvt.empty()){
				//on rajoute dans le context les pathogens d'avant le début de la période s'il y en avait
				idP = firstPathogen();
			}else{
				//il n'y a pas d'ancien prlvt donc on peut ajouter les bactéries à analyser dans le context (non liées à des ind)
				cout << " ooops pb avec la creation de first pathogen dans init " <<endl;
				idP = InitializeWithBacteriaToAnalyzed();
			}
		}
		if(gameStatus=="simu"){
			if(useOfPreviousPrlvt=="yes" && !previousPrlvt.empty()){
				idP = firstPathogen();
			}else{
				idP = InitializeWithBacteriaToAnalyzed();
			}
			
		}
	}
	boost::mpi::broadcast(*comm, mapId, 0);
	boost::mpi::all_gather(*comm, idP, pathCounts);	
	/* On copie ces pathogènes dans les autres ranks */
	AgentRequest request(rank);
	if (rank!=0){
		for(int j(0);j<pathCounts[0];j++){
			request.addRequest(AgentId(j,0,1));
		}					
	}
	repast::RepastProcess::instance()->requestAgents<Organism, BacteriaPackage, OrganismPackageProvider, OrganismPackageReceiver>(context, request, *provider, *receiver, *receiver, "SET_2");
	
	//pas de temps
	tickDay = this->findTickDay();
	timeResp = this->findTimeResponse();
	
	/* On initilialise les premiers individus */
	countOfIndividuals=initializeIndividuals(admin); 
	vector<Organism*> listOfI;
	context.selectAgents(context.size(), listOfI, 0);
	
	incubationPeriod = props->getProperty("incubationPeriod");
	incubationPeriodWithContagion = props->getProperty("incubationPeriodWithContagion");
	immunization = props->getProperty("immunization") == "yes" ? true : false;
	immunizationDuration = props->getProperty("immunizationDuration");
	immunizationVAR = props->getProperty("immunizationVAR");
	asymptomatic = props->getProperty("asymptomatic");
	severity = props->getProperty("severity");
	
	weeklyColStaff = strToInt(props->getProperty("weeklyColStaff"));
	
	
	stringCatStart = props->getProperty("cat.start");
	if(stringCatStart!="none"){
		boost::split(catStartV , stringCatStart , boost::is_any_of("-"),boost::token_compress_on);
	}
	
	stringHospStart = props->getProperty("hosp.start");
	if(stringHospStart!="none"){
		boost::split(hospStartV , stringHospStart , boost::is_any_of("-"),boost::token_compress_on);
	}
	
	//output --> acquisition et prévalence
	returnStatusByDay = props->getProperty("returnStatusByDay") == "yes" ? true : false;
	returnPrev = props->getProperty("returnPrevalence") == "yes" ? true : false;
	returnAcq = props->getProperty("returnAcquisition") == "yes" ? true : false;
	returnSwab = props->getProperty("returnSwab") == "yes" ? true : false;
	string FileNameForAc;	
	string FileNameStatusByDay;
 	if(gameStatus=="real"){
		FileNameForAc = props->getProperty("filenameAcquisitionReal");
		FileNameStatusByDay = props->getProperty("filenameStatusByDayReal");
		initializeFirstPathogens();
	}else if(gameStatus=="simu"){
		FileNameForAc = props->getProperty("filenameAcquisitionSimu");
		FileNameStatusByDay = props->getProperty("filenameStatusByDaySimu");
		if(useOfPreviousPrlvt=="yes"){
			initializeFirstPathogens();
		}
		if(nbOfInitializedPatient!=0 || nbOfInitializedStaff!=0 || nbOfInitializedIndividual!=0){
			chooseIndForSimu(nbOfInitializedIndividual, nbOfInitializedPatient, nbOfInitializedStaff);
		}
	}
	
	//immunisation au départ
	vector<string> paI;
	vector<string> peI;
	if(props->getProperty("percentageFirstImmunizedPA")!="NULL" && props->getProperty("percentageFirstImmunizedPE")!="NULL"){
		cout <<  " IMMUNIZATION ON " << endl;
		double percentageIPA = stod(props->getProperty("percentageFirstImmunizedPA"));
		double percentageIPE = stod(props->getProperty("percentageFirstImmunizedPE"));
		initializeFirstImmunized(percentageIPA, percentageIPE);
		
	}
	vector<Organism*> IndividualContextI;
	context.selectAgents(context.size(),IndividualContextI,0);
	for(auto const& elem:IndividualContextI){
		Individual* ind = dynamic_cast<Individual*>(elem);
		std::map<std::string, std::pair<std::string, std::string>> PS = ind->getPathogensStatus();
		for(auto const& [key, val]:PS){
			if(val.first=="immunization" && ind->getStatus()=="PA"){
				paI.push_back(ind->getCalc_ident());
			}else if(val.first=="immunization" && ind->getStatus()=="PE"){
				peI.push_back(ind->getCalc_ident());
			}
		}
	}
	
	if(returnAcq)myCSVacquisition.open(FileNameForAc);
	string nbOfAcquisitionWeekString = props->getProperty("nbOfAcquisitionWeek");
	nbOfAcquisitionWeek = strToInt(nbOfAcquisitionWeekString);
	firstAcqOnly = props->getProperty("firstAcqOnly") == "yes" ? true : false;
	string FileNameForPrev;
	if(gameStatus=="real"){
		FileNameForPrev = props->getProperty("filenamePrevalenceReal");
	}else{
		FileNameForPrev = props->getProperty("filenamePrevalenceSimu");
	}
	if(returnPrev)myCSVprevalence.open(FileNameForPrev);	
	
	//information relatif au nombre de prlvt
	string FileNameForSwab;
	if(gameStatus=="simu" && returnSwab){
		if(returnSwab)FileNameForSwab = "output/swab/swabSIMU"+ numberOfSim +".csv";
		if(returnSwab)myCSVswab.open(FileNameForSwab);
	}
	
	//synchronisation
	nonWorkingDays = findMeNonWorkingDays(startDate, stopDate, true);
	cleanIndividualSwab();
	synchInd();
	synchPath();
	
	//décompte des individus à l'initialisation
	vector<Organism*> allIndiv;
	context.selectAgents(context.size(), allIndiv, 0);
	int tot = 0;
	int totC = 0;
	int paNB = 0;
	int paNBC = 0;
	int peNB = 0;
	int peNBC = 0;
	vector<string>paCol;
	vector<string>peCol;
	for(auto const& elem: allIndiv){
		Individual* i = dynamic_cast<Individual*>(elem);
		tot = tot + 1;
		if(i->getStatus()=="PA"){
			paNB = paNB + 1;
			if(!i->getActualPositivePathogens().empty()){
				paCol.push_back(i->getCalc_ident());
				paNBC = paNBC + 1;
				totC = totC + 1;
			}
		}else{
			peNB = peNB + 1;
			if(!i->getActualPositivePathogens().empty()){
				peCol.push_back(i->getCalc_ident());
				peNBC = peNBC + 1;
				totC = totC + 1;
			}
		}
		
	}
	
	//intervention si besoin
	interventions = props->getProperty("interventions") == "yes" ? true : false;
	string scenario = props->getProperty("scenario");
	if(scenario=="0" || (scenario=="none" && props->getProperty("cohorting")=="yes")){
		interventions = false;
	}
	compliance = props->getProperty("contactPrecaution") == "yes" ? true : false;
	vaccination = props->getProperty("vaccination") == "yes" ? true : false;

	superC = props->getProperty("superContractorON") == "yes" ? true : false;

	cout << " interventions " << interventions << " et compliance " << compliance << " superContractor " << superC << " vaccination " << vaccination << endl;
	if(interventions && (compliance || vaccination)){
		
		string fileForInterventions = props->getProperty("interventionsFile");
		try{
			if(FileExists(fileForInterventions)==0){
				throw std::runtime_error(" fail to find interventionsFile file");
			}
		}catch(std::exception const& exc){
			cerr << exc.what() << "\n";
			exit(1);
		}
		Interventions* interventionContact = new Interventions(fileForInterventions);
		if(superC){
			superContractors = interventionContact->getSuperContractors();
		} else {
			complianceCouple = interventionContact->getComplianceCouple();
		}
		delete interventionContact;

		if(rank==0 && compliance)cout << "---- Contact precaution ON ---- " << endl;
		if(rank==0 && vaccination)cout << "---- Vaccination ON ---- " << endl;

		if(rank==0 && superC)cout << "---- Super Contractor ON ---- " << endl;
		if(rank==0 && !superC)cout << "---- Super Contractor OFF ---- " << endl;

		if(rank==0)cout << "InterventionFile: " << fileForInterventions << endl;
	}
	if(!probaByType){
		groups = buildGroup(groupFile);
		if(rank==0)cout << " groups: " << endl;
		for(auto const& [key, val]:groups){
			if(rank==0)cout << key << " --> " << val << endl;
		}
	}
	
	probaThreshold = props->getProperty("probaThreshold");
	
	
	
	//affichage des informations utiles
	if(rank==0){
		cout << "A l'initialisation il y a " << tot << " individus dont " << totC << " colonisés " << " il y a aussi " << paNB << " patient (" << paNBC << ")" << " et " << peNB << "("<<peNBC << ") staff "  << endl;
		cout<< "les patients sont: ";
		for(auto const& elem:paCol){
			cout << elem << " ";
			index_second_cases[elem];
		}
		cout<<endl;
		cout<< "les staffs sont: ";
		for(auto const& elem:peCol){
			cout << elem << " ";
			index_second_cases[elem];
		}
		cout<<endl;
		cout << "il y a " << paI.size() << " patients immunisés et " << peI.size() << " staff " << endl;
		cout << "Les fichiers de sortie sont: " <<endl;
		cout << "Acquisition: " << FileNameForAc <<endl;
		cout << "Prevalence: " << FileNameForPrev <<endl;
		cout << "StatusByDay: " << FileNameStatusByDay <<endl;
	}
	swabPeople = props->getProperty("swabPeople") == "yes" ? true : false;
	fixedColonization = props->getProperty("fixedColonization") == "yes" ? true : false;
	fixedColonizationNb = strToInt(props->getProperty("fixedColonizationNb"));
	fixedColonizationStatus=props->getProperty("fixedColonizationStatus");
}

int Model::InitializeWithBacteriaToAnalyzed(){
	/* Fonction qui initialize dans le context les bacteries que l'on doit analyser */
	if(debugMode)cout<< rank << " Fonction: InitializeWithBacteriaToAnalyzed " << " début de la fonction " << endl;
	int idP = -1;
	int res;
	vector<Organism*> currentPathogens;
	for(auto const& bacteriaToFind:allBacteriaToAnalyzed){
		if(debugMode)cout<< rank << " Fonction: InitializeWithBacteriaToAnalyzed " << " bacteriaToFind " << bacteriaToFind << " \n";
		vector<string> bacteriaVector;
		boost::split(bacteriaVector, bacteriaToFind, boost::is_any_of("_"));
		string atb_str = bacteriaVector[6];
		string listA;
		if(bacteriaVector[1]=="Enterobacteriaceae"){
			listA = enteroATB;
		}else if(bacteriaVector[1]=="Staphylococcaceae"){
			listA = staphATB;
		}
		vector<Organism*>::iterator itPath = find_if(currentPathogens.begin(), currentPathogens.end(),[bacteriaToFind](Organism* v) -> bool {Bacteria* m1 = dynamic_cast<Bacteria*>(v); return m1->getStringId()==bacteriaToFind;});
		if(itPath==currentPathogens.end()){
			idP++;
			repast::AgentId id(idP, rank, 1); 
			id.currentRank(rank);
			Bacteria* bacteria = new Bacteria(id,bacteriaVector,listA);
			if(debugMode)cout<< rank << " Fonction: InitializeWithBacteriaToAnalyzed " << " ajout de la bacterie " << bacteria->getStringId() << endl;
			mapId[bacteria->getStringId()] = id;
			context.addAgent(bacteria);
			currentPathogens.push_back(bacteria);
		}
	}
	res = currentPathogens.size();
	if(debugMode)cout<< rank << " Fonction: InitializeWithBacteriaToAnalyzed " << " nombre de bacterie ajouté au context " << res << endl;
	return res; 
}

void Model::chooseIndForSimu(int nbOfPeople){
	/* Fonction qui choisit nbOfPeople individus au hasard et qui attribut des bactéries parmis celle qui sont à analyser */
	boost::mpi::communicator* comm = repast::RepastProcess::instance()->getCommunicator();
	vector<Organism*> individualsContext;
	context.selectAgents(context.size(),individualsContext,0);
	vector<Organism*> pathogensContext;
	context.selectAgents(context.size(),pathogensContext,1);
	vector<string> choosingInd;
	IntUniformGenerator chooseProbaContener = Random::instance()->createUniIntGenerator(0,individualsContext.size()-1);
	int randomNb;
	for(auto const& bacteria:allBacteriaToAnalyzed){
		vector<Organism*>::iterator iter2 = find_if(pathogensContext.begin(),pathogensContext.end(), [bacteria](Organism* m) -> bool {Bacteria* m1 = dynamic_cast<Bacteria*>(m); return m1->getStringId() == bacteria; });
		if(iter2!=pathogensContext.end()){
			Individual* ind;
			Bacteria* bacteriaObject = dynamic_cast<Bacteria*>(*iter2);
			for(int i(0); i<nbOfPeople; i++){
				string theChosenOne;
				if(rank==0){
					randomNb = chooseProbaContener.next();
					ind = dynamic_cast<Individual*>(individualsContext.at(randomNb));
					vector<string>::iterator itC = find(choosingInd.begin(), choosingInd.end(), ind->getCalc_ident());
					vector<string>::iterator itBC = find(ind->getActualPositivePathogens().begin(), ind->getActualPositivePathogens().end(), bacteria);
					while(itC!=choosingInd.end() || itBC!=ind->getActualPositivePathogens().end()){
						randomNb = chooseProbaContener.next();
						ind = dynamic_cast<Individual*>(individualsContext.at(randomNb));
						itBC = find(ind->getActualPositivePathogens().begin(), ind->getActualPositivePathogens().end(), bacteria);
						itC = find(choosingInd.begin(), choosingInd.end(), ind->getCalc_ident());
					}
					theChosenOne = ind->getCalc_ident();
				}
				boost::mpi::broadcast(*comm,theChosenOne,0);
				choosingInd.push_back(theChosenOne);
				if(rank!=0){
					vector<Organism*>::iterator iterContext = find_if(individualsContext.begin(),individualsContext.end(), [theChosenOne](Organism* m) -> bool {Individual* m1 = dynamic_cast<Individual*>(m); return m1->getCalc_ident() == theChosenOne; });
					if(iterContext!=individualsContext.end()){
						ind = dynamic_cast<Individual*>((*iterContext));
					}else{
						cout << rank << " oooops la il y a un pb " << endl;
					}
				}
				// if(rank == ind->getId().currentRank()){
					// ind->addActualPositivePathogens(bacteriaObject->getStringId());
					// string endDateDuration;
					// string type = durationByType ? ind->getStatus(): returnCat(ind);

					// if(boolManDuration){
						// endDateDuration=findDate(manualDuration);
					// }else{
						// endDateDuration = findDurationOfColonization(bacteria, startDate.substr(0,10),type);
					// }
					// ind->addColonization(bacteria, endDateDuration);
					// string st = "";
					// string dateST;
					// if(incubationPeriod!="none"){
						// st="incubation";
						// time_t finaldateST = stringToDate( startDate.substr(0,10)) + time_t(stoi(incubationPeriod)*86400);
						// dateST = dateToString(finaldateST).substr(0,10);
					// }else{
						// st="symptomatic";
						// dateST=endDateDuration;
					// }
					// ind->changePathogensStatus(bacteria, st, dateST);
				// }
				//string level = findStartLevel(aleaLevel);
				string level = "symptomatic";
				changeIndividualPathogenStatus(ind, bacteriaObject->getStringId(), level, startDate.substr(0,10));
				
				if(rank == bacteriaObject->getId().currentRank()){
					bacteriaObject->addActualPositiveIndividuals(ind->getCalc_ident());
				}
			}
		}else{
			cout << " oooops pb " << endl;
		}		
	}
	synchInd();
	synchPath();
}

string Model::findStartLevel(bool aleaLevel){
	string level;
	boost::mpi::communicator* comm = repast::RepastProcess::instance()->getCommunicator();
	if(!aleaLevel){
		level = "symptomatic";
		boost::mpi::broadcast(*comm,level,0);
	}else{
		vector<string> everyLevel = {"incubation", "incubationWithContagion", "asymptomatic"};
		if(rank==0){
			IntUniformGenerator chooseProbaContener = Random::instance()->createUniIntGenerator(0,everyLevel.size()-1);
			int intLevel = chooseProbaContener.next();
			level = everyLevel[intLevel];
		}
		boost::mpi::broadcast(*comm,level,0);
	}

	return level;
}

int Model::returnUnif(string x){
	//attention dans un seul rank
	int res;
	vector<string> input_vector;
	boost::split(input_vector,x , boost::is_any_of("-"),boost::token_compress_on);
	int start = stoi(input_vector[0]);
	int end = stoi(input_vector[1]);
	IntUniformGenerator chooseProbaContener = Random::instance()->createUniIntGenerator(start,end);
	res = chooseProbaContener.next();
	return res;
}

void Model::changeIndividualPathogenStatus(Individual* ind, string bacteria, string level, string date){
	if(rank == ind->getId().currentRank()){
		string dateST;
		
		if(level=="end"){
			ind->supStatusPathogen(bacteria);
			// cout << " l'individus " << ind->getCalc_ident() << " this is the end on " << date << endl;
		}else{
			if(level=="incubation"){
				ind->addActualPositivePathogens(bacteria);
			}else if(level=="incubationWithContagion" || level=="asymptomatic" || level=="symptomatic" || level=="severity"){
				vector<string> pathInd = ind->getActualPositivePathogens();
				auto itF = find(pathInd.begin(), pathInd.end(), bacteria);
				if(itF==pathInd.end()){
					ind->addActualPositivePathogens(bacteria);
				}
			}
			
			
			// else if(level=="incubationWithContagion" && incubationPeriod=="none"){
				// ind->addActualPositivePathogens(bacteria);
			// }else if((level=="asymptomatic" || level=="symptomatic") && incubationPeriod=="none" && incubationPeriodWithContagion=="none"){
				// ind->addActualPositivePathogens(bacteria);
			// }
			
			if(level=="incubation"){
				// int incubationDuration = (int)randomExpo(stod(incubationPeriod), eng);
				// while(incubationDuration<1){
					// incubationDuration = (int)randomExpo(stod(incubationPeriod), eng);
				// }
				
				int incubationDuration = returnUnif(incubationPeriod);
				time_t finaldateST = stringToDate( date) + time_t((incubationDuration)*86400);
				dateST = dateToString(finaldateST).substr(0,10);
			}else if(level=="incubationWithContagion"){
				// int incubationWithContagionDuration = (int)randomExpo(stod(incubationPeriodWithContagion), eng);
				// while(incubationWithContagionDuration<1){
					// incubationWithContagionDuration = (int)randomExpo(stod(incubationPeriodWithContagion), eng);
				// }
				
				int incubationWithContagionDuration = returnUnif(incubationPeriodWithContagion);
				time_t finaldateST = stringToDate(date) + time_t((incubationWithContagionDuration)*86400);
				dateST = dateToString(finaldateST).substr(0,10);
			}else if(level=="asymptomatic" || level=="symptomatic" || level=="severity"){
				string endDateDuration;
				string type = durationByType ? ind->getStatus(): returnCat(ind);
				if(boolManDuration){
					endDateDuration = findDate(manualDuration);
				}else{
					endDateDuration = findDurationOfColonization(bacteria, date, type);
				}
				ind->addColonization(bacteria, endDateDuration);
				dateST = endDateDuration;
			}else if(level=="immunization"){
				time_t finaldateST;
				if(immunizationDuration=="Inf"){
					finaldateST = stringToDate(stopDate) + 86400;
				}else{
					LogNormalGenerator contenerNormal = Random::instance()->createLogNormalGenerator(stod(immunizationDuration), sqrt(stod(immunizationVAR)));
					double rndNb = contenerNormal.next();
					int immunizationDuration = (int)rndNb;
					while(immunizationDuration<1){
						rndNb = contenerNormal.next();
						immunizationDuration = (int)rndNb;
					}
					finaldateST = stringToDate( date) + time_t((immunizationDuration)*86400);
				}
				
				dateST = dateToString(finaldateST).substr(0,10);
				// cout << " l'individus " << ind->getCalc_ident() << " immunization on " << date << " jusqu'au " << dateST << endl;
			}
			ind->changePathogensStatus(bacteria, level, dateST);
		}
	}
}

bool Model::checkCdtAdm(Individual* ind){
	bool res = false;
	bool atAdm = false;
	bool goodCat = false;
	//on verif l'admission si besoin
	if(ind->getStatus()=="PA" && (!InitializedPatientAtAdmission || (InitializedPatientAtAdmission && ind->getDt_in() == startDate.substr(0,10)))){
		atAdm = true;
	}else if(ind->getStatus()=="PE" && (!InitializedStaffAtAdmission || (InitializedStaffAtAdmission && ind->getDt_in() == startDate.substr(0,10)))){
		atAdm = true;
	}else{
		atAdm = false;
	}
	if(atAdm){
		//on verif la bonne cat
		if((ind->getStatus()=="PA" && stringHospStart=="none") || (ind->getStatus()=="PE" && stringCatStart=="none")){
			goodCat = true;
		}else{
			if((ind->getStatus()=="PA" && stringHospStart!="none")){
				Patient* p = dynamic_cast<Patient*>(ind);
				auto it = find(hospStartV.begin(), hospStartV.end(), p->getHosp_flag());
				if(it!=hospStartV.end()){
					goodCat = true;
				}
			}else if((ind->getStatus()=="PE" && stringCatStart!="none")){
				Staff* s = dynamic_cast<Staff*>(ind);
				auto it = find(catStartV.begin(), catStartV.end(), s->getCat());
				if(it!=catStartV.end()){
					goodCat = true;
				}
			}else{
				cout << " petit soucis dans les conditions d'admission avec categorie " << endl;
			}
		}
		if(goodCat){
			res = true;
		}else{
			res = false;
		}
		
	}else{
		res = false;
	}
	return res;
}

vector<string> Model::findChosenOne(int nbOfPeople, string type, string bacteria){
	boost::mpi::communicator* comm = repast::RepastProcess::instance()->getCommunicator();
	
	vector<Organism*> individualsContext;
	context.selectAgents(context.size(),individualsContext,0);
	vector<Individual*> individualsType;
	bool cdtAdm = false;
	for(auto const& elem:individualsContext){
		Individual* ind = dynamic_cast<Individual*>(elem);
		
		//on verif qu'il rempli les conditions pour être le premier
		cdtAdm = checkCdtAdm(ind);
		
		if((type=="both" && cdtAdm) || (type=="PA" && ind->getStatus()=="PA" && cdtAdm) || (type=="PE" && ind->getStatus()=="PE" && cdtAdm)){
			individualsType.push_back(ind);
		}
	}
	vector<string> choosingInd;
	if(!individualsType.empty()){
		IntUniformGenerator chooseProbaContener = Random::instance()->createUniIntGenerator(0,individualsType.size()-1);
		
		for(int i(0); i<nbOfPeople; i++){
			string theChosenOne;
			if(rank==0){
				int randomNb;
				randomNb = chooseProbaContener.next();
				Individual* ind = dynamic_cast<Individual*>(individualsType.at(randomNb));
				vector<string>::iterator itC = find(choosingInd.begin(), choosingInd.end(), ind->getCalc_ident());
				vector<string>::iterator itBC = find(ind->getActualPositivePathogens().begin(), ind->getActualPositivePathogens().end(), bacteria);
				while(itC!=choosingInd.end() || itBC!=ind->getActualPositivePathogens().end()){
					randomNb = chooseProbaContener.next();
					ind = dynamic_cast<Individual*>(individualsType.at(randomNb));
					itBC = find(ind->getActualPositivePathogens().begin(), ind->getActualPositivePathogens().end(), bacteria);
					itC = find(choosingInd.begin(), choosingInd.end(), ind->getCalc_ident());
				}
				theChosenOne = ind->getCalc_ident();
			}
			boost::mpi::broadcast(*comm,theChosenOne,0);
			choosingInd.push_back(theChosenOne);
		}
	}else{
		cout << " oooops individualsType de findChosenOne est empty " << endl;
	}
	
	return choosingInd;
}

void Model::chooseIndForSimu(int nbOfInd, int nbOfPatient, int nbOfStaff){
	/* Fonction qui choisit nbOfPeople individus au hasard et qui attribut des bactéries parmis celle qui sont à analyser */
	boost::mpi::communicator* comm = repast::RepastProcess::instance()->getCommunicator();
	vector<Organism*> pathogensContext;
	context.selectAgents(context.size(),pathogensContext,1);
	vector<Organism*> individualsContext;
	context.selectAgents(context.size(),individualsContext,0);
	for(auto const& bacteria:allBacteriaToAnalyzed){
		vector<Organism*>::iterator iter2 = find_if(pathogensContext.begin(),pathogensContext.end(), [bacteria](Organism* m) -> bool {Bacteria* m1 = dynamic_cast<Bacteria*>(m); return m1->getStringId() == bacteria; });
		if(iter2!=pathogensContext.end()){
			Bacteria* bacteriaObject = dynamic_cast<Bacteria*>(*iter2);
			vector<string> peopleToFind;
			vector<string> staffToFind;
			if(nbOfInd!=0){
				bool equiProb = props->getProperty("nbOfInitializedIndividualEquiprob") == "yes" ? true : false;
				if(!equiProb){
					peopleToFind = findChosenOne(nbOfInd,"both",bacteria);
					nbOfPatient = 0;
					nbOfStaff = 0;
				}else{
					string typeChosen;
					if(rank==0){
						IntUniformGenerator chooseProbaContener = Random::instance()->createUniIntGenerator(1,2);
						int nb = chooseProbaContener.next();
						typeChosen = nb == 1 ? "PA":"PE";
						
					}
					boost::mpi::broadcast(*comm,typeChosen,0);
					if(typeChosen=="PA"){
						nbOfPatient = 1;
						nbOfStaff = 0;
					}else{
						nbOfPatient = 0;
						nbOfStaff = 1;
					}
				}
				
			}
			if(nbOfPatient!=0)peopleToFind = findChosenOne(nbOfPatient,"PA",bacteria);
			if(nbOfStaff!=0)staffToFind = findChosenOne(nbOfStaff,"PE",bacteria);
			peopleToFind.insert( peopleToFind.end(), staffToFind.begin(), staffToFind.end() );
			for(auto const& theChosenOne:peopleToFind){
				// string theChosenOne;
				Individual* ind;
				vector<Organism*>::iterator iterContext = find_if(individualsContext.begin(),individualsContext.end(), [theChosenOne](Organism* m) -> bool {Individual* m1 = dynamic_cast<Individual*>(m); return m1->getCalc_ident() == theChosenOne; });
				if(iterContext!=individualsContext.end()){
					ind = dynamic_cast<Individual*>((*iterContext));
				}else{
					cout << rank << " oooops la il y a un pb " << endl;
				}
				// if(rank == ind->getId().currentRank()){
					// ind->addActualPositivePathogens(bacteriaObject->getStringId());
					// string endDateDuration;
					// string type = durationByType ? ind->getStatus(): returnCat(ind);
					
					// if(boolManDuration){
						// endDateDuration=findDate(manualDuration);
					// }else{
						// endDateDuration = findDurationOfColonization(bacteria, startDate.substr(0,10),type);
					// }
					// ind->addColonization(bacteria, endDateDuration);
					// string st = "";
					// string dateST;
					// if(incubationPeriod!="none"){
						// st="incubation";
						// time_t finaldateST = stringToDate(startDate.substr(0,10)) + time_t(stoi(incubationPeriod)*86400);
						// dateST = dateToString(finaldateST).substr(0,10);
					// }else{
						// st="symptomatic";
						// dateST=endDateDuration;
					// }
					// ind->changePathogensStatus(bacteria, st, dateST);
					
					
					// cout << " on ajoute à " << ind->getCalc_ident() << " la baterie " << bacteria << " pour " << endDateDuration << endl;
				// }
				//string level=findStartLevel(aleaLevel);
				string level = "symptomatic";
				changeIndividualPathogenStatus(ind, bacteriaObject->getStringId(), level, startDate.substr(0,10));
				
				
				if(rank == bacteriaObject->getId().currentRank()){
					bacteriaObject->addActualPositiveIndividuals(ind->getCalc_ident());
				}
			}
		}else{
			cout << " oooops pb " << endl;
		}		
	}
	synchInd();
	synchPath();
}

int Model::firstPathogen(){
	/* Fonction qui ajoute dans le context les pathogènes portés par les individus avant la simulation */
	if(debugMode)cout<< rank << " Fonction: firstPathogen " << " début de la fonction " << endl; 
	int idP = -1;
	multimap<string, vector <string>>::iterator it;
	vector <Organism*> currentPathogens;
	for(it= previousPrlvt.begin(); it!=previousPrlvt.end();it++){
		vector<string> t_prlvt = it->second;
		Bacteria* bacteria;
		string family_tempo = t_prlvt[3];
		string genre_tempo = t_prlvt[4];
		string species_tempo = t_prlvt[5];
		string spatype_tempo = t_prlvt[6];
		string BMR_tempo = t_prlvt[7];
		string atb_str = t_prlvt[8];
		string bacteriaToFind;
		bacteriaToFind = "Bacteria_";
		for(int i(3); i<t_prlvt.size(); i++){
			if(i!=(t_prlvt.size()-1)){
				bacteriaToFind = bacteriaToFind + t_prlvt[i] + "_";
			}else{
				bacteriaToFind = bacteriaToFind + t_prlvt[i];
			}
		}
		Phenotype p;
		if(family_tempo=="Enterobacteriaceae"){
			p = Phenotype(enteroATB,atb_str);
		}else if(family_tempo=="Staphylococcaceae"){
			p = Phenotype(staphATB,atb_str);
		}
		vector<Organism*>::iterator itPath = find_if(currentPathogens.begin(), currentPathogens.end(),[bacteriaToFind](Organism* v) -> bool {Bacteria* m1 = dynamic_cast<Bacteria*>(v); return m1->getStringId()==bacteriaToFind;});
			
		if(itPath==currentPathogens.end()){
			idP++;
			repast::AgentId id(idP, rank, 1); //obligé d'avoir un id en int pour les rank :(
			id.currentRank(rank);
			bacteria = new Bacteria(id,"Bacteria", family_tempo,genre_tempo,species_tempo,spatype_tempo,BMR_tempo,p);
			mapId[bacteria->getStringId()] = id;
			context.addAgent(bacteria);
			currentPathogens.push_back(bacteria);
			if(debugMode)cout<< rank << " Fonction: firstPathogen " << " ajout de la bacterie dans le context " << bacteria->getStringId() << endl;
		}
	}
	return(currentPathogens.size());
}

int Model::findTickDay(){
	/* Fonction qui renvoie le pas de temps */
	string units = props->getProperty("units");
	Unit valUnit;
	if(units == "sec"){
		valUnit = second;
	}
	if(units == "min"){
		valUnit = minute;
	}
	if(units == "hour"){
		valUnit = hour;
	}
	return(valUnit/tickPath);
}

int Model::findTimeResponse(){
	/* Fonction qui renvoie le pas de temps */
	string response = props->getProperty("response");
	TimeResponse valResponse;
	if(response == "month"){
		valResponse = month;
	}
	if(response == "week"){
		valResponse = week;
	}
	if(response == "day"){
		valResponse = day;
	}
	return(valResponse);
}

void Model::afficheIndividuals(){
	/* fonction test pour voir les individus qui sont dans le context */
	vector<Organism*> listeInd;
	context.selectAgents(context.size(),listeInd,0);
	std::vector<Organism*>::iterator it = listeInd.begin();
	cout << " le nombre d'individu est " << listeInd.size() << endl;
	while(it != listeInd.end()){
		Individual* ind = dynamic_cast<Individual*>((*it));
		cout << rank << " " << ind->getId() << " ";
		ind->affiche();
		it++;
	}
	cout << endl;
}

void Model::afficheColonized(){
	/* fonction test pour voir les individus qui sont dans le context */
	synchInd();
	synchPath();
	vector<Organism*> listeInd;
	context.selectAgents(context.size(),listeInd,0);
	std::vector<Organism*>::iterator it = listeInd.begin();
	if(rank==0){
		cout << " le nombre d'individu est " << listeInd.size() << endl;
		int c = 0;
		vector<string> truc;
		while(it != listeInd.end()){
			Individual* ind = dynamic_cast<Individual*>((*it));
			vector<string> vectorActualPath = ind->getActualPositivePathogens();
			for(auto const& pathogen:allBacteriaToAnalyzed){
				vector<string>::iterator itP = find(vectorActualPath.begin(), vectorActualPath.end(), pathogen);
				if(itP!=vectorActualPath.end()){
					cout << "L'individu " << ind->getCalc_ident() << " porte " << pathogen << " \n";
					truc.push_back(ind->getCalc_ident());
					c++;
				}
			}
			it++;
		}
		cout << " Au total il y a " << c << " individus colonisés " << endl;
		cout << endl;
		cout<< " Au total il y a comme ind colonisé : ";
		for(auto const& elem:truc){
			cout << elem << "_";
		}
		cout<<endl;
	}
}

void Model::afficheId(int typeP){
	/* Fonction qui renvoie les Id des Organisms */
	vector<Organism*> orga;
	context.selectAgents(context.size(),orga,typeP);
	cout << " Dans le rank "<< rank << " il y a ";
	for(vector<Organism*>::iterator it = orga.begin(); it!=orga.end(); it++){
		cout << (*it)->getId() << " ";
	}
	cout<<endl;
}

void Model::testLocalNonLocal(){
	/* Fonction qui affiche les individus présent en local (dans le rank) et en non local (les copies dans les autres ranks) */
	if(repast::RepastProcess::instance()->rank() == 0) std::cout << " TICK " << repast::RepastProcess::instance()->getScheduleRunner().currentTick() << std::endl;
	
    if(repast::RepastProcess::instance()->rank() == 0){
		repast::SharedContext<Organism>::const_state_aware_bytype_iterator local_agents_iter      = context.byTypeBegin(repast::SharedContext<Organism>::LOCAL,0);
		repast::SharedContext<Organism>::const_state_aware_bytype_iterator local_agents_end       = context.byTypeEnd(repast::SharedContext<Organism>::LOCAL,0);
		while(local_agents_iter != local_agents_end){
			Organism* agent = (&**local_agents_iter);
			Individual* ind = dynamic_cast<Individual*>(agent);
			cout << rank << " " << ind->getId() << " ";
			ind->affiche();
			local_agents_iter++;
		}
		repast::SharedContext<Organism>::const_state_aware_bytype_iterator non_local_agents_iter  = context.byTypeBegin(repast::SharedContext<Organism>::NON_LOCAL,0);
		repast::SharedContext<Organism>::const_state_aware_bytype_iterator non_local_agents_end   = context.byTypeEnd(repast::SharedContext<Organism>::NON_LOCAL,0);
		while(non_local_agents_iter != non_local_agents_end){
			Organism* agent = (&**non_local_agents_iter);
			Individual* ind = dynamic_cast<Individual*>(agent);
			cout << rank << " " << ind->getId() << " ";
			ind->affiche();
			non_local_agents_iter++;
		}
    }
}

void Model::affichePathogens(){
	/* fonction test pour voir les pathogenes qui sont dans le context */
	vector<Organism*> listePath;
	context.selectAgents(context.size(),listePath,1);
	std::vector<Organism*>::iterator it = listePath.begin();
	while(it != listePath.end()){
		Bacteria* bact = dynamic_cast<Bacteria*>((*it));
		cout << bact->getId() << " ";
		bact->affiche();
		it++;
	}
	cout << endl;
}

void Model::requestOrganism(vector<int> prev, int typeId, string setId){
	/* Fonction qui permet de requerir les agents qui sont dans les autres process. Fait une copie des autres agents dans le process */
	
	boost::mpi::communicator* comm = repast::RepastProcess::instance()->getCommunicator();
	int worldSize = RepastProcess::instance()->worldSize();
	vector <int> act;
	if(typeId == 0){
		act = indCounts;
	}else if(typeId == 1){
		act = pathCounts;
	}
	AgentRequest request(rank);
	for(int i=0;i<worldSize;i++){
		if(i!=rank){
			if(act[i]==-1){
				continue;
			}
			for(int j=(prev[i]+1);j<=act[i];j++){
				repast::AgentId id = AgentId(j,i,typeId);
				if(!request.contains(id)){
					request.addRequest(id);
				}
			}
		}	
	}
	if(typeId == 0){
		repast::RepastProcess::instance()->requestAgents<Organism, IndividualPackage, OrganismPackageProvider, OrganismPackageReceiver>(context,request, *provider, *receiver, *receiver, setId);
	}else if(typeId == 1){
		repast::RepastProcess::instance()->requestAgents<Organism, BacteriaPackage, OrganismPackageProvider, OrganismPackageReceiver>(context,request, *provider, *receiver, *receiver, setId);
	}
}

string Model::currentDate(){
	/* Fonction qui retourne la date actuelle */
	double t = repast::RepastProcess::instance()->getScheduleRunner().currentTick();
	char buffer [22];	
	struct tm tmS = {0};
	const char *startDateChar = startDate.c_str();
	strptime(startDateChar, "%Y-%m-%d%t%T", &tmS);
	time_t tim = mktime(&tmS) + (time_t)(t*tickPath);
	strftime(buffer,22,"%Y-%m-%d %T",localtime(&tim));
	string str_buffer = buffer;
	return (str_buffer);
}

time_t Model::stringToDate(string st){
	/* Fonction qui prend un string et qui retourne une date. Si le string n'a pas d'heure, il lui en attribut */
	struct tm tmSt = {0};
	if(st.size()<11){
		st = st + string(" 00:00:00");
	}
	const char *stDateChar = st.c_str();
	strptime(stDateChar, "%Y-%m-%d%t%T", &tmSt);
	time_t timSt = mktime(&tmSt);
	return(timSt);
}

string Model::dateToString(time_t tm){
	/* Fonction qui à partir d'une date retourne un string */
	char buffer [22];
	strftime(buffer,22,"%Y-%m-%d %T",localtime(&tm));
	string str_buffer = buffer;
	return (str_buffer);
}

time_t Model::stringToDateWithHour(string st){
	/* Fonction qui donne une date avec des heures à partir d'un string sans heures */
	struct tm tm_prlvt = {0};
	string tempo_prlvtString = Filetreatment::checkDate(st) + " 00:00:00";
	const char *tempo_prlvtChar = tempo_prlvtString.c_str();
	strptime(tempo_prlvtChar, "%Y-%m-%d%t%T", &tm_prlvt);
	time_t timPrlvt = mktime(&tm_prlvt);
	return(timPrlvt);
}

void Model::checkInside(string name, string typeOrga){
	/* Vérifie si un organisme de type typeOrga est bien dans le context */
	try{
		string s;
		if(typeOrga=="Individuals"){
			vector<Organism*> individuals;
			context.selectAgents(context.size(),individuals,0);
			std::vector<Organism*>::iterator iter = find_if(individuals.begin(),individuals.end(), [name](Organism* m) -> bool {Individual* m1 = dynamic_cast<Individual*>(m); return m1->getCalc_ident() == name; });
			if(iter==individuals.end()){
				s = " ooops " + name + " n'est plus dans l'hôpital! ";
				throw s;
			}
		}else if(typeOrga=="Pathogens"){
			vector<Organism*> pathogens;
			context.selectAgents(context.size(),pathogens,1);
			std::vector<Organism*>::iterator iter2 = find_if(pathogens.begin(),pathogens.end(), [name](Organism* m) -> bool {Bacteria* m1 = dynamic_cast<Bacteria*>(m); return m1->getStringId() == name; });
			if(iter2==pathogens.end()){
				s = " ooops " + name + " n'est plus dans l'hôpital! ";
				throw s;
			}
		}else{
			s = " le type dans la fonction checkInside n'est pas bien renseigné ";
				throw s;
		}
	}catch(string const& chaine){
		cerr<< chaine << endl;
	}
}

void Model::checkContact(){
	/* La fonction par excellence qui attribut des contacts grâce aux données de contact à chaque pas de temps et qui permet aussi la transmission dans la partie simulation */

	if(ifs_ctc.is_open()){
		string date = currentDate();
		if(tempo_ctc[2] == date){
			string lineCtc;
			// vector<Organism*> individuals;
			// context.selectAgents(context.size(),individuals,0);
			do{
				string fromCtc = tempo_ctc[0];
				string toCtc = tempo_ctc[1];
				Individual* source = nullptr;
				Individual* target = nullptr;
				source = dynamic_cast<Individual*>(context.getAgent(mapId[fromCtc]));
				target = dynamic_cast<Individual*>(context.getAgent(mapId[toCtc]));
				string s;
				if(source == 0){
					s = " l'individus " + fromCtc + " n'est pas dans l'hôpital ";
					cout << s << endl;
				}
				if(target == 0){
					s = " l'individus " + toCtc + " n'est pas dans l'hôpital ";
					cout << s << endl;
				}
				if(rank == source->getId().currentRank()){
					organismNetwork->addEdge(source,target);
					//attention avec ce cout --> affiche tous les contacts 
					// cout<< " ----> CONTACT <---- " <<source->getCalc_ident()<< " et "<<target->getCalc_ident()<< " pendant "<< tempo_ctc[3]<< " " <<tempo_ctc[2]<< " rank: "<< rank <<endl;
				}
				if(gameStatus=="simu"){
					
					if(fixedColonization && !chosenFixedColonizedStaffThisWeek.empty()){
						auto it1 = find(chosenFixedColonizedStaffThisWeek.begin(), chosenFixedColonizedStaffThisWeek.end(), source->getCalc_ident());
						auto it2 = find(chosenFixedColonizedStaffThisWeek.begin(), chosenFixedColonizedStaffThisWeek.end(), target->getCalc_ident());
						if(it1==chosenFixedColonizedStaffThisWeek.end() && it2==chosenFixedColonizedStaffThisWeek.end()){
							transmissionByContact(source, target, tempo_ctc[3]);
						}else if(it1!=chosenFixedColonizedStaffThisWeek.end() && it2==chosenFixedColonizedStaffThisWeek.end()){
							for(auto const& [key, val]:chosenFixedColonizedStaff){
								vector<string> valST = val;
								auto itv = find(valST.begin(), valST.end(), source->getCalc_ident());
								if(itv!=valST.end() && stringToDate(key)<stringToDate(currentDate().substr(0,10))){
									// cout << source->getCalc_ident() << " est dedans " << key << " et " << currentDate().substr(0,10) << endl;
									transmissionByContact(source, target, tempo_ctc[3]);
								}
							}
						}else if(it1==chosenFixedColonizedStaffThisWeek.end() && it2!=chosenFixedColonizedStaffThisWeek.end()){
							for(auto const& [key, val]:chosenFixedColonizedStaff){
								vector<string> valST = val;
								auto itv = find(valST.begin(), valST.end(), target->getCalc_ident());
								if(itv!=valST.end() && stringToDate(key)<stringToDate(currentDate().substr(0,10))){
									transmissionByContact(source, target, tempo_ctc[3]);
								}
							}
						}
					}else{
						transmissionByContact(source, target, tempo_ctc[3]);
					}
					
				}
				getline(ifs_ctc, lineCtc);					
				if(ifs_ctc.eof()){
					ifs_ctc.close();
					break;

				}else{
					boost::trim_if(lineCtc, boost::is_any_of("\r\n|\r|\n|\t"));
					boost::split(tempo_ctc, lineCtc, boost::is_any_of(";"));
				}
			}while(tempo_ctc[2] == date);
		}
	}	
}

void Model::addManuportage(Individual* ind){
	double compliance;
	double manuportageProbability;
	if(ind->getStatus()=="PA"){
		compliance = stod(props->getProperty("PAcompliance"));
		manuportageProbability = stod(props->getProperty("PAmanuportage"));
	}else{
		compliance = stod(props->getProperty("PEcompliance"));
		manuportageProbability = stod(props->getProperty("PEmanuportage"));
	}
	if(rank==ind->getId().currentRank() && compliance!=1.0 && manuportageProbability!=0.0){
		string dateManuSTR = ind->getManuportage();
		bool manuOnGoing = false;
		if(dateManuSTR!=""){
			time_t dateManuTIME = stringToDate(dateManuSTR);
			if(stringToDate(currentDate()) < dateManuTIME){
				manuOnGoing = true;
			}
		}
		double randomNb = findRandomNb();
		if(randomNb < (manuportageProbability*(1-compliance)) && !manuOnGoing){
			//on va lui attribuer une date de fin de manuportage
			int durationM = stoi(props->getProperty("durationManuportage"));
			time_t endDuration = stringToDate(currentDate()) + time_t(durationM);
			string endM = dateToString(endDuration);
			ind->setManuportage(endM);
			// cout << " Manuportage de " << ind->getCalc_ident() << "  jusqu'au " << endM << " on est le " << currentDate() << endl;
		}
	}

}

void Model::transmissionByContact(Individual* source, Individual* target, string length){
	/* Fonction qui test s'il y a transmission entre source et target. Utilise les données de parameters pour connaitres la transmission à chaque contact. Pour le moment la proba de transmission est la même quelque soit la durée du contact mais il faudra modifier ça par la suite */
	boost::mpi::communicator* comm = repast::RepastProcess::instance()->getCommunicator();
	double proba;
	double randomNb;
	string date = currentDate().substr(0,10);
	// map<string, map<string, double>> mapProb = parameters->getMapProbabilityOfTransmission();
	// map<string, map<string, vector<double>>> mapProb = parameters->getMapProbabilityOfTransmission2();
	vector<string> positifSource = source->getActualPositivePathogens();
	vector<string> positifTarget = target->getActualPositivePathogens();
	vector<Organism*> PathogensContext;
	context.selectAgents(context.size(),PathogensContext,1);
	for(auto const& bacteriaToFind:allBacteriaToAnalyzed){
		Bacteria* bacteriaToFindObject;
		map<string, AgentId>::iterator itB = mapId.find(bacteriaToFind);
		if(itB!=mapId.end()){
			//la bacterie existe et est dans le context
			bacteriaToFindObject = dynamic_cast<Bacteria*>(context.getAgent(mapId[bacteriaToFind]));
			map<string, double> mapProbBacteria = mapProb[bacteriaToFind];
			// map<string, vector<double>> mapProbBacteria = mapProb[bacteriaToFind];
			bool boolSource;
			bool boolTarget;
			// bool boolSourceCanTransmit = true;
			// bool boolTargetCanTransmit = true;
			// bool boolSourceCanReceive = true;
			// bool boolTargetCanReceive = true;
			bool boolSourceCanTransmit;
			bool boolTargetCanTransmit;
			bool boolSourceCanReceive;
			bool boolTargetCanReceive;
			string statusSource;
			string statusTarget;
			
	
			if(rank == source->getId().currentRank()){
				//peut transmettre selon son status infectieux
				vector<string>::iterator itSource = find(positifSource.begin(),positifSource.end(),bacteriaToFind);
				statusSource = source->getPathogensStatus()[bacteriaToFind].first;
				if(itSource!=positifSource.end()){
					//il a la bacterie
					boolSource = true;
					//si il a la bacterie il ne peut pas la recevoir
					boolSourceCanReceive = false;
					
					if(statusSource=="incubation"){
						boolSourceCanTransmit = true;
					}else{
						boolSourceCanTransmit = true;
					}
				}else{
					boolSource = false;
					//si il n'a pas la bacterie il ne peut pas la transmettre
					boolSourceCanTransmit = false;
					
					if(statusSource=="immunization"){
						boolSourceCanReceive = true;
					}else{
						boolSourceCanReceive = true;
					}
				}
				
				//peut transmettre selon son status manuportage
				string sourceManuportage = source->getManuportage();
				if(sourceManuportage!=""&& target->getStatus()=="PA"){
					time_t sourceManuportageTIME = stringToDate(sourceManuportage);
					if(stringToDate(currentDate()) < sourceManuportageTIME){
						boolSource = true;
						boolSourceCanTransmit = true;
						// cout << "hey mais " << source->getCalc_ident() << " peut transmettre à "<< target->getCalc_ident() << " alors que " << statusSource << endl;
					}
				}
			}
			if(rank == target->getId().currentRank()){
				//peut transmettre selon son status infectieux
				vector<string>::iterator itTarget = find(positifTarget.begin(),positifTarget.end(),bacteriaToFind);
				statusTarget = target->getPathogensStatus()[bacteriaToFind].first;
				if(itTarget!=positifTarget.end()){
					//il a la bacterie
					boolTarget = true;
					//si il a la bacterie il ne peut pas la recevoir (il l'a deja)
					boolTargetCanReceive = false;
					
					if(statusTarget=="incubation"){
						boolTargetCanTransmit = true;
					}else{
						boolTargetCanTransmit = true;
					}
				}else{
					boolTarget = false;
					//si il n'a pas la bacterie il ne peut pas la transmettre
					boolTargetCanTransmit = false;
					
					if(statusTarget=="immunization"){
						boolTargetCanReceive = true;
					}else{
						boolTargetCanReceive = true;
					}
				}
				//peut transmettre selon son status manuportage (uniquement si l'autre est un patient)
				string targetManuportage = target->getManuportage();
				if(targetManuportage!="" && source->getStatus()=="PA"){
					time_t targetManuportageTIME = stringToDate(targetManuportage);
					if(stringToDate(currentDate()) < targetManuportageTIME){
						boolTarget = true;
						boolTargetCanTransmit = true;
						// cout << "hey mais " << target->getCalc_ident() << " peut transmettre à " << source->getCalc_ident() << " alors que " << statusTarget << endl;
					}
				}
			}
			boost::mpi::broadcast(*comm,boolSource,source->getId().currentRank());
			boost::mpi::broadcast(*comm,boolTarget,target->getId().currentRank());
			boost::mpi::broadcast(*comm,boolSourceCanTransmit,source->getId().currentRank());
			boost::mpi::broadcast(*comm,boolTargetCanTransmit,target->getId().currentRank());
			boost::mpi::broadcast(*comm,boolSourceCanReceive,source->getId().currentRank());
			boost::mpi::broadcast(*comm,boolTargetCanReceive,target->getId().currentRank());
			boost::mpi::broadcast(*comm,statusSource,source->getId().currentRank());
			boost::mpi::broadcast(*comm,statusTarget,target->getId().currentRank());
			
			//manuportage
			int durationMinManuportage = stoi(props->getProperty("durationMinManuportage"));
			int durationCtc = stoi(length);
			if(durationCtc > durationMinManuportage){
				if((boolTargetCanTransmit && target->getStatus()=="PA" && source->getStatus()=="PE")){
					//target est un patient qui peut transmettre à source qui est un PE qui ne peut pas transmettre donc qui n'est pas symptomatique/asympto/severe
					addManuportage(source);
				}else if((boolSourceCanTransmit && source->getStatus()=="PA" && target->getStatus()=="PE" )){
					//source est un patient qui peut transmettre à targer qui est un PE qui ne peut pas transmettre donc qui n'est pas symptomatique/asympto/severe
					addManuportage(target);
				}
			}

			if(boolSource && boolTarget){
				//les deux ont déjà le pathogen donc on va modifier la durée de colo??
			}else if ( ( (boolSource && boolSourceCanTransmit) && (!boolTarget && boolTargetCanReceive)) || ((!boolSource && boolSourceCanReceive) && (boolTarget && boolTargetCanTransmit) ) ){
				Individual* transmitter;
				Individual* receiver;
				if(boolSource && !boolTarget){
					//la source l'a mais pas la target donc on va faire une transmission source-->target
					transmitter = source;
					receiver = target;
				}else if(!boolSource && boolTarget){
					transmitter = target;
					receiver = source;
				}
				proba = findProba(transmitter, receiver, mapProbBacteria);
				// proba = findProba2(transmitter, receiver, mapProbBacteria);
				if(rank==0){
					randomNb = findRandomNb();
				}
				boost::mpi::broadcast(*comm,randomNb,0);
				string couple = transmitter->getStatus() + "-" + receiver->getStatus();
				// if((logPAPA && couple=="PA-PA") || (logPAPE && couple=="PA-PE") || (logPEPA && couple=="PE-PA") || (logPEPE && couple=="PE-PE")){
					// proba = proba + proba*log10(stod(length)/30);
				// }else{
					if(probaThreshold!="none"){
						double durationCtcForProb = stod(length);
						double probaThresholdDouble = stod(probaThreshold);
						double modifiedLength;
						if(durationCtcForProb>probaThresholdDouble){
							modifiedLength = probaThresholdDouble;
						}else{
							modifiedLength = durationCtcForProb;
						}
						// cout << durationCtcForProb << " and " << modifiedLength << endl;
						proba = proba*(modifiedLength/30);
					}else{
						proba = proba*(stod(length)/30);
					}
					
				// }
				if(interventions){
					if(compliance){
						if(couple=="PA-PE" || couple=="PE-PA"){
							if(!complianceCouple.empty()){
								string catT = returnCat(transmitter);
								string catR = returnCat(receiver);
								pair<string, string> coupleTR = make_pair(catT, catR);
								pair<string, string> coupleRT = make_pair(catR, catT);
								auto itComplianceTR = complianceCouple.find(coupleTR);
								auto itComplianceRT = complianceCouple.find(coupleRT);
								if(itComplianceTR!=complianceCouple.end()){
									proba = proba/complianceCouple[coupleTR];
								} else if(itComplianceRT!=complianceCouple.end()){
									proba = proba/complianceCouple[coupleRT];
								}
							}
							if(!superContractors.empty()){
								auto itSCt = superContractors.find(transmitter->getCalc_ident());
								auto itSCr = superContractors.find(receiver->getCalc_ident());
					
								if(itSCt!=superContractors.end()){
									// cout << transmitter->getCalc_ident() << " et " << receiver->getCalc_ident() << " proba avant " << proba << endl;
									proba = proba/superContractors[transmitter->getCalc_ident()];
									// cout<< transmitter->getCalc_ident() << " et " << receiver->getCalc_ident() << " proba après " << proba << endl;
								}else if(itSCr!=superContractors.end()){
									// cout << transmitter->getCalc_ident() << " et " << receiver->getCalc_ident() << " proba avant " << proba << endl;
									proba = proba/superContractors[receiver->getCalc_ident()];
									// cout<< " inverse " << transmitter->getCalc_ident() << " et " << receiver->getCalc_ident() << " proba après " << proba << endl;
								}
							}
						}
					}
					//vaccination intevention. If by cat, just searches for cat in compliance couple map. If SC, searches for receiver in SC, protect if find
					if(vaccination){
						if(!complianceCouple.empty()){
							string catR = returnCat(receiver);
							string catT = "Patient";
							pair<string, string> coupleRT = make_pair(catR, catT);
							auto itcatR = complianceCouple.find(coupleRT);
							if(itcatR!=complianceCouple.end()){
								proba = proba/complianceCouple[coupleRT];
							}
						}
						if(!superContractors.empty()){
							auto itSCr = superContractors.find(receiver->getCalc_ident());
							if(itSCr!=superContractors.end()){
								// cout << transmitter->getCalc_ident() << " et " << receiver->getCalc_ident() << " proba avant " << proba << endl;
								proba = proba/superContractors[receiver->getCalc_ident()];
								// cout<< " inverse " << transmitter->getCalc_ident() << " et " << receiver->getCalc_ident() << " proba après " << proba << endl;
							}
						}
					}
				}

				if(randomNb < proba){
					// if(rank == receiver->getId().currentRank()){						
						// receiver->addActualPositivePathogens(bacteriaToFindObject->getStringId());
						// string durationOfCol;
						// string type = durationByType ? receiver->getStatus(): returnCat(receiver);
						// if(boolManDuration){
							// durationOfCol=findDate(manualDuration);
						// }else{
							// durationOfCol = findDurationOfColonization(bacteriaToFind, date, type);
						// }
						// receiver->addColonization(bacteriaToFind, durationOfCol);
						// string st = "";
						// string dateST;
						// if(incubationPeriod!="none"){
							// st="incubation";
							// time_t finaldateST = stringToDate(date) + time_t(stoi(incubationPeriod)*86400);
							// dateST = dateToString(finaldateST).substr(0,10);
						// }else{
							// st="symptomatic";
							// dateST=durationOfCol;
						// }
						// receiver->changePathogensStatus(bacteriaToFind, st, dateST);
					// }
					//string level = findStartLevel(false);
					string level = "symptomatic";
					changeIndividualPathogenStatus(receiver, bacteriaToFindObject->getStringId(), level, date);
					// cout << " on est le " << currentDate() << " et " << receiver->getCalc_ident() << " recoit le virus en incubation " << endl;
					
					auto it = index_second_cases.find(transmitter->getCalc_ident());
					if(it!=index_second_cases.end()){
						index_second_cases[transmitter->getCalc_ident()][bacteriaToFindObject->getStringId()][currentDate().substr(0,10)].push_back(receiver->getCalc_ident());
					}
					
					if(rank == bacteriaToFindObject->getId().currentRank()){
						bacteriaToFindObject->addIndividual(receiver->getCalc_ident(), date);
					}
				}
			} 
		}	
	}
}

double Model::findProba(Individual* transmitter, Individual* receiver, map<string, double> mapProbBacteria){
	/* Fonction qui trouve la probabilité de transmission provenant de mapProbBacteria ente transmitter et receiver */
	double proba = 0;
	string catTransmitter = returnCat(transmitter);
	string catReceiver = returnCat(receiver);
	if(probaByType){
		catTransmitter = transmitter->getStatus();
		catReceiver = receiver->getStatus();
	}else{
		string catT = returnCat(transmitter);
		string catR = returnCat(receiver);
		catTransmitter = groups[catT];
		catReceiver = groups[catR];
	}
	string twoCat = catTransmitter + "-" + catReceiver;
	map<string, double>::iterator itFindCat = mapProbBacteria.find(twoCat);
	
	if(itFindCat!=mapProbBacteria.end()){
		proba = mapProbBacteria[twoCat];
	}
	return proba;
}

double Model::findProba2(Individual* transmitter, Individual* receiver, map<string, vector<double>> mapProbBacteria){
	/* Fonction qui trouve la probabilité de transmission provenant de mapProbBacteria ente transmitter et receiver */
	double proba = 0;
	string catTransmitter = returnCat(transmitter);
	string catReceiver = returnCat(receiver);
	if(probaByType){
		catTransmitter = transmitter->getStatus();
		catReceiver = receiver->getStatus();
	}else{
		string catT = returnCat(transmitter);
		string catR = returnCat(receiver);
		catTransmitter = groups[catT];
		catReceiver = groups[catR];
	}
	string twoCat = catTransmitter + "-" + catReceiver;
	map<string, vector<double>>::iterator itFindCat = mapProbBacteria.find(twoCat);
	
	if(itFindCat!=mapProbBacteria.end()){
		vector<double> vproba = mapProbBacteria[twoCat];
		// IntUniformGenerator chooseProbaContener = Random::instance()->createUniIntGenerator(0,vproba.size());
		DoubleUniformGenerator chooseProbaContener = Random::instance()->createUniDoubleGenerator(vproba[0],vproba[1]);
		// proba = vproba[chooseProbaContener.next()];
		proba = chooseProbaContener.next();
	}
	return proba;
}

double Model::findRandomNb(){
	double randomNb;
	DoubleUniformGenerator chooseProbaContener = Random::instance()->createUniDoubleGenerator(0,1);
	randomNb = chooseProbaContener.next();
	return randomNb;
}

string Model::findDate(int const& nbOfDay){
	/* Fonction qui trouve une date à partir du nombre de jour nbOfDay */
	string res;
	time_t finalDuration = stringToDate(currentDate().substr(0,10)) + time_t(nbOfDay*86400);
	res = dateToString(finalDuration).substr(0,10);
	return res;
}

string Model::findDurationOfColonization(string pathogen, string actualDate, string type){
	/* Fonction qui permet de déterminer la durée de colonization du pathogen chez un individus à partir de actualDate. Renvoie la date de fin de colonization. Prend des données de parameters pour trouver dans le vector min et max la valeur au hasard entre 25 et 75% des données. Retourne une date sans heure */
	string res;
	int duration;
	double mean;
	double variance;
	vector<pair<string, double>> vDuration;
	map<string, vector<pair<string, double>>> mapVDuration = parameters->getMapDuration()[pathogen];
	map<string, vector<pair<string, double>>>::iterator itFindCat = mapVDuration.find(type);
	if(itFindCat!=mapVDuration.end()){
		vDuration = mapVDuration[type];
	}else if(!durationByType){
		//pas géniale ça mais à voir dans l'avenir
		string ownT = type!= "Patient" ? "PE":"PA";
		double m =0;
		double v = 0;
		int count = 0;
		for(auto const& [key, val]:mapVDuration){
			string T = key!= "Patient" ? "PE":"PA";
			if(T==ownT){
				count = count + 1;
				vector<pair<string, double>> vectDuration = mapVDuration[key];
				m = m + vectDuration[0].second;
				v = v + vectDuration[1].second;
			}
		}
		vDuration.push_back(make_pair("mean", m/count));
		vDuration.push_back(make_pair("variance", v/count));
	}
	for(auto const& elem:vDuration){
		if(elem.first=="mean"){
			mean = elem.second;
		}else{
			variance = elem.second;
		}
	}
	double rndNb;
	if(distributionProb == "Gamma"){
		rndNb = randomGamma(mean, variance, eng);
	}else if(distributionProb == "Exponential"){
		rndNb = randomExpo(mean, eng);
	}else if(distributionProb=="Empiric distribution"){
		map<string, map<string, vector<double>>> mapDurationDistribution = parameters->getMapDistributionDuration();
		vector<double> distributionDuration = mapDurationDistribution[pathogen][type];
		IntUniformGenerator chooseProbaContener = Random::instance()->createUniIntGenerator(0,distributionDuration.size()-1);
		int randomIntUniform = chooseProbaContener.next();
		rndNb = distributionDuration[randomIntUniform];
	}else if(distributionProb == "LogNormal"){
		LogNormalGenerator contenerNormal = Random::instance()->createLogNormalGenerator(mean, sqrt(variance));
		rndNb = contenerNormal.next();
	}else if(distributionProb == "Normal"){
		NormalGenerator contenerNormal = Random::instance()->createNormalGenerator(mean, sqrt(variance));
		rndNb = contenerNormal.next();
	}
	duration = (int)rndNb;
	if(duration < 1 ){
		do{
			if(distributionProb == "Gamma"){
				rndNb = randomGamma(mean, variance, eng);
			}else if(distributionProb == "Exponential"){
				rndNb = randomExpo(mean, eng);
			}else if(distributionProb == "LogNormal"){
				LogNormalGenerator contenerNormal = Random::instance()->createLogNormalGenerator(mean,sqrt(variance));
				rndNb = contenerNormal.next();
			}else if(distributionProb == "Normal"){
				NormalGenerator contenerNormal = Random::instance()->createNormalGenerator(mean,sqrt(variance));
				rndNb = contenerNormal.next();
			}
			duration = (int)rndNb;
		}while(duration < 1 );
	}
	time_t finalDuration = stringToDate(actualDate.substr(0,10)) + time_t(duration*86400);
	res = dateToString(finalDuration).substr(0,10);
	return res;
}

double Model::randomGamma(double mean, double variance, boost::mt19937& rng){
    
	const double shape = ( mean*mean )/variance;
    double scale = variance/mean;
    boost::random::gamma_distribution<> gd( shape,scale );
    boost::variate_generator<boost::mt19937&,boost::random::gamma_distribution<> > var_gamma( rng, gd );
    return var_gamma();
}

double Model::randomExpo(double mean, boost::mt19937& rng){
    
    boost::random::exponential_distribution<> exp( 1/mean );
    boost::variate_generator<boost::mt19937&,boost::random::exponential_distribution<> > var_expo( rng, exp);
    return var_expo();
}

int Model::findQuantile(vector<int> inputVector, double q){
	/* Trouve le quantile d'un vector inputVector d'ordre q */
	int res;
	int size = inputVector.size();
	sort(inputVector.begin(), inputVector.end());
	int val = size*q;
	res = val % 2 ==0 ? (inputVector[val]+inputVector[val-1])/2 : inputVector[val];
	return res;
}

void Model::decolonization(){
	/* Fonction qui vérifie si les indvidus doivent se décoloniser */
	if(gameStatus=="simu"){
		boost::mpi::communicator* comm = repast::RepastProcess::instance()->getCommunicator();
		synchPath();
		synchInd();
		string date = currentDate().substr(0,10);
		vector<Organism*> individualsContext;
		context.selectAgents(context.size(),individualsContext,0);
		vector<Organism*> pathogensContext;
		context.selectAgents(context.size(),pathogensContext,1);
		for(auto const& organism:individualsContext){
			Individual* individual = dynamic_cast<Individual*>(organism);
			bool removeCol;
			map<string, string> mapColoIndividual = individual->getMapColonization();
			map<string, string>::iterator it = mapColoIndividual.begin();
			while((it=find_if(it, mapColoIndividual.end(), [date](pair<string, string> n)->bool {return n.second == date;})) != mapColoIndividual.end()){
				//il faut décoloniser l'individu
				string indName = individual->getCalc_ident();
				if(debugMode)cout<< rank << " Fonction: decolonization " << " décolo de " << individual->getCalc_ident() << " le " << currentDate().substr(0,10) << endl;
				string bacteriaToDecolo = (*it).first;
				Bacteria* bacteriaToDecoloObject;
				vector<Organism*>::iterator itOrga = find_if(pathogensContext.begin(), pathogensContext.end(), [bacteriaToDecolo](Organism* o)->bool {Bacteria* b = dynamic_cast<Bacteria*>(o); return b->getStringId()==bacteriaToDecolo;});
				if(itOrga==pathogensContext.end()){
					cout << " ooooooooooops pb dans decolonization la bactérie n'est pas dans le context :/ " << endl;
				}
				bacteriaToDecoloObject = dynamic_cast<Bacteria*>((*itOrga));
				if(rank == individual->getId().currentRank()){
					individual->supColonization(bacteriaToDecolo);
				}
				//il faut enlever l'ind de la bactérie
				if(rank == bacteriaToDecoloObject->getId().currentRank()){
					bacteriaToDecoloObject->removeIndFromActualPositiveIndividuals(indName);
				}
				it++;
			}
		}
		changePathogensStatusFromIndividuals();
	}
}

void Model::changePathogensStatusFromIndividuals(){
	if(gameStatus=="simu"){
		boost::mpi::communicator* comm = repast::RepastProcess::instance()->getCommunicator();
		synchPath();
		synchInd();
		string date = currentDate().substr(0,10);
		vector<Organism*> individualsContext;
		context.selectAgents(context.size(),individualsContext,0);
		for(auto const& organism:individualsContext){
			Individual* individual = dynamic_cast<Individual*>(organism);
			map<string, pair<string, string>> mapPS = individual->getPathogensStatus();
			map<string, string> mapColoIndividual = individual->getMapColonization();
			// cout << " l'ind " << individual->getCalc_ident() << " aujourd'hui " << currentDate() << " a :\n";
			// for(auto const& elem:mapColoIndividual){
				// cout << elem.first << " - " << elem.second << "\n";
			// }
			// for(auto const& elem:mapPS){
				// cout << elem.first << " - " << elem.second.first << " and " << elem.second.second << "\n";
			// }
			// cout << endl;
			
			map<string, pair<string, string>>::iterator it = mapPS.begin();
			while((it=find_if(it, mapPS.end(), [date](pair<string, pair<string, string>> n)->bool {return n.second.second == date;})) != mapPS.end()){
				//il faut changer le status
				string bacteria = (*it).first;
				string st = (*it).second.first;
				string datePS = (*it).second.second;
				string nextLevel;
				string dateCol = mapColoIndividual[bacteria];
				if(st=="incubation"){
					if(incubationPeriodWithContagion!="none"){
						nextLevel = "incubationWithContagion";
					}else if(asymptomatic!="0" && severity!="0"){
						nextLevel = "asymptomatic/symptomatic/severity";
					}else{
						nextLevel = "asymptomatic";
					}
				}else if(st=="incubationWithContagion"){
					if(asymptomatic!="0" && severity!="0"){
						nextLevel = "asymptomatic/symptomatic/severity";
					}else{
						nextLevel = "asymptomatic";
					}
				}else if((st=="asymptomatic" || st=="symptomatic" || st=="severity") && immunization){
					nextLevel = "immunization";
				}else if((st=="immunization" && immunization) || ( (st=="asymptomatic" || st=="symptomatic" || st=="severity") && !immunization) ){
					nextLevel = "end";
				}
				if(nextLevel=="asymptomatic/symptomatic/severity"){
					string level;
					if(rank==0){
						double seuil1 = stod(asymptomatic);
						double seuil2 = seuil1 + stod(severity);
						DoubleUniformGenerator chooseProbaContener = Random::instance()->createUniDoubleGenerator(0,1);
						double proba = chooseProbaContener.next();
						if(proba < seuil1){
							level = "symptomatic";
						}else if(proba >= seuil1 && proba < seuil2){
							level = "severity";
						}else{
							level = "symptomatic";
						}
					}
					boost::mpi::broadcast(*comm,level,0);
					nextLevel = level;
				}	
				// cout << individual->getCalc_ident() << " st " << st << " nextLevel " << nextLevel << " le " << date << endl;
				
				//ici on va mimer un remplacement de personnel quand il y a de la sévérité ou alors retour du personnel après maladie.
				if(individual->getStatus()=="PE" && nextLevel == "severity"){
					nextLevel = "immunization";
				}
				
				changeIndividualPathogenStatus(individual, bacteria, nextLevel, date);

				it++;
			}
			
		}
	}
}

void Model::cancelAgentRequests(){
	/* Arrête la synchronization avec le home process de l'agent copié*/
	if(rank==0)cout<< "CANCELING AGENT REQUESTS"<<endl;
	repast::AgentRequest req(rank);
	repast::SharedContext<Organism>:: const_state_aware_iterator non_local_agents_iter = context.begin(repast::SharedContext<Organism>::NON_LOCAL);
	repast::SharedContext<Organism>::const_state_aware_iterator non_local_agents_end   = context.end(repast::SharedContext<Organism>::NON_LOCAL);
	while(non_local_agents_iter != non_local_agents_end){
		req.addCancellation((*non_local_agents_iter)->getId());
		non_local_agents_iter++;
	}
	repast::RepastProcess::instance()->requestAgents<Organism, IndividualPackage, OrganismPackageProvider, OrganismPackageReceiver>(context, req, *provider, *receiver, *receiver);
	//ici ça l'enlève du process 
	vector<repast::AgentId> cancellations = req.cancellations();
	vector<repast::AgentId>::iterator idToRemove = cancellations.begin();
	while(idToRemove != cancellations.end()){
		context.importedAgentRemoved(*idToRemove);
		idToRemove++;
	}
}

void Model::testNetwork(){
	/* C'est une fonction qui permet de voir les successors dans un network */
	vector<Organism*> individuals;
	context.selectAgents(context.size(), individuals,0);
	for(vector<Organism*>::iterator iter = individuals.begin();iter!=individuals.end();iter++){
		vector<Organism*> out;
		organismNetwork->successors(*iter,out);
		for(int j = 0; j<out.size();j++){
			Individual* ind = dynamic_cast<Individual*>(*iter);
			Individual* ind_successor = dynamic_cast<Individual*>(out[j]);
			cout << " rank " << rank << " ind "<< ind->getCalc_ident() << " successors: " << ind_successor->getCalc_ident() << endl;
		}
	}
}

void Model::synchInd(){
	/* Fonction qui va synchroniser les informations des agents copiés vers les vrais agents */
	repast::RepastProcess::instance()->synchronizeAgentStates<IndividualPackage, OrganismPackageProvider, OrganismPackageReceiver>(*provider, *receiver, "SET_1");
}

void Model::synchPath(){
	/* Fonction qui va synchroniser les informations des agents copiés vers les vrais agents */
	repast::RepastProcess::instance()-> synchronizeAgentStates<BacteriaPackage, OrganismPackageProvider, OrganismPackageReceiver>(*provider, *receiver, "SET_2");
}

void Model::cleanIndividualSwab(){
	/* Fonction qui enlève les résultats des anciens prélèvements. Cette fonction est surtout utile si on prend en compte les données de prélèvement pour initialiser les simulations */
	vector<Organism*> listOfI;
	context.selectAgents(repast::SharedContext<Organism>::LOCAL,context.size(),listOfI,0);
	for(auto const& ind:listOfI){
		Individual* indObj = dynamic_cast<Individual*>(ind);
		if(!indObj->getPositivePathogens().empty()){
			indObj->removePositivePathogens();
		}
	}
}

vector<string> Model::findRandomInAList(int nb, vector<string> input_vector){
	vector<string> res;
	IntUniformGenerator chooseProbaContenerPA = Random::instance()->createUniIntGenerator(0,input_vector.size()-1);
	for(int i(0); i<nb; i++){
		int randomPA = chooseProbaContenerPA.next();
		string indC = input_vector[randomPA];
		auto itPA = find(res.begin(), res.end(), indC);
		while(itPA!=res.end()){
			randomPA = chooseProbaContenerPA.next();
			indC = input_vector[randomPA];
			itPA = find(res.begin(), res.end(), indC);
		}
		res.push_back(indC);
	}
	return(res);
}

void Model::initializeFirstImmunized(double percentagePA, double percentagePE){
	synchInd();
	boost::mpi::communicator* comm = repast::RepastProcess::instance()->getCommunicator();
	vector<Organism*> IndividualContext;
	context.selectAgents(context.size(),IndividualContext,0);
	vector<string> allChosen;
	map<string, vector<string>> individuals;
	
	if(rank==0){
		for(auto const& elem:IndividualContext){
			Individual* ind = dynamic_cast<Individual*>(elem);
			if(ind->getPathogensStatus().empty()){
				individuals[ind->getStatus()].push_back(ind->getCalc_ident());
			}
		}
		vector<string> vpa;
		vector<string> vpe;
		//selection des patients
		int nbPA = (int)(percentagePA * individuals["PA"].size());
		int nbPE = (int)(percentagePE * individuals["PE"].size());
		if(nbPA!=0){
			vpa = findRandomInAList(nbPA, individuals["PA"]);
		}
		if(nbPE!=0){
			vpe = findRandomInAList(nbPE, individuals["PE"]);
		}
		// cout << " la taille vpa est " << vpa.size() << endl;
		//selection des staff
		
		
		vpa.insert(vpa.end(), vpe.begin(), vpe.end());
		
		allChosen = vpa;
		// cout << " la taille allChosen est " << allChosen.size() << endl;
		// cout << " le nombre de PA PE " << nbPA << " / " << individuals["PA"].size() << " et " << nbPE << " / " << individuals["PE"].size() << endl;
	}
	
	boost::mpi::broadcast(*comm, allChosen, 0);
	// cout << " au final il y a " << allChosen.size() << endl;
	int nbT = 0;
	for(auto const& indSTR:allChosen){
		vector<Organism*>::iterator it = find_if(IndividualContext.begin(), IndividualContext.end(),[indSTR](Organism* o)->bool {Individual* i = dynamic_cast<Individual*>(o); return i->getCalc_ident()==indSTR;});
		if(it!=IndividualContext.end()){
			Individual* ind = dynamic_cast<Individual*>((*it));
			nbT = nbT + 1;
			for(auto const& bacteria:allBacteriaToAnalyzed){
				changeIndividualPathogenStatus(ind, bacteria, "immunization", startDate.substr(0,10));
				ImmunizedPeople[bacteria].push_back(ind->getCalc_ident());
			}
		}
	}
	// cout << " et donc " << nbT << endl;
	synchInd();
}
/*
void Model::initializeFirstImmunized(double percentagePA, double percentagePE){
	synchInd();
	boost::mpi::communicator* comm = repast::RepastProcess::instance()->getCommunicator();
	vector<Organism*> IndividualContext;
	context.selectAgents(context.size(),IndividualContext,0);
	vector<string> allChosen;
	if(rank==0){
		for(auto const& elem:IndividualContext){
			Individual* ind = dynamic_cast<Individual*>(elem);
			double percentage = ind->getStatus() == "PA" ? percentagePA:percentagePE;			
			DoubleUniformGenerator chooseProbaContener = Random::instance()->createUniDoubleGenerator(0,1);
			double randomNb = chooseProbaContener.next();
			if(randomNb < percentage){
				//il va être immunisé
				allChosen.push_back(ind->getCalc_ident());
				for(auto const& bacteria:allBacteriaToAnalyzed){
					// cout << " on rajoute l'état pour " << ind->getCalc_ident() << endl;
					// changeIndividualPathogenStatus(ind, bacteria, "immunization", startDate.substr(0,10));
					// ind->changePathogensStatus(bacteria, "immunization", startDate.substr(0,10));
					ImmunizedPeople[bacteria].push_back(ind->getCalc_ident());
				}
			}
		}
	}
	boost::mpi::broadcast(*comm,ImmunizedPeople, 0);
	boost::mpi::broadcast(*comm,allChosen, 0);
	for(auto const& indSTR:allChosen){
		vector<Organism*>::iterator it = find_if(IndividualContext.begin(), IndividualContext.end(),[indSTR](Organism* o)->bool {Individual* i = dynamic_cast<Individual*>(o); return i->getCalc_ident()==indSTR;});
		if(it!=IndividualContext.end()){
			Individual* ind = dynamic_cast<Individual*>((*it));
			for(auto const& bacteria:allBacteriaToAnalyzed){
				changeIndividualPathogenStatus(ind, bacteria, "immunization", startDate.substr(0,10));
			}
		}
	}
	synchInd();
}
*/

void Model::initializeFirstPathogens(){
	/* Fonction qui rajoute les individus colonisés dans les bactéries + rajoute la bactérie dans l'individus. Met à jour les vectors actual en début de simulation */
	synchInd();
	boost::mpi::communicator* comm = repast::RepastProcess::instance()->getCommunicator();
	vector<Organism*> IndividualContext;
	context.selectAgents(context.size(),IndividualContext,0);
	vector<Organism*> PathogensContext;
	context.selectAgents(context.size(),PathogensContext,1);
	vector<string> NonSwabInd;
	map<string, int> nbOfSwabInd;
	map<string, map<string, int>> nbOfColInd;
	for(auto const& elem:allBacteriaToAnalyzed){
		nbOfColInd[elem]["PA"] = 0;
		nbOfColInd[elem]["PE"] = 0;
	}
	map<string, map<string, string>> addMap;
	//1) on va se placer dans le rank 0 et choisir une date de décolo 
	if(rank == 0){
		nbOfSwabInd["PA"] = 0;
		nbOfSwabInd["PE"] = 0;
		for(auto const& elem:IndividualContext){
			Individual* ind = dynamic_cast<Individual*>(elem);
			vector<string> res;
			string LastDate;
			map<string, vector<string>> mapInd = ind->getPositivePathogens();
			vector<string> allDates = ind->getAllSwabDates();
			if(!allDates.empty()){
				nbOfSwabInd[ind->getStatus()] = nbOfSwabInd[ind->getStatus()] + 1;
				LastDate = allDates[allDates.size()-1];
				for(auto const& key:allBacteriaToAnalyzed){
					map<string, vector<string>>::iterator itFoundBacteria = mapInd.find(key);
					if(itFoundBacteria!=mapInd.end()){
						//il a eu la bacterie on va voir s'il a eu à lastDate
						vector<string> datesbacteria = mapInd[key];
						vector<string>::iterator itFoundLastDate = find(datesbacteria.begin(), datesbacteria.end(), LastDate);
						if(itFoundLastDate!=datesbacteria.end()){
							//il a eu la bacterie pendant lastDate du coup on va lui ajouté une date de décolo si on est en simu sinon on le met direct dans le addMap
							string endOfCol;
							if(gameStatus=="simu"){
								string type = durationByType ? ind->getStatus(): returnCat(ind);
								if(boolManDuration){
									endOfCol = findDate(manualDuration);
								}else{
									endOfCol = findDurationOfColonization(key, startDate.substr(0,10), type);
								}
								time_t endOfColTime = stringToDate(endOfCol);
								time_t startDateTime = stringToDate(startDate.substr(0,10));
								if(endOfColTime>=startDateTime){
									addMap[key][ind->getCalc_ident()] = endOfCol;
								}
							}else{
								addMap[key][ind->getCalc_ident()] = "real";
							}
							
						}
					}
				}
			}else{
				NonSwabInd.push_back(ind->getCalc_ident());
			}
		}
	}
	boost::mpi::broadcast(*comm,addMap, 0);
	boost::mpi::broadcast(*comm,NonSwabInd, 0);
	boost::mpi::broadcast(*comm,nbOfSwabInd, 0);
	
	for(auto const& [bacterie, mapIndividus]:addMap){
		map<string, string> mapIndividusAColo = mapIndividus;
		Bacteria* bacteria;
		vector<Organism*>::iterator itPath = find_if(PathogensContext.begin(), PathogensContext.end(),[bacterie](Organism* o)->bool {Bacteria* b = dynamic_cast<Bacteria*>(o); return b->getStringId()==bacterie;});
		if(itPath!=PathogensContext.end()){
			bacteria = dynamic_cast<Bacteria*>((*itPath));
		}
		for(auto const& indElem:IndividualContext){
			Individual* indObj = dynamic_cast<Individual*>(indElem);
			auto itFoundElem = mapIndividusAColo.find(indObj->getCalc_ident());
			if(itFoundElem!=mapIndividusAColo.end()){
				//on rajoute la bacterie à l'ind
				nbOfColInd[bacterie][indObj->getStatus()] = nbOfColInd[bacterie][indObj->getStatus()] + 1;
				if(rank == indObj->getId().currentRank()){
					indObj->addActualPositivePathogens(bacterie);
					if(gameStatus=="simu"){
						indObj->addColonization(bacterie, mapIndividusAColo[indObj->getCalc_ident()]);
						string dateST;
						dateST=mapIndividusAColo[indObj->getCalc_ident()];
						indObj->changePathogensStatus(bacterie, "asymptomatic", dateST);
					}
				}
				//on rajoute l'ind à la bacterie
				if(rank == bacteria->getId().currentRank()){
					bacteria->addActualPositiveIndividuals(indObj->getCalc_ident());
				}
			}
		}
	}

	comm->barrier();
	synchInd();
	synchPath();
	
	//2)les non swabbé
	if(gameStatus=="simu"){
		//on va recup dans le context les nonSwabInd
		vector<Individual*> NonSwabIndObjPA;
		vector<Individual*> NonSwabIndObjPE;
		map<string, vector<Individual*>> NonSwabIndObj;
		for(auto const& orgCtxt:IndividualContext){
			Individual* indCtxt = dynamic_cast<Individual*>(orgCtxt);
			auto itFoundIndCxt = find(NonSwabInd.begin(), NonSwabInd.end(), indCtxt->getCalc_ident());
			if(itFoundIndCxt!=NonSwabInd.end()){
				// NonSwabIndObj.push_back(indCtxt);
				if(indCtxt->getStatus()=="PA"){
					NonSwabIndObjPA.push_back(indCtxt);
				}else{
					NonSwabIndObjPE.push_back(indCtxt);
				}
			}
		}
		NonSwabIndObj["PA"] = NonSwabIndObjPA;
		NonSwabIndObj["PE"] = NonSwabIndObjPE;
		
		for(auto const& bact:allBacteriaToAnalyzed){
			Bacteria* bacteriaObject;
			vector<Organism*>::iterator itPath = find_if(PathogensContext.begin(), PathogensContext.end(),[bact](Organism* o)->bool {Bacteria* b = dynamic_cast<Bacteria*>(o); return b->getStringId()==bact;});
			if(itPath!=PathogensContext.end()){
				bacteriaObject = dynamic_cast<Bacteria*>((*itPath));
			}
			vector<string> allTypes = {"PA", "PE"};
			map<string, string> addToInd;
			if(rank==0){
				for(auto const& t:allTypes){
					double propCol = nbOfColInd[bact][t] / (double)nbOfSwabInd[t];
					int nbOfSI = NonSwabIndObj[t].size();
					int nbOfNonSwabCol = nbOfSI*propCol;
					IntUniformGenerator chooseProbaContener = Random::instance()->createUniIntGenerator(0,NonSwabIndObj[t].size()-1);
					vector<int> chooseNb;
					
					for(int i(0); i<nbOfNonSwabCol; i++){
						int randomNb = chooseProbaContener.next();
						vector<int>::iterator itNB = find(chooseNb.begin(), chooseNb.end(), randomNb);
						while(itNB!=chooseNb.end()){
							randomNb = chooseProbaContener.next();
							itNB = find(chooseNb.begin(), chooseNb.end(), randomNb);
						}
						chooseNb.push_back(randomNb);
					}
					for(auto const& randNb:chooseNb){
						Individual* indToColonized = NonSwabIndObj[t].at(randNb);
						string endOfCol;
						string type = durationByType ? indToColonized->getStatus(): returnCat(indToColonized);
						if(boolManDuration){
							endOfCol = findDate(manualDuration);
						}else{
							endOfCol = findDurationOfColonization(bact, startDate.substr(0,10), type);
						}
						addToInd[indToColonized->getCalc_ident()] = endOfCol;
					}
				}
			}
			boost::mpi::broadcast(*comm,addToInd,0);
			for(auto const& indElem:IndividualContext){
				Individual* indObj = dynamic_cast<Individual*>(indElem);
				auto itFoundElem = addToInd.find(indObj->getCalc_ident());
				if(itFoundElem!=addToInd.end()){
					if(rank == indObj->getId().currentRank()){
						indObj->addActualPositivePathogens(bact);
						indObj->addColonization(bact, addToInd[indObj->getCalc_ident()]);
						string dateST;
						dateST=addToInd[indObj->getCalc_ident()];
						indObj->changePathogensStatus(bact, "asymptomatic", dateST);
					}
					if(rank == bacteriaObject->getId().currentRank()){
						bacteriaObject->addActualPositiveIndividuals(indObj->getCalc_ident());
					}
				}
			}	
		}
	}
	comm->barrier();
	synchInd();
	synchPath();
}

void Model::addFirstPathogen(Individual* ind, vector<string> tmp){
	/* Fonction qui ajoute dans les map des individus et des pathogènes des pathogènes acquis et leur dates pour l'un et des individus colonisés et la dernière date de colonisation pour l'autre */
	vector<Organism*> PathogensContext;
	context.selectAgents(context.size(),PathogensContext,1);
	Bacteria* bacteria;
	string family_tempo = tmp[3];
	string genre_tempo = tmp[4];
	string species_tempo = tmp[5];
	string spatype_tempo = tmp[6];
	string BMR_tempo = tmp[7];
	string atb_str = tmp[8];
	vector<Organism*>::iterator itPath = find_if(PathogensContext.begin(), PathogensContext.end(),[genre_tempo, species_tempo, spatype_tempo, BMR_tempo,atb_str](Organism* v) -> bool {
	Bacteria* m1 = dynamic_cast<Bacteria*>(v);
	if(m1->getGenus()==genre_tempo && m1->getSpecies()==species_tempo && m1->getSpatype()==spatype_tempo && m1->getBMR()==BMR_tempo && m1->getPhenotype()==atb_str){
		return true;
		}else{
			return false;
		}});
	if(itPath!=PathogensContext.end()){
		/* La bacterie est deja dans le context */
		bacteria = dynamic_cast<Bacteria*>((*itPath));
		/* On verif que la bactérie n'est pas déjà dans la map de l'ind */
		string indOfBacteria = bacteria->getStringId();
		map<string,vector<string>>::iterator itMap;
		map<string,vector<string>> mapInd = ind->getPositivePathogens();
		itMap = find_if(mapInd.begin(), mapInd.end(),[indOfBacteria](std::pair<string, vector<string>> v) -> bool { return v.first == indOfBacteria;} );
		
		if(itMap!=mapInd.end()){
			/* si oui on change juste la date dans la map de l'ind mais aussi dans la map de la bacterie */
			(*itMap).second.push_back(tmp[1]);
			ind->setPositivePathogens(mapInd);
			map<string,string> mapB = bacteria->getPositiveIndividuals();
			map<string, string>::iterator itb;
			itb = mapB.find(ind->getCalc_ident());

			if(itb!=mapB.end()){
				(*itb).second = tmp[1];
				bacteria->setPositiveIndividuals(mapB);
			}else{
				cout << "hey mais l'individual n'a pas la bactérie au début!! trop chelou va voir la fonction addFirstPathogen NOW!" << endl;
			}
		}else{
			/* sinon on ajoute */
			vector<string> vMap;
			vMap.push_back(tmp[1]);
			ind->addPositivePathogens(bacteria->getStringId(),vMap);
			bacteria->addPositiveIndividuals(ind->getCalc_ident(),tmp[1]);
		}
	}
}

int Model::initializeIndividuals(string admin){
	/* fonction qui initialise les individus, on lui donne le fichier csv, elle va crée un objet filetreatment qui renvoie directement un tableau avec les élements interessant. Avec ce tableau ont construit les individus que l'on ajoute dans le context. */

	string date = startDate;
	Logger& logger = Log4CL::instance()->get_logger("root");
	boost::mpi::communicator* comm = repast::RepastProcess::instance()->getCommunicator();
	int worldSize = RepastProcess::instance()->worldSize();
	vector<Organism*> PathogensContext;
	context.selectAgents(context.size(),PathogensContext,1);
	if(rank==0){
		Filetreatment *f = new Filetreatment(admin);
		const char *vinit[] = {"id", "status","firstDate","lastDate","ward","sex","hospitalization","cat"}; //vecteur avec les caractéristiques que l'on souhaite récupérer
		
		vector<string> nameC(vinit, end(vinit));
		Mt=f->tabInfo(nameC); //on applique à f la fonction pour stocker les caractéristiques que l'on souhaite récupérés

		try{
			mapAdmission = f->buildMeAddDisMap(Mt, 2);
			mapDischarge = f->buildMeAddDisMap(Mt, 3);
		}catch(string const& chaine){
			cerr<< chaine << endl;
			return -1;
		}
		delete f;
		
	}
	boost::mpi::broadcast(*comm,Mt,0);
	boost::mpi::broadcast(*comm,mapAdmission,0);
	boost::mpi::broadcast(*comm,mapDischarge,0);
	int idI = -1;
	int previousidI = -1;
	vector <string> t;
	for(int i(0); i<Mt.size(); i++){
		t = Mt[i];
		string date1 = t[2];
		string date2 = t[3];
		if(date2=="")date2=stopDate.substr(0,10);
		if(stringToDateWithHour(date1)<=stringToDate(date) && stringToDateWithHour(date2)>=stringToDate(date)){
			int r = -1;
			for(int w(0); w<wards.size(); w++){
				if(wards[w]==t[4]){
					r = w;
				}
			}
			if(r==-1){
				cout << " il y a un pb avec les admissions " <<endl;
			}
			if(rank==r){
				idI++;
				if(t[1]=="PA"){
					repast::AgentId id(idI, rank, 0); //obligé d'avoir un id en int pour les rank :(
					id.currentRank(rank);
					/* creation */
					Patient* patient = new Patient(id, t);
					/* ajouter ses anciens swab si il y a des anciens swab à la map de l'individu */
					if((gameStatus == "real" && !previousPrlvt.empty()) || (gameStatus == "simu" && useOfPreviousPrlvt == "yes" && !previousPrlvt.empty())){
						pair<multimap<string,vector<string>>::iterator, multimap<string,vector<string>>::iterator> iiPatient;
						multimap<string,vector<string>>::iterator itPatient;
						iiPatient = previousPrlvt.equal_range(patient->getCalc_ident());
						for(itPatient = iiPatient.first; itPatient != iiPatient.second; ++itPatient){
							addFirstPathogen(patient,itPatient->second );
							mapPreviousSwab[patient->getCalc_ident()].push_back(itPatient->second[1]);
							if(debugMode)cout << rank << " add first pathogen to " << patient->getCalc_ident() << " " << itPatient->second[0] << " " << itPatient->second[1] << " " << itPatient->second[2] << " " << itPatient->second[3] << " " << itPatient->second[4] << " " << itPatient->second[5] << " " << itPatient->second[6] << " " << itPatient->second[7] <<endl;
						}
					}
					context.addAgent(patient);
					mapId.insert(make_pair(t[0], id));
				}else if (t[1]=="PE"){
					
					repast::AgentId id(idI, rank, 0);
					id.currentRank(rank);
					Staff* staff = new Staff(id, t);
					if((gameStatus == "real" && !previousPrlvt.empty()) || (gameStatus == "simu" && useOfPreviousPrlvt == "yes" && !previousPrlvt.empty())){
						pair<multimap<string,vector<string>>::iterator, multimap<string,vector<string>>::iterator> iiStaff;
						multimap<string,vector<string>>::iterator itStaff;
						iiStaff = previousPrlvt.equal_range(staff->getCalc_ident());
						for(itStaff = iiStaff.first; itStaff != iiStaff.second; ++itStaff){
							addFirstPathogen(staff,itStaff->second);
							mapPreviousSwab[staff->getCalc_ident()].push_back(itStaff->second[1]);
						}
					}			
					context.addAgent(staff);
					mapId.insert(make_pair(t[0], id));
				}else{
					cout<<"il y a un individu externe!"<<endl;
				}
			}
			boost::mpi::broadcast(*comm, mapId, r);
			boost::mpi::broadcast(*comm, mapPreviousSwab, r);
		}
	}
	vector <int> previousindCounts;
	boost::mpi::all_gather(*comm, idI, indCounts);
	boost::mpi::all_gather(*comm, previousidI, previousindCounts);
	requestOrganism(previousindCounts, 0, "SET_1");
	int const taille(Mt.size()); 
	return(taille);
}

void Model::checkDischarge(){
	/* Fonction qui vérifie si un individus doit sortir ou non de la simulation */
	/* d'abord on synchro*/
	if(debugMode)cout<< rank << " Fonction: checkDischarge " <<"checkDischarge ON " << currentDate() <<endl;
	synchInd();
	synchPath();
	boost::mpi::communicator* comm = repast::RepastProcess::instance()->getCommunicator();
	string date = currentDate().substr(0,10);
	map<string, map<string, vector<string>>>:: iterator itMap = mapDischarge.find(date);
	if(debugMode)cout << rank << " checkDischarge context before " << context.size() << endl;	
	vector<string> idInd;
	if(itMap!=mapDischarge.end()){
		vector<Organism*> individualsContext;
		context.selectAgents(context.size(),individualsContext,0);
		vector<Organism*> PathogensContext;
		context.selectAgents(context.size(),PathogensContext,1);
		map<string, vector<string>> mapIndividuals = (*itMap).second;
		vector<Organism*> allInd;
		vector<repast::AgentId> allIdAgent;	
		for(auto const& [keyInd, valInd]:mapIndividuals){
			vector<Organism*>::iterator itOrga = find_if(individualsContext.begin(), individualsContext.end(), [keyInd](Organism* o)->bool {Individual* i=dynamic_cast<Individual*>(o); return i->getCalc_ident()==keyInd;});
			if(itOrga!=individualsContext.end()){
				allInd.push_back((*itOrga));
				allIdAgent.push_back((*itOrga)->getId());
				//on les copies dans le movinPeople	
				Individual* indToAdd = dynamic_cast<Individual*>((*itOrga));
				//qu'est ce qu'il portait ?
				//1) actualPositivePathogens
				if(debugMode)cout << " on enlève " << indToAdd->getCalc_ident() << " qui portait donc dans actualpositivepatho :\n";
				for(auto const& elem:indToAdd->getActualPositivePathogens()){
					if(debugMode)cout << elem << " and ";
				}
				if(debugMode)cout<< "\n et dans positivepatho \n";
				//2) positivePathogens
				if(debugMode)cout << indToAdd->getPositivePathogens().size() << " \n";
				for(auto const& [key, val]:indToAdd->getPositivePathogens()){
					if(debugMode)cout << key << " : ";
					for(auto const& elem:val){
						if(debugMode)cout << elem << "_";
					}
					if(debugMode)cout<< "\n";
				}
				if(debugMode)cout<<endl;
				if(immunizationDuration=="Inf"){
					map<string, pair<string, string>> statP = indToAdd->getPathogensStatus();
					for(auto const& [key, val]:statP){
						if(val.first=="immunization"){
							ImmunizedPeople[key].push_back(indToAdd->getCalc_ident());
						}
					}
				}
				
				idInd.push_back(indToAdd->getCalc_ident());
				removeIndFromPathogens(indToAdd, PathogensContext);
				mapId.erase(indToAdd->getCalc_ident());
				copyToMovingPeople(indToAdd);
			}
		}
		comm->barrier();
		synchInd();
		synchPath();
		for(auto const& elem:allIdAgent){
			context.importedAgentRemoved(elem);
			repast::RepastProcess::instance()->agentRemoved(elem);
			context.removeAgent(elem);
		}
	}
	repast::RepastProcess::instance()->synchronizeAgentStatus<Organism, OrganismPackage, OrganismPackageProvider, OrganismPackageReceiver>(context, *provider, *receiver, *receiver);	
	if(debugMode)cout << rank << " checkDischarge context after " << context.size() << endl;
	comm->barrier();
	if(!idInd.empty()){
		for(auto const& elem:movingPeople){
			auto it = find(idInd.begin(), idInd.end(), elem->getCalc_ident());
			if(it!=idInd.end()){
				if(!elem->getActualPositivePathogens().empty()){
					vector < string> p;
					elem->setActualPositivePathogens(p);
					elem->clearAllColonization();
					elem->clearAllPathogensStatus();
				}
			}
		}
	}
	if(debugMode)cout << rank << " checkDischarge context ok " << endl; 
}

void Model::copyToMovingPeople(Individual* m1){
	/* Fonction qui ajoute m1 à movingPeople */
	if(m1->getStatus()=="PA"){
		Patient* p1 = dynamic_cast<Patient*>(m1);
		Patient* p2 = new Patient (*p1);		
		movingPeople.push_back(p2);
	}else{
		Staff* s1 = dynamic_cast<Staff*>(m1);
		Staff* s2 = new Staff(*s1);
		movingPeople.push_back(s2);
	}
}

void Model::showMeMovingPeople(){
	/* Fonction pour voir ce qui se passe dans le movingPeople */
	synchInd();
	cout << rank << " dans le movingPeople il y a aujourd'hui " << currentDate() << " : ";
	for(auto const& elem:movingPeople){
		cout << rank << elem->getCalc_ident() << " with " ;
		for(auto const& [key, val]:elem->getPositivePathogens()){
			cout<< key << " and " << val.size()<< " ";
		}
		cout << " -- ";
	}
	cout << endl;
}

void Model::removeIndFromPathogens(Individual* m1, vector<Organism*> PathogensContext){
	/* Fonction qui retire un individu d'un pathogen */
	string indToRemove = m1->getCalc_ident();
	map <string,vector<string>> map_path;
	vector <string> m1vect = m1->getActualPositivePathogens();
	if(!m1vect.empty()){	
		/* si jamais il a en ce moment une bacterie il faut qu'on enlève l'ind de la bacterie */
		for(vector<string>::iterator itPathAc =m1vect.begin(); itPathAc!=m1vect.end();++itPathAc){
			string path = (*itPathAc);
			vector<Organism*>::iterator itCtxt = find_if(PathogensContext.begin(), PathogensContext.end(),[path](Organism* v) -> bool {Bacteria* v1 = dynamic_cast<Bacteria*>(v);return v1->getStringId()== path;});
			if(itCtxt!=PathogensContext.end()){
				Pathogen* pathogen = dynamic_cast<Pathogen*>((*itCtxt));
				pathogen->removeIndFromActualPositiveIndividuals(indToRemove);
			}//pas de else car on est en local sur la bacterie
		}
		
	}
	synchPath();
}

void Model::clearPathogenacquisition(){
	/* Réinitialise le vector actualPositiveIndividual de tous les pathogènes */
	vector<Organism*> PathogensContext;
	// context.selectAgents(repast::SharedContext<Organism>::LOCAL,context.size(),PathogensContext,1);
	context.selectAgents(context.size(),PathogensContext,1);

	for(vector<Organism*>::iterator it(PathogensContext.begin());it!=PathogensContext.end();it++){
		Pathogen* m1 = dynamic_cast<Pathogen*>((*it));
		vector < string> p;
		m1->setActualPositiveIndividuals(p);
	}
	// synchPath();
}

void Model::checkClear(){
	/* Fonction qui vérifie s'il n'y a plus rien dans le getActualPositiveIndividuals de tous les individus */
	vector<Organism*> PathogensContext;
	context.selectAgents(context.size(),PathogensContext,1);
	for(vector<Organism*>::iterator it(PathogensContext.begin());it!=PathogensContext.end();it++){
		Bacteria* m1 = dynamic_cast<Bacteria*>((*it));
		if(m1->getActualPositiveIndividuals().size()!=0){
			cout <<"Dans le rank " <<rank << " "<< m1->getStringId() <<" : "<<m1->getActualPositiveIndividuals().size() << " pas normal ça! "<<endl;
		}	
	}
}

void Model::checkPrlvt(){
	/* Fonction qui à partir des données de prlvt attribut à chaque individus et à chaque pathogènes ces infos de prlvt. N'est valable que pour le realGame */
	if(gameStatus=="real"){
		try{
			synchInd();
			synchPath();
			clearPathogenacquisition();
			checkClear();
			if(ifs_prlvt.is_open()){
				string date = currentDate().substr(0,10);
				
				string datePrlvt = Filetreatment::checkDate(tempo_prlvt[1]);
				string line;
				if(datePrlvt == date){
					if(debugMode)cout<< rank << " Fonction: checkPrlvt " << " date " << date << " and date prlvt " << datePrlvt << endl;
					vector<Organism*> individualsContext;
					context.selectAgents(context.size(),individualsContext,0);
					vector<Organism*> PathogensContext;
					context.selectAgents(context.size(),PathogensContext,1);
					do{
						
						string nameIndLine = tempo_prlvt[0];
						if(debugMode)cout<< rank << " Fonction: checkPrlvt " << " prlvt: on s'occupe de " << nameIndLine << endl;
						Individual* ind;
						std::vector<Organism*>::iterator iterInd = find_if(individualsContext.begin(),individualsContext.end(), [nameIndLine](Organism* in) -> bool { Individual* in2 = dynamic_cast<Individual*>(in); return nameIndLine == in2->getCalc_ident();});
						if(iterInd!= individualsContext.end()){
							ind = dynamic_cast<Individual*>((*iterInd));
							auto itPrel = find(mapPrlvtALL[date].begin(), mapPrlvtALL[date].end(), ind->getCalc_ident());
							if(itPrel==mapPrlvtALL[date].end()){
								mapPrlvtALL[date].push_back(ind->getCalc_ident());
							}
							
						}else{
							cout<< "ooops the individual "<< nameIndLine <<" is not in the hospital!" << date <<endl;
						}
						Bacteria* bacteria;
						string indOfBacteria;
						int idP = pathCounts[rank];
						int previousidP = pathCounts[rank];
						vector<string> tmp_prlvt = tempo_prlvt;
						vector<string> tmpTransition(tmp_prlvt.begin()+3, tmp_prlvt.end());
						vector<Organism*>::iterator itPath = find_if(PathogensContext.begin(), PathogensContext.end(),[tmpTransition](Organism* v) -> bool {Pathogen* v1 = dynamic_cast<Pathogen*>(v);return(v1->isEqual(tmpTransition));});
						if(itPath!=PathogensContext.end()){
							// le pathogen est dans l'hopital on va donc lui ajouter l'individu //
							// on se met dans le rank du pathogen //
							bacteria = dynamic_cast<Bacteria*>((*itPath));
							indOfBacteria = bacteria->getStringId();
							if(debugMode)cout<< rank << " Fonction: checkPrlvt " << " ind " << nameIndLine << " --> " << indOfBacteria << endl;
							if(rank == bacteria->getId().currentRank()){						
								// est ce que l'individu a déjà eu la bacterie ? //
								map<string,string>::iterator itMapB;
								map<string,string> mapPath = bacteria->getPositiveIndividuals();
								itMapB = find_if(mapPath.begin(), mapPath.end(),[ind](std::pair<string, string> v) -> bool { return v.first==ind->getCalc_ident();} );
								if(itMapB!=mapPath.end()){
									// l'individu est dans la map de la bacterie on met à jour la date de dernière rencontre //
									if(debugMode)cout<< rank << " Fonction: checkPrlvt " << " ind " << nameIndLine << " est dans la map de " << indOfBacteria << " \n";
									(*itMapB).second = date;
									if(debugMode)cout<< " bacterie getPositiveIndividuals " << bacteria->getPositiveIndividuals().size() << " bacterie getActualPositiveIndividuals " << bacteria->getActualPositiveIndividuals().size() << "\n";
									
									bacteria->setPositiveIndividuals(mapPath);
									// et le actualPositiveIndividual
									bacteria->addActualPositiveIndividuals(ind->getCalc_ident());
									if(debugMode)cout <<  " ind " << nameIndLine << " bacterie getPositiveIndividuals " << bacteria->getPositiveIndividuals().size() << " bacterie  getActualPositiveIndividuals " << bacteria->getActualPositiveIndividuals().size() << "\n";
									if(debugMode)cout <<  " mis à jour " << indOfBacteria << endl;
								}else{
									// sinon c'est une acquisition donc on l'ajoute dans la map et dans le vecteur acquisition //
									if(debugMode)cout<< rank << " Fonction: checkPrlvt " << " la bacterie n'a pas l'ind " << nameIndLine << "\n";
									if(debugMode)cout << " bacterie getPositiveIndividuals " << bacteria->getPositiveIndividuals().size() << " bacterie getActualPositiveIndividuals " << bacteria->getActualPositiveIndividuals().size() << "\n";
									bacteria->addPositiveIndividuals(ind->getCalc_ident(), date);
									bacteria->addActualPositiveIndividuals(ind->getCalc_ident());
									if(debugMode)cout <<  " ind " << nameIndLine << " bacterie getPositiveIndividuals " << bacteria->getPositiveIndividuals().size() << " bacterie getActualPositiveIndividuals " << bacteria->getActualPositiveIndividuals().size() << endl;		
								}
							}
						}else{
							boost::mpi::communicator* comm = repast::RepastProcess::instance()->getCommunicator();
							//la bacterie n'est pas dedans donc on va la créer dans le rank de son premier individu //
							if(rank == ind->getId().currentRank()){
								if(debugMode)cout<< rank << " Fonction: checkPrlvt " << " création de la bactérie " << endl;
								string enteroATB = props->getProperty("enterobacteria.atb.list");
								string staphATB = props->getProperty("staphylococcus.atb.list");
								idP++;
								Phenotype p;
								if(tmp_prlvt[3]=="Enterobacteriaceae"){
									p = Phenotype(enteroATB,tmp_prlvt[8]);
								}else if(tmp_prlvt[3]=="Staphylococcaceae"){
									p = Phenotype(staphATB,tmp_prlvt[8]);
								}
								repast::AgentId id(idP, rank, 1); 
								id.currentRank(rank);
								bacteria = new Bacteria(id,"Bacteria", tmp_prlvt[3],tmp_prlvt[4],tmp_prlvt[5],tmp_prlvt[6],tmp_prlvt[7],p);
								
								bacteria->addPositiveIndividuals(ind->getCalc_ident(),datePrlvt);
								bacteria->addActualPositiveIndividuals(ind->getCalc_ident());
								indOfBacteria = bacteria->getStringId();
								mapId[indOfBacteria] = id;
								context.addAgent(bacteria);
								if(debugMode)cout<< rank << " Fonction: checkPrlvt " << " création de la bactérie fin "  << bacteria->getStringId() << endl;
								if(debugMode)cout << " checkPrlvt mapId size " << mapId.size() << endl;
							}
							boost::mpi::broadcast(*comm, mapId, ind->getId().currentRank());
							if(debugMode)cout << " checkPrlvt mapId size after broadcast " << mapId.size() << endl;
							vector<int> previouspathCounts;
							boost::mpi::all_gather(*comm, idP, pathCounts);
							boost::mpi::all_gather(*comm, previousidP, previouspathCounts);
							requestOrganism(previouspathCounts, 1, "SET_2");
						}						
						if(rank == ind->getId().currentRank()){
							// on va reinitialisé le vecteur actualPositivePathogens
							map<string,vector<string>>::iterator itMapDay;
							map<string,vector<string>> mapInd = ind->getPositivePathogens();
							itMapDay = find_if(mapInd.begin(), mapInd.end(),[datePrlvt](std::pair<string, vector<string>> v) -> bool {vector<string>::iterator it_lambda = find(v.second.begin(),v.second.end(),datePrlvt);
								if(it_lambda!=v.second.end()){
									return true;
								}else{	
									return false;
								}
							} );
							if(itMapDay == mapInd.end()){
								/* la date n'est pas dans la map c'est son premier prlvt */
								// vector<string> pTransition;
								ind->removeActualPositivePathogens();
								if(debugMode)cout<< rank << " Fonction: checkPrlvt " << " c'est le premier prlvt de " << ind->getCalc_ident() << endl;
							}
							//on va mettre à jour la bacterie dans la map de l'individu
							map<string,vector<string>>::iterator itMap;
							itMap = find_if(mapInd.begin(), mapInd.end(),[indOfBacteria](std::pair<string, vector<string>> v) -> bool {return v.first==indOfBacteria;} );
							
							if(itMap!=mapInd.end()){
								/* la bacterie est dans la map on met à jour la date */
								if(debugMode)cout<< rank << " Fonction: checkPrlvt " << " la bacterie est dans la map de l'ind " << ind->getCalc_ident() << " " << indOfBacteria << "\n";
								(*itMap).second.push_back(date);
								if(debugMode)cout << " la bacterie est dans la map de l'ind " << ind->getCalc_ident() << " " << indOfBacteria << " getPositivePathogens " << ind->getPositivePathogens().size() << " getActualPositivePathogens " << ind->getActualPositivePathogens().size() << "\n";
								ind->setPositivePathogens(mapInd);
								/* on la rajoute dans le actualPatho */
								ind->addActualPositivePathogens(indOfBacteria);
								if(debugMode)cout  << " la bacterie est dans la map de l'ind " << ind->getCalc_ident() << " bacterie " << indOfBacteria << " getPositivePathogens " << ind->getPositivePathogens().size() << " getActualPositivePathogens " << ind->getActualPositivePathogens().size() << endl;
							}else{
								/* la bacterie n'est pas dans la map on la rajoute */
								vector<string> vMap;
								vMap.push_back(date);
								if(debugMode)cout<< rank << " Fonction: checkPrlvt " << " la bacterie n'est pas dans la map de l'ind " << ind->getCalc_ident() << " indOfBacteria " << indOfBacteria << " getPositivePathogens " << ind->getPositivePathogens().size() << " getActualPositivePathogens " << ind->getActualPositivePathogens().size() << "\n";
								ind->addPositivePathogens(indOfBacteria,vMap);
								/* si la bacterie n'est pas dans la map alors elle n'a jamais été rencontré par l'ind donc on la rajoute dans le vcteur */
								ind->addActualPositivePathogens(bacteria->getStringId());
								if(debugMode)cout <<  " la bacterie n'est pas dans la map de l'ind " << ind->getCalc_ident() << " indOfBacteria " << indOfBacteria << " getPositivePathogens " << ind->getPositivePathogens().size() << " getActualPositivePathogens " << ind->getActualPositivePathogens().size() << endl;
							}
						}
						getline(ifs_prlvt, line);
						if(ifs_prlvt.eof()){
							ifs_prlvt.close();
							break;
						} 
						boost::trim_if(line, boost::is_any_of("\r\n|\r|\n|\t"));
						boost::split(tempo_prlvt, line, boost::is_any_of(";"));
						datePrlvt = Filetreatment::checkDate(tempo_prlvt[1]);
					}while(datePrlvt == date);
				}
			}
		}catch(exception const& e){
			cerr<< "Erreur dans checkPrlvt: " << e.what() << endl;
		}	
	}
}

void Model::testActualPatho(){
	/* Fonction test qui permet de savoir combien de pathogène colonise actuellement cet individus */
	vector<Organism*> listOfI;
	context.selectAgents(repast::SharedContext<Organism>::LOCAL,context.size(),listOfI,0);
	for(vector<Organism*>::iterator it_listOfI = listOfI.begin(); it_listOfI!=listOfI.end(); it_listOfI++){
		Individual* ind = dynamic_cast<Individual*>((*it_listOfI));
		cout << "Aujourd'hui le " << currentDate() << " l'individu " << ind->getCalc_ident() << " a actuellement " << ind->getActualPositivePathogens().size() << " pathogènes " << endl;
	}
}

void Model::testAc(){
	/* Fonction qui permet d'afficher combien d'individus sont colonisé par un pathogène et combien de personne ont eu le pathogène au cours de la simulation */
	vector<Organism*> listOfB;
	context.selectAgents(repast::SharedContext<Organism>::LOCAL,context.size(),listOfB,1);
	for(vector<Organism*>::iterator it_listOfB = listOfB.begin(); it_listOfB!=listOfB.end(); it_listOfB++){
		Pathogen* path = dynamic_cast<Pathogen*>((*it_listOfB));
		Bacteria* b = dynamic_cast<Bacteria*>(path);
		cout << "Aujourd'hui le " << currentDate() << " le patho ";
		b->affiche();
		cout << " a actuellement " << b->getActualPositiveIndividuals().size() << " Individus et a rencontré " << b->getPositiveIndividuals().size() << " personnes "; 
		cout << endl;
	}
}

vector<Organism*> Model::selectBacteria(vector<Organism*> listOfB){
	/* Fonction qui permet de récuperer sous la forme d'objet les pathogènes contenues dans la liste des pathogènes à analyser */
	vector<Organism*> res;
	vector<Organism*>::iterator it;
	for(auto const& elem:allBacteriaToAnalyzed){
		string bacteriaToFind = elem;
		it=find_if(listOfB.begin(), listOfB.end(),[bacteriaToFind](Organism* o)->bool {
			Bacteria* p = dynamic_cast<Bacteria*>(o);
			return p->getStringId()==bacteriaToFind;});
		if(it!=listOfB.end()){
			res.push_back((*it));
		}
	}
	return res;
}

vector<string> Model::findAbscenceDay(string ind){
	/* Fonction qui donne les jours de présence de l'individu ind */
	vector<string> res;
	for(auto const& [key, val]:presence){
		vector<string> vVal = val;
		vector<string>::iterator itV = find(vVal.begin(), vVal.end(), ind);
		if(itV==vVal.end()){
			res.push_back(key);
		}
	}
	return(res);
}

vector<string> Model::findDateBetweenTwoDays(string firstDay, string secondDay){
	/* Fonction qui retourne un vecteur de date sous forme de string entre firstDay et secondDay. secondDay non inclus */
	vector<string> res;
	time_t FD = stringToDate(firstDay);
	time_t SD = stringToDate(secondDay);
	for(time_t i(FD); i<SD; i = i + 86400){
		res.push_back(dateToString(i).substr(0,10));
	}
	return(res);
}

bool Model::absencePeriod(string ind, string previousDay, string actualDay){
	/* Fonction qui retourne un booleen qui vaut false si l'individu ind n'a pas été absent entre previousDay et actualDay et true sinon */
	bool res;
	vector<string> absenceDays = findAbscenceDay(ind);
	vector<string> daysBetween = findDateBetweenTwoDays(previousDay, actualDay);
	//on va chercher maintenant s'il y a des absencedays dans days between
	sort(absenceDays.begin(), absenceDays.end());
	sort(daysBetween.begin(), daysBetween.end());
	vector <string> common;
	set_intersection(absenceDays.begin(), absenceDays.end(), daysBetween.begin(), daysBetween.end(),  back_inserter(common));
	if(common.empty()){
		//pas d'absence
		res = false;
	}else{
		res = true;
	}
	return(res);
}

vector<string> Model::findIndividualFromThisCat(string nature, bool old){
	/* Fonction qui renvoie les individus de la catégorie nature. Si old est true alors ça donne même les individus qui sont parties */
	vector<Organism*> listOfI;
	context.selectAgents(context.size(),listOfI,0);
	if(old){
		listOfI.insert(listOfI.end(), movingPeople.begin(), movingPeople.end());
	}
	vector<string> results;
	for(vector<Organism*>::iterator itI=listOfI.begin(); itI!=listOfI.end(); itI++){
		Individual* ind = dynamic_cast<Individual*>((*itI));
		if(nature == "Patient"){
			if(ind->getStatus()=="PA"){
				results.push_back(ind->getCalc_ident());
			}
		}else{
			if(ind->getStatus()=="PE"){
				Staff* staff = dynamic_cast<Staff*>((ind));
				if(staff->getCat()==nature){
					results.push_back(staff->getCalc_ident());
				}
			}
		}
	}
	return results;
}

vector<string> Model::infectiousPeriod(string name, Bacteria* b){
	/* Fonction qui retourne les jours où l'individus name à eu la bactérie b */
	vector <string> results;
	vector<Organism*> listOfI;
	context.selectAgents(context.size(),listOfI,0);
	Individual* ind;
	std::vector<Organism*>::iterator iterInd = find_if(listOfI.begin(),listOfI.end(), [name](Organism* in) -> bool { Individual* in2 = dynamic_cast<Individual*>(in); return name == in2->getCalc_ident();});
	if(iterInd!= listOfI.end()){
		ind = dynamic_cast<Individual*>((*iterInd));
	}else{
		// l'ind est sortie donc on le recherche dans les movingPeople
		std::vector<Individual*>::iterator iterInd2 = find_if(movingPeople.begin(),movingPeople.end(), [name](Individual* in) -> bool { return name == in->getCalc_ident();});
		if(iterInd2!= movingPeople.end()){
			ind = (*iterInd2);
		}
	}
	map<string, vector<string>> listOfPath = ind->getPositivePathogens();
	string idOfB = b->getStringId();
	map<string, vector<string>> ::iterator itPath = find_if(listOfPath.begin(), listOfPath.end(),[idOfB](std::pair<string, vector<string>> v) -> bool {return v.first == idOfB;});
	if(itPath!=listOfPath.end()){
		results = itPath->second;
	}
	return results;
}

string Model::returnCat(Individual* ind){
	/* Fonction qui retourne la categorie de ind. Si ind est un PE, retourne la cat. Si ind est un PA retourne juste Patient */
	string key;
	if(ind->getStatus()=="PA"){
		key = "Patient";
	}else{
		Staff* staff = dynamic_cast<Staff*>(ind);
		key = staff->getCat();
	}
	return key;
}

map<string, vector<string>> Model::mapCat(vector<string> common){
	/* Fonction qui retourne une map avec en clé les catégories d'individus et en valeur les noms des individus appartenant à cette catégorie et qui sont dans common */
	map<string, vector<string>> results;
	vector<Organism*> listOfI;
	context.selectAgents(context.size(),listOfI,0);
	for(int j(0); j<common.size();j++){
		string nameIndLine = common[j];
		Individual* ind;
		string key;
		std::vector<Organism*>::iterator iterInd = find_if(listOfI.begin(),listOfI.end(), [nameIndLine](Organism* in) -> bool { Individual* in2 = dynamic_cast<Individual*>(in); return nameIndLine == in2->getCalc_ident();});
		if(iterInd!= listOfI.end()){
			ind = dynamic_cast<Individual*>((*iterInd));
		}else{
			std::vector<Individual*>::iterator iterInd2 = find_if(movingPeople.begin(),movingPeople.end(), [nameIndLine](Individual* in) -> bool { return nameIndLine == in->getCalc_ident();});
			if(iterInd2!= movingPeople.end()){
				ind = (*iterInd2);
			}
		}		
		key = returnCat(ind);
		if(results.find(key)!=results.end()){
			results[key].push_back(ind->getCalc_ident());
		}else{
			vector<string> t;
			t.push_back(ind->getCalc_ident());
			results[key] = t;
		}
	}
	return results;
}

DataSource_acquisition::DataSource_acquisition(repast::SharedContext<Organism>* c, std::vector<std::string>* v): context(c), vP(v){
	
}

int DataSource_acquisition::getData(){
	vector<Organism*> listePath;
	context->selectAgents(repast::SharedContext<Organism>::LOCAL,context->size(),listePath,1);
	vector<string> d = (*vP);
	string genre_tempo = d[1];
	string species_tempo = d[2];
	string spatype_tempo = d[3];
	string BMR_tempo = d[4];
	string atb_str = d[5];
	int taille=0;
	// cout << genre_tempo << " " << species_tempo << " " << spatype_tempo<< " " << BMR_tempo << " " << atb_str << endl;
	vector<Organism*>::iterator itPath = find_if(listePath.begin(), listePath.end(),[genre_tempo, species_tempo, spatype_tempo, BMR_tempo, atb_str](Organism* v) -> bool {
				Bacteria* m1 = dynamic_cast<Bacteria*>(v);
				if(m1->getGenus()==genre_tempo && m1->getSpecies()==species_tempo && m1->getSpatype()==spatype_tempo && m1->getBMR()==BMR_tempo && m1->getPhenotype()==atb_str){
					return true;
				}else{
					return false;
				}});
	
	if(itPath!=listePath.end()){
		//est dedans
		Bacteria* bacteria = dynamic_cast<Bacteria*>((*itPath));
		taille = bacteria->getActualPositiveIndividuals().size();
		
	}
	return taille;
}

void Model::synchronizeOrganism(){
	/* Fonction qui synchronise les deux types d'organisme */
	synchInd();
	synchPath();
}

//Fonctions relatives aux prélèvements
void Model::findMapDayRandom(){
	map<string, int> daysOfW;
	daysOfW["sunday"] = 0;
	daysOfW["monday"] = 1;
	daysOfW["tuesday"] = 2;
	daysOfW["wednesday"] = 3;
	daysOfW["thursday"] = 4;
	daysOfW["friday"] = 5;
	daysOfW["saturday"] = 6;
	map<string, map<string, map<string, double>>> mapDaysOfWeek = parameters->getMapDaysOfWeek();
	for(auto const& [day, mapType]:mapDaysOfWeek){
		map<string, map<string, double>> mType = mapType;
		for(auto const& [type, mapParam]:mapType){
			map<string, double> mParam = mapParam;
			int dow = daysOfW[day];
			mapDayRandom[type][dow] = make_pair(mParam["mean"], sqrt(mParam["variance"]));
		}
	}
}

void Model::mapPrlvtSimu(){
	/* Fonction qui rempli la map de prélèvement. En début de semaine, la fonction choisi des individus à prélever */
	if(gameStatus=="simu"){
		//on va choisir le nombre d'individus à prlvt cette semaine
		boost::mpi::communicator* comm = repast::RepastProcess::instance()->getCommunicator();
		synchInd();
		synchPath();
		int nbOfPA;
		int nbOfPE;
		vector<int> intDays;
		if(!mapDayRandom.empty() && swabPeople){
			for(auto const& [key, val]:mapDayRandom){
				for(auto const& [k, v]:val){
					
					vector<int>::iterator itF = find(intDays.begin(), intDays.end(), k);
					if(itF== intDays.end()){
						intDays.push_back(k);
					}
				}
			}
		}else{
			for(int i(0); i<7; i++){
				intDays.push_back(i);
			}
		}
		
		if(rank==0){
			//1)il faut choisir des PA et des PE parmi ceux qui sont présent donc on va faire une liste de PA et une liste de PE
			vector<Organism*> individuals;
			context.selectAgents(context.size(), individuals, 0);
			vector<Individual*> listPA;
			vector<Individual*> listPE;
			for(auto const& elem:individuals){
				Individual* ind = dynamic_cast<Individual*>(elem);
				if(ind->getStatus()=="PA"){
					listPA.push_back(ind);
				}else{
					listPE.push_back(ind);
				}
			}
			//cette semaine combien de PA et PE va t on choisir
			map<int, int> decomptePA;
			map<int, int> decomptePE;
			map<int, string> decompteDay;
			pair<map<string, int>, map<string, map<int, int>>> pairResIndToSwab;
			map<string, int> sumInd ;
			map<string, map<int, int>> nbOfIndByDays;
			int sumTotOfInd;
			int stop = 0;
			if(swabPeople){
				do{
					stop = stop + 1;
					pairResIndToSwab = findNbOfIndToSwab(mapDayRandom);
					sumInd = pairResIndToSwab.first;
					nbOfIndByDays = pairResIndToSwab.second;
					sumTotOfInd = 0;
					for(auto const& [key, val]:sumInd){
						sumTotOfInd = sumTotOfInd + val;
					}
				}while((sumInd["PA"]>=listPA.size() || sumInd["PE"]>=listPE.size()) && stop<100000);	
				try{
					if(stop == 100000){
						throw runtime_error(" not enough patient or staff to swab check your swab paramaters ");
					}
				}catch(const runtime_error& exc){
					cerr << exc.what() << "\n";
					exit(1);
				}
				
				decomptePA = nbOfIndByDays["PA"];
				decomptePE = nbOfIndByDays["PE"];
				nbOfPA = sumInd["PA"];
				nbOfPE = sumInd["PE"];
			}else{
				for(int i(0); i<7; i++){
					decomptePA[i] = listPA.size();
					decomptePE[i] = listPE.size();
				}
				nbOfPA = listPA.size();
				nbOfPE = listPE.size();
			}
						
			//1) si la dernière date est supérieur à stop date on la met à stop date (car on commence à currentDate())
			string date = currentDate().substr(0,10);
			time_t firstTimeWeek = stringToDateWithHour(date);
			time_t lastTimeWeek = firstTimeWeek + 604800;
			if(lastTimeWeek > stringToDateWithHour(stopDate.substr(0,10))){
				lastTimeWeek = stringToDateWithHour(stopDate.substr(0,10));
			}
			//3) on cherche les jours de la semaine 
			string lastDate = dateToString(lastTimeWeek).substr(0,10);
			map<int, string> dateWeek;
			mapPrlvt.clear();
			for(time_t i(firstTimeWeek); i<lastTimeWeek; i=i+time_t(86400)){
				string d = dateToString(i).substr(0,10);
				tm* time_st = localtime(&i);
				int wday = time_st->tm_wday;
				vector<string>::iterator itFerie = find(nonWorkingDays.begin(), nonWorkingDays.end(),d);
				if(itFerie==nonWorkingDays.end()){
					dateWeek[wday] = d;
					vector<int>::iterator itWday = find(intDays.begin(), intDays.end(), wday);
					if(itWday!=intDays.end()){
						decompteDay[wday] = d;
					}
					vector<string> emptyV;
					mapPrlvt.insert(make_pair(d, emptyV));
				}else{
					//faut retirer wday
					vector<int>::iterator itWday = find(intDays.begin(), intDays.end(), wday);
					if(itWday!=intDays.end()){
						decomptePA.erase(wday);
						decomptePE.erase(wday);
					}
				}
			}
			
			
			//4) on fait le remplissage
			if(!dateWeek.empty()){
				int minV = *min_element(intDays.begin(), intDays.end());
				int maxV = *max_element(intDays.begin(), intDays.end());
				IntUniformGenerator chooseProbaContener = Random::instance()->createUniIntGenerator(0,individuals.size()-1);
				//2) on va choisir des nombres aléatoires (à terme une petite fonction interm pour que ce soit plus propre)
				IntUniformGenerator chooseRandomP = Random::instance()->createUniIntGenerator(minV,maxV);
				IntUniformGenerator chooseProbaContenerPE = Random::instance()->createUniIntGenerator(0,listPE.size()-1);
				int choosingPositionPE;
				vector<int> positionsPE;
				for(int pe(0); pe<nbOfPE; pe++){
					choosingPositionPE = chooseProbaContenerPE.next();
					vector<int>::iterator itPos = find(positionsPE.begin(), positionsPE.end(), choosingPositionPE);
					while(itPos!=positionsPE.end() && positionsPE.size()<listPE.size()){
						choosingPositionPE = chooseProbaContenerPE.next();
						itPos = find(positionsPE.begin(), positionsPE.end(), choosingPositionPE);
					}
					positionsPE.push_back(choosingPositionPE);
					Individual* ind = listPE.at(choosingPositionPE);
					//ind est choisi on va lui donner une date de prlvt 
					int duration = chooseRandomP.next();
					bool emptyMap = zeroMap(decomptePE);
					if(!emptyMap){
						while(decomptePE[duration]==0){
							duration = chooseRandomP.next();
						}
						string dateP = decompteDay[duration];
						decomptePE[duration] = decomptePE[duration] - 1;
						mapPrlvt[dateP].push_back(ind->getCalc_ident());
					}//sinon on ne s'en occupe pas
				}
				IntUniformGenerator chooseProbaContenerPA = Random::instance()->createUniIntGenerator(0,listPA.size()-1);
				int choosingPositionPA;
				vector<int> positionsPA;
				// cout << " nbOfPA " << nbOfPA << " listPA.size() " << listPA.size() << endl;
				for(int pa(0); pa<nbOfPA; pa++){
					choosingPositionPA = chooseProbaContenerPA.next();
					vector<int>::iterator itPosPA = find(positionsPA.begin(), positionsPA.end(), choosingPositionPA);
					while(itPosPA!=positionsPA.end() && positionsPA.size()<listPA.size()){
						choosingPositionPA = chooseProbaContenerPA.next();
						itPosPA = find(positionsPA.begin(), positionsPA.end(), choosingPositionPA);
					}
					positionsPA.push_back(choosingPositionPA);
					Individual* ind = listPA.at(choosingPositionPA);
					//ind est choisi on va lui donner une date de prlvt 
					int duration = chooseRandomP.next();
					bool emptyMap = zeroMap(decomptePA);
					if(!emptyMap){
						while(decomptePA[duration]==0){
							duration = chooseRandomP.next();
						}
						string dateP = decompteDay[duration];
						decomptePA[duration] = decomptePA[duration] - 1;
						mapPrlvt[dateP].push_back(ind->getCalc_ident());
					}//sinon on ne s'en occupe pas
				}	
			}	
		}
		boost::mpi::broadcast(*comm,mapPrlvt,0);
		int nbOfS = 0;
		for(auto const& [key, val]:mapPrlvt){
			string allIndSwab;
			if(!val.empty()){
				allIndSwab = val[0];
				for(int i(1); i<val.size(); i++){
					allIndSwab = allIndSwab + "_" + val[i];
				}
			}
			if(timeResp!=7){
				if(returnSwab)myCSVswab<< key + ";" + to_string(val.size()) + ";" + allIndSwab + ";";
				if(returnSwab)myCSVswab<<endl;
			}else{
				nbOfS = nbOfS + val.size();
			}
		}
		if(timeResp==7){
			if(returnSwab)myCSVswab<< currentDate().substr(0,10) + ";" + to_string(nbOfS) + ";";
			if(returnSwab)myCSVswab<<endl;
		}
		//pour garder trace de tous les prlvt dans une map (test)
		for(auto const& [key, val]:mapPrlvt){
			mapPrlvtALL[key]=val;
		}
		
		// if(weeklyColStaff!=0 && currentDate().substr(0,10)!=startDate.substr(0,10)){
			// newColonizedIndividual(weeklyColStaff);
		// }
		
		
	}
}

bool Model::zeroMap(map<int,int> input_map){
	/* Fonction qui renvoie un booleen. Le booleen vaut true si input_map n'est pas rempli et false sinon. C'est à dire, s'il n'y a que des zero dans la map */
	bool res;
	int size_map = input_map.size();
	int c = 0;
	for(auto const& [key, val]:input_map){
		if(val==0){
			c = c + 1;
		}
	}
	if(c==size_map){
		res = true;
	}else{
		res = false;
	}
	return(res);
}

pair<map<string, int>, map<string, map<int, int>>> Model::findNbOfIndToSwab(std::map<string, std::map<int, std::pair<double,double>>> map_input){
	/* Fonction qui détermine le nombre d'individus à prélever */
	
	//ATTENTION UTILISATION QUE DANS UN SEUL RANK SINON C'EST LE BAZAR
	map<string, int> sumInd;
	map<string, map<int, int>> nbOfIndByDays;
	for(auto const& [key, val]:map_input){
		int sumI = 0;
		for(auto const& [k, v]:val){
			NormalGenerator contenerNormal = Random::instance()->createNormalGenerator(v.first, sqrt(v.second));
			double rndNorm = contenerNormal.next();
			int maxi = 0;
			while(rndNorm<0.0 && maxi<6){
				rndNorm = contenerNormal.next();
				maxi = maxi + 1;
			}
			if(rndNorm<0.0 && maxi==6){
				rndNorm = 0.0;
			}
			int NNB = (int)rndNorm;
			nbOfIndByDays[key][k] = NNB;
			sumI = sumI + NNB;
		}
		sumInd[key] = sumI;
	}
	pair<map<string, int>, map<string, map<int, int>>> res = make_pair(sumInd, nbOfIndByDays);
	return(res);
}


void Model::buildMeACompleteSwabFile(){
	string FileNameSwabCpt = "output/swab/swabCpt.csv";
	ofstream myCSVswabCpt;
	myCSVswabCpt.open(FileNameSwabCpt);
	for(auto const& [key, val]:mapPrlvtALL){
		vector<string> allIndividualSwabbed = val;
		for(auto const& elem:allIndividualSwabbed){
			myCSVswabCpt<<key + ";" + elem << endl;
		}
	}
	myCSVswabCpt.close();
}
//

//Fonctions relatives à l'acquisition
void Model::buildMeAcquisition(){
	/* Fonction qui ajoute dans un fichier csv les acquisitions pour chaque bactéries à analyser. A voir dans l'avenir pour faire un fichier par bacterie mais pour l'instant tout est dans un même fichier csv. La fonction construit le csv au fur et à mesure de la simulation (peut être construire uniquement à la fin un jour??) */
	synchInd();
	synchPath();
	if(currentDate().substr(0,10)!= startDate.substr(0,10) && returnAcq){
		getNumberOfAcquisitionWeeks();
	}
	if(rank==0 && returnAcq){
		if(currentDate().substr(0,10)== startDate.substr(0,10)){
			myCSVacquisition << "Date;";
			for(auto const& pathogen:allBacteriaToAnalyzed){
				myCSVacquisition << pathogen + "-" + "ALL" + ";" + pathogen + "-" + "PA" + ";" + pathogen + "-" + "PE" + ";" + pathogen + "-" + "nbPA" + ";"+ pathogen + "-" + "prelPA" + ";"+ pathogen + "-" + "nbPE" + ";"+ pathogen + "-" + "prelPE" + ";" + pathogen + "-" + "individual;";
			}
			myCSVacquisition << "\r" <<endl ;
		}else{
			myCSVacquisition << currentDate().substr(0,10) + ";";
			
			for(auto const& pathogen:allBacteriaToAnalyzed){
				vector<string> indAcq = mapAcquisitionWeekInd[pathogen][currentDate().substr(0,10)];
				string peopleAC;
				if(!indAcq.empty()){
					peopleAC=indAcq[0];
					for(int i(1); i!=indAcq.size(); i++){
						peopleAC = string(peopleAC) + string("_") + string(indAcq[i]);
					}
				}
				int ACALLpos = mapAcquisitionWeek[pathogen][currentDate().substr(0,10)]["PA"].first + mapAcquisitionWeek[pathogen][currentDate().substr(0,10)]["PE"].first;
				int ACALLneg =mapAcquisitionWeek[pathogen][currentDate().substr(0,10)]["PA"].second + mapAcquisitionWeek[pathogen][currentDate().substr(0,10)]["PE"].second;
				int ACPApos = mapAcquisitionWeek[pathogen][currentDate().substr(0,10)]["PA"].first;
				int ACPAneg = mapAcquisitionWeek[pathogen][currentDate().substr(0,10)]["PA"].second;
				int ACPEpos = mapAcquisitionWeek[pathogen][currentDate().substr(0,10)]["PE"].first;
				int ACPEneg = mapAcquisitionWeek[pathogen][currentDate().substr(0,10)]["PE"].second;
				double nbOfAcValALL;
				double nbOfAcValPA;
				double nbOfAcValPE; 
				if(ACALLpos!=0 && ACALLneg!=0){
					nbOfAcValALL = (double)ACALLpos/ACALLneg;
				}else{
					nbOfAcValALL = 0.0;
				} 
				if(ACPApos!=0 && ACPAneg!=0){
					nbOfAcValPA = (double)ACPApos/ACPAneg;
				}else{
					nbOfAcValPA = 0.0;
				}
				if(ACPEpos!=0 && ACPEneg!=0){
					nbOfAcValPE = (double)ACPEpos/ACPEneg;
				}else{
					nbOfAcValPE = 0.0;
				}
				myCSVacquisition<< to_string(nbOfAcValALL) + ";" + to_string(nbOfAcValPA) + ";" + to_string(nbOfAcValPE) + ";" + to_string(mapAcquisitionWeek[pathogen][currentDate().substr(0,10)]["PA"].first) + ";" + to_string(mapAcquisitionWeek[pathogen][currentDate().substr(0,10)]["PA"].second) + ";" + to_string(mapAcquisitionWeek[pathogen][currentDate().substr(0,10)]["PE"].first) + ";" + to_string(mapAcquisitionWeek[pathogen][currentDate().substr(0,10)]["PE"].second) + ";" + peopleAC+ ";";
			}
			myCSVacquisition << "\r"<<endl ;
		}
	}
}

void Model::getNumberOfAcquisitionWeeks(){
	/* Fonction qui retourne une map avec en clé la bactérie et en valeur une autre map dont la clé est le type et la valeur l'incidence de la semaine */
	//pour chaque pathogen on va récuperer les acquisitions
	for(auto const& bacteria:allBacteriaToAnalyzed){
		getAcquisitionWeeks(bacteria);
	}
}

void Model::getAcquisitionWeeks(string bacteria){
	/* Fonction qui retourne une map avec en clé le type PE/PA et en valeur l'incidence de la semaine */
	//première date de la semaine d'avant
	time_t currentTime = stringToDate(currentDate().substr(0,10));
	time_t firstDay = currentTime - 604800;
	if(firstDay < stringToDate(startDate)){
		firstDay = stringToDate(startDate.substr(0,10));
	}
	//récup les individus
	vector<Organism*> listOfI;
	context.selectAgents(context.size(), listOfI,0);
	//ajout des gens du movingPeople qui etait présent la semaine dernière
	vector<Individual*> addOldPeople;
	for(auto const& oldPeople:movingPeople){
		if(stringToDateWithHour(oldPeople->getDt_out())>=firstDay){
			//il est partie en cours de route la semaine dernière mais était bien présent
			addOldPeople.push_back(oldPeople);
		}
	}
	listOfI.insert(listOfI.end(), addOldPeople.begin(), addOldPeople.end());
	vector<string> weekDates;
	//récup toutes les dates de la semaine d'avant
	for(time_t i(firstDay); i < currentTime; i=i+time_t(86400)){
		weekDates.push_back(dateToString(i).substr(0,10));
	}
	pair<map<string, int>, vector<string>> resultAcquisition = findMeAcquisitionWeek(listOfI, bacteria, weekDates);
	map<string, int> tailleMap = resultAcquisition.first;
	vector<string> individualsAcq = resultAcquisition.second;
	map<string, vector<string>> negativePeople = findMePreviousNegIndividual(dateToString(firstDay).substr(0,10), bacteria, nbOfAcquisitionWeek);
	map<string, pair<int, int>> interm_map;
	interm_map["ALL"] = make_pair(tailleMap["ALL"], negativePeople["ALL"].size());
	interm_map["PA"] = make_pair(tailleMap["PA"], negativePeople["PA"].size());
	interm_map["PE"] = make_pair(tailleMap["PE"], negativePeople["PE"].size());
	mapAcquisitionWeek[bacteria][currentDate().substr(0,10)] = interm_map;
	mapAcquisitionWeekInd[bacteria][currentDate().substr(0,10)] = individualsAcq;
}

pair<map<string, int>, vector<string>> Model::findMeAcquisitionWeek(vector<Organism*> listOfI, string bacteria, vector<string> weekDates){
	/* Fonction qui retourne une map avec en clé le type PA/PE/ALL et en valeur le nombre d'acquisition */
	synchInd();
	synchPath();
	int taille = 0;
	map<string, int> res;
	pair<map<string, int>, vector<string>> result;
	res["PA"] = 0;
	res["PE"] = 0;
	res["ALL"] = 0;
	vector<string> individualAcquisition;
	if(!peopleAcquisition.empty())peopleAcquisition.clear();
	//les individus colonisés
	for(auto const& i:listOfI){
		//pour chaque individus on va chercher s'il a eu la bactérie 
		Individual* ind = dynamic_cast<Individual*>(i);
		string typeInd = ind->getStatus();
		bool acquisitionInd;
		if(firstAcqOnly){
			vector<string>::iterator itC = find(colonizedIndividuals[bacteria].begin(), colonizedIndividuals[bacteria].end(), ind->getCalc_ident());
			if(itC==colonizedIndividuals[bacteria].end()){
				acquisitionInd = acquisitionStatus(ind, weekDates, bacteria, false);
			}else{
				acquisitionInd = false;
			}
		}else{
			acquisitionInd = acquisitionStatus(ind, weekDates, bacteria, false);
		}
		if(acquisitionInd){
			taille = taille + 1;
			res[typeInd] = res[typeInd] + 1;
			individualAcquisition.push_back(ind->getCalc_ident());
		}
	}
	res["ALL"] = taille;
	result = make_pair(res, individualAcquisition);
	return result;
}

bool Model::acquisitionStatus(Individual* ind, vector<string> weekDates, string bacteria, bool accountPreviousSwab){
	/* Fonction qui retourne un booleen et vaut true si l'individu à eu une acquisition de bacteria pendant les weekDAtes et false sinon. Cette fonction rempli aussi les map colonizedIndividuals et peopleAcquisition */
	string firstDateInd = ind->getDt_in();
	string dateInit;
	dateInit = startDate.substr(0,10);

	//toutes ses dates
	vector<string> dateSwabs = ind->getAllSwabDates();
	sort(dateSwabs.begin(), dateSwabs.end());

	// on cherche s'il a deja eu la bacterie
	map<string, vector<string>> mapPosI = ind->getPositivePathogens();
	map<string, vector<string>>::iterator it = mapPosI.find(bacteria);
	bool acquisitionOfInd = false;
	if(it!=mapPosI.end()&& dateSwabs.size()>nbOfAcquisitionWeek){
		vector<string> dateBacteria = mapPosI[bacteria];
		sort(dateBacteria.begin(), dateBacteria.end());
		sort(weekDates.begin(), weekDates.end());
		vector <string> weekDatesBacteria;
		set_intersection(dateBacteria.begin(), dateBacteria.end(), weekDates.begin(), weekDates.end(),  back_inserter(weekDatesBacteria));
		if(!weekDatesBacteria.empty()){
			vector <string> :: iterator swabDateOfTheWeek = weekDatesBacteria.begin();
			while(!acquisitionOfInd && swabDateOfTheWeek!=weekDatesBacteria.end()){
				if((*swabDateOfTheWeek)==dateInit){
					//cas où c'est sa première date donc on est sure que ce n'est pas une acquisition
					++swabDateOfTheWeek;
				}else{
					vector<string> allPreviousSwab;
					vector<string>::reverse_iterator itDateSwabs = find(dateSwabs.rbegin(), dateSwabs.rend(), (*swabDateOfTheWeek));
					int itNb = 1;
					bool ok = true;
					++itDateSwabs;
					string previousDate;
					while(itNb < (nbOfAcquisitionWeek+1) && ok && itDateSwabs!=dateSwabs.rend()){
						previousDate = (*itDateSwabs);
						allPreviousSwab.push_back(previousDate);
						auto found = find(dateBacteria.rbegin(), dateBacteria.rend(), previousDate);
						if(found==dateBacteria.rend()){ 
							ok = true;
						}else{
							ok = false;
						}
						itNb++;
						++itDateSwabs;
					}
					if(ok && itNb==(nbOfAcquisitionWeek+1)){
						//il faut quand mm que les previousSwab soient dans la periode ou le PA ou le pE est présent si par exemple il sort et reviens on ne va pas prendre
						bool absenceP = absencePeriod(ind->getCalc_ident(),  allPreviousSwab[allPreviousSwab.size()-1], (*swabDateOfTheWeek));
						
						if(firstAcqOnly && !absenceP){
							//on va verif que c'est une vraie premiere acquisition. On regarde s'il y a une date antérieur à previousDate mais superieur à firstDateInd dans les dates de colonisation
						
							vector<string>::iterator foundPreviousCol = find(colonizedIndividuals[bacteria].begin(), colonizedIndividuals[bacteria].end(), ind->getCalc_ident());
							
							if(foundPreviousCol==colonizedIndividuals[bacteria].end()){
								acquisitionOfInd = true;
							}
							
						}else if(!absenceP){
							acquisitionOfInd = true;
						}
					}
					++swabDateOfTheWeek;
				}
			}
			if(firstAcqOnly){
				//il a eu la bacterie on rempli colonizedIndividuals que dans le cas d'une firstAcqOnly
				vector<string>::iterator itC = find(colonizedIndividuals[bacteria].begin(), colonizedIndividuals[bacteria].end(), ind->getCalc_ident());
				if(itC==colonizedIndividuals[bacteria].end()){
					colonizedIndividuals[bacteria].push_back(ind->getCalc_ident());
				}
			}
		}
	}
	if(firstAcqOnly && dateSwabs.size()<=nbOfAcquisitionWeek){
		if(!mapPosI[bacteria].empty()){
			vector<string>::iterator itC = find(colonizedIndividuals[bacteria].begin(), colonizedIndividuals[bacteria].end(), ind->getCalc_ident());
			if(itC==colonizedIndividuals[bacteria].end()){
				colonizedIndividuals[bacteria].push_back(ind->getCalc_ident());
			}
		}
	}	
	return(acquisitionOfInd);
}

map<string, map<string, int>> Model::getNumberOfAcquisition(){
	/* Fonction qui retourne le nombre d'acquisition pour chaque bacterie provenant du vecteur des pathogènes à analyser pour un moment donné */
	map<string, map<string, int>> res;
	boost::mpi::communicator* comm = repast::RepastProcess::instance()->getCommunicator();
	synchInd(); /* pour synchroniser les contacts dans les copies des ind */
	synchPath();
	vector<Organism*> listOfB;
	context.selectAgents(context.size(),listOfB,1);
	vector<Organism*> listOfIAll;
	context.selectAgents(context.size(),listOfIAll,0);
	if(!peopleAcquisition.empty())peopleAcquisition.clear();
	for(vector<string>::iterator it = allBacteriaToAnalyzed.begin(); it!=allBacteriaToAnalyzed.end(); it++){
		/*on cherche pour chaque bactérie que l'on doit analyser */
		int taille = 0;
		map<string, int> mapInterm;
		mapInterm["ALL"] = 0;
		mapInterm["PA"] = 0;
		mapInterm["PE"] = 0;
		string bacteriaToAnalyzed = (*it);
		vector<Organism*>::iterator itBacteria = find_if(listOfB.begin(), listOfB.end(), [bacteriaToAnalyzed](Organism* o )->bool {Bacteria* b = dynamic_cast<Bacteria*>(o); return b->getStringId()==bacteriaToAnalyzed;});
		if(itBacteria!=listOfB.end()){
			/* on la récupère dans le context */
			Bacteria* bacteria = dynamic_cast<Bacteria*>((*itBacteria));
			if(rank == bacteria->getId().currentRank()){
				/*on se place dans le rank de la bactérie*/
				if(!bacteria->getActualPositiveIndividuals().empty()){						
					for(auto const& individual:bacteria->getActualPositiveIndividuals()){
						auto ifFindAllPeopleAcquisition = find(allPeopleAcquisition.begin(), allPeopleAcquisition.end(), individual);
						if(!firstAcqOnly  || (firstAcqOnly && ifFindAllPeopleAcquisition==allPeopleAcquisition.end())){
							/*Pour chaque individu actuellement porteur de la bacterie on va le récupéré dans le context */
							Individual* ind;
							vector<Organism*>::iterator itIndName = find_if(listOfIAll.begin(), listOfIAll.end(),[individual](Organism* org) -> bool {Individual* itIndName = dynamic_cast<Individual*>(org);return individual == itIndName->getCalc_ident();} );
							if(itIndName!=listOfIAll.end()){
								ind = dynamic_cast<Individual*>(*itIndName);
							}else{
								/* s'il n'est pas dans le context c'est qu'il est déjà sortie et c'est un pb */
								cout <<rank<< "ooooop l'ind n'est pas dans dans la liste mais il est dans la bacterie " << individual << endl;
							}
							map<string,vector<string>> mapInd = ind->getPositivePathogens();
							vector<string> allDates = mapInd[bacteria->getStringId()];
							vector<string> indDates = ind->getAllSwabDates();
							string typeInd = ind->getStatus();
							/*on vérif qu'on est au bon jour pour l'acquisition (donc le jour où il a eu la bacterie)*/
							if(indDates.size()>nbOfAcquisitionWeek && allDates.size()!=0 && currentDate().substr(0,10)==allDates[allDates.size()-1]){
								/* premier cas il n'a qu'une date avec cette bacterie mais déja plus de deux dates c'est donc une première vraie acquisition */
								if(allDates.size()==1){
									if(nbOfAcquisitionWeek==1){
										taille=taille+1;
										mapInterm[typeInd] = mapInterm[typeInd] + 1;
										if(debugMode)cout << rank << " l'individu " << ind->getCalc_ident() << " a eu une acquisition le " << currentDate().substr(0,10) << endl;
										peopleAcquisition.push_back(ind->getCalc_ident());
										allPeopleAcquisition.push_back(ind->getCalc_ident());
									}else{
										vector<string>::reverse_iterator itPrevious = find(indDates.rbegin(), indDates.rend(), currentDate().substr(0,10));
										++itPrevious;
										int compt = 0;
										while(itPrevious!=indDates.rend() && compt!=nbOfAcquisitionWeek){
											compt++;
											++itPrevious;
										}
										if(compt==nbOfAcquisitionWeek){
											taille=taille+1;
											mapInterm[typeInd] = mapInterm[typeInd] + 1;
											if(debugMode)cout << rank << " l'individu " << ind->getCalc_ident() << " a eu une acquisition le " << currentDate().substr(0,10) << endl;
											peopleAcquisition.push_back(ind->getCalc_ident());
											allPeopleAcquisition.push_back(ind->getCalc_ident());
										}//sinon c'est qu'on est dans un cas limite par exemple de trois prlvt avec une acquisition du style --+ et qu'en fait on a -+-
									}
									
								}else if (allDates.size()>1 && nbOfAcquisitionWeek!=0){
									//on prend en compte les réacquisitions
									//il a eu plusieurs dates avec cette bacterie il faut donc vérifié que c'est une acquisition et que donc il n'a pas eu la bacterie au prlvt d'avant
									int itNb = 1;
									bool ok = true;
									while(itNb != (nbOfAcquisitionWeek+1) && ok){
										string lastDateAll = indDates[indDates.size()-(1+itNb)];
										auto found = find(allDates.rbegin(), allDates.rend(), lastDateAll);
										if(found==allDates.rend()){
											ok = true;
										}else{
											ok = false;
										}
										itNb++;
									}
									if(ok){
										taille=taille+1;
										mapInterm[typeInd] = mapInterm[typeInd] + 1;
										peopleAcquisition.push_back(ind->getCalc_ident());
										allPeopleAcquisition.push_back(ind->getCalc_ident());
									}
								}
							}
								
						}
					}
				}
			}
			/* comme on était dans le rank de la bactérie on transfert la valeur aux autres ranks */
			boost::mpi::broadcast(*comm,taille,bacteria->getId().currentRank());
			boost::mpi::broadcast(*comm,mapInterm,bacteria->getId().currentRank());
		}
		mapInterm["ALL"] = taille;
		res.insert(make_pair(bacteriaToAnalyzed, mapInterm));
	}
	return res;
}

map<string, vector<string>> Model::findMePreviousNegIndividual(string date, string pathogen, int nbOfWeek){
	/* Fonction qui recherche les individus qui étaient négatif pour pathogen de date à date-nbOfWeek. Utiliser dans les acquisitions */
	synchInd();
	synchPath();
	map<string, vector<string>> res;
	map<string, vector<string>> pos;
	vector<string> vectInterm;
	res["ALL"] = vectInterm;
	res["PA"] = vectInterm;
	res["PE"] = vectInterm;
	time_t currentTime = stringToDate(date);
	time_t firstDay = currentTime - (604800*nbOfWeek);
	if(firstDay < stringToDate(startDate)){
		firstDay = stringToDate(startDate.substr(0,10));
	}
	//récup les individus
	vector<Organism*> listOfI;
	context.selectAgents(context.size(), listOfI,0);
	//ajout des gens du movingPeople qui etait présent la semaine dernière
	vector<Individual*> addOldPeople;
	for(auto const& oldPeople:movingPeople){
		if(stringToDateWithHour(oldPeople->getDt_out())>=firstDay){
			addOldPeople.push_back(oldPeople);
		}
	}
	listOfI.insert(listOfI.end(), addOldPeople.begin(), addOldPeople.end());
	vector<string> weekDates;
	//récup toutes les dates de la semaine d'avant
	for(time_t i(firstDay); i < currentTime; i=i+time_t(86400)){
		weekDates.push_back(dateToString(i).substr(0,10));
	}
	
	int c = 0;
	for(auto const& individual:listOfI){
		Individual* ind = dynamic_cast<Individual*>(individual);
		vector<string> swabDateOfI = ind->getAllSwabDates();
		map<string, vector<string>> mapPosI = ind->getPositivePathogens();
		sort(swabDateOfI.begin(), swabDateOfI.end());
		sort(weekDates.begin(), weekDates.end());
		vector <string> common;
		set_intersection(swabDateOfI.begin(), swabDateOfI.end(), weekDates.begin(), weekDates.end(),  back_inserter(common));
		//si common est empty alors il n'a pas été prlvt dans les deux dernières semaines donc on ne s'en occupe pas
		if(!common.empty()){
			
			//dans common il y a ses prlvt qui sont dans les deux semaines précédentes. On veut voir s'il est négatif du coup
			vector<string> datePathogen = mapPosI[pathogen];
			
			sort(datePathogen.begin(), datePathogen.end());
			sort(common.begin(), common.end());
			vector<string> commonPositive;
			set_intersection(common.begin(), common.end(), datePathogen.begin(), datePathogen.end(),  back_inserter(commonPositive));
			
			//si commonPositive est empty alors il n'a pas de prlvt positif donc il est négatif
			if(commonPositive.empty()){
				res["ALL"].push_back(ind->getCalc_ident());
				if(ind->getStatus()=="PA"){
					res["PA"].push_back(ind->getCalc_ident());
				}else{
					res["PE"].push_back(ind->getCalc_ident());
				}
				c++;
			}else{
				pos["ALL"].push_back(ind->getCalc_ident());
				if(ind->getStatus()=="PA"){
					pos["PA"].push_back(ind->getCalc_ident());
				}else{
					pos["PE"].push_back(ind->getCalc_ident());
				}
			}
		}
	}
	return(res);
}

void Model::buildSecondaryCases(){
	if(rank==0){
		string sc = props->getProperty("addN");
		for(auto const& b:allBacteriaToAnalyzed){
			string filename = "output/secondaryCases/secondaryCases"+ sc +"_SIM" + numberOfSim + "_" + b + ".csv";
			std::ofstream myCSV;
			myCSV.open(filename);
			cout << "filename secondary cases  " << filename << endl;
			myCSV << "Index;Date;nb;cases";
			myCSV << "\r" << endl;
			for(auto const& [key, val]:index_second_cases){
				cout << "key " << key << endl;
				map<string, map<string, vector<string>>> result_map = val;
				auto it = result_map.find(b);
				if(it!=result_map.end()){
					cout << "b " << b << endl;
					map<string, vector<string>> result_map2 = result_map[b];
					for(auto const& [k, v]:result_map2){
						
						vector<string> all_ind = v;
						int nb = all_ind.size();
						cout << "k " << k  << " nb " << nb << endl;
						string all_ind_str = all_ind[0];
						if(nb>1){
							for(int i(1); i!=all_ind.size(); i++){
								cout << "i " << i << " all_ind[i] " << all_ind[i] << endl;
								all_ind_str = string(all_ind_str) + string("_") + string(all_ind[i]);
							}
						}
						
						myCSV<<key + ";" + k + ";" + to_string(nb) + ";" + all_ind_str + "\r" <<endl;
					}
				}
				
			}
			myCSV.close();
		}
	}
}

void Model::EndOfAcquisition(){
	/* Fonction qui termine de remplir l'acquistion à la fin de la simulation. J'avais dans l'idée de rajouter l'acquisition pour les jours supplémentaire si jamais pas une semaine complète mais je ne pense pas que ce soit une bonne idée dans le principe car dépend aussi des semaines précédentes donc risque de chevauchement avec d'autres chiffres. Pour l'instant il faut une semaine complète pour avoir une acquisition. */
	synchInd();
	synchPath();
	buildMeStatusByDay();
	buildSecondaryCases();
	time_t startDateTime = stringToDate(startDate.substr(0,10));
	time_t stopDateTime = stringToDate(stopDate.substr(0,10));
	int modulo = ((stopDateTime - startDateTime)/86400)%timeResp;
	if(modulo!=0){
		// time_t firstDay = stopDateTime - (modulo*86400);
		// time_t currentTime = stopDateTime;
		// récup les individus
		// vector<Organism*> listOfI;
		// context.selectAgents(context.size(), listOfI,0);
		// ajout des gens du movingPeople qui etait présent la semaine dernière
		// vector<Individual*> addOldPeople;
		// for(auto const& oldPeople:movingPeople){
			// if(stringToDateWithHour(oldPeople->getDt_out())>=firstDay){
				// il est partie en cours de route la semaine dernière mais était bien présent
				// addOldPeople.push_back(oldPeople);
			// }
		// }
		// listOfI.insert(listOfI.end(), addOldPeople.begin(), addOldPeople.end());
		// vector<string> weekDates;
		// récup toutes les dates de la semaine d'avant
		// for(time_t i(firstDay); i < currentTime; i=i+time_t(86400)){
			// weekDates.push_back(dateToString(i).substr(0,10));
		// }
		// if(rank==0)myCSVacquisition << stopDate.substr(0,10) + ";";
		
		// string peopleAC;
		// for(auto const& bacteria:allBacteriaToAnalyzed){
			// map<string, int> taille = findMeAcquisitionWeek(listOfI, bacteria, weekDates);
			// if(!peopleAcquisition.empty()){
				// peopleAC=peopleAcquisition[0];
				// for(int i(1); i!=peopleAcquisition.size(); i++){
					// peopleAC = string(peopleAC) + string("_") + string(peopleAcquisition[i]);
				// }
			// }
			// string nbOfAcValALL = to_string(taille["ALL"]);
			// string nbOfAcValPA = to_string(taille["PA"]);
			// string nbOfAcValPE = to_string(taille["PE"]);
			
			// if(rank==0)myCSVacquisition<< nbOfAcValALL + ";" + nbOfAcValPA + ";" + nbOfAcValPE + ";";
		// }
		// if(rank==0)myCSVacquisition<< peopleAC + ";";
		// if(rank==0)myCSVacquisition << "\r"<<endl ;		
	}
	/*
	if(gameStatus=="simu"){
		
		string FileNameFor = "output/coloAdmission/coloAdmSIMU"+ numberOfSim +".csv";
		std::ofstream myCSVcolo;
		myCSVcolo.open(FileNameFor);
		map<int, map<string, int>> nbOfColoAtAdmissionByWeek = buildMeNbOfColoAtAdmission();
		for(auto const& [key, val]:nbOfColoAtAdmissionByWeek){
			map<string, int> vVal = val;
			myCSVcolo<<to_string(key) + ";";
			for(auto const& [k, v]:vVal){
				myCSVcolo<<to_string(v)+ ";";
			}
			myCSVcolo <<endl;
		}
	}
	*/
	showMeOutputPrevAndAcq();
	cout << "That's all Folks! " << endl;
}

void Model::showMeAcquisition(){
	/* Fonction qui affiche les acquisitions par jour */
	synchInd();
	synchPath();
	map<string, map<string, int>> todaysMap = getNumberOfAcquisition();
	if(rank==0){
		cout<< "today " << currentDate() << " we have ";
		for(auto const& [key, val]:todaysMap){
			map<string, int> mapInterm = val;
			cout<< " the value: ";
			for(auto const& [k, v]:val){
				cout<< k << " and " << v << " for ";
				cout<< key << " and ";
			}
		}
		cout << endl;
	}
}
//

//fonctions relatives à la prévalence
int Model::nbOf(vector<Organism*> o, string cat, string ward){
	/* Fonction qui détermine le nombre d'individu de la catégorie cat et du service ward dans le vecteur o. Si ward=='tot' alors ne prend pas en compte de le service */
	int res = 0;
	for(auto const& elem:o){
		Individual* i = dynamic_cast<Individual*>(elem);
		if(cat == i->getStatus() && (ward=="tot" || i->getWard()==ward)){
			res = res +1;
		}
	}
	return res;
}

map<string, double> Model::getPrevalenceNow(string bacteria, string ward){
	/* Fonction qui calcule la prévalence au jour courrant */
	vector<Organism*> listOfI;
	context.selectAgents(context.size(), listOfI,0);
	vector<string> listOfIPrlvt = mapPrlvtALL[currentDate().substr(0,10)];
	vector <Organism*> colonizedIndividual;
	vector<Organism*> prlvtIndividual;
	for(auto const& i:listOfI){
		Individual* ind = dynamic_cast<Individual*>(i);
		vector<string>::iterator itIndObj = find(listOfIPrlvt.begin(), listOfIPrlvt.end(), ind->getCalc_ident());
		if(itIndObj!=listOfIPrlvt.end()){
			prlvtIndividual.push_back(i);
		}
		if(ward == "tot" || ind->getWard()==ward){
			map<string, vector<string>> mapBacteria = ind->getPositivePathogens();
			vector<string> allDateBacteria = mapBacteria[bacteria];
			vector<string> positivePath = ind->getActualPositivePathogens();
			vector<string>::iterator it = find(allDateBacteria.begin(), allDateBacteria.end(), currentDate().substr(0,10));
			if(it!=allDateBacteria.end()){
				colonizedIndividual.push_back(ind);
			}
		}
	}
	map <string, double> keys = returnMapPrevRes(colonizedIndividual, prlvtIndividual, ward, bacteria);
	return keys;
}

map<string, double> Model::getPrevalenceByWeeks(string bacteria, string ward){
	/* Fonction qui retourne la prévalence à la semaine */
	time_t currentTime = stringToDate(currentDate().substr(0,10));
	vector<Organism*> listOfI;
	context.selectAgents(context.size(), listOfI,0);
	time_t firstDay = currentTime - 604800;
	if(firstDay < stringToDate(startDate.substr(0,10))){
		firstDay = stringToDate(startDate.substr(0,10));
	}
	//cas de la fin
	if(currentDate().substr(0,10) == stopDate.substr(0,10)){
		time_t startDateTime = stringToDate(startDate.substr(0,10));
		time_t stopDateTime = stringToDate(stopDate.substr(0,10));
		int modulo = ((stopDateTime - startDateTime)/86400)%timeResp;
		if(modulo!=0){
			firstDay = stopDateTime - (modulo*86400);
		}
	}
	//ajout des gens du movingPeople qui etait présent la semaine dernière
	vector<Individual*> addOldPeople;
	for(auto const& oldPeople:movingPeople){
		if(stringToDateWithHour(oldPeople->getDt_out())>=firstDay){
			//il est partie en cours de route la semaine dernière mais était bien présent
			addOldPeople.push_back(oldPeople);
		}
	}
	listOfI.insert(listOfI.end(), addOldPeople.begin(), addOldPeople.end());
	//les individus qui ont été prélevés la semaine dernière
	vector<string> listOfIPrlvt;
	vector<string> weekDates;
	for(time_t i(firstDay); i<currentTime; i=i+time_t(86400)){
		string d = dateToString(i).substr(0,10);
		weekDates.push_back(d);
		listOfIPrlvt.insert(listOfIPrlvt.end(), mapPrlvtALL[d].begin(), mapPrlvtALL[d].end());
	}
	sort( listOfIPrlvt.begin(), listOfIPrlvt.end() );
	listOfIPrlvt.erase( unique( listOfIPrlvt.begin(), listOfIPrlvt.end() ), listOfIPrlvt.end() );
	vector<Organism*> prlvtIndividual;
	vector<Organism*> colonizedIndividual;
	//les individus colonisés
	for(auto const& i:listOfI){
		Individual* ind = dynamic_cast<Individual*>(i);
		vector<string>::iterator itIndObj = find(listOfIPrlvt.begin(), listOfIPrlvt.end(), ind->getCalc_ident());
		if(itIndObj!=listOfIPrlvt.end()){
			prlvtIndividual.push_back(i);
		}
		if(ward == "tot" || ind->getWard()==ward){
			map<string, vector<string>> mapPosI = ind->getPositivePathogens();
			map<string, vector<string>>::iterator it = mapPosI.find(bacteria);
			if(it!=mapPosI.end()){
				//il a eu la bactérie on regarde si il a été prlvt cette semaine là
				vector<string> dateBacteria = mapPosI[bacteria];
				sort(dateBacteria.begin(), dateBacteria.end());
				sort(weekDates.begin(), weekDates.end());
				vector <string> common;
				set_intersection(dateBacteria.begin(), dateBacteria.end(), weekDates.begin(), weekDates.end(),  back_inserter(common));
				if(!common.empty()){
					colonizedIndividual.push_back(ind);
				}
			}
		}
	}
	//les individus colonisé de la semaine 
	map <string, double> keys = returnMapPrevRes(colonizedIndividual, prlvtIndividual, ward, bacteria);
	return keys;
}

map<string, double> Model::returnMapPrevRes(vector<Organism*>colonizedIndividual, vector<Organism*> listOfIPrel, string ward, string path){
	/* Fonction qui prend en argument un vecteur d'individus colonisé, un vecteur d'individu prélevé et le service dans lequel on veut calculer la prévalence. Si le service ward == 'tot' alors on ne prend pas en compte le service. Retourne une map avec en clé le type (PA/PE/tot) et en valeur la prévalence. Cette fonction rempli aussi mapPrevalenceWeek qui prend en clé la date courrante et en valeur une map avec en clé le nombre d'individu colonisé au total et en valeur le nombre d'individu prélevé */
	int nbOfcolTot = colonizedIndividual.size();
	int nbOfcolPA = nbOf(colonizedIndividual, "PA", ward);
	int nbOfcolPE = nbOf(colonizedIndividual, "PE", ward);
	int nbOfPA = nbOf(listOfIPrel, "PA", ward);
	int nbOfPE = nbOf(listOfIPrel, "PE", ward);
	double resTot = 0;
	double resPA = 0;
	double resPE = 0;
	if(nbOfcolTot!=0 && listOfIPrel.size()!=0){
		resTot = (double)nbOfcolTot/listOfIPrel.size();
	}
	if(nbOfcolPA!=0 && nbOfPA!=0){
		resPA = (double)nbOfcolPA / nbOfPA;
	}
	if(nbOfcolPE!=0 && nbOfPE!=0){
		resPE = (double)nbOfcolPE / nbOfPE;
	}
	map <string, double> keys;
	keys.insert(make_pair("Tot", resTot));
	keys.insert(make_pair("PA", resPA));
	keys.insert(make_pair("PE", resPE));
	map<string, pair<int, int>> map_interm;
	// map_interm[nbOfcolTot] = listOfIPrel.size();
	string tPA;
	string tPE;
	if(ward=="tot"){
		tPA = "PA";
		tPE = "PE";
	}else{
		tPA = "PA_" + ward;
		tPE = "PE_" + ward;
	}
	map_interm[tPA] = make_pair(nbOfcolPA, nbOfPA);
	map_interm[tPE] = make_pair(nbOfcolPE, nbOfPE);
	mapPrevalenceWeek[path][currentDate().substr(0,10)] = map_interm;
	return keys;
}

map<string, map<string, pair<int, int>>> Model::changeMapPrevalence(map<string, pair<int, int>> input_map){
	map<string, map<string, pair<int, int>>> res;
	for(auto const& [key, val]:input_map){
		vector<string> tempo;
		boost::split(tempo, key, boost::is_any_of("_"));
		res[tempo[0]][tempo[1]] = val;
	}
	return res;
}

void Model::buildMePrevalence(){
	/* La fonction qui ajoute la prévalence dans un fichier csv */
	synchInd();
	synchPath();
	if(rank==0 && returnPrev){
		//on travail dans le rank 0 car on rempli qu'un seul fichier et pas besoin de le remplir en parallèle car pas très long
		time_t stDate = stringToDate(startDate);
		tm* time_st = localtime(&stDate);
		int wday = time_st->tm_wday;
		time_t currentTime = stringToDate(currentDate());
		tm* time_out = localtime(&currentTime);
		map<string, double> resMap;
		//on rempli le header le premier jour
		if(currentDate().substr(0,10)==startDate.substr(0,10)){
			myCSVprevalence<<"Date;bacteria;ward;type;value;colonized;swabbed";
			myCSVprevalence<<"\r"<<endl;
		}	
		//pour chaque bactérie a analyser on va calculer la prévalence. Cette prévalence peut être calculé par ward par l'option prevalenceByWard
		for(auto const& bacteria:allBacteriaToAnalyzed){
			if(prevalenceByWard){
				for(auto const& w:wards){
					//pour chaque ward on calcule la prévalence le premier jour puis les autres semaines dans la map resMap
					if(currentDate().substr(0,10)==startDate.substr(0,10) || timeResp!=7){
						resMap = getPrevalenceNow(bacteria, w);
					}else if (timeResp==7){
						resMap = getPrevalenceByWeeks(bacteria, w);
					}
					map<string, map<string, pair<int, int>>> mapPWWard = changeMapPrevalence(mapPrevalenceWeek[bacteria][currentDate().substr(0,10)]);
					map<string, pair<int, int>> mapPWWard2 = mapPWWard[w];
					int colonizedTOT = mapPWWard2["PA"].first + mapPWWard2["PE"].first;
					int swabbedTOT = mapPWWard2["PA"].second + mapPWWard2["PE"].second;
					mapPWWard2["Tot"] = make_pair(colonizedTOT, swabbedTOT);
					if(resMap.empty()){
						//si la map est vide on met des zero partout
						double nb = 0;
						myCSVprevalence<<currentDate().substr(0,10) + ";" + bacteria + ";" + w + ";" + "Tot" + ";" + to_string(nb) + ";" + to_string(mapPWWard2["Tot"].first) + ";" + to_string(mapPWWard2["Tot"].second) + "\r" <<endl;
						myCSVprevalence<<currentDate().substr(0,10) + ";" + bacteria + ";" + w + ";" + "PA" + ";" + to_string(nb)+ ";" + to_string(mapPWWard2["PA"].first) + ";" + to_string(mapPWWard2["PA"].second) + "\r"<<endl;
						myCSVprevalence<<currentDate().substr(0,10) + ";" + bacteria + ";" + w + ";" + "PE" + ";" + to_string(nb)+ ";" + to_string(mapPWWard2["PE"].first) + ";" + to_string(mapPWWard2["PE"].second) + "\r"<<endl;
					}else{
						//sinon on ajoute dans le csv la prévalence
						for(auto const& [key,val]:resMap){
							myCSVprevalence<<currentDate().substr(0,10) + ";" + bacteria + ";" + w + ";" + key + ";" + to_string(val) + ";" + to_string(mapPWWard2[key].first) + ";" + to_string(mapPWWard2[key].second) + "\r"<<endl;
						}
					}
				}
			}else{
				//on calcule la prévalence le premier jour puis toutes les semaines
				if(currentDate().substr(0,10)==startDate.substr(0,10) || timeResp!=7){
					resMap = getPrevalenceNow(bacteria, "tot");
				}else if(timeResp==7){
					resMap = getPrevalenceByWeeks(bacteria, "tot");
				}
				map<string, pair<int, int>> mapPWWard = mapPrevalenceWeek[bacteria][currentDate().substr(0,10)];
				int colonizedTOT = mapPWWard["PA"].first + mapPWWard["PE"].first;
				int swabbedTOT = mapPWWard["PA"].second + mapPWWard["PE"].second;
				mapPWWard["Tot"] = make_pair(colonizedTOT, swabbedTOT);
				if(resMap.empty()){
					double nb = 0;
					myCSVprevalence<<currentDate().substr(0,10) + ";" + bacteria + ";" + "Tot" + ";" + to_string(nb) + ";" + to_string(mapPWWard["Tot"].first) + ";" + to_string(mapPWWard["Tot"].second) + "\r" <<endl;
					myCSVprevalence<<currentDate().substr(0,10) + ";" + bacteria +  ";" + "PA" + ";" + to_string(nb) + ";" + to_string(mapPWWard["PA"].first) + ";" + to_string(mapPWWard["PA"].second) + "\r"<<endl;
					myCSVprevalence<<currentDate().substr(0,10) + ";" + bacteria + ";" + "PE" + ";" + to_string(nb) + ";" + to_string(mapPWWard["PE"].first) + ";" + to_string(mapPWWard["PE"].second) + "\r"<<endl;
				}else{
					for(auto const& [key,val]:resMap){
						myCSVprevalence<<currentDate().substr(0,10) + ";" + bacteria + ";" + key + ";" + to_string(val) + ";" + to_string(mapPWWard[key].first) + ";" + to_string(mapPWWard[key].second) + "\r"<<endl;
					}
				}
			}
		}		
	}
}
//

void Model::choseFixedColonizedIndividual(){
	boost::mpi::communicator* comm = repast::RepastProcess::instance()->getCommunicator();
	chosenFixedColonizedStaff.clear();
	chosenFixedColonizedStaffThisWeek.clear();
	chosenAdmittedPatient.clear();
	string who;
	if(fixedColonizationStatus=="PA"){
		who="PA";
	}else if(fixedColonizationStatus=="PE"){
		who="PE";
	}else if(fixedColonizationStatus=="both"){
		who="both";
	}else if(fixedColonizationStatus=="either"){
		if(rank==0){
			IntUniformGenerator chooseProbaContener = Random::instance()->createUniIntGenerator(0,1);
			int randomNb = chooseProbaContener.next();
			who = randomNb == 1 ? "PA":"PE";
		}
		boost::mpi::broadcast(*comm, who, 0);
	}
	string date = currentDate().substr(0,10);
	time_t firstTimeWeek = stringToDateWithHour(date);
	time_t lastTimeWeek = firstTimeWeek + 604800;
	if(lastTimeWeek > stringToDateWithHour(stopDate.substr(0,10))){
		lastTimeWeek = stringToDateWithHour(stopDate.substr(0,10));
	}
	if(firstTimeWeek==stringToDateWithHour(startDate.substr(0,10))){
		firstTimeWeek = firstTimeWeek + time_t(86400);
	}
	if(lastTimeWeek>firstTimeWeek){
		vector<string> allImmun;
		for(auto const& [key, val]:ImmunizedPeople){
			vector<string> stVal = val;
			allImmun.insert(allImmun.end(),stVal.begin(), stVal.end());
		}
		if(rank==0)cout << " c'est partie pour fixedcolo on est avec " << who << " cette semaine qui démarre le " << date << endl;
		if(who=="PA" || who=="both"){
			//on choisi un patient admis qui sera colonisé
			vector<string> indPAadmitted;
			for(time_t i(firstTimeWeek); i<lastTimeWeek; i=i+time_t(86400)){
				string d = dateToString(i).substr(0,10);
				std::map<std::string, std::vector<std::string>> admitted = mapAdmission[d];
				
				for(auto const& [key, val]:admitted){
					std::vector<std::string> info = val;
					auto it = find(allImmun.begin(), allImmun.end(), key);
					if(info[1]=="PA" && it==allImmun.end()){
						indPAadmitted.push_back(key);
					}
				}
			}
			if(indPAadmitted.size()==fixedColonizationNb || indPAadmitted.size()>fixedColonizationNb){
				//maintenant on va en choisir fixedColonizationNb
				
				if(rank==0){
					IntUniformGenerator chooseProbaContener = Random::instance()->createUniIntGenerator(0,indPAadmitted.size()-1);
					vector<int> chosenNb;
					for(int i(0); i<fixedColonizationNb;i++){
						int randomNb = chooseProbaContener.next();
						vector<int>::iterator it = find(chosenNb.begin(), chosenNb.end(), randomNb);
						while(it!=chosenNb.end()){
							randomNb = chooseProbaContener.next();
							it = find(chosenNb.begin(), chosenNb.end(), randomNb);
						}
						chosenNb.push_back(randomNb);
						chosenAdmittedPatient.push_back(indPAadmitted[randomNb]);
						cout << " dans fixed " << indPAadmitted[randomNb] << " a ete choisi " << endl;
					}
				}
				boost::mpi::broadcast(*comm, chosenAdmittedPatient, 0);
			}
			
		}else if(who=="PE" || who=="both"){
			vector<string> datesWeek;
			for(time_t i(firstTimeWeek); i<lastTimeWeek; i=i+time_t(86400)){
				string d = dateToString(i).substr(0,10);
				datesWeek.push_back(d);
			}
			//on choisi un staff présent qui sera colonisé
			//1) on recup tout les individus présent
			vector<Organism*> individualsContext;
			context.selectAgents(context.size(),individualsContext,0);
			vector<Individual*> allstaff;
			for(auto const& elem:individualsContext){
				Individual* staff = dynamic_cast<Individual*>((elem));
				if(staff->getStatus()=="PE"){
					
					//on va verif qu'il ne part pas cette semaine là
					string outDate = staff->getDt_out();
					auto it = find(datesWeek.begin(), datesWeek.end(), outDate);
					auto it2 = find(allImmun.begin(), allImmun.end(), staff->getCalc_ident());
					if(it==datesWeek.end() && it2==allImmun.end() && staff->getPathogensStatus().empty()){
						allstaff.push_back(staff);
					}
				}
			}
			if(allstaff.size()==fixedColonizationNb || allstaff.size()>fixedColonizationNb){
				
				if(rank==0){
					IntUniformGenerator chooseProbaContener = Random::instance()->createUniIntGenerator(0,allstaff.size()-1);
					IntUniformGenerator chooseDateContener = Random::instance()->createUniIntGenerator(0,datesWeek.size()-1);
					vector<int> chosenNb;
					for(int i(0); i<fixedColonizationNb;i++){
						int randomNb = chooseProbaContener.next();
						vector<int>::iterator it = find(chosenNb.begin(), chosenNb.end(), randomNb);
						while(it!=chosenNb.end()){
							randomNb = chooseProbaContener.next();
							it = find(chosenNb.begin(), chosenNb.end(), randomNb);
						}
						chosenNb.push_back(randomNb);
						//maintenant on choisi sa date
						int nbDate = chooseDateContener.next();
						chosenFixedColonizedStaff[datesWeek[nbDate]].push_back(allstaff[randomNb]->getCalc_ident());
						chosenFixedColonizedStaffThisWeek.push_back(allstaff[randomNb]->getCalc_ident());
						cout << " dans fixed " << allstaff[randomNb]->getCalc_ident() << " a ete choisi pour le " << datesWeek[nbDate] << endl;
					}
				}
				boost::mpi::broadcast(*comm, chosenFixedColonizedStaff, 0);
				boost::mpi::broadcast(*comm, chosenFixedColonizedStaffThisWeek, 0);
			}	
		}
	}
	
	
}

//fonctions relatives aux admissions
void Model::checkAdmission(){
	if(debugMode)cout<< rank << " Fonction: checkAdmission " << "checkAdmission ON " << currentDate() << endl;
	/* Fonction qui vérifie si un individus doit entrer ou non dans la simulation */
	synchInd();
	synchPath();
	boost::mpi::communicator* comm = repast::RepastProcess::instance()->getCommunicator();
	string date = currentDate().substr(0,10);
	int previousidI = indCounts[rank];
	int idI = addAdmission(previousidI);
	if(debugMode)cout<< rank << " Fonction: checkAdmission " << " admission " << previousidI << " and " << idI << endl;
	vector<int> previousindCounts;
	boost::mpi::all_gather(*comm, idI, indCounts);
	boost::mpi::all_gather(*comm, previousidI, previousindCounts);
	requestOrganism(previousindCounts, 0, "SET_1");	
	//maintenant on va ajouter les colo
	synchInd();
	if(gameStatus=="simu" && !newAdmission[currentDate().substr(0,10)].empty())addColoAtAdmission(newAdmission[currentDate().substr(0,10)]);
	
	if(props->getProperty("admissionImmun")=="yes" && !newAdmission[currentDate().substr(0,10)].empty()){
		double percentagePA = stod(props->getProperty("percentageFirstImmunizedPA"));
		double percentagePE = stod(props->getProperty("percentageFirstImmunizedPE"));
		vector<string> indAdmitted = newAdmission[currentDate().substr(0,10)];
		vector<Organism*> IndividualContext;
		context.selectAgents(context.size(),IndividualContext,0);
		vector<Individual*>indAdmittedObj;
		map<string, vector<string>> mapA;
		for(auto const& indSTR:indAdmitted){
			vector<Organism*>::iterator it = find_if(IndividualContext.begin(), IndividualContext.end(),[indSTR](Organism* o)->bool {Individual* i = dynamic_cast<Individual*>(o); return i->getCalc_ident()==indSTR;});
			if(it!=IndividualContext.end()){
				Individual* ind = dynamic_cast<Individual*>((*it));
				indAdmittedObj.push_back(ind);
				mapA[ind->getStatus()].push_back(ind->getCalc_ident());
			}
		}
		vector<string> allChosen;
		if(rank==0){
			vector<string> vpa;
			vector<string> vpe;
			int nbPA = 0;
			int nbPE = 0;
			if(mapA["PA"].size()!=0){
				nbPA = (int)(percentagePA * mapA["PA"].size());
				vpa = findRandomInAList(nbPA, mapA["PA"]);
			}
			if(mapA["PE"].size()!=0){
				nbPE = (int)(percentagePE * mapA["PE"].size());
				vpe = findRandomInAList(nbPE, mapA["PE"]);
			}
			
			vpa.insert(vpa.end(), vpe.begin(), vpe.end());
			allChosen = vpa;
			// cout << " aujourd'hui il y a des immunisations, et " << nbPA  << " PA et " << nbPE << " sont choisi dans les " << mapA["PA"].size() << " pa " << mapA["PE"].size() << " pe " << endl;
		}
		boost::mpi::broadcast(*comm, allChosen, 0);
		for(auto const& indObj:indAdmittedObj){
			auto it = find(allChosen.begin(), allChosen.end(), indObj->getCalc_ident());
			if(it!=allChosen.end()){
				for(auto const& bacteria:allBacteriaToAnalyzed){
					changeIndividualPathogenStatus(indObj, bacteria, "immunization", currentDate().substr(0,10));
					ImmunizedPeople[bacteria].push_back(indObj->getCalc_ident());
				}
			}
		}
		
		// for(auto const& indSTR:indAdmitted){
			// vector<Organism*>::iterator it = find_if(IndividualContext.begin(), IndividualContext.end(),[indSTR](Organism* o)->bool {Individual* i = dynamic_cast<Individual*>(o); return i->getCalc_ident()==indSTR;});
			// if(it!=IndividualContext.end()){
				// Individual* ind = dynamic_cast<Individual*>((*it));
				// for(auto const& bacteria:allBacteriaToAnalyzed){
					// changeIndividualPathogenStatus(ind, bacteria, "immunization", currentDate().substr(0,10));
					// ImmunizedPeople[bacteria].push_back(ind->getCalc_ident());
				// }
			// }
		// }
	}
}

int Model::addAdmission(int previousidI){
	/* Fonction qui ajoute un individus dans la simulation grace aux données des admissions ou qui le réintègre s'il a déjà eu une première admission grâce au vector movingPeople qui permet de savoir quels individus sont partis au cours de la simulation */
	string date = currentDate().substr(0,10);
	vector<Organism*> individualsContext;
	context.selectAgents(context.size(),individualsContext,0);
	vector<string> t;
	int idI = indCounts[rank];
	boost::mpi::communicator* comm = repast::RepastProcess::instance()->getCommunicator();
	vector<string> newIndividual;
	for(auto const& [keyDate, valIndividuals]:mapAdmission){
		if(keyDate == date){
			for(auto const& [nameIndLine, vectorIndLine]: valIndividuals){
				bool changeWard = false;
				/* on va ajouter l'individu */
				/* d'abord on vérif qu'il n'est pas déjà présent */
				std::vector<Organism*>::iterator iterInd = find_if(individualsContext.begin(),individualsContext.end(), [nameIndLine](Organism* in) -> bool { Individual* in2 = dynamic_cast<Individual*>(in); return nameIndLine == in2->getCalc_ident();});
				if(iterInd== individualsContext.end()){
					newIndividual.push_back(nameIndLine);
					/* il est pas dedans */
					/* On verifie qu'il n'a pas déjà été crée et qu'en réalité il revient */
					std::vector<Individual*>::iterator iterOld = find_if(movingPeople.begin(),movingPeople.end(), [nameIndLine](Individual* in) -> bool { return nameIndLine == in->getCalc_ident();});
					if(iterOld!= movingPeople.end()){
						Individual* indi = (*iterOld);
						/* il est dans le movingpeople donc on le rajoute dans le context */
						if(rank == indi->getId().startingRank()){
							if(indi->getStatus()=="PA"){
								Patient* p1 = dynamic_cast<Patient*>((*iterOld));
								Patient* p2 = new Patient (*p1);
								p2->setDt_in(vectorIndLine[2]);
								p2->setDt_out(vectorIndLine[3]);
								//cas particulier du changement de service
								if(vectorIndLine[4]!=p2->getWard()){
									p2->setWard(vectorIndLine[4]);
									context.addAgent(p2);
									vector<string>::iterator itWard = find(wards.begin(), wards.end(),vectorIndLine[4]);
									int newRank = distance(wards.begin(), itWard);
									repast::RepastProcess::instance()->moveAgent(p2->getId(), newRank);
									changeWard = true;										
								}else{
									context.addAgent(p2);
								}
								mapId[p2->getCalc_ident()] = p2->getId();
							}else{
								Staff* s1 = dynamic_cast<Staff*>((*iterOld));
								Staff* s2 = new Staff(*s1);
								s2->setDt_in(vectorIndLine[2]);
								s2->setDt_out(vectorIndLine[3]);
								if(vectorIndLine[4]!=s2->getWard()){
									s2->setWard(vectorIndLine[4]);
									context.addAgent(s2);
									vector<string>::iterator itWard = find(wards.begin(), wards.end(),vectorIndLine[4]);
									int newRank = distance(wards.begin(), itWard);
									repast::RepastProcess::instance()->moveAgent(s2->getId(), newRank);
									changeWard = true;
								}else{
									context.addAgent(s2);
								}
								mapId[s2->getCalc_ident()] = s2->getId();
							}
						}
						boost::mpi::broadcast(*comm, mapId, indi->getId().startingRank());
						/* on le recopie dans les autres ward */
						AgentRequest request(rank);
						if (rank!=indi->getId().startingRank()){
							request.addRequest(indi->getId());	
						}
						repast::RepastProcess::instance()->requestAgents<Organism, IndividualPackage, OrganismPackageProvider, OrganismPackageReceiver>(context, request, *provider, *receiver, *receiver, "SET_1");
						/* et on l'enlève du movingPeople */
						delete (*iterOld);
						movingPeople.erase(iterOld);					
					}else{
						int r = -1;
						for(int w(0); w<wards.size(); w++){
							if(wards[w]==vectorIndLine[4]){
								r = w;
							}
						}
						if(r==-1){
							cout << " ooops pb de ward dans les nouveaux admis " << endl;
						}
						if(rank == r){
							idI ++;
							if(vectorIndLine[1]=="PA"){
								repast::AgentId id(idI, rank, 0); 
								id.currentRank(rank);
								Patient* patient = new Patient(id, vectorIndLine);
								context.addAgent(patient);
								mapId.insert(make_pair(vectorIndLine[0], id));
							}else if (vectorIndLine[1]=="PE"){
								repast::AgentId id(idI, rank, 0);
								id.currentRank(rank);
								Staff* staff = new Staff(id, vectorIndLine);			
								context.addAgent(staff);
								mapId.insert(make_pair(vectorIndLine[0], id));
							}else{
								cout << "oooops pb dans le status de l'ind " << endl;
							}
							if(debugMode)cout<< rank << " Fonction: checkAdmission " <<" i'm coming! " << vectorIndLine[0] << endl;
						}
						boost::mpi::broadcast(*comm, mapId, r);
					}
				}else{
					/* il est deja dans le context */
					/* soit il revient mais on ne connait pas sa premiere date de sortie donc on va remplacer par la seconde date de sortie */
					Individual* indAdm = dynamic_cast<Individual*>((*iterInd));
					indAdm->setDt_out(vectorIndLine[3]);
					//on verif qu'il ne change pas non plus de service
					if(vectorIndLine[4]!=indAdm->getWard()){
						vector<string>::iterator itWard = find(wards.begin(), wards.end(),vectorIndLine[4]);
						int newRank = distance(wards.begin(), itWard);
						repast::RepastProcess::instance()->moveAgent(indAdm->getId(), newRank);
						changeWard = true;
					}
					mapId[indAdm->getCalc_ident()] = indAdm->getId();
				}
				if(changeWard){				
					repast::RepastProcess::instance()->synchronizeAgentStatus<Organism, IndividualPackage, OrganismPackageProvider, OrganismPackageReceiver>(context, *provider, *receiver, *receiver);
				}
			}
		}
	}
	for(auto const& elem:newIndividual){
		newAdmission[currentDate().substr(0,10)].push_back(elem);
	}
	return idI;
}

void Model::addColoAtAdmission(vector<string> newIndividual){
	/* Fonction qui prend en argument un vecteur de nouveaux individus et détermine si ces individus sont colonisés à l'admission */
	boost::mpi::communicator* comm = repast::RepastProcess::instance()->getCommunicator();
	vector<Organism*> individualsContextForNewCol;
	context.selectAgents(context.size(),individualsContextForNewCol,0);
	map<string, map<string, string>> mapIndNewCol;
	//1) on va choisir si l'individus doit être colonisé à l'admission + une date de décolo mais comme c'est très random et pour éviter le problème de communication qui provoque des perturbations on va se mettre dans le rank 0!!!
	vector<string> alreadyImmunizedIndividual;
	if(immunizationDuration=="Inf"){
		for(auto const& [key, val]:ImmunizedPeople){
			vector<string> immunizedP = val;
			for(auto const& elem:newIndividual){
				auto it = find(immunizedP.begin(), immunizedP.end(), elem);
				if(it!=immunizedP.end()){
					alreadyImmunizedIndividual.push_back(elem);
				}
			}
		}
	}
	if(!fixedColonization){
		if(rank==0){
			//A cause des random je dois me mettre dans un seul rank ici pour avoir la même date de décolo
			for(auto const& elem:individualsContextForNewCol){
				Individual* indObj = dynamic_cast<Individual*>(elem);
				vector<string>::iterator itFoundInd = find(newIndividual.begin(), newIndividual.end(), indObj->getCalc_ident());
				auto itImmun = find(alreadyImmunizedIndividual.begin(), alreadyImmunizedIndividual.end(), indObj->getCalc_ident());
				if(itFoundInd!=newIndividual.end() && itImmun==alreadyImmunizedIndividual.end()){
					map<string, string> newB = colonizedAtAdmission(indObj);
					if(!newB.empty())mapIndNewCol[indObj->getCalc_ident()] = newB;
				}
			}
		}
		boost::mpi::broadcast(*comm, mapIndNewCol, 0);
	}else{
		for(auto const& bact:allBacteriaToAnalyzed){
			for(auto const& elem:chosenAdmittedPatient){
				auto it = find(newIndividual.begin(), newIndividual.end(), elem);
				if(it!=newIndividual.end()){
					cout << " attention " <<  elem << " va arrivé colonisé le " << currentDate() << endl;
					map<string, string> newB;
					newB[bact] = "";
					mapIndNewCol[elem] = newB;
				}
				
			}
		}
	}
	
	for(auto const& [key, val]:mapIndNewCol){
		coloAtAdmi[currentDate().substr(0,10)].push_back(key);
	}
	//2) on va rajouter cette date dans l'individus et l'individus dans la bacterie + ajouter un nouveau prlvt (ici j'ai besoin de tous les ranks en même temps)
	for(auto const& elem:individualsContextForNewCol){
		Individual* indObj = dynamic_cast<Individual*>(elem);
		vector<string>::iterator itFoundInd = find(newIndividual.begin(), newIndividual.end(), indObj->getCalc_ident());
		if(itFoundInd!=newIndividual.end()){
			map<string, map<string, string>>::iterator itNewC = mapIndNewCol.find(indObj->getCalc_ident());
			if(itNewC!=mapIndNewCol.end()){
				map<string, string> mapIndNewColI = mapIndNewCol[indObj->getCalc_ident()];
				addNewCol(indObj, mapIndNewColI);
			}
			indObj->addAPrlvt(currentDate().substr(0,10));
			mapPrlvtALL[currentDate().substr(0,10)].push_back(indObj->getCalc_ident());
		}
	}
}

map<string, string> Model::colonizedAtAdmission(Individual* indObj){
	// ATTENTION : A N'UTILISER QUE DANS UN SEUL RANK!!!!!!
	/* Fonction qui prend en argument un individu et détermine si l'individu sera ou non colonisé par des pathogènes. Retourne une map avec en clé le pathogène et en valeur la date de décolonisation */
	boost::mpi::communicator* comm = repast::RepastProcess::instance()->getCommunicator();
	map<string, string> res;
	map<string, map<string, double>> mapAtAdmission = parameters->getMapColoAtAdmission();
	for(auto const& [pathogen, valMap]:mapAtAdmission){
		map<string, double> mapAdd = valMap;
		double meanProba = mapAdd["all"];
		if(indObj->getStatus()=="PA"){
			meanProba = mapAdd["PA"];
		}else{
			meanProba = mapAdd["PE"];
		}
		double probaAtAd = meanProba;
		double randomNbAtAdd;
		DoubleUniformGenerator chooseProbaContenerAd = Random::instance()->createUniDoubleGenerator(0,1);
		randomNbAtAdd = chooseProbaContenerAd.next();
		if(randomNbAtAdd < probaAtAd){
			//on va choisir une date de decolo
			string durationOfCol;
			string type = durationByType ? indObj->getStatus(): returnCat(indObj);
			if(boolManDuration){
				durationOfCol = findDate(manualDuration);
			}else{
				durationOfCol =  findDurationOfColonization(pathogen, currentDate().substr(0,10), type);
			}
			res[pathogen]=durationOfCol;
			cout << " attention " <<  indObj->getCalc_ident() << " va arrivé colonisé le " << currentDate() << endl;
		}
	}
	return res;
}

void Model::addNewCol(Individual* indObj, map<string, string> newB){
	/* Fonction qui ajoute une nouvelle colonisation. Prend en argument un individus et une map avec pour chaque pathogène, la date de décolonisation de l'individus. Cette fonction ajoute l'individu au pathogène */
	//on va tirer au sort pour savoir s'il va avoir une bacterie ou non	
	if(newB.size()!=0){
		vector<Organism*> PathogensContext;
		context.selectAgents(context.size(), PathogensContext, 1);
		for(auto const& [path, date]:newB){
			Bacteria* bacteria;
			vector<Organism*>::iterator itPath = find_if(PathogensContext.begin(), PathogensContext.end(),[path](Organism* o)->bool {Bacteria* b = dynamic_cast<Bacteria*>(o); return b->getStringId()==path;});
			if(itPath!=PathogensContext.end()){
				bacteria = dynamic_cast<Bacteria*>(*itPath);
			}
			// if(rank==indObj->getId().currentRank()){
				// indObj->addActualPositivePathogens(bacteria->getStringId());
				// indObj->addColonization(path, date);
				// string st = "";
				// string dateST;
				// if(incubationPeriod!="none"){
					// st="incubation";
					// time_t finaldateST = stringToDate(currentDate().substr(0,10)) + time_t(stoi(incubationPeriod)*86400);
					// dateST = dateToString(finaldateST).substr(0,10);
				// }else{
					// st="symptomatic";
					// dateST=date;
				// }
				// indObj->changePathogensStatus(path, st, dateST);
			// }
			// string level;
			// if(props->getProperty("admissionImmun")=="yes"){
				// level="immunization";
			// }else{
				// level = findStartLevel(aleaLevel);
			// }
			string level = findStartLevel(aleaLevel);
			changeIndividualPathogenStatus(indObj, bacteria->getStringId(), level, currentDate().substr(0,10));
			
			if(rank==bacteria->getId().currentRank()){
				bacteria->addIndividual(indObj->getCalc_ident(), currentDate().substr(0,10));
			}
		}
	}
}

void Model::addNewCol2(Individual* indObj, Bacteria* bacteriaObject){
	vector<Organism*> PathogensContext;
	context.selectAgents(context.size(), PathogensContext, 1);
	for(auto const& bact:allBacteriaToAnalyzed){
		Bacteria* bacteriaObject;
		vector<Organism*>::iterator itPath = find_if(PathogensContext.begin(), PathogensContext.end(),[bact](Organism* o)->bool {Bacteria* b = dynamic_cast<Bacteria*>(o); return b->getStringId()==bact;});
		if(itPath!=PathogensContext.end()){
			bacteriaObject = dynamic_cast<Bacteria*>((*itPath));
		}
		// string level = findStartLevel(aleaLevel);
		string level;
		if(props->getProperty("admissionImmun")=="yes"){
			level="immunization";
		}else{
			level = findStartLevel(aleaLevel);
		}
		changeIndividualPathogenStatus(indObj, bacteriaObject->getStringId(), level, currentDate().substr(0,10));
		
		if(rank==bacteriaObject->getId().currentRank()){
			bacteriaObject->addIndividual(indObj->getCalc_ident(), currentDate().substr(0,10));
		}
	}
}

map<int, map<string, int>> Model::buildMeNbOfColoAtAdmission(){
	/* Fonction qui construit une map avec en clé la semaine et en valeur une autre map avec en clé le type PA/PE/ALL et en valeur le nombre d'individu colonisé à l'admission */
	time_t startDateTime = stringToDate(startDate.substr(0,10));
	time_t stopDateTime = stringToDate(stopDate.substr(0,10));
	int modulo = ((stopDateTime - startDateTime)/86400)%timeResp;
	int week = 0;
	map<int, vector<int>> coloByWeek;
	map<int, map<string, int>> coloByWeekCat;
	for(int i(startDateTime); i<stopDateTime; i = i + 604800){
		vector<string> dates;
		for(int j(i); j< (i + 604800); j = j + 86400){
			string d = dateToString(j).substr(0,10);
			map<string, vector<string>>::iterator it = coloAtAdmi.find(d);
			if(it!=coloAtAdmi.end()){
				dates.push_back(d);
				coloByWeek[week].push_back(coloAtAdmi[d].size());
				int pa = 0;
				int pe = 0;
				for(auto const& e:coloAtAdmi[d]){
					if(e.substr(0,2)=="PA"){
						pa = pa + 1;
					}else{
						pe = pe + 1;
					}
				}
				coloByWeekCat[week]["PA"] = coloByWeekCat[week]["PA"] + pa;
				coloByWeekCat[week]["PE"] = coloByWeekCat[week]["PE"] + pe;
			}
		}
		if(dates.empty()){
			coloByWeek[week].push_back(0);
			coloByWeekCat[week]["PA"] = 0;
			coloByWeekCat[week]["PE"] = 0;
		}
		week = week +1;
	}
	if(modulo!=0){
		time_t firstDay = stopDateTime - (modulo*86400);
		time_t currentTime = stopDateTime;
		vector<string> dates;
		for(int i(firstDay); i<currentTime; i = i + 86400){
			string d = dateToString(i).substr(0,10);
			map<string, vector<string>>::iterator it = coloAtAdmi.find(d);
			if(it!=coloAtAdmi.end()){
				dates.push_back(d);
				coloByWeek[week].push_back(coloAtAdmi[d].size());
				int pa = 0;
				int pe = 0;
				for(auto const& e:coloAtAdmi[d]){
					if(e.substr(0,2)=="PA"){
						pa = pa + 1;
					}else{
						pe = pe + 1;
					}
				}
				coloByWeekCat[week]["PA"] = coloByWeekCat[week]["PA"] + pa;
				coloByWeekCat[week]["PE"] = coloByWeekCat[week]["PE"] + pe;
			}
		}
		if(dates.empty()){
			coloByWeek[week].push_back(0);
			coloByWeekCat[week]["PA"] = 0;
			coloByWeekCat[week]["PE"] = 0;
		}
	}
	map<int, int> res;
	for(auto const& [key, val]:coloByWeek){
		int c = 0;
		for(auto const& elem:val){
			c = c + elem;
		}
		res[key] = c;
	}
	
	for(auto const& [key, val]:res){
		coloByWeekCat[key]["ALL"] = val;
	}
	return(coloByWeekCat);
}
//

void Model::drawMePrevalence(){
	for(auto const& [key, val]:mapPrevalence){
		cout << key << " : ";
		map<string, vector<double>> res = val;
		for(auto const& [k, v]:res){
			cout<< k << " --> ";
			for(auto const& e:v){
				cout << e << " ";
			}
		}
	}
	cout<<endl;
}

void Model::checkMapPrlvt(){
	/* Fonction qui pour chaque jour, prélève les individus de la map de prélèvement */
	if(gameStatus=="simu"){
		string date = currentDate().substr(0,10);
		vector<string>::iterator itFerie = find(nonWorkingDays.begin(), nonWorkingDays.end(), date);
		int c = 0;
		if(itFerie==nonWorkingDays.end()){
			synchInd();
			synchPath();
			string date = currentDate().substr(0,10);
			vector<string> allI = mapPrlvt[date];
			vector<Organism*> listOfAllI;
			context.selectAgents(context.size(), listOfAllI, 0);
			for(auto const& ind:allI){
				vector<Organism*>::iterator it = find_if(listOfAllI.begin(), listOfAllI.end(), [ind](Organism* o)->bool {Individual* i = dynamic_cast<Individual*>(o); return i->getCalc_ident()==ind;});
				if(it!=listOfAllI.end()){
					Individual* indObj = dynamic_cast<Individual*>(*it);
					c = c +1;
					if(rank==indObj->getId().currentRank()){
						indObj->addAPrlvt(date);
					}
				}//sinon il est sorti		
			}
		}
	}
}

void Model::testGnuPlot(){
	// int sum=0;
	// for(auto const& e:vectAllDuration){
		// cout<< e << " -> ";
		// sum = sum + e;
	// }
	// cout<<endl;
	// if(gameStatus=="simu")cout<< rank << " ------> " << sum / vectAllDuration.size() << endl;
	if(rank==0){
		
		// cout<<endl;
		// vector<double> ar = mapPrevalence.begin()->second;
		drawMePrevalence();
		// cout << " les val sont " << nbOfCtc << " et " << nbOfCtcTransmission << endl;
		// vector<string> title;
		// int c = 1;
		// vector<pair<string, double>> data;
		// for(auto const& elem:ar){
			// string weekS = "S" + to_string(c);
			// title.push_back(weekS);
			// data.push_back(make_pair(weekS, elem));
			// c++;
		// }
		for(auto const& [key, val]:mapPrevalence){
			map<string, vector<double>> mapRes = mapPrevalence[key];
			vector<double> tot = mapRes["Tot"];
			vector<double> pa = mapRes["PA"];
			vector<double> pe = mapRes["PE"];
			// vector<boost::tuple<string, double, double, double>> data;
			vector<pair<string, pair<double, pair<double, double>>>> data;
			// vector<pair<string, double>> data;
			vector<string> title;
			int c = 1;
			for(int i(0); i<tot.size(); i++){
				string weekS = "S" + to_string(c);
				title.push_back(weekS);
				// data.push_back(boost::make_tuple(weekS, tot[i], pa[i], pe[i]));
				data.push_back(make_pair(weekS, make_pair(tot[i], make_pair(pa[i], pe[i]))));
				// data.push_back(make_pair(weekS, tot[i]));
				c = c + 1;
				cout << weekS << endl;
			}
			Gnuplot gp;
			gp<<"set terminal png size 600,800  \n";
			if(gameStatus=="simu"){
				gp<<"set title 'simulated prevalence' \n";
				gp<<"set output 'prevalenceSIMU \"" << key <<"\".png' \n";
			}else{
				gp<<"set title 'real prevalence' \n";
				gp<<"set output 'prevalenceREAL \"" << key <<"\".png' \n";
			}
			
			gp<< "set style data histogram \n";
			// gp<<"unset key \n";
			gp<<"set style histogram clustered \n";
			gp<< "set style fill solid border \n";
			gp<<"set auto x \n";
			gp<< "set yrange [0:0.5] \n";
			gp<< "plot '-' using 2:xticlabels(1)\n";
			// gp<< "plot for [COL=2:4] '-' using COL:xticlabels(1) \n";
			gp.send1d(boost::make_tuple(title, tot, pa, pe));
			// gp.send1d(data);
			// gp<< "plot \"" << "myDAT.dat"<< "\" using 3:xticlabels(1) \n";
			#ifdef _WIN32
			// For Windows, prompt for a keystroke before the Gnuplot object goes out of scope so that
			// the gnuplot window doesn't get closed.
			std::cout << "Press enter to exit." << std::endl;
			std::cin.get();
			#endif
		}
	}
}

void Model::compteur(){
	/* Fonction pour le scheduler, pour savoir la date actuelle. Permet aussi de remplir une map de presence pour savoir quels sont les individus présent à la date actuelle */
	if(rank==0 && gameStatus=="real")cout<< "REAL La date est: "<<  currentDate().substr(0,10)<<endl;
	if(rank==0 && gameStatus=="simu")cout<< "SIMU La date est: "<<  currentDate().substr(0,10)<<endl;
	synchInd();
	synchPath();
	vector<Organism*> listOfI;
	context.selectAgents(context.size(), listOfI, 0);
	for(auto const& elem:listOfI){
		Individual* i = dynamic_cast<Individual*>(elem);
		presence[currentDate().substr(0,10)].push_back(i->getCalc_ident());
	}
}

void Model::showMeOutputPrevAndAcq(){
	if(rank==0){
		// buildMeACompleteSwabFile();
		if(returnPrev){
			cout << "\n PREVALENCE BY WEEK \n";
			for(auto const& [key, val]:mapPrevalenceWeek){
				cout << key << " :\n";
				for(auto const& [k2, v2]:val){
					cout << k2<< " :\n";
					for(auto const& [k,v]:v2){
						cout << "for " << k << " nb of Colo : " << to_string(v.first) << " and nb of prel : " << to_string(v.second) << "\n";
					}
				}
				cout<<endl;
			}
			cout<<endl;
		}
		if(returnAcq){
			cout << "\n ACQUISITION BY WEEK \n";
			for(auto const& [key, val]:mapAcquisitionWeek){
				cout << key << " :\n";
				for(auto const& [k2, v2]:val){
					cout << k2<< " :\n";
					for(auto const& [k,v]:v2){
						cout << " for " << k << " nb of Acquisition : " << to_string(v.first) << " and nb of Neg People : " << to_string(v.second) << "\n";
					}
				}
				
				cout<< endl;
			}
			cout<<endl;
		}
		
	}
}

void Model::showMe(){
	/* Petite fonction test pour voir on en est dans les individus */
	vector<Organism*> listOfI;
	context.selectAgents(context.size(), listOfI,0);
	if(rank==0){
		for(auto const& individual:listOfI){
			Individual* ind = dynamic_cast<Individual*>(individual);
			cout << " ajourd'hui le " << currentDate() << " l'individu " << ind->getCalc_ident() << " possède comme dates de prlvt : ";
			for(auto const& jour:ind->getAllSwabDates()){
				cout << jour <<  " - ";
			}
			cout<<endl;
			cout << " ajourd'hui le " << currentDate() << " l'individu " << ind->getCalc_ident() << " possède comme dates de prlvt positif : ";
			for(auto const& bacteria:allBacteriaToAnalyzed){
				map<string, vector<string>> mapPos = ind->getPositivePathogens();
				for(auto const& day:mapPos[bacteria]){
					cout << day <<  " - ";
				}
				cout<<endl;
			}
			
		}
	}
	
}

string Model::dateOfEaster(int year){
    int Y = year;
    int a = Y % 19;
    int b = Y / 100;
    int c = Y % 100;
    int d = b / 4;
    int e = b % 4;
    int f = (b + 8) / 25;
    int g = (b - f + 1) / 3;
    int h = (19 * a + b - d - g + 15) % 30;
    int i = c / 4;
    int k = c % 4;
    int L = (32 + 2 * e + 2 * i - h - k) % 7;
    int m = (a + 11 * h + 22 * L) / 451;
	int month = (h + L - 7 * m + 114) / 31;
    int day = ((h + L - 7 * m + 114) % 31) + 1;
	string res = to_string(Y) + "-" + to_string(month) + "-" + to_string(day);
	return(res);
}

vector<string> Model::findMeNonWorkingDays(string startD, string stopD, bool bridge){
	vector<string> res;
	string firstD = startD.substr(0,10);
	string secondD = stopD.substr(0,10);
	int firstYear;
	int secondYear;
	vector<string> tempo_firstD;
	boost::split(tempo_firstD, firstD, boost::is_any_of("-"));
	vector<string> tempo_secondD;
	boost::split(tempo_secondD, secondD, boost::is_any_of("-"));
	firstYear = stoi(tempo_firstD[0]);
	secondYear = stoi(tempo_secondD[0]);
	vector<int> years;
	if(firstYear!=secondYear){
		for(int y(firstYear); y<=secondYear; y++){
			years.push_back(y);
		}
	}else{
		years.push_back(firstYear);
	}
	for(auto const& elem:years){
		string easterSunday = dateOfEaster(elem);
		string easterMonday = dateToString(stringToDate(easterSunday) + 1*86400).substr(0,10);
		string ascension = dateToString(stringToDate(easterSunday) + 39*86400).substr(0,10);
		string pentecost = dateToString(stringToDate(easterSunday) + 50*86400).substr(0,10);
		string christmas = to_string(elem) + "-12-25";
		string nationalDay = to_string(elem) + "-07-14";
		string madoneDesMotards = to_string(elem) + "-08-15";
		string labourDay = to_string(elem) + "-05-01";
		string victoire = to_string(elem) + "-05-08";
		string armistice = to_string(elem) + "-11-11";
		string toussaint = to_string(elem) + "-11-01";
		string beginningOfTheYear = to_string(elem) + "-01-01";
		res.push_back(easterMonday);
		res.push_back(ascension);
		res.push_back(pentecost);
		res.push_back(christmas);
		res.push_back(nationalDay);
		res.push_back(madoneDesMotards);
		res.push_back(labourDay);
		res.push_back(victoire);
		res.push_back(armistice);
		res.push_back(toussaint);
		res.push_back(beginningOfTheYear);
	}
	vector<string> addDatesBridge = frenchBridge(res);
	if(!addDatesBridge.empty() && bridge){
		res.insert(res.end(), addDatesBridge.begin(), addDatesBridge.end());
	}
	return(res);
}

vector<string> Model::frenchBridge(vector<string> input_vector){
	vector<string> resDates;
	for(auto const& date:input_vector){
		time_t stDate = stringToDate(date);
		tm* time_st = localtime(&stDate);
		int wday = time_st->tm_wday;
		if(wday==2){
			//si c'est un mardi ou un jeudi on suppose que les gens font le pont
			resDates.push_back(dateToString(stringToDate(date) - 1*86400).substr(0,10));
		}else if(wday==4){
			resDates.push_back(dateToString(stringToDate(date) + 1*86400).substr(0,10));
		}
	}
	return(resDates);
}

map<string, string> Model::buildGroup(string file_){
	/* Fonction qui prend en argument un fichier dans lequel l'utilisateur défini les groupes (analyse de sensibilité) de PE pour les probabilités de transmission */
	map<string, string> res;
	string line;
	ifstream ifs (file_);
	getline(ifs,line);
	while(getline(ifs, line)){
		vector<string> stock_tempo;
		boost::split(stock_tempo, line, boost::is_any_of(";"));
		
		res[stock_tempo[0]]=stock_tempo[1];
	}
	ifs.close();
	res["Patient"]="Patient";
	return res;
}

void Model::colonizationStatus(){
	if(returnStatusByDay){
		synchInd();
		synchPath();
		// cout << " colonizationStatus ON le " << currentDate().substr(0,10) << endl;
		vector<Organism*> allIndiv;
		context.selectAgents(context.size(), allIndiv, 0);
		string day = currentDate().substr(0,10);
		for(auto const & elem:allIndiv){
			Individual* individual = dynamic_cast<Individual*>(elem);
			// vector<string> allPatho = individual->getActualPositivePathogens();
			vector<pair<string, string>> allPathoToAnalyzed;
			// for(auto const& b:allBacteriaToAnalyzed){
				// auto it = find(allPatho.begin(), allPatho.end(), b);
				// if(it!=allPatho.end()){
					// string st = individual->getPathogensStatus()[b].first;
					// allPathoToAnalyzed.push_back(make_pair(b, st));
				// }
			// }
			map<string, pair<string, string>> indStat = individual->getPathogensStatus();
			for(auto const& elem:indStat){
				string b = elem.first;
				string st = elem.second.first;
				allPathoToAnalyzed.push_back(make_pair(b, st));
			}
			statusMap[individual->getCalc_ident()][day] = allPathoToAnalyzed;
		}
		
	}
}

void Model::buildMeStatusByDay(){
	if(rank==0 && returnStatusByDay){
		cout<< " buildMeStatusByDay ON " << endl;
		
		vector<Organism*> listOfI;
		context.selectAgents(context.size(),listOfI,0);
		listOfI.insert(listOfI.end(), movingPeople.begin(), movingPeople.end());
		string filenameDayStatusALL;
		if(gameStatus=="simu"){
			filenameDayStatusALL = props->getProperty("filenameStatusByDaySimu");
		}else{
			filenameDayStatusALL = props->getProperty("filenameStatusByDayReal");
		}
		for(auto const& b:allBacteriaToAnalyzed){
			std::ofstream myCSV;
			string miniF = filenameDayStatusALL.substr(0, filenameDayStatusALL.size()-4);
			cout << "miniF " << miniF << endl;
			string filenameDayStatus = miniF + b + ".csv";
			myCSV.open(filenameDayStatus);
			cout << "filenameDayStatus " << filenameDayStatus << endl;
			myCSV<<"id;cat;ward;";
			vector<string> alldates = findDateBetweenTwoDays(startDate, stopDate);
			auto italldates = alldates.rbegin();
			for(auto const& d:alldates){
				if(d!=(*italldates)){
					myCSV<<d + ";";
				}else{
					myCSV << d + ";" << endl;
				}
			}
			for(auto const& [ind, mapIndiv]:statusMap){
				// cout << " c'est partie avec : " << ind <<endl;
				map<string, vector<pair<string, string>>> resStatus = mapIndiv;
				std::vector<Organism*>::iterator iterInd = find_if(listOfI.begin(),listOfI.end(), [ind](Organism* in) -> bool { Individual* in2 = dynamic_cast<Individual*>(in); return ind == in2->getCalc_ident();});
				Individual* indObject = dynamic_cast<Individual*>((*iterInd));
				string cat = returnCat(indObject);
				myCSV<<ind + ";" + cat + ";" + indObject->getWard() + ";";
				for(auto const& d:alldates){
					auto itD = resStatus.find(d);
					if(itD!=resStatus.end()){
						if(resStatus[d].empty()){
							myCSV << "0;";
						}else{
							// auto itP = find(resStatus[d].begin(), resStatus[d].end(), b);

							auto itP = find_if(resStatus[d].begin(), resStatus[d].end(), [b](pair<string, string> n)-> bool {return n.first==b;});
							if(itP!=resStatus[d].end()){
								if((*itP).second=="incubation"){
									myCSV << "1;";
								}else if((*itP).second=="incubationWithContagion"){
									myCSV << "2;";
								}else if((*itP).second=="asymptomatic"){
									myCSV << "3I;";
								}else if((*itP).second=="symptomatic"){
									myCSV << "3II;";
								}else if((*itP).second=="severity"){
									myCSV << "3III;";
								}else if((*itP).second=="immunization"){
									myCSV << "4;";
								}
								
							}else{
								myCSV << "0;";
							}
						}
					}else{
						myCSV << "-;";
					}
				}
				myCSV << endl;
			}
			myCSV.close();
		}
		
	}
}

void Model::newColonizedIndividual(int nbOfWeekCol){
	// synchInd();
	// synchPath();
	boost::mpi::communicator* comm = repast::RepastProcess::instance()->getCommunicator();
	vector<Organism*> individualsContextForNewCol;
	context.selectAgents(context.size(),individualsContextForNewCol,0);
	vector<Individual*> allStaff;
	for(auto const& elem:individualsContextForNewCol){
		Individual* indObj = dynamic_cast<Individual*>(elem);
		if(indObj->getStatus()=="PE"){
			allStaff.push_back(indObj);
		}
	}
	vector<Organism*> PathogensContext;
	context.selectAgents(context.size(), PathogensContext, 1);
	for(auto const& bact:allBacteriaToAnalyzed){
		Bacteria* bacteriaObject;
		vector<Organism*>::iterator itPath = find_if(PathogensContext.begin(), PathogensContext.end(),[bact](Organism* o)->bool {Bacteria* b = dynamic_cast<Bacteria*>(o); return b->getStringId()==bact;});
		if(itPath!=PathogensContext.end()){
			bacteriaObject = dynamic_cast<Bacteria*>((*itPath));
		}
		
		for(int i(0); i<nbOfWeekCol; i++){
			vector<string> choosenInd;
			string individualName;
			Individual* ind;
			if(rank==0){
				IntUniformGenerator chooseProbaContener = Random::instance()->createUniIntGenerator(0,allStaff.size()-1);
				int randomNb = chooseProbaContener.next();
				ind = allStaff.at(randomNb);
				vector<string>::iterator it = find(choosenInd.begin(), choosenInd.end(), ind->getCalc_ident());
				while(it!=choosenInd.end() && choosenInd.size()<(nbOfWeekCol+1)){
					randomNb = chooseProbaContener.next();
					ind = allStaff.at(randomNb);
					it = find(choosenInd.begin(), choosenInd.end(), ind->getCalc_ident());
				}
				individualName = ind->getCalc_ident();
			}
			boost::mpi::broadcast(*comm,individualName,0);
			choosenInd.push_back(individualName);
			
			vector<Individual*>::iterator iterContext = find_if(allStaff.begin(),allStaff.end(), [individualName](Individual* m) -> bool {Individual* m1 = dynamic_cast<Individual*>(m); return m1->getCalc_ident() == individualName; });
			if(iterContext!=allStaff.end()){
				ind = (*iterContext);
			}
			
			addNewCol2(ind, bacteriaObject);
			cout << " hey! " << ind->getCalc_ident() << " va devenir colonisé! " << endl;
		}
	}
	synchInd();
	synchPath();
}

void Model::checkFixedColonizedStaff(){
	// synchInd();
	// synchPath();
	string d = currentDate().substr(0,10);
	auto it = chosenFixedColonizedStaff.find(d);
	if(it!=chosenFixedColonizedStaff.end()){
		vector<string> indStaff =  chosenFixedColonizedStaff[d];
		//on recup les ind en object
		vector<Organism*> PathogensContext;
		context.selectAgents(context.size(), PathogensContext, 1);
		vector<Organism*> individualsContext;
		context.selectAgents(context.size(),individualsContext,0);
		vector<Individual*> allStaffObject;
		for(auto const& elem:indStaff){
			cout << " hey! " << elem << " va devenir colonisé! " << d << endl;
			vector<Organism*>::iterator iterContext = find_if(individualsContext.begin(),individualsContext.end(), [elem](Organism* m) -> bool {Individual* m1 = dynamic_cast<Individual*>(m); return m1->getCalc_ident() == elem; });
			if(iterContext!=individualsContext.end()){
				Individual* ind = dynamic_cast<Individual*>((*iterContext));
				allStaffObject.push_back(ind);
				for(auto const& bact:allBacteriaToAnalyzed){
					Bacteria* bacteriaObject;
					vector<Organism*>::iterator itPath = find_if(PathogensContext.begin(), PathogensContext.end(),[bact](Organism* o)->bool {Bacteria* b = dynamic_cast<Bacteria*>(o); return b->getStringId()==bact;});
					if(itPath!=PathogensContext.end()){
						bacteriaObject = dynamic_cast<Bacteria*>((*itPath));
					}
					addNewCol2(ind, bacteriaObject);
				}
				
			}
		}
		
	}
	synchInd();
	synchPath();
}

void Model::initSchedule(repast::ScheduleRunner& runner){
	/* initialise le scheduler et les évenements qui vont se dérouler durant la simu */
	runner.scheduleStop(stopAt);
	runner.scheduleEvent(0,tickDay, repast::Schedule::FunctorPtr(new repast::MethodFunctor<Model> (this, &Model::compteur)));
	
	if(gameStatus=="simu")runner.scheduleEvent(1,tickDay*7, repast::Schedule::FunctorPtr(new repast::MethodFunctor<Model> (this, &Model::mapPrlvtSimu)));
	
	if(gameStatus=="simu" && fixedColonization)runner.scheduleEvent(tickDay*7, tickDay*7, repast::Schedule::FunctorPtr(new repast::MethodFunctor<Model> (this, &Model::choseFixedColonizedIndividual)));
	
	runner.scheduleEvent(tickDay+0.5,tickDay, repast::Schedule::FunctorPtr(new repast::MethodFunctor<Model> (this, &Model::checkAdmission)));//commence au second jour puisqu'on prend les individus du premier jour
	
	runner.scheduleEvent(1,1, repast::Schedule::FunctorPtr(new repast::MethodFunctor<Model> (this, &Model::checkContact)));
	
	if(gameStatus=="real")runner.scheduleEvent((tickDay+1)/2, tickDay, repast::Schedule::FunctorPtr(new repast::MethodFunctor<Model> (this, &Model::checkPrlvt)));
	
	if(gameStatus=="simu" && fixedColonization)runner.scheduleEvent(1.1,tickDay, repast::Schedule::FunctorPtr(new repast::MethodFunctor<Model> (this, &Model::checkFixedColonizedStaff)));
	
	if(gameStatus=="simu")runner.scheduleEvent(2,tickDay, repast::Schedule::FunctorPtr(new repast::MethodFunctor<Model> (this, &Model::checkMapPrlvt)));
	
	runner.scheduleEvent(2.5,tickDay, repast::Schedule::FunctorPtr(new repast::MethodFunctor<Model> (this, &Model::colonizationStatus)));
	
	if(gameStatus=="simu")runner.scheduleEvent(3, tickDay, repast::Schedule::FunctorPtr(new repast::MethodFunctor<Model> (this, &Model::decolonization)));
	
	// if(gameStatus=="simu")runner.scheduleEvent(4, tickDay, repast::Schedule::FunctorPtr(new repast::MethodFunctor<Model> (this, &Model::changePathogensStatusFromIndividuals)));
	
	// runner.scheduleEvent(4,tickDay, repast::Schedule::FunctorPtr(new repast::MethodFunctor<Model> (this, &Model::colonizationStatus)));
	
	runner.scheduleEvent(tickDay-0.5,tickDay, repast::Schedule::FunctorPtr(new repast::MethodFunctor<Model> (this, &Model::checkDischarge)));
	
	runner.scheduleEvent((tickDay-1), tickDay*7, repast::Schedule::FunctorPtr(new repast::MethodFunctor<Model> (this, &Model::buildMeAcquisition)));
	runner.scheduleEvent((tickDay-1),tickDay*timeResp, repast::Schedule::FunctorPtr(new repast::MethodFunctor<Model> (this, &Model::buildMePrevalence)));
	runner.scheduleEndEvent(repast::Schedule::FunctorPtr(new repast::MethodFunctor<Model> (this, &Model::EndOfAcquisition)));
}

bool Model::FileExists(const std::string& name) {
  struct stat buffer;
  return (stat (name.c_str(), &buffer) == 0);
}

