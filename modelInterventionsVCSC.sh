#!/usr/bin/env bash
#SBATCH -n 6
#SBATCH -o slurm_simulation/test/test-%j.out # STDOUT
#SBATCH -e slurm_simulation/error/err-%j.err # STDERR
#SBATCH --time=00:05:00


# on s'assure d'avoir un environnement propre
# module purge

# on charge les module necessaire au le script
module load gcc/9.2.0
module load openmpi/4.0.5
module load netcdf/4.7.3
module load boost/1.72.0
module load repast_hpc/2.3.0

nbOfScenario=$1
nbOfSim=$2
fileNameAnalyze=$3
xtra=$4
nbSupers=$5
sensib=""
sensib=$6
# allSC="byCatSCtc_byCatSCtd_byCatSCPEtc_byCatSCPEtd_byCatSCPAtc_byCatSCPAtd_byCatSCtb_byCatSCPEtb_byCatSCPAtb_byCatSCtbm_byCatSCPEtbm_byCatSCPAtbm"
# allSC="byCatSCtc2_byCatSCtd2_byCatSCPAtc2_byCatSCPAtd2_byCatSCtb2_byCatSCPAtb2_byCatSCtbm2_byCatSCPAtbm2"
# allSC="byCatSCPAtd2"

FA="yes"
NOA=2
BTV="yes"
NCIV=0

SCVClab="VC"
declare -a addNameVector
addNameVector=("byCatSCtc" "byCatSCtd" "byCatSCtm" "byCatSCPEtc" "byCatSCPEtd" "byCatSCPEtm" "byCatSCPAtc" "byCatSCPAtd" "byCatSCPAtm")
# addNameVector=("byCatSCtc" "byCatSCtd" "byCatSCtb" "byCatSCtm" "byCatSCPEtc" "byCatSCPEtd" "byCatSCPEtb" "byCatSCPEtm" "byCatSCPAtc" "byCatSCPAtd" "byCatSCPAtb" "byCatSCPAtm")
# addNameVector=("byCatSCtc2" "byCatSCtd2" "byCatSCPAtc2" "byCatSCPAtd2" "byCatSCtb2" "byCatSCPAtb2" "byCatSCtbm2" "byCatSCPAtbm2")
# addNameVector=("byCatSCPAtd2")

# ensuite on calcule le learning
learningF="parameters/resultParameters_InterventionsSCVC"$fileNameAnalyze$sensib".csv"
Command="sbatch -p common,dedicated --qos=fast --dependency=singleton --job-name=analyze ./modelCluster.sh -o no -b yes -d none -f $FA -n $NOA -t $BTV -z $NCIV -l $learningF -k $sensib"
Submit_Output="$($Command 2>&1)"
JobIdL=`echo $Submit_Output | grep 'Submitted batch job' | awk '{print $4}'`
echo "le job id du learning supercontactors est "
echo $JobIdL

compteur=0
declare -A jobSimulation
for addName in ${addNameVector[*]};do
	 fileNameAnalyze=$addName
	for j in `seq 0 $nbOfScenario`;do
		(( compteur = compteur+1 ))
		Command="sbatch -p common,dedicated --qos=fast --job-name=intervention --array=1-$nbOfSim --dependency=afterany:$JobIdL ./modelCluster.sh -o no -b no -m sim -d none -f $FA -n $NOA -t $BTV -z $NCIV -h $j -l $learningF -r $addName -w SCvaccination -v $SCVClab -x $xtra -q $nbSupers"
		Submit_Output="$($Command 2>&1)"
		JobId1=`echo $Submit_Output | grep 'Submitted batch job' | awk '{print $4}'`
		jobSimulation[$compteur]=$JobId1
		echo "le job simulation vaccination SC est "
		echo $JobId1
	done
done

declare -a addNameVector2
addNameVector2=("RandomAll" "RandomPatients")
for addName in ${addNameVector2[*]};do
	for j in `seq 0 $nbOfScenario`;do
		for k in `seq 1 $nbOfSim`;do
      addNamek=$addName$k
		  Command="sbatch -p common,dedicated --qos=fast --job-name=intervention --dependency=afterany:$JobIdL ./modelCluster.sh -o no -b no -m sim -d none -f $FA -n $NOA -t $BTV -z $NCIV -h $j -l $learningF -r $addNamek -w SCvaccination -v $SCVClab -a $addName -x $xtra -q $nbSupers"
		  Submit_Output="$($Command 2>&1)"
		  JobId2=`echo $Submit_Output | grep 'Submitted batch job' | awk '{print $4}'`
		  jobSimulation[$compteur]=$JobId2
		  echo "le job simulation vaccination random est "
		  echo $JobId2
    done
	done
done

#jR=${jobSimulation[1]}
#echo $jR
#for k in `seq 2 ${#jobSimulation[@]}`;do
#	jR+=":${jobSimulation[$k]}"
#done

# for addName in ${addNameVector[*]};do
#	echo "analyse job : "
#	Command="sbatch -p common,dedicated --qos=fast --dependency=afterany:$jR --job-name=analyze ./other/interventionBash.sh CP $fileNameAnalyze $fileNameAnalyze $nbOfScenario $allSC"
#	Submit_Output="$($Command 2>&1)"
#	JobIdAnalyze=`echo $Submit_Output | grep 'Submitted batch job' | awk '{print $4}'`
#	echo $JobIdAnalyze
# done


