/* INDIVIDUAL CLASS
* this file gather all agent from the project
*
*	
*	Individual.cpp
*
*	Created on: Nov 17, 2017
*	Author: Audrey
*
*/

#include "Individual.h"

#include "repast_hpc/io.h"
#include "repast_hpc/logger.h"
#include "repast_hpc/initialize_random.h"
#include "repast_hpc/Utilities.h"
#include "repast_hpc/NetworkBuilder.h"
#include "repast_hpc/SVDataSetBuilder.h"

#include <boost/lexical_cast.hpp>
#include <boost/mpi.hpp>
#include <boost/mpi/collectives.hpp>

#include <sstream>
#include <cstdlib>
#include <algorithm>
#include <stdexcept>
#include <math.h>
#include <vector>
#include <iomanip>

using namespace std;
using namespace repast;

BOOST_CLASS_EXPORT(Individual)
BOOST_CLASS_EXPORT(Patient)
BOOST_CLASS_EXPORT(Staff)

Individual::Individual(){
	
}

Individual::Individual(repast::AgentId id, std::string calc_ident, std::string status,std::string dt_in, std::string dt_out, std::string ward, std::string gender): 
	Organism(id),calc_ident_(calc_ident), status_(status),dt_in_(dt_in),dt_out_(dt_out),ward_(ward),gender_(gender){
	
}


Individual::Individual(repast::AgentId id, std::string calc_ident, std::string status,std::string dt_in, std::string dt_out, std::string ward, std::string gender, std::map <std::string, std::vector<std::string>> positivePathogens, std::vector <std::string> actualPositivePathogens, std::map<std::string, std::string> mapColonization, std::map<std::string, std::pair<std::string, std::string>> pathogensStatus_, std::string manuportage_): 
	Organism(id),calc_ident_(calc_ident), status_(status),dt_in_(dt_in),dt_out_(dt_out),ward_(ward),gender_(gender), positivePathogens_(positivePathogens), actualPositivePathogens_(actualPositivePathogens), mapColonization_(mapColonization), pathogensStatus(pathogensStatus_), manuportage(manuportage_){
	
}

Individual::Individual(std::string calc_ident, std::string status,std::string dt_in, std::string dt_out, std::string ward, std::string gender, std::map <std::string, std::vector<std::string>> positivePathogens, std::vector <std::string> actualPositivePathogens, std::map<std::string, std::string> mapColonization, std::map<std::string, std::pair<std::string, std::string>> pathogensStatus_, std::string manuportage_): 
	calc_ident_(calc_ident), status_(status),dt_in_(dt_in),dt_out_(dt_out),ward_(ward),gender_(gender), positivePathogens_(positivePathogens), actualPositivePathogens_(actualPositivePathogens),  mapColonization_(mapColonization), pathogensStatus(pathogensStatus_), manuportage(manuportage_){
	
}

Individual::Individual(std::string calc_ident, std::string status,std::string dt_in, std::string dt_out, std::string ward, std::string gender): 
	calc_ident_(calc_ident), status_(status),dt_in_(dt_in),dt_out_(dt_out),ward_(ward),gender_(gender){
	
}

Individual::Individual(repast::AgentId id, const vector<string>& v): 
	Organism(id), calc_ident_(v[0]), status_(v[1]),dt_in_(v[2]), dt_out_(v[3]),ward_(v[4]),gender_(v[5]){
	
}

Individual::Individual(const vector<string>& v): 
	calc_ident_(v[0]), status_(v[1]),dt_in_(v[2]), dt_out_(v[3]),ward_(v[4]),gender_(v[5]){
	
}

Individual::~Individual(){
	
}

void Individual::set(int currentRank,std::string newCalc_ident, std::string newStatus,std::string newDt_in, std::string newDt_out, std::string newWard, std::string newGender){
	id_.currentRank(currentRank);
	calc_ident_ =newCalc_ident;
	status_ = newStatus;
	dt_in_ = newDt_in;
	dt_out_ = newDt_out;
	ward_ = newWard;
	gender_ = newGender;
}                

void Individual::affiche() const{
	
	cout<< "I'm alive! and my name is " << calc_ident_ <<endl;
}

void Individual::afficheSuccessor(repast::SharedNetwork<Individual,repast::RepastEdge<Individual>, repast::RepastEdgeContent<Individual>, repast::RepastEdgeContentManager<Individual> > *network){
	vector<Individual*> indSucc;
	network->successors(this, indSucc);
	vector<Individual*>::iterator iter = indSucc.begin();
	while(iter!=indSucc.end()){
		cout<< " connexion entre "<< calc_ident_ << " et " << (*iter)->getCalc_ident() << endl;
		iter++;
	}
}

void Individual::affichePredecessor(repast::SharedNetwork<Individual,repast::RepastEdge<Individual>, repast::RepastEdgeContent<Individual>, repast::RepastEdgeContentManager<Individual> > *network){
	vector<Individual*> indSucc;
	network->predecessors(this, indSucc);
	vector<Individual*>::iterator iter = indSucc.begin();
	while(iter!=indSucc.end()){
		cout<< " connexion predecesseur entre "<< calc_ident_ << " et " << (*iter)->getCalc_ident() << endl;
		iter++;
	}
}


void Individual::removePathFromActualPositivePathogens(std::string i){
	if(!actualPositivePathogens_.empty()){
		if(actualPositivePathogens_.size()==1){
			vector<std::string> vectToy;
			actualPositivePathogens_ = vectToy;
		}else{
			actualPositivePathogens_.erase(std::remove_if(actualPositivePathogens_.begin(), actualPositivePathogens_.end(),[i](std::string j) { return i==j; }),actualPositivePathogens_.end() );
		}
	}
	
}

vector<string> Individual::getAllSwabDates(){

	vector<time_t> allDates;
	vector<string> allDatesString;
	if(!positivePathogens_.empty()){
		for(auto const& [key, val]:positivePathogens_){
			for(auto const& elem:val){
				time_t date = Filetreatment::stringToDate(elem);
				vector<time_t>::iterator it = find(allDates.begin(), allDates.end(), date);
				if(it==allDates.end()){
					allDates.push_back(date);
				}
			}
		}
		sort(allDates.begin(), allDates.end());
		
		for(auto const& d:allDates){
			allDatesString.push_back(Filetreatment::dateToString(d).substr(0,10));
		}
	}
	
	return allDatesString;
}


void Individual::addInsidePosPathogens(std::string pathogen, string dateAdd){
	// Bacteria* bacteria = dynamic_cast<Bacteria*>(pathogen);
	//on va mettre à jour la bacterie dans la map de l'individu
	map<std::string,vector<string>>::iterator itMap;
	itMap = find_if(this->positivePathogens_.begin(), this->positivePathogens_.end(),[pathogen](std::pair<std::string, vector<string>> v) -> bool {return v.first==pathogen;} );
	if(itMap!=this->positivePathogens_.end()){
		/* la bacterie est déjà dans la map on met à jour la date */
		/* on regarde si la date est déjà dedans */
		vector<string> dates = (*itMap).second;
		vector < string> :: iterator itDate = find_if(dates.begin(), dates.end(),[dateAdd](string dateInside)->bool {return dateAdd.compare(dateInside)==0;});
		if(itDate==dates.end()){
			(*itMap).second.push_back(dateAdd);
		}
		
	}else{
		/* la bacterie n'est pas dans la map on la rajoute */
		vector<string> vMap;
		vMap.push_back(dateAdd);
		this->addPositivePathogens(pathogen,vMap);
	}
}
void Individual::addPathogen(std::string pathogen, string dateAdd){
	//on va mettre à jour la bacterie dans la map de l'individu
	this->addInsidePosPathogens(pathogen, dateAdd);
	/* on la rajoute dans le actualPatho si elle n'a jamais été rencontré dedans */
	vector<std::string>::iterator it_actualPos = find_if(actualPositivePathogens_.begin(), actualPositivePathogens_.end(),[pathogen](std::string v) -> bool {return v==pathogen;} );
	if(it_actualPos==actualPositivePathogens_.end()){
		this->addActualPositivePathogens(pathogen);
	}
}

void Individual::removePositivePathogens(){
	// for(auto const& [key,val]:positivePathogens_){
		// positivePathogens_[key].clear();
		// vector<string>().swap(positivePathogens_[key]);
	// }

	// positivePathogens_.clear();
	// map<string, vector<string>>().swap(positivePathogens_);
	
	
	// for(auto const& [key,val]:this->positivePathogens_){
		// vector<string>().swap(this->positivePathogens_[key]);
	// }
	
	//étape obligatoire sinon bug dans certaines itérations...
	this->positivePathogens_.clear();
	map<string, vector<string>> my_map;
	// vector<string> my_vector;
	// my_map["no_pathogens"] = my_vector;
	this->positivePathogens_ = my_map;
	
	
}

void Individual::removeActualPositivePathogens(){
	this->actualPositivePathogens_.clear();
	vector<string>().swap(this->actualPositivePathogens_);
}

void Individual::supColonization (std::string key){
	
	// cout << calc_ident_ <<" sup colo before " <<  actualPositivePathogens_.size() << endl;
	vector<string>::iterator itKey = find(this->actualPositivePathogens_.begin(), this->actualPositivePathogens_.end(), key);
	if(itKey!=this->actualPositivePathogens_.end()){
		if(this->actualPositivePathogens_.size()==1){
			this->removeActualPositivePathogens();
		}else{
			this->actualPositivePathogens_.erase(std::remove(this->actualPositivePathogens_.begin(), this->actualPositivePathogens_.end(), key), actualPositivePathogens_.end());
		}
		
	}else{
		cout << " BIG BIG BIG PB SUPCOLONIZATION " << this->getCalc_ident() << endl;
		cout << " dans actual pathogens : " << endl;
		for(auto const& elem:this->actualPositivePathogens_){
			cout << elem << " and ";
		}
		cout<<endl;
		cout << " dans mapColo : " << endl;
		for(auto const& [key, val]:this->mapColonization_){
			cout<< key << " " << val << " and ";
		}
		cout<<endl;
		cout << " dans positive patho : " << endl;
		for(auto const& [key, val]:this->positivePathogens_){
			cout<< key << " : " ;
			for(auto const& elem:val){
				cout << elem << " - ";
			}
			cout<<"\n";
		}
		cout<<endl;
		
	}
	this->mapColonization_.erase(key);
	
	// cout << calc_ident_ <<" sup colo after " <<  actualPositivePathogens_.size() << endl;
}

void Individual::supStatusPathogen(std::string key){
	this->pathogensStatus.erase(key);
}

void Individual::addAPrlvt(std::string date){
	if(!actualPositivePathogens_.empty()){
		for(auto const& path:actualPositivePathogens_){
			this->addInsidePosPathogens(path, date);
		}
	}else{
		this->addInsidePosPathogens("no_pathogens", date);
	}
}

void Individual::changePathogensStatus(std::string pathogen, std::string newStatus, std::string dateE){
	pathogensStatus[pathogen] = make_pair(newStatus, dateE);
}

Patient::Patient(){
	
}

Patient::Patient(repast::AgentId id, std::string calc_ident, std::string status,std::string dt_in, std::string dt_out, std::string ward, std::string gender,std::string hosp_flag): Individual(id,calc_ident, status,dt_in,dt_out,ward,gender),hosp_flag_(hosp_flag){
	
}

Patient::Patient(repast::AgentId id, std::string calc_ident, std::string status,std::string dt_in, std::string dt_out, std::string ward, std::string gender,std::map <std::string, std::vector<std::string>> positivePathogens, std::vector <std::string> actualPositivePathogens,std::map<std::string, std::string> mapColonization, std::map<std::string, std::pair<std::string, std::string>> pathogensStatus, std::string manuportage, std::string hosp_flag): Individual(id,calc_ident,status,dt_in,dt_out,ward,gender, positivePathogens,actualPositivePathogens, mapColonization, pathogensStatus, manuportage),hosp_flag_(hosp_flag){
	
}

Patient::Patient(Patient const& patientCopy):Individual(patientCopy.id_, patientCopy.calc_ident_, patientCopy.status_,patientCopy.dt_in_,patientCopy.dt_out_,patientCopy.ward_,patientCopy.gender_, patientCopy.positivePathogens_,patientCopy.actualPositivePathogens_, patientCopy.mapColonization_, patientCopy.pathogensStatus, patientCopy.manuportage),hosp_flag_(patientCopy.hosp_flag_){
	
}

Patient::Patient(std::string calc_ident, std::string status,std::string dt_in, std::string dt_out, std::string ward, std::string gender):Individual(calc_ident,status,dt_in,dt_out,ward,gender){
	
}

Patient::Patient (repast::AgentId id, const vector<string>& v): Individual(id, v),hosp_flag_(v[6]){
	
}

Patient::Patient (const vector<string>& v): Individual(v),hosp_flag_(v[6]){
	
}

Patient::~Patient(){
	
}

void Patient::set(int currentRank,std::string newCalc_ident, std::string newStatus,std::string newDt_in, std::string newDt_out, std::string newWard, std::string newGender,std::map <std::string, std::vector<std::string>> newPositivePathogens,std::vector <std::string> newActualPositivePathogens,  std::map<std::string, std::string> newMapColonization, std::map<std::string, std::pair<std::string, std::string>> newPathogensStatus, std::string newManuportage, std::string newHosp_flag){
	id_.currentRank(currentRank);
	calc_ident_ =newCalc_ident;
	status_ = newStatus;
	dt_in_ = newDt_in;
	dt_out_ = newDt_out;
	ward_ = newWard;
	gender_ = newGender;
	positivePathogens_ = newPositivePathogens;
	actualPositivePathogens_ = newActualPositivePathogens;
	mapColonization_ = newMapColonization;
	pathogensStatus = newPathogensStatus;
	manuportage = newManuportage;
	hosp_flag_ = newHosp_flag;
	
} 

void Patient::affiche() const{
	
	cout<< "I'm alive! and my name is " << calc_ident_ << " and I'm here for "<< hosp_flag_ <<endl;
}

Staff::Staff(){
	
}

Staff::Staff(repast::AgentId id, std::string calc_ident, std::string status,std::string dt_in, std::string dt_out, std::string ward, std::string gender,std::string cat): Individual(id,calc_ident,status,dt_in,dt_out,ward,gender),cat_(cat){
	
}

Staff::Staff(repast::AgentId id, std::string calc_ident,std::string status,std::string dt_in, std::string dt_out, std::string ward, std::string gender,std::map <std::string, std::vector<std::string>> positivePathogens, std::vector <std::string> actualPositivePathogens,std::map<std::string, std::string> mapColonization, std::map<std::string, std::pair<std::string, std::string>> pathogensStatus, std::string manuportage, std::string cat): Individual(id,calc_ident,status,dt_in,dt_out,ward,gender, positivePathogens,actualPositivePathogens, mapColonization, pathogensStatus, manuportage),cat_(cat){
	
}

Staff::Staff(std::string calc_ident, std::string status,std::string dt_in, std::string dt_out, std::string ward, std::string gender):Individual(calc_ident,status,dt_in,dt_out,ward,gender){
	
}

Staff::Staff (repast::AgentId id, const vector<string>& v): Individual(id, v),cat_(v[7]){
	
}

Staff::Staff (const vector<string>& v): Individual(v),cat_(v[7]){
	
}

Staff::Staff(Staff const& staffCopy):Individual(staffCopy.id_, staffCopy.calc_ident_,staffCopy.status_,staffCopy.dt_in_,staffCopy.dt_out_,staffCopy.ward_,staffCopy.gender_, staffCopy.positivePathogens_,staffCopy.actualPositivePathogens_,staffCopy.mapColonization_, staffCopy.pathogensStatus, staffCopy.manuportage),cat_(staffCopy.cat_){
	
}

Staff::~Staff(){
	
}

void Staff::set(int currentRank,std::string newCalc_ident, std::string newStatus,std::string newDt_in, std::string newDt_out, std::string newWard, std::string newGender, std::map <std::string, std::vector<std::string>> newPositivePathogens,std::vector <std::string> newActualPositivePathogens, std::map<std::string, std::string> newMapColonization, std::map<std::string, std::pair<std::string, std::string>> newPathogensStatus, std::string newManuportage,std::string newCat){
	id_.currentRank(currentRank);
	calc_ident_ =newCalc_ident;
	status_ = newStatus;
	dt_in_ = newDt_in;
	dt_out_ = newDt_out;
	ward_ = newWard;
	gender_ = newGender;
	positivePathogens_ = newPositivePathogens;
	actualPositivePathogens_ = newActualPositivePathogens;
	mapColonization_ = newMapColonization;
	pathogensStatus = newPathogensStatus;
	manuportage = newManuportage;
	cat_ = newCat;
	
}

void Staff::affiche() const{
	
	cout<< "I'm alive! and my name is " << calc_ident_ << " and I'm a " << cat_ <<endl;
}

/* Serializable individual Package Data */

IndividualPackage::IndividualPackage(){
	
}

IndividualPackage::IndividualPackage(int _id, int _rank, int _type, int _currentRank, std::string _calc_ident,std::string _status,std::string _dt_in,std::string _dt_out,std::string _ward,std::string _gender, std::map <std::string, std::vector<std::string>> _positivePathogens, std::vector <std::string> _actualPositivePathogens,  std::map<std::string, std::string> _mapColonization, std::map<std::string, std::pair<std::string, std::string>> _pathogensStatus, std::string _manuportage, std::string _supp):
OrganismPackage(_id, _rank, _type, _currentRank), calc_ident(_calc_ident), status(_status),dt_in(_dt_in),dt_out(_dt_out),ward(_ward),gender(_gender), positivePathogens(_positivePathogens), actualPositivePathogens(_actualPositivePathogens), mapColonization(_mapColonization), pathogensStatus(_pathogensStatus), manuportage(_manuportage), supp(_supp){ 

}

Contact::Contact(){
	
}

Contact::Contact(vector<string> v):from(v[0]),to(v[1]),day(v[2]),length(v[3]){

}






