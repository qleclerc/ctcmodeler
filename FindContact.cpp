/* FINDCONTACT CLASS
* this file gather all organism from the project
*
*	
*	FindContact.cpp
*
*	Created on: sept 04, 2018
*	Author: Audrey
*
*/
#include "repast_hpc/initialize_random.h"

#include "FindContact.h"

using namespace std;
using namespace repast;


/*
FindContact::FindContact(repast::Properties* mainProps_, string ctc_, string admin_, string schedule_, string beginDate_, string endDate_, int nbOfSimuWeeks_, string filenameParameters_, bool recordTest_, bool catBool_):mainProps(mainProps_), ctc(ctc_), admin(admin_), schedule(schedule_), beginDate(beginDate_), endDate(endDate_), nbOfSimuWeeks(nbOfSimuWeeks_), filenameParameters(filenameParameters_), recordTest(recordTest_), catBool(catBool_){
	beginDate = beginDate + " 00:00:00";
	// endDate = endDate + " 23:59:30";
	endDate = endDate + " 00:00:00";
	// beginDateSimuTime = Learning::stringToDate(endDate) + time_t(24*60*60);
	beginDateSimuTime = Learning::stringToDate(endDate) + time_t(60);
	endDateSimuTime = beginDateSimuTime + time_t(nbOfSimuWeeks*7*24*60*60);
	cout << "Compute probabilities begin at " << beginDate << " end at " << endDate << endl;
	cout<< "Find people... " << endl;
	tabAdmin = Learning::buildTabAdmin(admin);
	
	buildMeIndividuals(); // à voir mais je pense que mapTabadmin, pairCat ne servent plus à rien il faut les sup
	// cout << " taille des gens : " << allIndividuals.size() << endl;
	// for(auto const& elem:allIndividuals){
		// cout<< elem->getCalc_ident() << "_";
	// }
	// cout<<endl;
	// fillMapCatGeneral();
	mapTabAdmin = Learning::updateTabAdmin(tabAdmin);
	tuple<map<string, string>, vector<string>, map<string, string>, vector<string>> pairCat = findMapCat(tabAdmin);
	cout<< "Find categories... " << endl;
	mapCat = get<0>(pairCat);
	keyCat = get<1>(pairCat);
	cout<< "Find wards... " << endl;
	mapWard = get<2>(pairCat);
	keyWard = get<3>(pairCat);
	
	nbOfIndPerDay = computeNbOfIndPerDay(beginDate, Learning::dateToString(endDateSimuTime));
	initializeRandom(*mainProps);
	eng = Random::instance()->engine();
	// cout << "seed inside " <<  Random::instance()->seed() << endl;
}
*/

FindContact::FindContact(repast::Properties* mainProps_, string ctc_, string admin_, string schedule_, string beginDate_, string endDate_, bool recordTest_, bool catBool_):mainProps(mainProps_), ctc(ctc_), admin(admin_), schedule(schedule_), beginDate(beginDate_), endDate(endDate_), recordTest(recordTest_), catBool(catBool_){
	beginDate = beginDate + " 00:00:00";
	endDate = endDate + " 00:00:00";
	cout << "admin file: " << admin << endl;
	cout << "contact file: " << ctc << endl;
	cout << "schedule file: " << schedule << endl;

	cout << "Compute begin at " << beginDate << " end at " << endDate << endl;
	cout<< "Find people... " << endl;
	tabAdmin = Learning::buildTabAdmin(admin);
	
	buildMeIndividuals(); // à voir mais je pense que mapTabadmin, pairCat ne servent plus à rien il faut les sup

	mapTabAdmin = Learning::updateTabAdmin(tabAdmin);
	tuple<map<string, string>, vector<string>, map<string, string>, vector<string>> pairCat = findMapCat(tabAdmin);
	cout<< "Find categories... " << endl;
	mapCat = get<0>(pairCat);
	keyCat = get<1>(pairCat);
	cout<< "Find wards... " << endl;
	mapWard = get<2>(pairCat);
	keyWard = get<3>(pairCat);
	
	nbOfIndPerDay = computeNbOfIndPerDay(beginDate, endDate);
	initializeRandom(*mainProps);
	eng = Random::instance()->engine();
}


FindContact::~FindContact(){
	for(auto const& [key, val]:allContacts){
		for(auto const& elem:val){
			delete elem;
		}
	}
	for(auto const& elem:allIndividuals){
		delete elem;
	}
}

FindContact::FindContact(){
	
}

void FindContact::buildSchedule(){
	Filetreatment *f_schedule = new Filetreatment(schedule);
	string line_schedule;
	std::ifstream ifs_schedule;
	ifs_schedule =  ifstream(f_schedule->getFile());
	delete f_schedule;
	vector<string> tempo_s;
	getline(ifs_schedule,line_schedule); //la première ligne c'est le header
	boost::trim_if(line_schedule, boost::is_any_of("\r "));
	boost::split(tempo_s, line_schedule, boost::is_any_of(";"));
	//let's go
	while(getline(ifs_schedule,line_schedule)){
		boost::trim_if(line_schedule, boost::is_any_of("\r "));
		boost::split(tempo_s, line_schedule, boost::is_any_of(";"));
		string day1 = tempo_s[4].substr(0,10);
		string day2 = tempo_s[5].substr(0,10);
		
		time_t day1_time = Learning::stringToDate(tempo_s[4]);
		tm* day1_st = localtime(&day1_time);
		int  firstHour = day1_st->tm_hour;
		int fristMin = day1_st->tm_min;
		// if(fristMin<1)firstHour = firstHour - 1;
		
		time_t day2_time = Learning::stringToDate(tempo_s[5]);
		tm* day2_st = localtime(&day2_time);
		int  secondHour = day2_st->tm_hour;
		int secondMin = day2_st->tm_min;
		// if(secondMin<10)secondHour = secondHour - 1;
		
		// int firstHour = ( (Learning::stringToDate(tempo_s[4]) / 3600) % 24);
		// int secondHour = ( (Learning::stringToDate(tempo_s[5]) / 3600) % 24);
		// cout << tempo_s[5] << " --> " << secondHour << endl;
		
		if(day1==day2){
			vector<int> vector_hours;
			for(int i(firstHour); i<=secondHour; i++){
				vector_hours.push_back(i);
			}
			schedulePE[tempo_s[0]][day1] = vector_hours;
		}else{
			//ça commence le jour 1 et fini le jour 2
			vector<int> vector_hours1;
			for(int i(firstHour); i<24; i++){
				vector_hours1.push_back(i);
			}
			auto itDay = schedulePE[tempo_s[0]].find(day1);
			if(itDay!=schedulePE[tempo_s[0]].end()){
				//déjà dedans
				for(auto const& vh:vector_hours1){
					auto itVH = find(schedulePE[tempo_s[0]][day1].begin(), schedulePE[tempo_s[0]][day1].end(), vh);
					if(itVH==schedulePE[tempo_s[0]][day1].end()){
						schedulePE[tempo_s[0]][day1].push_back(vh);
					}
				}
				
				// schedulePE[tempo_s[0]][day1].insert(schedulePE[tempo_s[0]][day1].end(), vector_hours1.begin(), vector_hours1.end());
			}else{
				schedulePE[tempo_s[0]][day1] = vector_hours1;
			}
			vector<int> vector_hours2;
			for(int i(0); i<=secondHour; i++){
				vector_hours2.push_back(i);
			}
			auto itDay2 = schedulePE[tempo_s[0]].find(day2);
			if(itDay2!=schedulePE[tempo_s[0]].end()){
				//déjà dedans
				for(auto const& vh2:vector_hours2){
					auto itVH2 = find(schedulePE[tempo_s[0]][day2].begin(), schedulePE[tempo_s[0]][day2].end(), vh2);
					if(itVH2==schedulePE[tempo_s[0]][day2].end()){
						schedulePE[tempo_s[0]][day2].push_back(vh2);
					}
				}
				
				// schedulePE[tempo_s[0]][day2].insert(schedulePE[tempo_s[0]][day2].end(), vector_hours2.begin(), vector_hours2.end());
			}else{
				schedulePE[tempo_s[0]][day2] = vector_hours2;
			}
			
		}
		
	}
	// map<string, vector<int>> test = schedulePE["PE-559-BED"];
	// for(auto const& [key, val]: test){
		// cout << key << " : ";
		// for(auto const v:val){
			// cout << v<< " ";
		// }
		// cout<<endl;
	// }
}

void FindContact::buildHourlyProb(string date1, string date2, bool randomGraph_){
	vector<string> allDatesPeriod = returnVectorOfDay(date1, date2);
	sort(allDatesPeriod.begin(), allDatesPeriod.end());
	map<string, map<string, map<int, vector<double>>>> result;
	for(auto const& [individual, days]:allIndDates){
		if(individual->getStatus()=="PE"){
			vector<string> all_dates = days;
			string cat = findMeCatGeneral(individual, withHosp);
			string ward = individual->getWard();
			string cc = cat + "_" + ward;
			// string cc = cat;
			if(randomGraph_)cc = cat;
			sort(all_dates.begin(), all_dates.end());
			vector<string> common;
			set_intersection(allDatesPeriod.begin(), allDatesPeriod.end(), all_dates.begin(), all_dates.end(),  back_inserter(common));
			if(!common.empty()){
				//common = toutes ses dates supposés
				int wd_common = numberOfDaysByWeekStatus(common, "weekday");
				int we_common = numberOfDaysByWeekStatus(common, "weekend");
				map<string, map<int, int>> nbOfCreneaux;
				for(int i(0); i<24;i++){
					nbOfCreneaux["weekend"][i] = 0;
					nbOfCreneaux["weekday"][i] = 0;
				}
				
				// if(individual->getCalc_ident()=="PE-544-LEB"){
					// for(auto const& elem:common){
						// cout<<elem << " ";
					// }
					// cout<<endl;
					// cout << " wd_common " << wd_common << " we_common " << we_common << endl;
				// }
				
				map<string, vector<int>> SPE = schedulePE[individual->getCalc_ident()];
				for(auto const& [day, hours]:SPE){
					auto itF = find(common.begin(), common.end(), day);
					if(itF!=common.end()){
						string weekStat = findWeekStatus(Learning::stringToDate(day));
						for(auto const& h:hours){
							nbOfCreneaux[weekStat][h] = nbOfCreneaux[weekStat][h] + 1;
							// if(individual->getCalc_ident()=="PE-544-LEB")cout << " day " << day << " h " << h << endl;
						}
					}
				}
				for(auto const& [ws, mmap]:nbOfCreneaux){
					for(auto const& [hour, value]:mmap){
						double nb = 0.0;
						if(value!=0 && ws=="weekday")nb = (double)value/wd_common;
						if(value!=0 && ws=="weekend")nb = (double)value/we_common;
						// if(individual->getCalc_ident()=="PE-544-LEB")cout<< ws << " hour " << hour << " value " << value << " wd_common " << wd_common <<endl;
						result[cc][ws][hour]. push_back(nb);
					}
				}
			}
		}
		
	}
	
	
	cout << "hourly prob process... " <<endl;
	for(auto const& [cc, mmap]:result){
		for(auto const& [WS, mmap2]:mmap){
			for(auto const& [hour, values]:mmap2){
				double res = Learning::computeMean(values);
				double resVar = Learning::computeSampleVariance(res, values);
				double tot=0;
				// if(!isnan(resVar)){
					// LogNormalGenerator contenerNormal = Random::instance()->createLogNormalGenerator(res,sqrt(resVar));
					// double toChoose = contenerNormal.next();
					// while(toChoose>1.0){
						// toChoose = contenerNormal.next();
					// }
					// tot = toChoose;
				// }else{
					// cout << cc << " --> " << WS << " --> " << hour << " --> " << res << endl;
					tot = res;
				// }
				// if(randomGraph_)tot=res;
				hourlyProb[cc][WS][hour] = tot;
				// cout << cc << " --> " << WS << " --> " << hour << " --> " << res << endl;
			}
		}
	}
	
}


void FindContact::buildMeIndividuals(){
	/* Fonction qui construit des objets de type Individuals à partir du fichier d'admission */
	vector<string> allDatesPeriod = returnVectorOfDay(beginDate, endDate);
	sort(allDatesPeriod.begin(), allDatesPeriod.end());
	for(auto const& elem:tabAdmin){
		string date1 = elem[2];
		string date2 = elem[3];
		string name = elem[0];
		if(date2=="")date2=endDate.substr(0,10);
		vector<string> allDatesInd = returnVectorOfDay(date1, date2);
		sort(allDatesInd.begin(), allDatesInd.end());
		vector<string> common;
		set_intersection(allDatesPeriod.begin(), allDatesPeriod.end(), allDatesInd.begin(), allDatesInd.end(),  back_inserter(common));
		if(!common.empty()){
			vector<Individual*>::iterator itIndividual = find_if(allIndividuals.begin(), allIndividuals.end(),[name](Individual* i)->bool {return(i->getCalc_ident()==name);});
			if(itIndividual==allIndividuals.end()){
				if(elem[1]=="PA"){
					Patient* patient = new Patient(elem);
					allIndividuals.push_back(patient);
					admissionDischarge[patient->getCalc_ident()]["admission"].push_back(date1);
					admissionDischarge[patient->getCalc_ident()]["discharge"].push_back(date2);
					allIndDates[patient] = common;
					string hosp = patient->getHosp_flag();
					if(hosp==""){
						hosp = "Patient";
					}
					auto itHosp = find(hospitalizationStatus.begin(), hospitalizationStatus.end(), hosp);
					if(itHosp == hospitalizationStatus.end()){
						hospitalizationStatus.push_back(hosp);
					}
					auto itWard = find(catByWard[patient->getWard()].begin(), catByWard[patient->getWard()].end(), "Patient");
					if(itWard==catByWard[patient->getWard()].end()){
						catByWard[patient->getWard()].push_back("Patient");
					}
					
				}else if(elem[1]=="PE"){
					Staff* staff = new Staff(elem);
					allIndividuals.push_back(staff);
					admissionDischarge[staff->getCalc_ident()]["admission"].push_back(date1);
					admissionDischarge[staff->getCalc_ident()]["discharge"].push_back(date2);
					string cat = staff->getCat();
					vector<string>::iterator itC = find(keyCat.begin(), keyCat.end(), cat);
					if(itC==keyCat.end()){
						keyCat.push_back(cat);
					}
					allIndDates[staff] = common;
					auto itWard = find(catByWard[staff->getWard()].begin(), catByWard[staff->getWard()].end(), staff->getCat());
					if(itWard==catByWard[staff->getWard()].end()){
						catByWard[staff->getWard()].push_back(staff->getCat());
					}
				}else{
					cout << " ooooops je ne sais pas ce que c'est je ne peux pas initialiser ce truc! " << endl;
				}	
			}else{
				//il est deja dedans on rajoute des dates
				allIndDates[(*itIndividual)].insert(allIndDates[(*itIndividual)].end(), common.begin(), common.end());
				admissionDischarge[(*itIndividual)->getCalc_ident()]["admission"].push_back(date1);
				admissionDischarge[(*itIndividual)->getCalc_ident()]["discharge"].push_back(date2);
			}	
		}
	}
	keyCat.push_back("Patient");
}

vector<string> FindContact::returnVectorOfDay(string bday, string eday){
	vector<string> allDatesPeriod;
	for(time_t i(Learning::stringToDate(bday.substr(0,10))); i<=Learning::stringToDate(eday.substr(0,10)); i=i+time_t(86400)){
		allDatesPeriod.push_back(Learning::dateToString(i).substr(0,10));
	}
	return(allDatesPeriod);
}

tuple<std::map<std::string, std::string>, vector<string>, map<string, string>, vector<string>> FindContact::findMapCat(vector<vector<string>> tabAdmin){
	/* Fonction qui pour chaque catégorie de tabAdmin renvoie une map avec comme clé la catégorie et comme valeur un vector d'individu appartenant à cette catégorie */
	map<string, string> results;
	map<string, string> resultsWard;
	vector<string> vectorCat;
	vector<string> vectorWard;
	for(int i(0); i<tabAdmin.size(); i++){
		vector<string> tmp = tabAdmin[i];
		string cat;
		string ward;
		if(tmp[0].compare(0,2,"PA")==0){
			cat = string("Patient");
		}else{
			cat = tmp[7];
		}
		ward = tmp[4];
		results.insert(make_pair(tmp[0], cat));
		resultsWard.insert(make_pair(tmp[0], ward));
		vector<string>::iterator it = find(vectorCat.begin(), vectorCat.end(), cat);
		if(it==vectorCat.end()){
			vectorCat.push_back(cat);
		}
		vector<string>::iterator itW = find(vectorWard.begin(), vectorWard.end(), ward);
		if(itW==vectorWard.end()){
			vectorWard.push_back(ward);
		}
	}
	return make_tuple(results, vectorCat, resultsWard, vectorWard);
}

map<string, vector<Individual*>> FindContact::computeNbOfIndPerDay(string fDate, string lDate){
	/* Fonction qui détermine le nombre d'individus présent par jour sous la forme d'une map */
	map<string,vector<Individual*>> results;
	for(auto const& [ind, dates]:allIndDates){
		for(auto const& d:dates){
			results[d].push_back(ind);
		}
	}
	return results;
}

map<pair<string, string>, pair<double, double>> FindContact::sensorsBias(string fDate, string lDate){
	map<pair<string, string>, vector<double>>	res;
	map<pair<string, string>, pair<double, double>> result;
	vector<string> allDatesPeriod = returnVectorOfDay(beginDate, endDate);
	sort(allDatesPeriod.begin(), allDatesPeriod.end());
	for(auto const& ind:allIndividuals){
		vector<string> dates = allIndDates[ind];
		vector<string> realDates;
		string catInd;
		string ward = ind->getWard();
		pair<string, string> key;
		if(ind->getStatus()=="PA"){
			catInd = "Patient";
			realDates = dates;
		}else{
			Staff* s = dynamic_cast<Staff*>(ind);
			catInd = s->getCat();
			map<string, vector<int>> scheduleIndividual = schedulePE[ind->getCalc_ident()];
			vector<string> datePE;
			for(auto const& dp:dates){
				auto itF = scheduleIndividual.find(dp);
				if(itF!=scheduleIndividual.end()){
					datePE.push_back(dp);
				}
			}
			realDates = datePE;
		}
		sort(realDates.begin(), realDates.end());
		vector<string> common;
		set_intersection(realDates.begin(), realDates.end(), allDatesPeriod.begin(), allDatesPeriod.end(),  back_inserter(common));
		if(!common.empty()){
			double nbCommon = common.size();
			vector<string> recordedDays;
			map<string, vector<int>> recordMap = daysOfRecordByInd[ind->getCalc_ident()];
			for(auto const& [days, hours]:recordMap){
				auto it = find(allDatesPeriod.begin(), allDatesPeriod.end(), days);
				if(it!=allDatesPeriod.end()){
					recordedDays.push_back(days);
				}
			}
			if(!recordedDays.empty()){
				double nbRD = recordedDays.size();
				double proportion = (nbRD/nbCommon);
				key = make_pair(catInd, ward);
				res[key].push_back(proportion);
			}
		}
	}
	for(auto const& [cat, vector_Proportion]:res){
		vector<double> vProp = vector_Proportion;
		double meanProp = Learning::computeMean(vProp);
		double varProp = Learning::computeSampleVariance(meanProp, vProp);
		double sdProp = sqrt(varProp);
		result[cat] = make_pair(meanProp, sdProp);
	}
	return(result);
}

map<string, map<int, vector<Individual*>>> FindContact::computeNbOfIndPerDayWithHours(string fDate, string lDate, bool recorded, bool randomGraph_){
	/* Fonction qui détermine le nombre d'individu présent par heure et par jour */
	map<string, map<int, vector<Individual*>>> res;
	DoubleUniformGenerator rndContener = Random::instance()->createUniDoubleGenerator(0,1);
	map<Individual*, vector<string>> IndDates;
	if(recorded){	
	cout << "size daysOfRecordByInd " << daysOfRecordByInd.size() << endl;
		for(auto const& [ind, mmap]:daysOfRecordByInd){
			vector<string> datesRecord;
			string indSTR = ind;
			for(auto const& [key, val]:daysOfRecordByInd[indSTR]){
				datesRecord.push_back(key);
			}
			vector<Individual*>::iterator it = find_if(allIndividuals.begin(), allIndividuals.end(), [indSTR](Individual* i)->bool {return(i->getCalc_ident()==indSTR);});
			IndDates[(*it)] = datesRecord;
		}
	}else{
		IndDates = allIndDates;
	}
	for(auto const& [ind, dates]:IndDates){
		for(auto const& d:dates){
			time_t dayTime = Learning::stringToDate(d);
			tm* time_st = localtime(&dayTime);
			int wday = time_st->tm_wday;
			string wsStatus;
			if(wday==0 || wday==6){
				wsStatus="weekend";
			}else{
				wsStatus="weekday";
			}
			for(int hour(0); hour<24; hour++){
				if(!recorded){
					if(ind->getStatus()=="PE"){
						auto itAdm = find(admissionDischarge[ind->getCalc_ident()]["admission"].begin(), admissionDischarge[ind->getCalc_ident()]["admission"].end(), d);
						auto itDis = find(admissionDischarge[ind->getCalc_ident()]["discharge"].begin(), admissionDischarge[ind->getCalc_ident()]["discharge"].end(), d);
						if((itAdm == admissionDischarge[ind->getCalc_ident()]["admission"].end() && itDis == admissionDischarge[ind->getCalc_ident()]["discharge"].end()) || (itAdm != admissionDischarge[ind->getCalc_ident()]["admission"].end() && hour>1) || (itDis != admissionDischarge[ind->getCalc_ident()]["discharge"].end() && hour<23)){
							Staff* s = dynamic_cast<Staff*>(ind);							
							string cc = s->getCat() + "_" + s->getWard();
							if(randomGraph_)cc = s->getCat();
							double probPresence = hourlyProb[cc][wsStatus][hour];
							double randomProbaRes = rndContener.next();
							if(randomProbaRes<probPresence){
								res[d][hour].push_back(ind);
							}
						}
					}else{
						auto itAdm = find(admissionDischarge[ind->getCalc_ident()]["admission"].begin(), admissionDischarge[ind->getCalc_ident()]["admission"].end(), d);
						auto itDis = find(admissionDischarge[ind->getCalc_ident()]["discharge"].begin(), admissionDischarge[ind->getCalc_ident()]["discharge"].end(), d);
						if((itAdm == admissionDischarge[ind->getCalc_ident()]["admission"].end() && itDis == admissionDischarge[ind->getCalc_ident()]["discharge"].end()) || (itAdm != admissionDischarge[ind->getCalc_ident()]["admission"].end() && hour>7) || (itDis != admissionDischarge[ind->getCalc_ident()]["discharge"].end() && hour<20)){
							res[d][hour].push_back(ind);
						}
					}
				}else{
					auto itHour = find(daysOfRecordByInd[ind->getCalc_ident()][d].begin(), daysOfRecordByInd[ind->getCalc_ident()][d].end(), hour);
					if(itHour!=daysOfRecordByInd[ind->getCalc_ident()][d].end()){
						res[d][hour].push_back(ind);
					}
				}
			}			
		}
	}
	return res;
}

vector<string> FindContact::keepPeriodDates(vector<string> input_vector, string fDate, string lDate){
	/* Fonction qui retourne les dates de input_vecteur qui sont comprise en fDate et lDate */
	vector<string> res;
	for(auto const& d:input_vector){
		if(Learning::stringToDate(d)>=Learning::stringToDate(fDate) && Learning::stringToDate(d)<Learning::stringToDate(lDate)){
			res.push_back(d);
		}
	}
	return res;
}

map<string, vector<Individual*>> FindContact::computeNbOfIndPerDayRecord(string fDate, string lDate){
	/* Fonction qui détermine le nombre d'individu par jour en fonction du biais de capteur */
	map<string,vector<Individual*>> results;
	for(auto const& [ind, dates]:allIndDates){
		vector<string> datesP = keepPeriodDates(dates, fDate, lDate);
		if(datesP.size()!=0){
			double p=0.0;
			double et=0.0;
			if(ind->getStatus()=="PA"){
				et=0.22;
				p = 0.55;
			}else{
				et=0.25;
				p = 0.51;
			}
			NormalGenerator contenerNormal = Random::instance()->createNormalGenerator(p,et);
			double toChoose = contenerNormal.next();
			vector<string> datesR = chooseRandomDate(datesP, toChoose);
			for(auto const& d:datesR){
				results[d].push_back(ind);
			}
		}
	}
	return results;
}

vector<string> FindContact::chooseRandomDate(vector<string> input_vector, double p){
	/* Fonction qui choisi un ensemble de dates de façon aléatoire selon la probabilité p dans le vecteur input_vector. La première date est choisi de façon aléatoire et les autres se suivent */
	vector<string> res;
	if(p>1.0){
		p=1.0;
	}
	int nbOfDays = (int)round(input_vector.size() * p);
	if(nbOfDays<=0){
		nbOfDays = 1;
	}
	if(nbOfDays==input_vector.size()){
		return input_vector;
	}else{
		vector<int> index;
		// on va choisir des dates qui se suivent donc il faut une date de départ qui permet d'avoir nbOfDays dates après
		int sizeF = input_vector.size()-nbOfDays;
		vector<string> newInput;
		newInput.assign(input_vector.begin(), input_vector.begin()+sizeF);
		//on va choisir une date de départ dedans
		IntUniformGenerator chooseRandom = Random::instance()->createUniIntGenerator(0,newInput.size()-1);
		int indice_deb = chooseRandom.next();
		int indice_fin = (indice_deb + nbOfDays);
		//maintenant on récup dans le vecteur les dates
		for(int i(indice_deb); i<indice_fin; i++){
			res.push_back(input_vector[i]);
		}
		return(res);
	}
}

map<string, vector<Individual*>> FindContact::computeNbOfIndPerDayWithSchedule(string fDate, string lDate){
	map<string,vector<Individual*>> results;
	// vector<string> allDatesPeriod = returnVectorOfDay(beginDate, endDate);
	map<string, vector<int>> scheduleIndividual;
	for(auto const& [ind, dates]:allIndDates){
		if(ind->getStatus()=="PE")scheduleIndividual = schedulePE[ind->getCalc_ident()];
		for(auto const& d:dates){
			if(ind->getStatus()=="PE"){
				auto it = scheduleIndividual.find(d);
				if(it!=scheduleIndividual.end()){
					results[d].push_back(ind);
				}
			}else{
				results[d].push_back(ind);
			}
		}
	}
	return results;
}

map<string, map<string, vector<Individual*>>> FindContact::nbOfIndByCatByDay(string fDate, string lDate){
	
	map<string, map<string, vector<Individual*>>> results;
	// vector<string> allDatesPeriod = returnVectorOfDay(beginDate, endDate);

	for(auto const& [ind, dates]:allIndDates){
		string cat = findMeCatGeneral(ind, withHosp);
		for(auto const& d:dates){
			results[cat][d].push_back(ind);
		}
	}
	return results;
}


void FindContact::buildMapContact(){
	/* Fonction qui construit une map de contact à partir du fichier de contact en csv */
	Filetreatment *f_ctc = new Filetreatment(ctc);
	string line_ctc;
	std::ifstream ifs_ctc;
	ifs_ctc =  ifstream(f_ctc->getFile());
	delete f_ctc;
	vector<string> tempo_ctc;
	getline(ifs_ctc,line_ctc); 
	getline(ifs_ctc, line_ctc); 
	boost::trim_if(line_ctc, boost::is_any_of("\r "));
	boost::split(tempo_ctc, line_ctc, boost::is_any_of(";"));
	struct tm tm_ctc = {0};
	const char *tempo_ctcChar = tempo_ctc[2].c_str();
	strptime(tempo_ctcChar, "%Y-%m-%d%t%T", &tm_ctc);
	time_t timCtc = mktime(&tm_ctc);
	tm* time_st = localtime(&timCtc);
	int wday = time_st->tm_hour;
	time_t timStart = Learning::stringToDate(endDate);	
	int c = 0;
	if(timCtc < Learning::stringToDate(beginDate)){
		while(timCtc < Learning::stringToDate(beginDate) && !ifs_ctc.eof()){
			getline(ifs_ctc, line_ctc);
			boost::trim_if(line_ctc, boost::is_any_of("\r "));
			boost::split(tempo_ctc, line_ctc, boost::is_any_of(";"));
			tempo_ctcChar = tempo_ctc[2].c_str();
			strptime(tempo_ctcChar, "%Y-%m-%d%t%T", &tm_ctc);
			timCtc = mktime(&tm_ctc);
		}
	}	
	if(timCtc < timStart){
		do{
			c = c + 1;
			shared_ptr<Contact> ctcObjet = make_shared<Contact>(tempo_ctc);
			shared_ptr<Contact> ctcObjet2 (ctcObjet);
			mapCtc[tempo_ctc[0]].push_back(ctcObjet);
			int hour = (Learning::stringToDate(tempo_ctc[2]) / 3600) % 24;
			
			auto itInd1 = find(daysOfRecordByInd[tempo_ctc[0]][tempo_ctc[2].substr(0,10)].begin(), daysOfRecordByInd[tempo_ctc[0]][tempo_ctc[2].substr(0,10)].end(), hour);
			if(itInd1 == daysOfRecordByInd[tempo_ctc[0]][tempo_ctc[2].substr(0,10)].end()){
				daysOfRecordByInd[tempo_ctc[0]][tempo_ctc[2].substr(0,10)].push_back(hour);
			}
			auto itInd2 = find(daysOfRecordByInd[tempo_ctc[1]][tempo_ctc[2].substr(0,10)].begin(), daysOfRecordByInd[tempo_ctc[1]][tempo_ctc[2].substr(0,10)].end(), hour);
			if(itInd2 == daysOfRecordByInd[tempo_ctc[1]][tempo_ctc[2].substr(0,10)].end()){
				daysOfRecordByInd[tempo_ctc[1]][tempo_ctc[2].substr(0,10)].push_back(hour);
			}
			string cat = mapCat[tempo_ctc[0]] + "-" + mapCat[tempo_ctc[1]];
			getline(ifs_ctc, line_ctc);
			boost::trim_if(line_ctc, boost::is_any_of("\r "));
			boost::split(tempo_ctc, line_ctc, boost::is_any_of(";"));
			tempo_ctcChar = tempo_ctc[2].c_str();
			strptime(tempo_ctcChar, "%Y-%m-%d%t%T", &tm_ctc);
			timCtc = mktime(&tm_ctc);
			time_st = localtime(&timCtc);
			wday = time_st->tm_hour;
			people.push_back(tempo_ctc[0]);
			people.push_back(tempo_ctc[1]);
			
		}while(timCtc < timStart && !ifs_ctc.eof());
	}
	if(ifs_ctc.is_open()){
		ifs_ctc.close();
	}
	sort(people.begin(), people.end());
	people.erase(unique(people.begin(), people.end()), people.end());
	int ct = 0;
	for(auto const& [key, val]:mapCtc){
		ct = ct + val.size();
	}
	cout << " le nombre de contact est : " << ct << " " << c << endl;
	cout << " la derniere date de contact est : " << tempo_ctc[2] << endl;
	cout << " daysOfRecordByInd " << daysOfRecordByInd.size() << endl;
}

int FindContact::nbOf(vector<string> individuals, string cat){
	/* Fonction qui détermine le nombre d'individus du vecteur individuals appartenant à la catégorie cat */
	int c = 0;
	for(auto const& elem:individuals){
		if(mapCat[elem] == cat){
			c++;
		}
	}
	return c;
}

map<string, int> FindContact::nbOfCat(vector<string> individuals){
	/* Fonction qui subdivise le vecteur d'individus individuals en une map avec en clé la catégorie et en valeur le nombre d'individus appartenant à cette catégorie */
	map<string, int> res;
	for(auto const& k:keyCat){
		res.insert(make_pair(k, 0));
	}
	for(auto const& elem:individuals){
		res[mapCat[elem]] = res[mapCat[elem]] +1 ;
	}
	return res;
}

bool FindContact::meetingStatus(string ind1, string ind2){
	bool res;
	vector<Individual*> allPeopleOUT = alreadyMeetPeople[ind1]["OutsideWard"];
	vector<Individual*> allPeople = alreadyMeetPeople[ind1]["InsideWard"];
	allPeople.insert( allPeople.end(), allPeopleOUT.begin(), allPeopleOUT.end() );
	vector<Individual*>::iterator itMet = find_if(allPeople.begin(), allPeople.end(), [ind2](Individual* n)->bool {return n->getCalc_ident()==ind2;});
	if(itMet!= allPeople.end()){
		//il est dedans
		res = true;
	}else{
		res = false;
	}
	return (res);
}

map<string, map<string, vector<Individual*>>> FindContact::catInsideOutsideWard(vector<Individual*> individuals, string ward, Individual* ind){
	//retourne une map <InsideWard/OutsideWard, map<cat, vector<individus>>> d'individus du service ward
	map<string, map<string, vector<Individual*>>> res;
	for(auto const& elem:individuals){
		if(elem->getCalc_ident() != ind->getCalc_ident()){
			string cat;
			if(catBool){
				cat = findMeCatGeneral(elem, withHosp);
			}else{
				cat = elem->getStatus();
			}
			string wardStatus = ward + "_" + elem->getWard(); 
			string statusWardMet;			
			if(randomGraph || test=="test1"){
				statusWardMet = "one ward";
			}else{
				statusWardMet = wardStatus;
			}
			res[statusWardMet][cat].push_back(elem) ;
		}
	}
	return res;
}

map<string, map<string, vector<Individual*>>> FindContact::catInsideOutsideWardCohort(vector<Individual*> individuals, string ward, Individual* ind, map<string, vector<string>> map_cohort, std::map<std::pair<std::string, std::string>, std::map<std::string, int>> couple_vector){
	//retourne une map <InsideWard/OutsideWard, map<cat, vector<individus>>> d'individus du service ward
	map<string, map<string, vector<Individual*>>> res;
	string couple1;
	if(ind->getStatus()=="PE"){
		Staff* s = dynamic_cast<Staff*>(ind);
		couple1 = s->getCat();
	}else{
		couple1 = "Patient";
	}
	for(auto const& elem:individuals){
		if(elem->getCalc_ident() != ind->getCalc_ident()){
			string wardStatus = ward + "_" + elem->getWard(); 
			string statusWardMet;
			if(randomGraph || test=="test1"){
				statusWardMet = "one ward";
			}else{
				statusWardMet = wardStatus;
			}
			string cat;
			if(catBool){
				cat = findMeCatGeneral(elem, withHosp);
			}else{
				cat = elem->getStatus();
			}
			string couple2;
			if(cat=="PE"){
				Staff* s2 = dynamic_cast<Staff*>(elem);
				couple2 = s2->getCat();
			}else{
				couple2 = "Patient";
			}
			pair couple = make_pair(couple1, couple2);
			auto it = couple_vector.find(couple);
			pair coupleInverse = make_pair(couple2, couple1);
			auto itInverse = couple_vector.find(coupleInverse);
			if(it!=couple_vector.end()){
				//le couple est dedans donc on va ajouter l'individu que s'il est dans le vecteur
				vector<string> indToAdd = map_cohort[ind->getCalc_ident()];
				auto it2 = find(indToAdd.begin(), indToAdd.end(), elem->getCalc_ident());
				if(it2!=indToAdd.end()){
					res[statusWardMet][cat].push_back(elem) ;
				}
			}else if(itInverse!=couple_vector.end()){
				//le couple est dedans mais en inverse
				vector<string> indToAdd = map_cohort[ind->getCalc_ident()];
				auto it2 = find(indToAdd.begin(), indToAdd.end(), elem->getCalc_ident());
				if(it2!=indToAdd.end()){
					res[statusWardMet][cat].push_back(elem) ;
				}
			}else{
				//le couple est pas dedans on ne fait pas de cohorting pour eux donc on l'ajoute
				res[statusWardMet][cat].push_back(elem) ;
			}
		}
	}
	return res;
}


map<string, vector<string>> FindContact::InsideOutsideWard(vector<string> individuals, string ward){
	/* Fonction qui retourne une map avec en clé InsideWard/OutsideWard et en valeur les individus qui sont à l'interieur du service ward ou à l'exterieur du service ward */
	map<string, vector<string>> res;
	for(auto const& elem:individuals){
		if(mapWard[elem]==ward){
			res["InsideWard"].push_back(elem);
		}else{
			res["OutsideWard"].push_back(elem);
		}
	}
	return res;	
}

string FindContact::findWeekStatus(time_t date){
	string res;
	tm* time_st = localtime(&date);
	int wday = time_st->tm_wday;
	if(wday == 0 || wday == 6){
		res = "weekend";
	}else{
		res = "weekday";
	}
	return res;
}


void FindContact::findMeContactProbabilities(bool randomGraph_in, string test_in){
	//fonction qui va remplir les deux map contenant les proba nécessaire pour construire les contacts. 
	cout << " c'est partie pour les proba " << endl;
	randomGraph = randomGraph_in;
	test = test_in;
	buildSchedule();
	map<string, map<string, map<string, double>>> mapProbaRes;
	map<string, map<string, map< string, pair< vector<int>, vector<double>>>>> mapDistributionRes;
	map<string, map<string, map< string, pair< vector<int>, vector<double>>>>> mapDistributionResInd;
	map<string, vector<string>> alreadyMetRes;
	time_t timFirstDate = Learning::stringToDate(beginDate.substr(0,10));
	time_t timLastDate = Learning::stringToDate(endDate.substr(0,10));
	vector<string> allDatesLearn;
	for(time_t i(timFirstDate); i<=timLastDate; i = i + 86400){
		allDatesLearn.push_back(Learning::dateToString(i).substr(0,10));
	}
	map<string, vector<Individual*>> indPerDay = computeNbOfIndPerDay(beginDate, endDate);
	map<string, map<string, vector<string>>> indPerhour;
	map<string, map<string, int>> creneaux;
	vector<Individual*> individualsCTC;
	//cat - ind - days
	map<string, map<string, vector<string>>> recordedDays = findRecordedDaysByWeekST();
	for(auto const& [cat, indMap]:recordedDays){
		nbOfIndRecord[cat] = indMap.size();
	}
	for(auto const& individual:allIndividuals){
		string catInd = findMeCatGeneral(individual, withHosp);
		string wardInd = individual->getWard();
		vector<string> ARMet = alreadyMetRes[individual->getCalc_ident()];
		map<string, map<string, map<string, map<string, map<string, int>>>>> map_ind;
		map<string, map<string, map<string, map<string, int>>>> map_indDistrib;
		vector<shared_ptr<Contact>> contactOfInd = mapCtc[individual->getCalc_ident()];
		if(!contactOfInd.empty()){
			individualsCTC.push_back(individual);
			for(auto const& contact:contactOfInd){
				//1) trie des info de l'individus pour remplir ensuite la map
				string dateCtc = contact->getDay();
				//comparaison avec l'emploie du temps				
				//si le crenaux est bien dans l'emploie du temps
				string weekdayCtc = findWeekStatus(Learning::stringToDate(dateCtc));
				int hour = (Learning::stringToDate(dateCtc) / 3600) % 24;
				string hour_week = to_string(hour) + "_" + weekdayCtc;
				string other = contact->getTo();
				//on recup l'individus other sous la forme d'un objet
				vector<Individual*>::iterator itTo = find_if(allIndividuals.begin(), allIndividuals.end(), [other](Individual* i)->bool {return(i->getCalc_ident()==other);});
				if(itTo!=allIndividuals.end()){
					//on recup les info propre de other
					string catOther = findMeCatGeneral((*itTo), withHosp);
					string typeOther = (*itTo)->getStatus();
					string wardOther = (*itTo)->getWard();
					string wardStatus; 
					if(randomGraph || test=="test1"){
						wardStatus = "one ward";
					}else{
						wardStatus = wardInd + "_" + wardOther;
					}
					string ARStats;
					vector<string>::iterator itARpeople = find(ARMet.begin(), ARMet.end(), other);
					if(itARpeople!=ARMet.end()){
						ARStats = "already";
					}else{
						ARStats = "notAlready";
						ARMet.push_back(other);
					}
					string condition = wardStatus;
					//on va remplir la map pour les probabilités qui sera differencié en fonction des heures de weekstatus et de la categorie de other ainsi que du jour
					string catType;
					if(catBool){
						catType = catOther;
					}else{
						catType = typeOther;
					}
					map<string, int>::iterator itOther = map_ind[hour_week][condition][catType][dateCtc.substr(0,10)].find(other);
					if(itOther!=map_ind[hour_week][condition][catType][dateCtc.substr(0,10)].end()){
						// il est dedans 
						map_ind[hour_week][condition][catType][dateCtc.substr(0,10)][other] = map_ind[hour_week][condition][catType][dateCtc.substr(0,10)][other] + stod(contact->getLength());
					}else{
						//il est pas dedans donc on somme les durées comme ça c'est aggregé par heure
						map_ind[hour_week][condition][catType][dateCtc.substr(0,10)][other] = stod(contact->getLength());
					}
					
					//on va remplir la map pour les distributions qui depend de l'heure, du type de other et du jour
					string testWard =  hour_week + "-" + wardStatus;
					map<string, int>::iterator itOtherDistrib = map_indDistrib[testWard][catType][dateCtc.substr(0,10)].find(other);
					if(itOtherDistrib!=map_indDistrib[testWard][catType][dateCtc.substr(0,10)].end()){
						map_indDistrib[testWard][catType][dateCtc.substr(0,10)][other] = map_indDistrib[testWard][catType][dateCtc.substr(0,10)][other] + stod(contact->getLength());
					}else{
						map_indDistrib[testWard][catType][dateCtc.substr(0,10)][other] = stod(contact->getLength());
					}
				}else{
					cout << other << " n'est pas dedans pb pb " << endl;
				}
			}

			//2) on rempli les map pour les heures 
			for(auto const& [h_week, mmap]:map_ind){
				//cdt - catOther - date - other - length
				map<string, map<string, map<string, map<string, int>>>> mapIndH = map_ind[h_week];
				for(auto const& [key, val]:mapIndH){
					string cdt = key;
					//catOther - date - other - length
					map<string, map<string, map<string, int>>> valCDT = val;
					for(auto const& [k, v]:valCDT){
						//date - other - length
						map<string, map<string, int>> mapOther = v;
						string couple;
						if(catBool){
							couple = catInd + "_" + k;
						}else{
							couple = individual->getStatus() + "_" + k;
						}
						int vInt = v.size(); //donne le nombre de fois où l'ind a été en contact avec un individus de la categorie k. ce nb c'est donc le nombre de jour (nombre de crenaux)
						int nbP = 0;
						for(auto const& [d, mapV]:mapOther){
						//pour chaque jour
							nbP = nbP + mapV.size(); 
						}						
						double toAdd = (double)nbP;
						//remplissage des proba
						map<string, double>::iterator itMapRes = mapProbaRes[h_week][cdt].find(couple);
						if(itMapRes!=mapProbaRes[h_week][cdt].end()){
							//on va sommer pour chaque individu
							mapProbaRes[h_week][cdt][couple] = mapProbaRes[h_week][cdt][couple] + toAdd;
						}else{							
							mapProbaRes[h_week][cdt][couple] = toAdd;
						}
					}
				}
			}
			//remplissage des distributions
			for(auto const& [h_week_ward, mmap]:map_indDistrib){
				//hour - type/cat - day - other - length
				vector<string> h_week_test;
				boost::split(h_week_test, h_week_ward, boost::is_any_of("-"));
				string h_week = h_week_test[0];
				string wardT = h_week_test[1];				
				for(auto const& [k, mmap2]:mmap){
					//date - other - length
					map<string, map<string, int>> mapIntermDay = mmap2;
					vector<double> nbP;
					vector<double> lengthP;
					string coupleDistrib;
					if(catBool){
						coupleDistrib =  catInd + "_" + k;
					}else{
						coupleDistrib = individual->getStatus() + "_" + k;
					}
					
					for(auto const& [d, mapV]:mapIntermDay){
					//pour chaque jour
						nbP.push_back(mapV.size()); //on recup le nb d'individu de la cat/type
						//other - length
						map<string, int> mapPeopleLength = mapV;
						for(auto const& [people, nb]:mapPeopleLength){
							lengthP.push_back((double)nb); //pour chaque individu on recup sa durée de contact pour cette heure ce jour
						}
						
					}
					string newCDT = wardT;
					string newHour = "one hour";
					//puis on rempli
					mapDistributionRes[h_week][newCDT][coupleDistrib].first.insert(mapDistributionRes[h_week][newCDT][coupleDistrib].first.end(), nbP.begin(), nbP.end());
					mapDistributionRes[h_week][newCDT][coupleDistrib].second.insert(mapDistributionRes[h_week][newCDT][coupleDistrib].second.end(), lengthP.begin(), lengthP.end());
				}
				
			}
		}
	}
	//cat - weekstatus - hour - nb
	map<string, map<string, map<int, int>>> recordedHours = findRecordedHoursByWeekST(randomGraph);

	//3) on rajoute le denominateur à la map
	for(auto const& [key, val]:mapProbaRes){
		vector<string> h_week;
		boost::split(h_week, key, boost::is_any_of("_"));
		for(auto const& [k, v]:val){
			vector<string> w_wards;
			boost::split(w_wards, k , boost::is_any_of("_"));
			//k = condition
			for(auto const& [k2, v2]:v){
				//k2 = cat/type
				vector<string> coupleST;
				boost::split(coupleST, k2, boost::is_any_of("_"));
				string couple_ward = coupleST[0] + "_" + w_wards[0];
				if(randomGraph)couple_ward = coupleST[0];
				int nbOf = recordedHours[couple_ward][h_week[1]][stoi(h_week[0])];				
				mapProbaRes[key][k][k2] = (mapProbaRes[key][k][k2]/(double)nbOf); //ici il y a la somme du nombre de jour où un individu a au moins eu un contact avec un individu k2 pour chaque individus. Diviser par le nombre de patient-crénau. 
			}
		}
	}
	resProba = mapProbaRes;
	
	//4) on fait deux maps: une pour le nombre et une pour lenght avec mean et sd
	for(auto const& [key, val]:mapDistributionRes){
		for(auto const& [k, v]:val){
			for(auto const& [k2, v2]:v){
				pair<vector<int>, vector<double>> pairNBL = v2;
				vector<double> numbers; 
				for(auto const& n:pairNBL.first){
					numbers.push_back((double)n);
				}
				//le nombre
				double meanNumbers = Learning::computeMean(numbers);
				double varNumbers = numbers.size();
				mapDistributionNB[key][k][k2] = make_pair(meanNumbers, varNumbers);
				//la durée
				vector<double> durations = pairNBL.second;
				double meanDurations = Learning::computeMean(durations);
				double varDurations = Learning::computeSampleVariance(meanDurations, durations);
				mapDistributionLength[key][k][k2] = make_pair(meanDurations, varDurations);	
			}
		}
	}
}

void FindContact::writeContactParameters(string filenameContactParameters, string date1, string date2, bool randomGBool){
	ofstream myCSV;
	myCSV.open(filenameContactParameters);	
	for(auto const& [key, val]:resProba){
		for(auto const& [k,v]:val){
			for(auto const& [k2, v2]:v){
				myCSV << "contact probabilities;" + key + ";" + k + ";" + k2 + ";" + to_string(v2) + ";" << endl;
			}
		}
	}	
	for(auto const& [key, val]:mapDistributionNB){
		for(auto const& [k, v]:val){
			for(auto const& [k2, v2]:v){
				pair<double, double> param = v2;
				myCSV << "contact distribution;" + key + ";" + k + ";" + k2 + ";" + to_string(param.first) + ";" + to_string(param.second) + ";" << endl;
			}
		}
	}
	for(auto const& [key, val]:mapDistributionLength){
		for(auto const& [k, v]:val){
			for(auto const& [k2, v2]:v){
				pair<double, double> param = v2;
				myCSV << "length distribution;" + key + ";" + k + ";" + k2 + ";" + to_string(param.first) + ";" + to_string(param.second) + ";" << endl;
			}
		}
	}	
	buildHourlyProb(date1, date2, randomGBool);
	for(auto const& [key, val]:hourlyProb){
		for(auto const& [k,v]:val){
			for(auto const& [k2, v2]:v){
				myCSV << "hourly proba;" + key + ";" + k + ";" + to_string(k2) + ";" + to_string(v2) + ";" << endl;
			}
		}
	}
	myCSV << "meet proba;" + to_string(0.79) + ";" + to_string(0.71) + ";" << endl;
	myCSV.close();
}

void FindContact::getContactParameters(string filename){
	string line;
	ifstream ifs (filename);
	getline(ifs,line);
	while(getline(ifs, line)){
		vector<string> stock_tempo;
		boost::split(stock_tempo, line, boost::is_any_of(";"));
		if(stock_tempo[0]=="contact probabilities"){
			resProba[stock_tempo[1]][stock_tempo[2]][stock_tempo[3]] = stod(stock_tempo[4]);
		}else if(stock_tempo[0]=="contact distribution"){
			mapDistributionNB[stock_tempo[1]][stock_tempo[2]][stock_tempo[3]] = make_pair(stod(stock_tempo[4]), stod(stock_tempo[5]));
		}else if(stock_tempo[0]=="length distribution"){
			mapDistributionLength[stock_tempo[1]][stock_tempo[2]][stock_tempo[3]] = make_pair(stod(stock_tempo[4]), stod(stock_tempo[5]));
		}else if(stock_tempo[0]=="hourly proba"){
			hourlyProb[stock_tempo[1]][stock_tempo[2]][stoi(stock_tempo[3])] = stod(stock_tempo[4]);
		}else if(stock_tempo[0]=="meet proba"){
			meetProb["PA"] = stod(stock_tempo[1]);
			meetProb["PE"] = stod(stock_tempo[2]);
		}

	}
	ifs.close();
}



std::map<int, std::map<std::string, std::map<std::string, std::map< std::string, std::pair< double, double>>>>> FindContact::orderedMap(std::map<std::string, std::map<std::string, std::map< std::string, std::pair< double, double>>>> input_map){

	std::map<int, std::map<std::string, std::map<std::string, std::map< std::string, std::pair< double, double>>>>> res;
	for(auto const& [key, val]:input_map){
		string HW = key;
		vector<string> hour_week;
		boost::split(hour_week, HW, boost::is_any_of("_"));
		for(auto const& [k, v]:val){
			for(auto const& [k2, v2]:v){
				res[stoi(hour_week[0])][hour_week[1]][k][k2] = make_pair(v2.first, v2.second);
				// res[stoi(hour_week[0])]["one weekS"][k][k2] = make_pair(v2.first, v2.second);
			}	
		}
	}
	return(res);
}


void FindContact::writeAllProbabilities(){
	//fonction qui va écrire trois csv: un pour les proba, un pour la distribution (nb) et un pour la distribution (duree)
	//1) resproba
	//hour - week - condition - cat - nb
	map<int, map<string, map<string, map<string, double>>>> probaInterm;
	for(auto const& [key, val]:resProba){
		string HW = key;
		vector<string> hour_week;
		boost::split(hour_week, HW, boost::is_any_of("_"));
		for(auto const& [k, v]:val){
			for(auto const& [k2, v2]:v){
				probaInterm[stoi(hour_week[0])][hour_week[1]][k][k2] = v2;
				// probaInterm[stoi(hour_week[0])]["one weekS"][k][k2] = v2;
			}	
		}
	}
	//2) NB et length
	map<int, map<string, map<string, map<string, pair<double, double>>>>> NBInterm = orderedMap(mapDistributionNB);
	map<int, map<string, map<string, map<string, pair<double, double>>>>> LengthInterm = orderedMap(mapDistributionLength);
	
	//3)ecriture
	//proba
	string FileNameProba = "findContact_proba.csv";
	ofstream myContactProbCSV;
	myContactProbCSV.open(FileNameProba);
	myContactProbCSV<<"hour;week;condition;category;probability"<<endl;
	for(auto const& [key, val]:probaInterm){
		for(auto const& [k, v]:val){
			for(auto const& [k2, v2]:v){
				for(auto const& [k3, v3]:v2){
					myContactProbCSV << to_string(key) << ";" << k << ";" << k2 << ";" << k3 << ";" << to_string(v3) << endl;
				}
			}	
		}
	}
	//NB
	string FileNameNB = "findContact_distributionNB.csv";
	ofstream myContactNBCSV;
	myContactNBCSV.open(FileNameNB);
	myContactNBCSV<<"hour;week;condition;category;mean;var"<<endl;
	for(auto const& [key, val]:NBInterm){
		for(auto const& [key2, val2]:val){
			for(auto const& [k, v]:val2){
				for(auto const& [k2, v2]:v){
					myContactNBCSV << to_string(key) << ";" << key2 << ";" << k << ";" << k2 << ";" << to_string(v2.first) << ";" << to_string(v2.second) << endl;
				}	
			}
		}
	}
	//length
	string FileNameL = "findContact_distributionLength.csv";
	ofstream myContactLengthCSV;
	myContactLengthCSV.open(FileNameL);
	myContactLengthCSV<<"hour;week;condition;category;mean;var"<<endl;
	for(auto const& [key, val]:LengthInterm){
		for(auto const& [key2, val2]:val){
			for(auto const& [k, v]:val2){
				for(auto const& [k2, v2]:v){
					myContactLengthCSV << to_string(key) << ";" << key2 << ";" << k << ";" << k2 << ";" << to_string(v2.first) << ";" << to_string(v2.second) << endl;
				}	
			}
		}
	}
}

map<string, map<string, vector<string>>> FindContact::findRecordedDaysByWeekST(){
	map<string, map<string, vector<string>>> res;
	for(auto const& [ind, days]:daysOfRecordByInd){
		vector<string> allDays;
		for(auto const& [d, h]:days){
			allDays.push_back(d);
		}
		auto it = find_if(allIndividuals.begin(), allIndividuals.end(), [ind](Individual* i )->bool {return i->getCalc_ident()==ind;});
		if(it!=allIndividuals.end()){
			string cat = findMeCatGeneral((*it), withHosp);
			res[cat][ind] = allDays;
		}
	}
	return res;
}

map<string, map<string, map<int, int>>> FindContact::findRecordedHoursByWeekST(bool randomGraph_in){
	//fonction qui redonne le nombre d' individu-crenau (représente pour chaque categorie, pour chaque weekstatus pour chaque heure --> nombre d'individu-crenau)
	map<string, map<string, map<int, int>>> res;
	int nb = 0;
	randomGraph = randomGraph_in;
	for(auto const& [ind, days]:daysOfRecordByInd){
		vector<string> ddays;
		map<string, vector<int>> mdays = days;
		for(auto const& [dday, dhour]:mdays){
			ddays.push_back(dday);
		}
		auto it = find_if(allIndividuals.begin(), allIndividuals.end(), [ind](Individual* i )->bool {return i->getCalc_ident()==ind;});
		if(it!=allIndividuals.end()){
			for(auto const& d:ddays){
				vector<int> allHours = daysOfRecordByInd[ind][d];
				time_t d_time = Learning::stringToDate(d);
				tm* time_st = localtime(&d_time);
				int wday = time_st->tm_wday;
				string weekS;
				if(wday==0 || wday==6){
					weekS = "weekend";
				}else{
					weekS = "weekday";
				}
				// weekS = "one weekS";
				// string cat = findMeCatGeneral((*it), true);
				// string cat = (*it)->getStatus();
				string cat;
				if(randomGraph || test=="test1"){
					if(catBool){
						cat = findMeCatGeneral((*it), withHosp);
					}else{
						cat = (*it)->getStatus();
					}
					
				}else{
					if(catBool){
						cat = findMeCatGeneral((*it), withHosp) + "_" + (*it)->getWard();
					}else{
						cat = (*it)->getStatus() + "_" + (*it)->getWard();
					}
					
				}
				vector<int> recordedHours = mdays[d];
				for(auto const& h:recordedHours){
					auto itF2 = res[cat][weekS].find(h);
					if(itF2!=res[cat][weekS].end()){
						res[cat][weekS][h] = res[cat][weekS][h] + 1;
					}else{
						res[cat][weekS][h] = 1;
					}
				}
				
				/*
				// string cat = (*it)->getStatus() + "_" + (*it)->getWard();
				if((*it)->getStatus()=="PA"){
					//c'est un PA il est présent tous le temps
					// auto itAdm = find(admissionDischarge[(*it)->getCalc_ident()]["admission"].begin(), admissionDischarge[(*it)->getCalc_ident()]["admission"].end(), d);
					// auto itDis = find(admissionDischarge[(*it)->getCalc_ident()]["discharge"].begin(), admissionDischarge[(*it)->getCalc_ident()]["discharge"].end(), d);
					int deb = 0;
					int fin = 24;
					// if(itAdm!=admissionDischarge[(*it)->getCalc_ident()]["admission"].end())deb=8;
					// if(itDis!=admissionDischarge[(*it)->getCalc_ident()]["discharge"].end())fin=20;
					for(int i(deb); i<fin; i++){
						auto itF = res[cat][weekS].find(i);
						if(itF!=res[cat][weekS].end()){
							res[cat][weekS][i] = res[cat][weekS][i] + 1;
						}else{
							res[cat][weekS][i] = 1;
						}
						
					}
				}else{
					//c'est pas un PA donc on va chercher ses heures dans la map schedulePE
					vector<int> allHours = schedulePE[(*it)->getCalc_ident()][d];
					
					for(auto const& h:allHours){
						auto itF2 = res[cat][weekS].find(h);
						if(itF2!=res[cat][weekS].end()){
							res[cat][weekS][h] = res[cat][weekS][h] + 1;
						}else{
							res[cat][weekS][h] = 1;
						}
					}
				}
				*/
			}
		}
		
	}
	
	// for(auto const& [key, val]:res){
		// for(auto const& [k,v]:val){
			// for(auto const& [h, value]:v){
				// if(key=="IDE")cout << k << " " << h << " --> " << value << endl;
			// }
		// }
	// }

	return res;
}

int FindContact::numberOfDaysByWeekStatus(vector<string> input_vector, string week){
	int nb = 0;
	vector<int> to_find;
	for(int i(0); i<7; i++){
		if((week == "weekend") && (i == 0 || i == 6)){
			to_find.push_back(i);
		}else if((week =="weekday") && (i!=0 && i!= 6)){
			to_find.push_back(i);
		}
	}
	for(auto const& elem:input_vector){
		time_t elem_time = Learning::stringToDate(elem);
		tm* time_st = localtime(&elem_time);
		int wday = time_st->tm_wday;
		auto it = find(to_find.begin(), to_find.end(), wday);
		if(it!=to_find.end()){
			nb = nb + 1;
		}
	}
	return nb;
}

map<int, map<string, int>> FindContact::computeNbOfIndPerHour(string fDate, string lDate){
	map<int, map<string, int>> res;
	for(int i(0); i<24; i++){
		for(auto const& elem:keyCat){
			res[i][elem] = 0;
		}
	}
	
	for(auto const& [ind, dates]:allIndDates){
		string cat = findMeCatGeneral(ind, withHosp);
		for(auto const& d:dates){
			for(int i(0); i<24; i++){
				res[i][cat] = res[i][cat] + 1;
			}
		}
	}
	return res;
}

double FindContact::computeMedian(std::vector<double>& numbers){
	if (numbers.empty()){
		return std::numeric_limits<double>::quiet_NaN();
	}else{
		size_t n = numbers.size() / 2;
		nth_element(numbers.begin(), numbers.begin()+n, numbers.end());
		return numbers[n];
	}
   
	
}

std::map<std::string, std::vector<Individual*>> FindContact::fillMapTypeGeneral(vector<Individual*> input_vector){
	map<string, vector<Individual*>> mapCatGeneral;
	for(auto const& elem:input_vector){
		if(elem->getStatus()=="PE"){
			mapCatGeneral["PE"].push_back(elem);
		}else{
			mapCatGeneral["PA"].push_back(elem);
		}
	}
	return(mapCatGeneral);
}

std::map<std::string, std::vector<Individual*>> FindContact::fillMapCatGeneral(vector<Individual*> input_vector){
	map<string, vector<Individual*>> mapCatGeneral;
	for(auto const& elem:input_vector){
		string cat = findMeCatGeneral(elem, withHosp);
		mapCatGeneral[cat].push_back(elem);
	}
	return(mapCatGeneral);
}

string FindContact::findMeCatGeneral(Individual* individual, bool patientHosp){
	string catInd;
	if(individual->getStatus()=="PE"){
		Staff* staffInd = dynamic_cast<Staff*>(individual);
		catInd = staffInd->getCat();
	}else{
		Patient* patientInd = dynamic_cast<Patient*>(individual);
		string hosp = patientInd->getHosp_flag();
		if(patientHosp && hosp!=""){
			catInd = hosp;
		}else{
			catInd = "Patient";
		}
	}
	return(catInd);
}

std::map<int, std::map<string, std::map<string, std::pair<double, double>>>> FindContact::findMeanVarInAMap(std::map<int, std::map<string, std::map<string, std::vector<double>>>> input_map){
	/* Fonction qui retourne la moyenne et la variance d'une map */
	std::map<int, std::map<string, std::map<string, std::pair<double, double>>>> res;
	for(auto const& [key, val]:input_map){
		int hour = key;
		std::map<string, std::map<string, std::vector<double>>> mapWard = val;
		for(auto const& [keyWard, valWard]:mapWard){
			string wardCondition = keyWard;
			std::map<string, std::vector<double>> mapCat = valWard;
			for(auto const& [keyCat, valCat]:mapCat){
				string catCondition = keyCat;
				vector<double> numbers = valCat;
				double meanNumbers = Learning::computeMean(numbers);
				double varNumbers = Learning::computeSampleVariance(meanNumbers, numbers);
				res[hour][wardCondition][catCondition] = make_pair(meanNumbers, varNumbers);
			}
		}
	}
	return res;
}

vector<time_t> FindContact::returnVectorOfTime(time_t day, time_t hour){
	vector<time_t> res;
	time_t firstHour = day + hour;
	time_t endHour = day + hour + 60*60;
	for(time_t b(firstHour); b<endHour; b=b+time_t(30)){
		res.push_back(b);
	}
	return res;
}

map<string, vector<Individual*>> FindContact::catInsideVector(vector<Individual*> input_vector){
	/* Fonction qui retourne une map avec en clé la catégorie et en valeur un vecteur d'individu appartenant à cette catégorie présent dans input_vector */
	map<string, vector<Individual*>> res;
	for(auto const& elem:input_vector){
		string cat = findMeCatGeneral(elem, withHosp);
		res[cat].push_back(elem);
	}
	return res;
}

vector<Individual*> FindContact::checkAdmissionDischarge(vector<Individual*> input_vector, string day, int hour){
	vector<Individual*> res;
	DoubleUniformGenerator rndContener = Random::instance()->createUniDoubleGenerator(0,1);
	time_t dayTime = Learning::stringToDate(day);
	tm* time_st = localtime(&dayTime);
	int wday = time_st->tm_wday;
	string wsStatus;
	if(wday==0 || wday==6){
		wsStatus="weekend";
	}else{
		wsStatus="weekday";
	}
	for(auto const& elem:input_vector){

		auto itAdm = find(admissionDischarge[elem->getCalc_ident()]["admission"].begin(), admissionDischarge[elem->getCalc_ident()]["admission"].end(), day);
		auto itDis = find(admissionDischarge[elem->getCalc_ident()]["discharge"].begin(), admissionDischarge[elem->getCalc_ident()]["discharge"].end(), day);
		if((itAdm == admissionDischarge[elem->getCalc_ident()]["admission"].end() && itDis == admissionDischarge[elem->getCalc_ident()]["discharge"].end()) || (itAdm != admissionDischarge[elem->getCalc_ident()]["admission"].end() && hour>7) || (itDis != admissionDischarge[elem->getCalc_ident()]["discharge"].end() && hour<20)){

			if(elem->getStatus()=="PE"){
				Staff* s = dynamic_cast<Staff*>(elem);
				double probPresence = hourlyProb[s->getCat()][wsStatus][hour];
				double randomProbaRes = rndContener.next();
				if(randomProbaRes<probPresence){
					res.push_back(elem);
				}
				
			}else{
				res.push_back(elem);
			}
			
		}
	}
	return res;
}

map<string, vector<string>> FindContact::patientAndStaffFromDays(vector<string> dates, map<string, vector<Individual*>> input_indPerDay){
	map<string, vector<string>> res;
	vector<string> patients;
	vector<string> staffs;
	for(auto const& d: dates){
		vector<Individual*> here = input_indPerDay[d];
		for(auto const& h:here){
			if(h->getStatus()=="PA"){
				auto itPA = find(patients.begin(), patients.end(), h->getCalc_ident());
				if(itPA==patients.end()){
					patients.push_back(h->getCalc_ident());
				}
			}else{
				auto itPE = find(staffs.begin(), staffs.end(), h->getCalc_ident());
				if(itPE==staffs.end()){
					staffs.push_back(h->getCalc_ident());
				}
			}
		}
	}
	
	res["PA"] = patients;
	res["PE"] = staffs;
	
	return res;
}

map<string, vector<string>> FindContact::findPatientAndStaffFromVector(vector<string> input_vector){
	map<string, vector<string>> res;
	for(auto const& elem:input_vector){
		auto itInd = find_if(allIndividuals.begin(), allIndividuals.end(), [&elem](Individual* i)->bool {return(i->getCalc_ident()==elem);});
		if((*itInd)->getStatus()=="PA"){
			res["PA"].push_back(elem);
		}else{
			res["PE"].push_back(elem);
		}
	}
	return res;
}

map<string, vector<string>> FindContact::catInsideVectorString(vector<string> input_vector){
	map<string, vector<string>> res;
	for(auto const& elem:input_vector){
		auto itInd = find_if(allIndividuals.begin(), allIndividuals.end(), [&elem](Individual* i)->bool {return(i->getCalc_ident()==elem);});
		string cat = findMeCatGeneral((*itInd), withHosp);
		res[cat].push_back(elem);
	}
	return res;
}

map<string, vector<string>> FindContact::findLinkedIndividualsAfterCohort(map<string, vector<string>> linked_map, map<string, vector<string>> cohorting_map){
	
	for(auto const& [indCohort, vector_cohorted]: cohorting_map){
		vector<string> linkedInd = linked_map[indCohort];
		vector<string> cohortedInd = vector_cohorted;
		map<string, vector<string>> linkedIndByCat = catInsideVectorString(linkedInd);
		map<string, vector<string>> cohortedIndByCat = catInsideVectorString(cohortedInd);
		for(auto const& [cat, vector_cohortedCat]:cohortedIndByCat){
			vector<string> input_cohort = vector_cohortedCat;
			if(linkedIndByCat[cat].empty()){
				//il avait pas d'ind de cette cat donc on va les lui rajouté tant pis ça va rajouter des ind parfois
				linked_map[indCohort].insert(linked_map[indCohort].end(), input_cohort.begin(), input_cohort.end());
			}else{
				//je vais enlever tout les individus de cette catégorie dans le vecteur des linkedInd
				sort(linked_map[indCohort].begin(), linked_map[indCohort].end());
				sort(linkedIndByCat[cat].begin(), linkedIndByCat[cat].end());
				vector<string> common;
				set_difference(linked_map[indCohort].begin(), linked_map[indCohort].end(), linkedIndByCat[cat].begin(), linkedIndByCat[cat].end(), back_inserter(common));
				common.insert(common.end(),cohortedIndByCat[cat].begin(), cohortedIndByCat[cat].end()); 
				linked_map[indCohort] = common;
			}
		}
		
	}
	return linked_map;
}

void FindContact::createMeNewContact(string fromStart, string filenameContact, bool randomGraph_in, bool cohorting_in, string test_in){
	/* C'est la fonction qui va créé les contacts */
	cout << "createMeNewContact " << endl;
	buildSchedule();
	cohorting = cohorting_in;
	randomGraph = randomGraph_in;
	test = test_in;
	time_t timFirstDate;
	if(fromStart=="none"){
		timFirstDate = Learning::stringToDate(beginDate);
	}else{
		string firstDatefromStart = fromStart + " 00:00:00";
		timFirstDate = Learning::stringToDate(fromStart);
	}
	time_t timLastDate = Learning::stringToDate(endDate);
	string date1 = Learning::dateToString(timFirstDate).substr(0,10);
	string date2 = Learning::dateToString(timLastDate).substr(0,10);
	bool bridge = false;
	nonWorkingDays = Learning::findMeNonWorkingDays(date1, date2, bridge);
	cout << " la taille de hourlyProb est " << hourlyProb.size() << endl;
	cout << " la taille de resProba est " << resProba.size() << endl;
	cout << " la taille de mapDistributionLength est " << mapDistributionLength.size() << endl;
	cout << " la taille de mapDistributionNB est " << mapDistributionNB.size() << endl;
	cout << " la taille de meetProb est " << meetProb.size() << endl;
	cout << " random graph " << randomGraph << endl;
	cout << " recordTest " << recordTest << endl;
	alreadyMeetPeople.clear();
	mapLastCTCPeople.clear();	
	map<string, map<int, vector<Individual*>>> indPerDay;
	if(recordTest){
		cout << "let's go for record " << endl;
		indPerDay = computeNbOfIndPerDayWithHours(beginDate, Learning::dateToString(timLastDate), recordTest, randomGraph);
		cout << " size " << indPerDay.size() << endl;
	}else{
		indPerDay = computeNbOfIndPerDayWithHours(beginDate, Learning::dateToString(timLastDate), recordTest, randomGraph);
	}
	cout << "Build contacts begin at " << Learning::dateToString(timFirstDate) << " end at " << Learning::dateToString(timLastDate) << endl;
	//on recup les individus qui seront présent durant la construction des contacts
	vector<Individual*> individualInCtc;
	for(auto const& [key, val]:indPerDay){
		if(Learning::stringToDate(key)>=timFirstDate && Learning::stringToDate(key)<timLastDate){
			for(auto const& [k, v]:val){
				individualInCtc.insert(individualInCtc.end(), v.begin(), v.end());
			}			
		}
	}
	sort(individualInCtc.begin(), individualInCtc.end());
	individualInCtc.erase( unique( individualInCtc.begin(), individualInCtc.end() ), individualInCtc.end() );	
	map<string, vector<string>> cohortingMap;
	if(cohorting){
		//on attribuer dès le début un cohorting entre les catégories que l'on veut 
		cohortingMap = mapCohort(individualInCtc, coupleToCohort, beginDate, Learning::dateToString(timLastDate));
		cout << " cohorting ok " << endl;
	}
	
	
	//pour chaque jour entre la première date de simulation et la fin 
	for(time_t day(timFirstDate); day<timLastDate; day = day + time_t(86400)){
		cout << "le jour " <<Learning::dateToString(day).substr(0,10) << endl;
		auto itNonWorking = find(nonWorkingDays.begin(), nonWorkingDays.end(), Learning::dateToString(day).substr(0,10));		
		map<int, vector<Individual*>> mapPeopleHere = indPerDay[Learning::dateToString(day).substr(0,10)];
		//on va diviser les individus présents en fonction du fait d'être ou non dans le même service et de la cat
		for(auto const& [h, peopleHere]:mapPeopleHere){
			for(auto const& people:peopleHere){
				//inside/outside - cat - individus
				map<string, map<string, vector<Individual*>>> peopleToMeet;
				if(cohorting){
					peopleToMeet = catInsideOutsideWardCohort(peopleHere, people->getWard(), people, cohortingMap, coupleToCohort);
				}else{
					peopleToMeet = catInsideOutsideWard(peopleHere, people->getWard(), people);
				}
				mapPeopleToMeet[people->getCalc_ident()][h] = peopleToMeet;
			}
		}
		string weekST = findWeekStatus(day);
		bool weekend = false;
		tm* time_st = localtime(&day);
		int wday = time_st->tm_wday;
		if(wday==0 || wday==6 || itNonWorking!=nonWorkingDays.end()){
			weekend = true;
		}
		//pour chaque heure du jour day
		for(int hour(0); hour<24; hour++){
			vector<time_t> pickHour = returnVectorOfTime(day, time_t(hour*60*60));
			string h_week = to_string(hour) +  "_" + weekST;			
			//pour chaque personne présente le jour day
			int c = 0;
			vector<Individual*> newPeopleHere = mapPeopleHere[hour];
			for(auto const& people:newPeopleHere){
				c = c + 1;
				vector<string> contactPeople;
				string cat;
				if(catBool){
					cat = findMeCatGeneral(people, withHosp);
				}else{
					cat = people->getStatus();
				}
				string ward = people->getWard();
				//condition - cat - individus
				map<string, map<string, vector<Individual*>>> contactWithInd = findMePeopleInContactWith(mapPeopleToMeet[people->getCalc_ident()][hour], cat, h_week, people, pickHour, weekend, newPeopleHere);
				if(!contactWithInd.empty()){
					for(auto const& [status, mapCatCtc]:contactWithInd){
						for(auto const& [catOther, peopleInCtc]:mapCatCtc){
							string couple = cat + "_" + catOther;
							string newCDT = "one condition";
							string newHour = "one hour";
							map<string, pair<double, double>> mapLength = mapDistributionLength[h_week][status];
							fillContactFromOtherCat(peopleInCtc, couple, people, h_week, pickHour, mapLength, status);
						}
					}
				}
			}
		}
		int ctcCount = 0;
		for(auto const& [key, val]:allContacts){
			ctcCount = ctcCount + val.size();
		}
	}
	int ctcCount = 0;
	for(auto const& [key, val]:allContacts){
		ctcCount = ctcCount + val.size();
	}
	cout<<"resultat : " << ctcCount << endl;	
	writeContacts(filenameContact, allContacts);
}

vector<Individual*> FindContact::checkTemporalCoherence(Individual* input_ind, vector<Individual*> input_vector, map<Individual*, vector<string>> allIndDates_, map<Individual*, vector<string>> allIndDatesTot){
	/* Fonction qui prend en argument un ind, un vecteur d'individu, des dates de présences et une map de dates de présence pour chaque individus. Elle retourne un vecteur d'individus qui sont présent au meme moment que input_ind. */
	vector<Individual*> res;
	vector<string> indDates = allIndDatesTot[input_ind];
	sort(indDates.begin(), indDates.end());
	for(auto const& elem:input_vector){
		vector<string> inputDates = allIndDates_[elem];
		sort(inputDates.begin(), inputDates.end());
		vector<string> common;
		set_intersection(indDates.begin(), indDates.end(), inputDates.begin(), inputDates.end(),  back_inserter(common));
		if(!common.empty()){
			res.push_back(elem);
		}
	}
	return res;
}


map<string, vector<string>> FindContact::mapCohort(vector<Individual*> input_individual, std::map<std::pair<std::string, std::string>, map<string, int>> couple, string beginDate, string endDate){
	/* Fonction qui prend en argument un vecteur d'individus, une map avec l'ensemble des couples, une date de début et une date de fin. Retour une map avec en clé un individus en calc_ident, et en valeur le vecteur des autres individus de l'autre catégorie avec lequel il doit être en contact */
	map<string, vector<string>> peopleInCtcCohort;
	vector<string> allDatesPeriod = returnVectorOfDay(beginDate, endDate);
	sort(allDatesPeriod.begin(), allDatesPeriod.end());
	//1) on va faire une map avec les individus par categorie
	map<string, map<string, vector<Individual*>>> categoriesByWard;
	map<string, map<Individual*, vector<string>>> datesPatientsBrut;
	map<Individual*, vector<string>> datesIndBrut;
	for(auto const& elem:input_individual){
		vector<string> datesP = allIndDates[elem];
		sort(datesP.begin(), datesP.end());
		vector<string> common;
		set_intersection(datesP.begin(), datesP.end(), allDatesPeriod.begin(), allDatesPeriod.end(),  back_inserter(common));
		datesIndBrut[elem] = common;
		if(elem->getStatus()=="PE"){
			Staff* s = dynamic_cast<Staff*>(elem);
			categoriesByWard[s->getCat()][s->getWard()].push_back(elem);
			categoriesByWard[s->getCat()]["all"].push_back(elem);
		}else{
			categoriesByWard["Patient"][elem->getWard()].push_back(elem);
			categoriesByWard["Patient"]["all"].push_back(elem);
			datesPatientsBrut[elem->getWard()][elem] = datesIndBrut[elem];
			datesPatientsBrut["all"][elem] = datesIndBrut[elem];
		}	
	}
	//2) pour chaque couple on va attribuer des individus en contact
	for(auto const& [couple, ward_nb]:coupleToCohort){
		string cat1 = couple.first;
		string cat2 = couple.second;
		for(auto const& [ward, nb]:ward_nb){
			vector<Individual*> cat1_vector = categoriesByWard[cat1][ward];
			vector<Individual*> categoriesCat2 = categoriesByWard[cat2][ward];
			map<Individual*, vector<string>> datesPatients = datesPatientsBrut[ward];
			if(categoriesCat2.empty()){
				categoriesCat2 = categoriesByWard[cat2]["all"];
				datesPatients = datesPatientsBrut["all"];
			}
			vector<Individual*> categoriesCat2INIT = categoriesCat2;
			int nbUnique = nb;
			for(auto const& c1:cat1_vector){
				vector<string> c1_dates = datesIndBrut[c1];
				sort(c1_dates.begin(), c1_dates.end());
				vector<Individual*> cat2_vector = categoriesCat2;
				vector<Individual*> cat2_vector_temporal = checkTemporalCoherence(c1, cat2_vector, datesPatients, datesIndBrut);
				vector<Individual*> cat2_vectorINIT_temporal = checkTemporalCoherence(c1, categoriesCat2INIT, datesPatients, datesIndBrut);
				vector<string> toRemove;
				if(cat2_vector_temporal.size()>nbUnique){
					IntUniformGenerator chooseRandom = Random::instance()->createUniIntGenerator(0,cat2_vector_temporal.size()-1);
					vector<int> input_int;
					for(int i(0); i<nbUnique; i++){
						int index = chooseRandom.next();
						auto itIndex = find(input_int.begin(), input_int.end(), index);
						while(itIndex!=input_int.end()){
							index = chooseRandom.next();
							itIndex = find(input_int.begin(), input_int.end(), index);
						}
						input_int.push_back(index);
						peopleInCtcCohort[c1->getCalc_ident()].push_back(cat2_vector_temporal[index]->getCalc_ident());
						peopleInCtcCohort[cat2_vector_temporal[index]->getCalc_ident()].push_back(c1->getCalc_ident());
						toRemove.push_back(cat2_vector_temporal[index]->getCalc_ident());
					}
				}else if(!cat2_vector_temporal.empty()){
					for(auto const& elem:cat2_vector_temporal){
						peopleInCtcCohort[c1->getCalc_ident()].push_back(elem->getCalc_ident());
						peopleInCtcCohort[elem->getCalc_ident()].push_back(c1->getCalc_ident());
						toRemove.push_back(elem->getCalc_ident());
					}
					cat2_vector_temporal.clear();
				}else if(cat2_vector_temporal.empty() && !cat2_vectorINIT_temporal.empty()){
					IntUniformGenerator chooseRandom = Random::instance()->createUniIntGenerator(0,cat2_vectorINIT_temporal.size()-1);
					vector<int> input_int;
					if(cat2_vectorINIT_temporal.size()<=nbUnique){
						for(auto const& elem:cat2_vectorINIT_temporal){
							peopleInCtcCohort[c1->getCalc_ident()].push_back(elem->getCalc_ident());
							peopleInCtcCohort[elem->getCalc_ident()].push_back(c1->getCalc_ident());
						}
					}else{
						for(int i(0); i<nbUnique; i++){
							int index = chooseRandom.next();
							auto itIndex = find(input_int.begin(), input_int.end(), index);
							while(itIndex!=input_int.end()){
								index = chooseRandom.next();
								itIndex = find(input_int.begin(), input_int.end(), index);
							}
							input_int.push_back(index);
							peopleInCtcCohort[c1->getCalc_ident()].push_back(cat2_vectorINIT_temporal[index]->getCalc_ident());
							peopleInCtcCohort[cat2_vectorINIT_temporal[index]->getCalc_ident()].push_back(c1->getCalc_ident());
						}
					}
					cout << "oooops " << c1->getCalc_ident() << " aura des " << cat2 << " déjà attribués! " << endl;
				}
				for(auto const& elem:toRemove){
					string toF = elem;
					vector<Individual*>::iterator itErase = find_if(cat2_vector.begin(), cat2_vector.end(), [&toF](Individual* i)->bool {return(i->getCalc_ident()==toF);});
					if(itErase!=cat2_vector.end()){
						vector<string> cat2_dates = datesPatients[(*itErase)];
						sort(cat2_dates.begin(), cat2_dates.end());
						vector<string> common;
						set_difference(cat2_dates.begin(), cat2_dates.end(), c1_dates.begin(), c1_dates.end(),  back_inserter(common));
						if(common.empty()){
							auto itCommon = find_if(datesPatients.begin(), datesPatients.end(),[&toF](pair<Individual*, vector<string>> i)->bool {return(i.first->getCalc_ident()==toF);});
							datesPatients.erase(itCommon);
							cat2_vector.erase(itErase);
							
						}else{
							//sinon il reste des dates donc il faudra lui attribuer des PE
							datesPatients[(*itErase)] = common;
						}
					}else{
						cout << "problem mapCohort " << endl;
					}
				}
				categoriesCat2 = cat2_vector;
			}
			if(!datesPatients.empty()){
				for(auto const& [elem, val]:datesPatients){
					vector<string> datesIndVal = val;
					vector<Individual*> vector_temporal = checkTemporalCoherence(elem, cat1_vector, datesIndBrut, datesPatients);
					string wardExtraPA = elem->getWard();
					while(!datesIndVal.empty() && !vector_temporal.empty()){
						vector<Individual*> vectorToChoose;
						vector<Individual*> vector_tempo_ward;
						for(auto const& elem2:vector_temporal){
							if(elem2->getWard()==wardExtraPA){
								vector_tempo_ward.push_back(elem2);
							}
						}
						if(vector_tempo_ward.empty()){
							vectorToChoose = vector_temporal;
						}else{
							vectorToChoose = vector_tempo_ward;
						}
						IntUniformGenerator chooseRandom = Random::instance()->createUniIntGenerator(0,vectorToChoose.size()-1);
						int index = chooseRandom.next();
						peopleInCtcCohort[elem->getCalc_ident()].push_back(vectorToChoose[index]->getCalc_ident());
						peopleInCtcCohort[vectorToChoose[index]->getCalc_ident()].push_back(elem->getCalc_ident());	
						sort(datesIndVal.begin(), datesIndVal.end());
						vector<string> c1_dates = datesIndBrut[vectorToChoose[index]];
						sort(c1_dates.begin(), c1_dates.end());
						vector<string> common;
						set_difference(datesIndVal.begin(), datesIndVal.end(), c1_dates.begin(), c1_dates.end(),  back_inserter(common));
						datesIndVal = common;
						datesPatients[elem] = common;
						vector_temporal = checkTemporalCoherence(elem, cat1_vector, datesIndBrut, datesPatients);
					}
				}
			}
		}
	}
	return(peopleInCtcCohort);
}


void FindContact::writeContacts(string filenameContact, std::map<time_t, std::vector<Contact*>> input_ctc){
	string FileName = filenameContact;
	ofstream myContactCSV;
	myContactCSV.open(FileName);
	myContactCSV<<"from;to;date_posix;length"<<endl;
	for(auto const& [key, val]:input_ctc){
		vector<Contact*> ctcVect = val;
		for(auto const& elem:ctcVect){
			myContactCSV<<elem->getFrom() << ";" << elem->getTo() << ";" << elem->getDay() << ";" << elem->getLength() << endl;
		}
	}
}

void FindContact::fillContactFromOtherCat(vector<Individual*> peopleInCtc, string couple, Individual* people, string hour_week, vector<time_t> pickHour, map<string, pair<double, double>> mapLength, string statAR){
	/* Fonction  qui détermine les durées de contact entre les individus peopleInCtc et people */
	vector<string> coupleStatus;
	boost::split(coupleStatus, couple, boost::is_any_of("_"));
	string coupleDistrib;
	string catD;
	string catOtherD;
	auto itCat = find(hospitalizationStatus.begin(), hospitalizationStatus.end(), coupleStatus[0]);
	if(itCat == hospitalizationStatus.end()){
		catD = "PA";
	}else{
		catD = "PE";
	}
	auto itCatOther = find(hospitalizationStatus.begin(), hospitalizationStatus.end(), coupleStatus[1]);
	if(itCatOther == hospitalizationStatus.end()){
		catOtherD = "PA";
	}else{
		catOtherD = "PE";
	}
	coupleDistrib = catD + "_" + catOtherD;
	pair<double, double> durationParameters = mapLength[couple];
	for(auto const& p:peopleInCtc){
		//1) on choisi une duree
		double meanDUR = durationParameters.first;
		double varDUR = durationParameters.second;
		double lengthDUR = 0.0;
		//si la variance est à nan c'est qu'il y a eu qu'une seule valeur
		if(!isnan(varDUR) && (meanDUR!=0.0 && varDUR!=0.0)){
			LogNormalGenerator contenerNormal = Random::instance()->createLogNormalGenerator(meanDUR, sqrt(varDUR));
			lengthDUR = contenerNormal.next();
			while(lengthDUR < 30.0){
				lengthDUR = contenerNormal.next();
			}
			if((int)lengthDUR%30!=0){
				lengthDUR = (double)(((int)lengthDUR + 15) - (((int)lengthDUR + 15)%30));
			}
		}
		if(meanDUR!=0.0 && (isnan(varDUR) || varDUR==0.0)){
			varDUR = meanDUR * 2;
			LogNormalGenerator contenerNormal = Random::instance()->createLogNormalGenerator(meanDUR, sqrt(varDUR));
			lengthDUR = contenerNormal.next();
			while(lengthDUR < 30.0){
				lengthDUR = contenerNormal.next();
			}
			if((int)lengthDUR%30!=0){
				lengthDUR = (double)(((int)lengthDUR + 15) - (((int)lengthDUR + 15)%30));
			}
		}
		//2) on choisi une date
		string couplePeople = people->getCalc_ident() + "_" + p->getCalc_ident();
		string couplePeopleOther = p->getCalc_ident() + "_" + people->getCalc_ident();
		map<string, pair<time_t, time_t>>::iterator itDay = mapLastCTCPeople.find(couplePeople);
		vector<time_t> timesToChoose;
		if(itDay!=mapLastCTCPeople.end()){
			//il y a deja eu un contact avec donc on va check
			time_t lastTimeInCtcWith = mapLastCTCPeople[couplePeople].second;
			time_t lastTimeInCtcWithOther = mapLastCTCPeople[couplePeopleOther].second;
			time_t maxLastTime;
			if(lastTimeInCtcWith>lastTimeInCtcWithOther){
				maxLastTime = lastTimeInCtcWith;
			}else{
				maxLastTime = lastTimeInCtcWithOther;
			}
			// donc la nouvelle date doit être superieur à maxLastTime
			if(pickHour[0]<maxLastTime){
				for(time_t b(maxLastTime + 30); b<=pickHour[pickHour.size()-1]; b = b + time_t(30)){
					timesToChoose.push_back(b);
				}
				try{
					if(timesToChoose.empty()){
						throw string("empty timesToChoose !");
					}
				}catch(string const& chaine){
					cerr << chaine << " " << people->getCalc_ident()<< " et " << p->getCalc_ident() <<  " maxLastTime " << Learning::dateToString(maxLastTime) << " " << Learning::dateToString(pickHour[pickHour.size()-1]) << " " << timesToChoose.size() << " lastTimeInCtcWith " << Learning::dateToString(lastTimeInCtcWith) << " " << couplePeople << " lastTimeInCtcWithOther " << Learning::dateToString(lastTimeInCtcWithOther) << " " << couplePeopleOther << endl;
				}				
			}else{
				timesToChoose = pickHour;
			}
		}else{
			timesToChoose = pickHour;
		}
		IntUniformGenerator chooseRandom = Random::instance()->createUniIntGenerator(0,timesToChoose.size()-1);
		int index = chooseRandom.next();
		time_t date = pickHour[index];
		int lengthDURInt = (int)lengthDUR;
		time_t endDateCTC = date + time_t(lengthDURInt);
		mapLastCTCPeople[couplePeople] = make_pair(date, endDateCTC);
		auto itNBctc = mapNbCTCPeople.find(couplePeople);
		if(itNBctc != mapNbCTCPeople.end()){
			mapNbCTCPeople[couplePeople] = mapNbCTCPeople[couplePeople] + 1;
			mapNbCTCPeople[couplePeopleOther] = mapNbCTCPeople[couplePeopleOther] + 1;			
		}else{
			mapNbCTCPeople[couplePeople] = 1;
			mapNbCTCPeople[couplePeopleOther] = 1;
		}
		
		//4) On fait le contact
		vector<string> vectCTC = {people->getCalc_ident(), p->getCalc_ident(), Learning::dateToString(date), to_string(lengthDURInt)};
		Contact* ctc = new Contact(vectCTC);
		allContacts[date].push_back(ctc);
	}
}

map<string, map<string, vector<Individual*>>> FindContact::findMePeopleInContactWith(map<string, map<string, vector<Individual*>>> input_map, string cat, string hour_week, Individual* people, vector<time_t> pickHour, bool weekend, vector<Individual*> newPeopleHere_){
	//prend en argument une map <InsideWard/OutsideWard, map<cat, vector<individus>>> de caracteristique d'individus autours de people à l'heure hour. cat = catégorie de people, insideOutside c'est pour retrouve à l'interieur ou à l'exterieur du ward de people. 
	//retourne une map<from/to, map<cat en contact, vector d'individus de la cat en contact>>
	
	map<string, map<string, vector<Individual*>>> res;
	map<string, map<string, double>> probaHour = resProba[hour_week];
	for(auto const& [stat, mapCatProba]:probaHour){
		map<string, vector<Individual*>> peopleToMeetInside = input_map[stat];
		if(!peopleToMeetInside.empty()){
			map<string, map<string, double>> fromTo = fromToProbabilities(mapCatProba, cat);
			map<string, double> probabilities = fromTo["from"];
			if(!probabilities.empty()) res[stat] = findMeFromToPeople(probabilities, stat, cat, hour_week, peopleToMeetInside, people, pickHour, weekend, newPeopleHere_);
		}

	}
	return res;
}

vector<Individual*> FindContact::checkContact(vector<Individual*> input_vector, Individual* ind, vector<time_t> pickHour){
	//check contact permet de savoir si deux ind sont deja en contact et si oui, de savoir si on peut avoir un second co,tact pendant pickHour
	vector<Individual*> res;
	for(auto const& other:input_vector){
		string couple = ind->getCalc_ident() + "_" + other->getCalc_ident();
		string coupleOther = other->getCalc_ident() + "_" + ind->getCalc_ident() ;
		//dernier contact
		time_t secondDateCtc = 0.0;
		time_t secondDateCtcOther = 0.0;
		secondDateCtc = mapLastCTCPeople[couple].second;
		secondDateCtcOther = mapLastCTCPeople[coupleOther].second;
		if(secondDateCtc!=0.0 || secondDateCtcOther!=0.0){
			if(secondDateCtc < pickHour[pickHour.size()-1] && secondDateCtcOther < pickHour[pickHour.size()-1]){
				res.push_back(other);
			}
		}else{
			res.push_back(other);
		}
	}
	
	return(res);
}

vector<Individual*> FindContact::checkSchedule(vector<Individual*> input_vector, string day, int hour){
	vector<Individual*> res;
	for(auto const& ind:input_vector){
		if(ind->getStatus()=="PE"){
			auto it = find(schedulePE[ind->getCalc_ident()][day].begin(),schedulePE[ind->getCalc_ident()][day].end(), hour);
			if(it!=schedulePE[ind->getCalc_ident()][day].end()){
				// cout << " le " << day << " il y " << ind->getCalc_ident() << " qui est présent à " << hour << endl;
				res.push_back(ind);
			}
		}else{
			// if(ind->getCalc_ident()=="PA-819-RUL"){
				// cout << " c'est partie avec l'ind le " << day << " à " << hour << endl;
				// for(auto const& t:admissionDischarge[ind->getCalc_ident()]["admission"]){
					// cout << t << " ";
				// }
				// cout <<endl;
				// for(auto const& tt:admissionDischarge[ind->getCalc_ident()]["discharge"]){
					// cout << tt << " ";
				// }
				// cout <<endl;
			// }
			
			
			
			auto itAdm = find(admissionDischarge[ind->getCalc_ident()]["admission"].begin(), admissionDischarge[ind->getCalc_ident()]["admission"].end(), day);
			auto itDis = find(admissionDischarge[ind->getCalc_ident()]["discharge"].begin(), admissionDischarge[ind->getCalc_ident()]["discharge"].end(), day);
			if((itAdm == admissionDischarge[ind->getCalc_ident()]["admission"].end() && itDis == admissionDischarge[ind->getCalc_ident()]["discharge"].end()) || (itAdm != admissionDischarge[ind->getCalc_ident()]["admission"].end() && hour>7) || (itDis != admissionDischarge[ind->getCalc_ident()]["discharge"].end() && hour<20)){
				res.push_back(ind);
				// if(ind->getCalc_ident()=="PA-819-RUL")cout << " il aura des contact le " << day << " à " << hour << endl;
			}
			
			// res.push_back(ind);
		}
	}
	return(res);
}

map<string, vector<Individual*>> FindContact::findMeFromToPeople(map<string, double> input_map, string stat, string cat, string hour_week, map<string, vector<Individual*>> peopleToMeetInside, Individual* people, vector<time_t> pickHour, bool weekend, vector<Individual*> newPeopleHere_){
	//fonction qui retrouve une map d'individus avec qui people sera en contact à l'heure de hour_week en fonction du fait qu'il est deja rencontré ou non ces personnes via peopletomeetinside. input_map est une map de proba de contact. pickHour le vecteur de temps
	
	//cat - individus
	map<string, vector<Individual*>> res;
	vector<string> wardMetStatus;
	boost::split(wardMetStatus, stat, boost::is_any_of("_"));
	
	//pour chaque catOther en position to on va voir s'il y a contact ou non
	for(auto const& [catOther, value]:input_map){
		//on va tester la proba
		vector<string> hour_weekVector;
		boost::split(hour_weekVector, hour_week, boost::is_any_of("_"));
		double v = value;
		vector<Individual*> contactPeople;
		string couple = cat + "_" + catOther;
		string coupleDistrib;
		string catD;
		string catOtherD;
		auto itCat = find(hospitalizationStatus.begin(), hospitalizationStatus.end(), cat);
		if(itCat == hospitalizationStatus.end()){
			catD = "PA";
		}else{
			catD = "PE";
		}
		auto itCatOther = find(hospitalizationStatus.begin(), hospitalizationStatus.end(), catOther);
		if(itCatOther == hospitalizationStatus.end()){
			catOtherD = "PA";
		}else{
			catOtherD = "PE";
		}
		coupleDistrib = catD + "_" + catOtherD;		
		string newCDT = stat;
		double randomProbaRes;
		if(v!=0.0){			
			randomProbaRes = randomPoisson(v, eng);
		}else{
			randomProbaRes = 0.0;
		}
		vector<Individual*> individualToFind;
		for(auto const& elem:peopleToMeetInside[catOther]){
			string chosen = elem->getCalc_ident();
			auto itFind = find_if(newPeopleHere_.begin(), newPeopleHere_.end(), [chosen](Individual* i)->bool {return i->getCalc_ident() == chosen;});
			if(itFind!=newPeopleHere_.end()){
				individualToFind.push_back(elem);
			}
		}
		if(randomProbaRes !=0.0 && !individualToFind.empty()){
			double nbOfCTCHour = randomProbaRes;
			int nbOfCTCHourINT = (int)round(nbOfCTCHour);
			vector<Individual*> allPeople ;
			allPeople = checkContact(individualToFind, people, pickHour);
			if(!allPeople.empty() && nbOfCTCHourINT>0){
				//dans pickPeople c'est forcement des gens qui qui ont peu avoir des contacts durant pickHour
				vector<Individual*> pickPeople;
				if(randomGraph || test=="test2"){
					pickPeople = chooseCandidatesInsideVector(allPeople, nbOfCTCHourINT);
				}else{
					
					pickPeople = chooseCandidatesInsideVector(allPeople, nbOfCTCHourINT, people);
				}
				contactPeople.insert(contactPeople.end(), pickPeople.begin(), pickPeople.end());
				//ensuite on rempli si jamais rencontré	
				res[catOther] = contactPeople;
			}
		}		
	}
	return res;
}

vector<Individual*> FindContact::chooseCandidatesInsideVector(vector<Individual*> input_vector, int nb, Individual* people){
	/* Fonction qui choisi nb individus au hasard dans input_vector et qui seront en contact avec people. Ces individus sont choisi selon s'ils ont déjà eu des contacts avec people */
	if(input_vector.empty())cout<<"oooooops c'est vide" << endl;
	vector<Individual*> individualsToChoose;
	double probMeet = 0.0;
	if(people->getStatus()=="PE"){
		probMeet = (0.71);
	}else{
		probMeet = (0.79);
	}
	
	if(nb>=input_vector.size()){
		individualsToChoose = input_vector;
	}else{
		int sum = 0;
		int sumKnown = 0;
		vector<Individual*> known;
		vector<Individual*> unknown;
		vector<string> individualsToChooseSTR;
		for(auto const& elem:input_vector){
			string couple = people->getCalc_ident() + "_" + elem->getCalc_ident();
			int compt = 0;
			auto itF = mapNbCTCPeople.find(couple);
			if(itF!=mapNbCTCPeople.end()){
				compt = mapNbCTCPeople[couple];
				for(int j(0); j<compt; j++){
					individualsToChooseSTR.push_back(people->getCalc_ident());
				}
				sum = sum + compt;
				sumKnown = sumKnown + 1;
				known.push_back(elem);
			}else{
				unknown.push_back(elem);
			}
		}
		vector<Individual*> ind_input = input_vector;
		
		DoubleUniformGenerator chooseR = Random::instance()->createUniDoubleGenerator(0,1);
		bool ok = false;
		for(int i(0); i<nb; i++){
			double p1 = chooseR.next();
			bool boolK = false;
			if(known.empty()){
				boolK = false;
			}else if(unknown.empty()){
				boolK = true;
			}else{
				if(p1<probMeet){
					boolK = true;
				}else{
					boolK = false;
				}
			}
			
			if(boolK){
				
				double p2 = chooseR.next();
				int j = 0;
				bool ok = false;
				while(!ok){
					while(j<known.size() && !ok){
						Individual* ind = known[j];
						string couple = people->getCalc_ident() + "_" + ind->getCalc_ident();
						double poids = (double)mapNbCTCPeople[couple] / sum;
						if(p2<poids){
							ok = true;
							individualsToChoose.push_back(ind);
							known.erase(known.begin()+j);
						}else{
							j = j + 1;
						}
					}
					if(!ok){
						p2 = chooseR.next();
						j=0;
					}
				}
				

				
			}else{
				IntUniformGenerator chooseRandomUK = Random::instance()->createUniIntGenerator(0,unknown.size()-1);
				int index_unknown = chooseRandomUK.next();
				Individual* ind = unknown[index_unknown];
				individualsToChoose.push_back(ind);
				unknown.erase(unknown.begin()+index_unknown);
			}
		}
	}
	
	return individualsToChoose;
}

vector<Individual*> FindContact::chooseCandidatesInsideVector(vector<Individual*> input_vector, int nb){
	/* Fonction qui choisi nb individus au hasard dans input_vector */
	if(input_vector.empty())cout<<"oooooops c'est vide" << endl;
	IntUniformGenerator chooseRandom = Random::instance()->createUniIntGenerator(0,input_vector.size()-1);
	vector<Individual*> res;
	vector<int> chooseIndex;
	if(nb>input_vector.size()){
		nb = input_vector.size();
	}
	
	for(int pick(0); pick< nb ; pick++){
		int index = chooseRandom.next();
		vector<int>::iterator itIndex = find(chooseIndex.begin(), chooseIndex.end(), index);
		while(itIndex!=chooseIndex.end() && chooseIndex.size() < nb){
			index = chooseRandom.next();
			itIndex = find(chooseIndex.begin(), chooseIndex.end(), index);
		}
		chooseIndex.push_back(index);
		res.push_back(input_vector[index]);
	}
	return res;
}

map<string, map<string, double>> FindContact::fromToProbabilities(map <string, double> input_map, string cat){
	//fonction qui prend en argument une map avec en clé des catégories sous la forme cat1-cat2 et en valeur la probabilité correspondante ainsi que la cat de l'ind recherché. Elle retourne une map avec en clé from ou to et en val une map avec la catégorie opposé à cat ainsi que la proba correspondante. From = si l'ind de catégorie cat est en première position et To s'il est en second position. Pour connaitre le sens du contact et recréer une asymétrie.
	map<string, map<string, double>> res;
	for(auto const& [key, val]:input_map){
		vector<string>tempo;
		boost::split(tempo, key, boost::is_any_of("_"));
		if(tempo[0]==cat){
			res["from"][tempo[1]]=val;
		}else if(tempo[1]==cat){
			res["to"][tempo[0]]=val;
		}
	}
	return res;
}

double FindContact::randomGamma(double mean, double variance, boost::mt19937& rng){
    
	const double shape = ( mean*mean )/variance;
    double scale = variance/mean;
    boost::random::gamma_distribution<> gd( shape,scale );
    boost::variate_generator<boost::mt19937&,boost::random::gamma_distribution<> > var_gamma( rng, gd );
    return var_gamma();
}

double FindContact::randomNormal(double mean, double variance, boost::mt19937& rng){
    
	const double sd = sqrt(variance);
    boost::random::normal_distribution<> nm( mean, sd );
    boost::variate_generator<boost::mt19937&,boost::random::normal_distribution<> > var_normal( rng, nm );
    return var_normal();
}

double FindContact::randomLogNormal(double mean, double variance, boost::mt19937& rng){
    
	const double sd = sqrt(variance);
    boost::random::lognormal_distribution<> nm( mean, sd );
    boost::variate_generator<boost::mt19937&,boost::random::lognormal_distribution<> > var_normal( rng, nm );
    return var_normal();
}

double FindContact::randomExpo(double mean, boost::mt19937& rng){
    
    boost::random::exponential_distribution<> exp( 1/mean );
    boost::variate_generator<boost::mt19937&,boost::random::exponential_distribution<> > var_expo( rng, exp);
    return var_expo();
}

double FindContact::randomPoisson(double mean, boost::mt19937& rng){

    boost::random::poisson_distribution<> exp( mean );
    boost::variate_generator<boost::mt19937&,boost::random::poisson_distribution<> > var_expo( rng, exp);
    return var_expo();
}

pair<double, double> FindContact::computeLogMeanAndVariance(vector<double> input_vector){
	vector<double> logVector;
	for(auto const& elem:input_vector){ 
		logVector.push_back(log(elem));
	}
	double meanNumbers = Learning::computeMean(logVector);
	double varNumbers = Learning::computeSampleVariance(meanNumbers, logVector);
	pair<double, double> res = make_pair(meanNumbers, varNumbers);
	return res;
}

double FindContact::randomNBinom(double size, double mean, boost::mt19937& rng){
	double r = size;
	double proba = size/(size + mean);
    boost::random::negative_binomial_distribution<> rnb(r,proba);
    boost::variate_generator<boost::mt19937&,boost::random::negative_binomial_distribution<> > val_rnb(rng, rnb);
    return val_rnb() ;
}

std::map<std::string, std::map<std::string, std::map<std::string, double>>> FindContact::getResProba(){
	return resProba;
}

void FindContact::setResProba(std::map<std::string, std::map<std::string, std::map<std::string, double>>> input_map){
	resProba = input_map;
}
