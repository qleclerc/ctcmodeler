/* Filetreatment
* this file allow the treatment of the different files
*
*	
*	Filetreatment.cpp
*
*	Created on: Nov 17, 2017
*	Author: Audrey
*
*/

#include "repast_hpc/AgentId.h"
#include "repast_hpc/RepastProcess.h"
#include "repast_hpc/Properties.h"
#include "repast_hpc/Utilities.h"
#include "repast_hpc/logger.h"

#include <boost/lexical_cast.hpp>
#include <boost/mpi.hpp>
#include <boost/mpi/collectives.hpp>
#include <boost/algorithm/string.hpp>

#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <vector> 
#include <boost/algorithm/string.hpp>
#include <map>
#include <locale.h>
#include <regex>

#include "Filetreatment.h"

using namespace repast;
using namespace std;

//--------------------------------------- PARAMETERS ---------------------------------------//
Parameters::Parameters(){
	
}

Parameters::~Parameters(){
	
}

//1er constructeur
Parameters::Parameters(std::map<std::string, std::map<std::string, std::vector<std::pair<std::string, double>>>> mapDuration_, std::map<std::string, double> mapAdmission_, std::map<std::string, double> mapDischarge_, std::map<std::string, std::map<std::string, double>> mapColoAtAdmission_, std::map<std::string, std::map<std::string, double>> mapProbabilityOfTransmission_): mapDuration(mapDuration_), mapAdmission(mapAdmission_), mapDischarge(mapDischarge_), mapColoAtAdmission(mapColoAtAdmission_), mapProbabilityOfTransmission(mapProbabilityOfTransmission_){
	
}

Parameters::Parameters(std::map<std::string, std::map<std::string, std::vector<std::pair<std::string, double>>>> mapDuration_, std::map<std::string, double> mapAdmission_, std::map<std::string, double> mapDischarge_, std::map<std::string, std::map<std::string, double>> mapColoAtAdmission_, std::map<std::string, std::map<std::string, std::vector<double>>> mapProbabilityOfTransmission2_): mapDuration(mapDuration_), mapAdmission(mapAdmission_), mapDischarge(mapDischarge_), mapColoAtAdmission(mapColoAtAdmission_), mapProbabilityOfTransmission2(mapProbabilityOfTransmission2_){
	
}

//2eme constructeur
Parameters::Parameters(std::map<std::string, double> mapAdmission_, std::map<std::string, double> mapDischarge_):mapAdmission(mapAdmission_), mapDischarge(mapDischarge_){
	
}

//3eme constructeur
Parameters::Parameters(std::map<std::string, std::map<std::string, std::vector<std::pair<std::string, double>>>> mapDuration_, std::map<std::string, std::map<std::string, double>> mapColoAtAdmission_, std::map<std::string, std::map<std::string, double>> mapProbabilityOfTransmission_): mapDuration(mapDuration_), mapColoAtAdmission(mapColoAtAdmission_), mapProbabilityOfTransmission(mapProbabilityOfTransmission_){
	
}

Parameters::Parameters(std::map<std::string, std::map<std::string, std::vector<std::pair<std::string, double>>>> mapDuration_, std::map<std::string, std::map<std::string, double>> mapColoAtAdmission_, std::map<std::string, std::map<std::string, std::vector<double>>> mapProbabilityOfTransmission2_): mapDuration(mapDuration_), mapColoAtAdmission(mapColoAtAdmission_), mapProbabilityOfTransmission2(mapProbabilityOfTransmission2_){
	
}

//4eme constructeur
Parameters::Parameters(std::map<std::string, std::map<std::string, std::vector<std::pair<std::string, double>>>> mapDuration_, std::map<std::string, double> mapAdmission_, std::map<std::string, double> mapDischarge_, std::map<std::string, std::map<std::string, double>> mapColoAtAdmission_, std::map<std::string, std::map<std::string, double>> mapProbabilityOfTransmission_, std::map<std::string, std::map<std::string, double>> mapLengthOfStay_, std::map<std::string, double> mapAdmitted_): mapDuration(mapDuration_), mapAdmission(mapAdmission_), mapDischarge(mapDischarge_), mapColoAtAdmission(mapColoAtAdmission_), mapProbabilityOfTransmission(mapProbabilityOfTransmission_), mapLengthOfStay(mapLengthOfStay_), mapAdmitted(mapAdmitted_){
	
}

Parameters::Parameters(std::map<std::string, std::map<std::string, std::vector<std::pair<std::string, double>>>> mapDuration_, std::map<std::string, double> mapAdmission_, std::map<std::string, double> mapDischarge_, std::map<std::string, std::map<std::string, double>> mapColoAtAdmission_, std::map<std::string, std::map<std::string, std::vector<double>>> mapProbabilityOfTransmission2_, std::map<std::string, std::map<std::string, double>> mapLengthOfStay_, std::map<std::string, double> mapAdmitted_): mapDuration(mapDuration_), mapAdmission(mapAdmission_), mapDischarge(mapDischarge_), mapColoAtAdmission(mapColoAtAdmission_), mapProbabilityOfTransmission2(mapProbabilityOfTransmission2_), mapLengthOfStay(mapLengthOfStay_), mapAdmitted(mapAdmitted_){
	
}

//5eme constructeur
Parameters::Parameters(std::map<std::string, double> mapAdmission_, std::map<std::string, double> mapDischarge_, std::map<std::string, std::map<std::string, double>> mapLengthOfStay_, std::map<std::string, double> mapAdmitted_):mapAdmission(mapAdmission_), mapDischarge(mapDischarge_), mapLengthOfStay(mapLengthOfStay_), mapAdmitted(mapAdmitted_){
	
}

//6eme constructeur
Parameters::Parameters(std::map<std::string, std::map<std::string, std::vector<std::pair<std::string, double>>>> mapDuration_, std::map<std::string, double> mapAdmission_, std::map<std::string, double> mapDischarge_, std::map<std::string, std::map<std::string, double>> mapColoAtAdmission_, std::map<std::string, std::map<std::string, double>> mapProbabilityOfTransmission_, std::map<std::string, std::map<std::string, std::map<std::string, double>>> mapDaysOfWeek_): mapDuration(mapDuration_), mapAdmission(mapAdmission_), mapDischarge(mapDischarge_), mapColoAtAdmission(mapColoAtAdmission_), mapProbabilityOfTransmission(mapProbabilityOfTransmission_), mapDaysOfWeek(mapDaysOfWeek_){
	
}

Parameters::Parameters(std::map<std::string, std::map<std::string, std::vector<std::pair<std::string, double>>>> mapDuration_, std::map<std::string, double> mapAdmission_, std::map<std::string, double> mapDischarge_, std::map<std::string, std::map<std::string, double>> mapColoAtAdmission_, std::map<std::string, std::map<std::string, std::vector<double>>> mapProbabilityOfTransmission2_, std::map<std::string, std::map<std::string, std::map<std::string, double>>> mapDaysOfWeek_): mapDuration(mapDuration_), mapAdmission(mapAdmission_), mapDischarge(mapDischarge_), mapColoAtAdmission(mapColoAtAdmission_), mapProbabilityOfTransmission2(mapProbabilityOfTransmission2_), mapDaysOfWeek(mapDaysOfWeek_){
	
}

//7eme constructeur
Parameters::Parameters(std::map<std::string, std::map<std::string, std::vector<std::pair<std::string, double>>>>mapDuration_, std::map<std::string, std::map<std::string, std::vector<double>>> mapDistributionDuration_, std::map<std::string, std::map<std::string, double>> mapColoAtAdmission_, std::map<std::string, std::map<std::string, double>> mapProbabilityOfTransmission_, std::map<std::string, std::map<std::string, std::map<std::string, double>>> mapDaysOfWeek_): mapDuration(mapDuration_), mapDistributionDuration(mapDistributionDuration_), mapColoAtAdmission(mapColoAtAdmission_), mapProbabilityOfTransmission(mapProbabilityOfTransmission_), mapDaysOfWeek(mapDaysOfWeek_){
	
}

Parameters::Parameters(std::map<std::string, std::map<std::string, std::vector<std::pair<std::string, double>>>>mapDuration_, std::map<std::string, std::map<std::string, std::vector<double>>> mapDistributionDuration_, std::map<std::string, std::map<std::string, double>> mapColoAtAdmission_, std::map<std::string, std::map<std::string, std::vector<double>>> mapProbabilityOfTransmission2_, std::map<std::string, std::map<std::string, std::map<std::string, double>>> mapDaysOfWeek_): mapDuration(mapDuration_), mapDistributionDuration(mapDistributionDuration_), mapColoAtAdmission(mapColoAtAdmission_), mapProbabilityOfTransmission2(mapProbabilityOfTransmission2_), mapDaysOfWeek(mapDaysOfWeek_){
	
}

//8eme constructeur
Parameters::Parameters(std::map<std::string, std::map<std::string, std::vector<std::pair<std::string, double>>>>mapDuration_, std::map<std::string, std::map<std::string, double>> mapColoAtAdmission_, std::map<std::string, std::map<std::string, double>> mapProbabilityOfTransmission_,std::map<std::string, std::map<std::string, std::map<std::string, double>>> mapDaysOfWeek_): mapDuration(mapDuration_), mapColoAtAdmission(mapColoAtAdmission_), mapProbabilityOfTransmission(mapProbabilityOfTransmission_), mapDaysOfWeek(mapDaysOfWeek_){
	
}

Parameters::Parameters(std::map<std::string, std::map<std::string, std::vector<std::pair<std::string, double>>>>mapDuration_, std::map<std::string, std::map<std::string, double>> mapColoAtAdmission_, std::map<std::string, std::map<std::string, std::vector<double>>> mapProbabilityOfTransmission2_,std::map<std::string, std::map<std::string, std::map<std::string, double>>> mapDaysOfWeek_): mapDuration(mapDuration_), mapColoAtAdmission(mapColoAtAdmission_), mapProbabilityOfTransmission2(mapProbabilityOfTransmission2_), mapDaysOfWeek(mapDaysOfWeek_){
	
}

//constructeur admission1
Parameters::Parameters(std::map<std::string, double> mapAdmission_, std::map<std::string, double> mapDischarge_, std::map<std::string, std::map<std::string, double>> mapAdmissionDistribution_, std::map<std::string, std::map<std::string, double>> mapDischargeDistribution_, std::map<std::string, double> mapNbOfInd_, std::map<std::string, std::map<std::string, double>> mapLengthOfStay_): mapAdmission(mapAdmission_), mapDischarge(mapDischarge), mapAdmissionDistribution(mapAdmissionDistribution_), mapDischargeDistribution(mapDischargeDistribution_), mapNbOfInd(mapNbOfInd_), mapLengthOfStay(mapLengthOfStay_){
	
}

//constructeur admission2
Parameters::Parameters(std::map<std::string, double> mapAdmission_, std::map<std::string, std::map<std::string, double>> mapAdmissionDistribution_, std::map<std::string, double> mapNbOfInd_, std::map<std::string, std::map<std::string, double>> mapLengthOfStay_):mapAdmission(mapAdmission_), mapAdmissionDistribution(mapAdmissionDistribution_), mapNbOfInd(mapNbOfInd_), mapLengthOfStay(mapLengthOfStay_){
	
}

//----------------------------- FILETREATMENT -----------------------------//
//constructeur par défaut de Filetreatment
Filetreatment::Filetreatment(){
	
}

//second constructeur
Filetreatment::Filetreatment(string file): file_(file){
	
}

vector<vector<string>> Filetreatment::tabInfo(const vector<string>& name_colomn){
	//---- un tableau de vecteur pour avoir un vecteur = les caractéristiques d'un individu. Comme je ne veux pas toutes les caractéristiques, je stocke celle que je veux dans un tableau puis j'initialiserait les individus dans le context dans la class Model ----//
	string line;
	vector<vector<string>> table;
	ifstream ifs (file_);
	vector<string> tempo;
	vector <string> first_line;
	getline(ifs,line);
	boost::trim_if(line, boost::is_any_of("\r\n|\r|\n|\t"));
	boost::split(first_line, line, boost::is_any_of(";"));
	map<string,int> colomn;
	//je recupe dans une map les indices des colonnes
	for(int i(0);i<first_line.size();i++){
		colomn[first_line[i]] = i;
		//je récup les indices qui ne sont pas dans name_colomn
	}
	vector<int> name_colomn_index;
	for(auto &elem : name_colomn){
		name_colomn_index.push_back(distance(first_line.begin(), find(first_line.begin(), first_line.end(), elem)));
	}	
	if(!ifs.good()){
		cout<<"enable to open file"<<endl;
	}
	while(getline(ifs, line)){
		boost::trim_if(line, boost::is_any_of("\r\n|\r|\n|\t"));
		boost::split(tempo, line, boost::is_any_of(";"));
		vector<string> stock_tempo;
		for(int s(0); s<name_colomn_index.size();s++){
			if(s==1 || s==2){
				stock_tempo.push_back(checkDate(tempo[name_colomn_index[s]]));
			}else{
				stock_tempo.push_back(tempo[name_colomn_index[s]]);
			}
			
		}
		table.push_back(stock_tempo);
	}
	ifs.close();
	return table;
}

vector<vector<string>> Filetreatment::tabInfoPathogen(){
	//---- redonne sous la forme d'un vector de vector les pathogènes à étudier ainsi que leurs caractéristiques ----//
	string line;
	vector<vector<string>> table;
	ifstream ifs (file_);
	vector<string> tempo;
	vector <string> first_line;
	getline(ifs,line);
	boost::trim_if(line, boost::is_any_of("\r\n|\r|\n|\t"));
	boost::split(first_line, line, boost::is_any_of(";"));	
	if(!ifs.good()){
		cout<<"enable to open file"<<endl;
	}
	while(getline(ifs, line)){
		boost::trim_if(line, boost::is_any_of("\r\n|\r|\n|\t"));
		boost::split(tempo, line, boost::is_any_of(";"));
		vector<string> stock_tempo;
		for(int s(0); s<tempo.size();s++){
			stock_tempo.push_back(tempo[s]);
		}
		table.push_back(stock_tempo);
	}
	ifs.close();
	return table;
}

string Filetreatment::checkDate(string d){
	//---- vérification du format de la date ----//
	boost::trim_if(d, boost::is_any_of(" "));
	std::regex base_regex("\\b\\d{2}[/]\\d{2}[/]\\d{4}\\b");
	if(regex_match(d.begin(),d.end(),base_regex)){
		vector<string> v;
		boost::split(v,d, boost::is_any_of("/"),boost::token_compress_on);
		string d2 = v[2]+ "-" + v[1]+ "-" + v[0];
		return d2;
	}else{
		return d;
	}
}

time_t Filetreatment::stringToDate(string st){
	//---- donne une date à partir d'un string sous la forme time_t ----//
	struct tm tmSt = {0};
	if(st.size()<11){
		st = st + string(" 00:00:00");
	}
	const char *stDateChar = st.c_str();
	strptime(stDateChar, "%Y-%m-%d%t%T", &tmSt);
	time_t timSt = mktime(&tmSt);
	return(timSt);
}

string Filetreatment::dateToString(time_t tm){
	//---- donne un string à partir d'une date en time_t ----//
	char buffer [22];
	strftime(buffer,22,"%Y-%m-%d %T",localtime(&tm));
	string str_buffer = buffer;
	return (str_buffer);
}

multimap<string,vector<string>> Filetreatment::tabPrlvt(const vector<string>& name_colomn){
	//---- redonne une multimap à partir du fichier de prlvt en ne sélectionnant que les colonnes d'intéret ----//
	string line;
	multimap<string,vector<string>> stock;
	ifstream ifs (file_);
	vector<string> tempo;
	vector <string> first_line;
	getline(ifs,line);
	boost::trim_if(line, boost::is_any_of("\r\n|\r|\n|\t"));
	boost::split(first_line, line, boost::is_any_of(";"));
	vector<int> name_colomn_index;
	for(auto &elem : name_colomn){
		name_colomn_index.push_back(distance(first_line.begin(), find(first_line.begin(), first_line.end(), elem)));
	}
	while(getline(ifs, line)){
		boost::trim_if(line, boost::is_any_of("\r\n|\r|\n|\t"));
		boost::split(tempo, line, boost::is_any_of(";"));
		vector<string> stock_tempo;
		for(int s(0); s<name_colomn_index.size();s++){
			if(s==0){
				string st = checkDate(tempo[name_colomn_index[s]]);
				stock_tempo.push_back(st);
			}else{
				stock_tempo.push_back(tempo[name_colomn_index[s]]);
			}
		}
		stock.insert(pair<string,vector<string>>(stock_tempo[0],stock_tempo));
	}
	ifs.close();
	return stock;
}

Parameters* Filetreatment::findADParameters(){
	//---- Fonction qui détermine tout les paramètres nécessaire pour faire un fichier d'admission simulé. Ces paramètres sont des variables de l'objet Parameters ainsi créé à partir d'un fichier de paramètres d'admission. Les variables sont: 
	// admissionDistribution = map avec les paramètres nécessaire pour déterminer le nombre d'admission par jour
	// dischargeDistribution = map avec les paramètres nécessaire pour déterminer le nombre de discharge par jour
	// admission = map avec les probabilités d'admission par jour (maintenant depuis la dernière mise à jour ce sont les taux d'admission)
	// discharge = map avec les probabilités de discharge par jour
	// nbOfInd = map avec la répartition des individus au début de la simulation
	// LOS = map avec les paramètres nécessaire pour déterminer la durée de séjour des individus
	// ----//
	Parameters* parameters;
	map<string, map<string, double>> admissionDistribution;
	map<string, map<string, double>> dischargeDistribution;
	map<string, double> admission;
	map<string, double> discharge;
	map<string, double> nbOfInd;
	std::map<std::string, std::map<std::string, double>> LOS;
	ifstream ifs (file_);
	string line;
	vector<string> tempo;
	while(getline(ifs,line)){
		boost::trim_if(line, boost::is_any_of("\r\n|\r|\n|\t"));
		boost::split(tempo, line, boost::is_any_of(";"));
		if(tempo[0]=="admission"){
			admission[tempo[1]] = stod(tempo[2]);
		}else if(tempo[0]=="discharge"){
			discharge[tempo[1]] = stod(tempo[2]);
		}else if(tempo[0]=="admission distribution"){
			admissionDistribution[tempo[1]][tempo[2]] = stod(tempo[3]);
		}else if(tempo[0]=="discharge distribution"){
			dischargeDistribution[tempo[1]][tempo[2]] = stod(tempo[3]);
		}else if(tempo[0]=="number of individual"){
			nbOfInd[tempo[1]] = stod(tempo[2]);
		}else if(tempo[0]=="length of stay"){
			LOS[tempo[1]][tempo[2]] = stod(tempo[3]);
		}
	}
	ifs.close();
	if(dischargeDistribution.empty()){
		parameters = new Parameters(admission, admissionDistribution, nbOfInd, LOS);
	}else{
		parameters = new Parameters(admission, discharge, admissionDistribution, dischargeDistribution, nbOfInd, LOS);
	}
	return parameters;
}

vector<string> Filetreatment::buildMeAVectorOfPathogens(vector<vector<string>> Mt_path){
	//---- fonction qui renvoie un vector de string pour les pathogènes ----//
	vector<string> res;
	for(int i(0); i<Mt_path.size(); i++){
		string pathogen;
		if(Mt_path[i][0]!=""){
			for(int j(0); j<Mt_path[i].size(); j++){
				if(j==Mt_path[i].size()-1){
					pathogen = pathogen + Mt_path[i][j];
				}else{
					pathogen = pathogen + Mt_path[i][j] + "_";
				}	
			}
			res.push_back(pathogen);
		}
	}
	return res;
}

Parameters* Filetreatment::findParameters(){
	//---- fonction qui retourne un objet Parameters. Dans cette objet, on créé les variables nécessaire au model à partir du fichier csv des paramètres. Ces variables sont:
	// resDOW = map avec les paramètres pour le prélèvement des individus en fonction du jour de la semaine
	// resDuration = map avec les paramètres pour calculer les durées de colonisation pour chaque pathogène d'interet
	// resColo = map avec les taux d'admis colonisés
	// resProba = map avec les taux de transmission
	// resDurationDistribution = map avec les durées de colonisation dans le cas où l'utilisateur souhaite utiliser une distribution empirique dans le choix de la durée de colonisation
	// ----//
	Parameters* parameters;
	string line;
	map<string, map<string, map<string, double>>> resDOW;
	map<string, map<string, vector<pair<string, double>>>> resDuration;
	map<string, map<string, double>> resColo;
	// map<string, map<string, double>> resProba;
	map<string, map<string, vector<double>>> resProba;
	map<string, map<string, vector<double>>> resDurationDistribution;
	double mean;
	double variance;
	ifstream ifs (file_);
	vector<string> tempo;
	while(getline(ifs,line)){
		boost::trim_if(line, boost::is_any_of("\r\n|\r|\n|\t"));
		boost::split(tempo, line, boost::is_any_of(";"));
		string firstString = tempo[0];
		vector<string>::iterator it;
		vector<string>::iterator itHosp;
		if(tempo[0] == "duration of colonization"){
			//on est dans les durations
			resDuration[tempo[1]][tempo[2]].push_back(make_pair(tempo[3], stod(tempo[4])));
		}else if(tempo[0]=="distribution of duration"){
			vector<double> vectorDistribDuration;
			for(int i(3); i<tempo.size();i++){
				if(tempo[i]!="")vectorDistribDuration.push_back(stod(string(tempo[i])));
			}
			resDurationDistribution[tempo[1]][tempo[2]]=vectorDistribDuration;
		// }else if(tempo[0]=="colonization at admission" || tempo[0]=="probability of transmission"){
		}else if(tempo[0]=="colonization at admission"){
			//proba ou colo mais c'est la même map
			string path = tempo[1];
			map<string, map<string, double>>* pointeMap;
			if(tempo[0]=="colonization at admission"){
				pointeMap = &resColo;
			}
			// else{
				// pointeMap = &resProba;
			// }
			map<string, map<string, double>>::iterator itMap = find_if(pointeMap->begin(), pointeMap->end(),[path](std::pair<string, map<string, double> > n)->bool {return n.first==path;});
			if(itMap!=pointeMap->end()){
				(*itMap).second.insert(make_pair(tempo[2], stod(tempo[3])));
			}else{
				map<string, double> mapInterm;
				mapInterm.insert(make_pair(tempo[2], stod(tempo[3])));
				pointeMap->insert(make_pair(tempo[1], mapInterm));
			}
		}else if (tempo[0]=="days of week"){
			//il est dans l'autre liste 
			string cat = tempo[1];
			string value = tempo[2];
			resDOW[cat][value][tempo[3]] = stod(tempo[4]);
		// }
		}else if(tempo[0]=="probability of transmission"){
			vector<double> vectorProb;
			for(int i(3); i<tempo.size();i++){
				if(tempo[i]!="")vectorProb.push_back(stod(string(tempo[i])));
			}
			resProba[tempo[1]][tempo[2]]=vectorProb;
		}
	}
	if(resDurationDistribution.empty()){

		parameters = new Parameters(resDuration, resColo, resProba, resDOW);
	}else{
		parameters = new Parameters(resDuration, resDurationDistribution,resColo, resProba, resDOW);
	}
	ifs.close();
	return parameters;
}

map<string, map<string, vector<string>>> Filetreatment::buildMeAddDisMap(vector<vector<string>> Mt, int pos){
	//---- fonction générique pour avoir les admissions discharge dans parameters ----//
	map<string, map<string, vector<string>>> res;
	for(int i(0); i<Mt.size(); i++){
		vector<string> tempo = Mt[i];
		string date = tempo[pos];
		string name = tempo[0];
		map<string, map<string, vector<string>>>::iterator itMap = find_if(res.begin(), res.end(), [date](pair<string, map<string, vector<string>>> n)->bool {return n.first==date;});
		if(itMap!=res.end()){
			//la date est dedans on vérif si l'ind est pas déjà dedans
			map<string, vector<string>> individuals = (*itMap).second;
			map<string, vector<string>>::iterator itVect =individuals.find(name);
			if(itVect==individuals.end()){
				//name n'est dans individuals on le rajoute
				(*itMap).second.insert(make_pair(name, tempo));
			}else{
				//c'est un problème car l'individus a deux fois la date dans ce cas il faut faire une erreure
				string s = " l'individus " + name + " a deux dates d'entrée ou de sortie identiques ";
				throw s;
			}
		}else{
			//la date n'est pas dedans
			map<string, vector<string>> firstMap;
			firstMap.insert(make_pair(name, tempo));
			res.insert(make_pair(date, firstMap));
		}
	}
	return res;
}
