#!/usr/bin/env bash

#SBATCH --ntasks=3
#SBATCH -o slurm_simulation/test/test-%j.out # STDOUT
#SBATCH -e slurm_simulation/error/err-%j.err # STDERR


# on s'assure d'avoir un environnement propre
module purge

# on charge les module necessaire au le script
module load gcc/9.2.0
module load openmpi/4.0.5
module load netcdf/4.7.3
module load boost/1.72.0
module load repast_hpc/2.3.0


nbOfNet=50
nbOfSim=100

addName1="byCat"
fileNameAnalyze1="byCat"
xtra1="validation/"

#sbatch -p common,dedicated --qos=fast --job-name=analysis ./modelMinimumBoucle.sh $nbOfNet $nbOfSim $addName1 $fileNameAnalyze1 $xtra1

nbOfScenario=63
addName2="byCatCohorting"
fileNameAnalyze2="byCatCohorting"
xtra2="cohorting/"

sbatch -p common,dedicated --qos=fast --job-name=analysis ./modelInterventions.sh $nbOfScenario $nbOfSim $addName2 $fileNameAnalyze2 $xtra2

nbOfScenario2=30
addName3="byCatCP"
fileNameAnalyze3="byCatCP"
xtra3="compliance/"

sbatch -p common,dedicated --qos=fast --job-name=analysis ./modelInterventionsCP2.sh $nbOfScenario2 $nbOfSim $addName3 $fileNameAnalyze3 $xtra3

nbOfScenario3=5
addName4="byCatSC"
fileNameAnalyze4="byCatSC"
xtra4="complianceSC/"
nbSupers=60

sbatch -p common,dedicated --qos=fast --job-name=analysis ./modelInterventionsCP.sh $nbOfScenario3 $nbOfSim $fileNameAnalyze4 $xtra4 $nbSupers

nbOfScenario4=30
addName5="byCatVC"
fileNameAnalyze5="byCatVC"
xtra5="vaccination/"

sbatch -p common,dedicated --qos=fast --job-name=analysis ./modelInterventionsVC.sh $nbOfScenario4 $nbOfSim $addName5 $fileNameAnalyze5 $xtra5

nbOfScenario5=5
addName6="byCatSCVC"
fileNameAnalyze6="byCatSCVC"
xtra6="vaccinationSC/"

sbatch -p common,dedicated --qos=fast --job-name=analysis ./modelInterventionsVCSC.sh $nbOfScenario5 $nbOfSim $fileNameAnalyze6 $xtra6 $nbSupers


