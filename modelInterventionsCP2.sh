#!/usr/bin/env bash
#SBATCH -n 6
#SBATCH -o slurm_simulation/test/test-%j.out # STDOUT
#SBATCH -e slurm_simulation/error/err-%j.err # STDERR
#SBATCH --time=00:05:00


# on s'assure d'avoir un environnement propre
# module purge

# on charge les module necessaire au le script
module load gcc/9.2.0
module load openmpi/4.0.5
module load netcdf/4.7.3
module load boost/1.72.0
module load repast_hpc/2.3.0

nbOfScenario=$1
nbOfSim=$2
addName=$3
fileNameAnalyze=$4
xtra=$5

FA="yes"
NOA=2
BTV="yes"
NCIV=0

# ensuite on calcule le learning
learningF="parameters/resultParameters_InterventionsCP"$addName".csv"
Command="sbatch -p common --qos=fast --dependency=singleton --job-name=analyze ./modelCluster.sh -o no -b yes -d none -f $FA -n $NOA -t $BTV -z $NCIV -l $learningF"
Submit_Output="$($Command 2>&1)"
JobIdL=`echo $Submit_Output | grep 'Submitted batch job' | awk '{print $4}'`
echo "le job id du learning compliance est "
echo $JobIdL

compteur=0
declare -A jobSimulation
for j in `seq 0 $nbOfScenario`;do
	(( compteur = compteur+1 ))
	Command="sbatch -p common,dedicated --qos=fast --job-name=intervention --array=1-$nbOfSim --dependency=afterany:$JobIdL ./modelCluster.sh -o no -b no -m sim -d none -f $FA -n $NOA -t $BTV -z $NCIV -h $j -l $learningF -r $addName -w compliance -x $xtra"
	Submit_Output="$($Command 2>&1)"
	JobId1=`echo $Submit_Output | grep 'Submitted batch job' | awk '{print $4}'`
	jobSimulation[$compteur]=$JobId1
	echo "le job simulation compliance est "
	echo $JobId1
done

#jR=${jobSimulation[1]}
#echo $jR
#for k in `seq 2 ${#jobSimulation[@]}`;do
#	jR+=":${jobSimulation[$k]}"
#done

#echo "analyse job : "
#Command="sbatch -p common,dedicated --qos=fast --dependency=afterany:$jR --job-name=analyze ./other/interventionBash.sh CP2 $fileNameAnalyze $addName $nbOfScenario"
#Submit_Output="$($Command 2>&1)"
#JobIdAnalyze=`echo $Submit_Output | grep 'Submitted batch job' | awk '{print $4}'`
#echo $JobIdAnalyze

