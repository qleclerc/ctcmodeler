#!/usr/bin/env bash
#SBATCH -n 6
#SBATCH -o slurm_simulation/test/test-%j.out # STDOUT
#SBATCH -e slurm_simulation/error/err-%j.err # STDERR
#SBATCH --time=00:05:00

# on s'assure d'avoir un environnement propre
# module purge

# on charge les module necessaire au le script
module load gcc/9.2.0
module load openmpi/4.0.5
module load netcdf/4.7.3
module load boost/1.72.0
module load repast_hpc/2.3.0

nbOfScenario=$1
nbOfSim=$2
addName=$3
fileNameAnalyze=$4
xtra=$5

nbOfNet=10


FA="yes"
NOA=2
BTV="yes"
NCIV=0


#---------------GRAPH ---------------#
# on va faire d'abord le real
Command="sbatch -p common,dedicated --qos=fast --dependency=singleton --job-name=analyze ./modelCluster.sh -o no -b no -m real -d none -f $FA -n $NOA -r $addName"
Submit_Output="$($Command 2>&1)"
JobId=`echo $Submit_Output | grep 'Submitted batch job' | awk '{print $4}'`
echo "le job id du real est "
echo $JobId

#ctc random
Command="sbatch -p common,dedicated --qos=fast --dependency=singleton --job-name=analyze ./modelCluster.sh -o no -b no -d build -f $FA -n $NOA -t $BTV -z $NCIV -g RandomGraph -r $addName"
Submit_Output="$($Command 2>&1)"
JobIdRandCtc=`echo $Submit_Output | grep 'Submitted batch job' | awk '{print $4}'`
echo "le job id du graph des contacts random est "
echo $JobIdRandCtc

# ensuite on calcule le simu
simNameFile="SimulatedIntervention"$addName
Command="sbatch -p common,dedicated --qos=fast --dependency=singleton --job-name=analyze ./modelCluster.sh -o no -b no -d build -f $FA -n $NOA -t $BTV -z $NCIV -g $simNameFile"
Submit_Output="$($Command 2>&1)"
JobId2=`echo $Submit_Output | grep 'Submitted batch job' | awk '{print $4}'`
echo "le job id du graph simulation est "
echo $JobId2

#-------------------LEARNING---------------#
# ensuite on calcule le learning
learningF="parameters/resultParameters_Interventions"$addName".csv"
Command="sbatch -p common --qos=fast --dependency=afterany:$JobId2 ./modelCluster.sh -o no -b yes -d use -f $FA -n $NOA -t $BTV -z $NCIV  -l $learningF -g $simNameFile"
Submit_Output="$($Command 2>&1)"
JobIdL=`echo $Submit_Output | grep 'Submitted batch job' | awk '{print $4}'`
echo "le job id du learning cohorting est "
echo $JobIdL

#-------------------SIMULATIONS-------------#
echo "les jobs contacts random sont: "
sbatch -p common,dedicated --qos=fast --array=1-$nbOfSim --dependency=afterany:$JobIdRandCtc ./modelCluster.sh -o no -b no -m sim -d use -f $FA -n $NOA -t $BTV -z $NCIV -l $learningF -g RandomGraph -r $addName -x $xtra

echo "les jobs contacts simu sont: "
sbatch -p common,dedicated --qos=fast --array=1-$nbOfSim --dependency=afterany:$JobIdL ./modelCluster.sh -o no -b no -m sim -d use -f $FA -n $NOA -t $BTV -z $NCIV -l $learningF -g $simNameFile -x $xtra

declare -A jobScenario
# puis on fait les interventions
for i in `seq 1 $nbOfScenario`;do
	echo $i
	graph=$simNameFile$i
	Command="sbatch -p common,dedicated --qos=fast --array=1-$nbOfNet --dependency=afterany:$JobIdL ./modelCluster.sh -o no -b no -d build -f $FA -n $NOA -t $BTV -z $NCIV -h $i -w cohorting -l $learningF -g $graph"
	Submit_Output="$($Command 2>&1)"
	JobId3=`echo $Submit_Output | grep 'Submitted batch job' | awk '{print $4}'`
	jobScenario[$i]=$JobId3
done


declare -A jobSimulation
for j in `seq 1 $nbOfScenario`;do
   for k in `seq 1 $nbOfNet`;do
    graph2=$simNameFile$j"_iter_"$k
	  Command="sbatch -p common,dedicated --qos=fast --job-name=intervention --array=1-$nbOfSim --dependency=afterany:${jobScenario[$j]} ./modelCluster.sh -o no -b no -m sim -d use -f $FA -n $NOA -t $BTV -z $NCIV -h $j -w cohorting -l $learningF -g $graph2 -x $xtra"
	  Submit_Output="$($Command 2>&1)"
	  JobId4=`echo $Submit_Output | grep 'Submitted batch job' | awk '{print $4}'`
	  jobSimulation[$j]=$JobId4
 done
done


#jR=${jobSimulation[1]}
#echo $jR
#for k in `seq 2 ${jobSimulation[@]}`;do
#	jR+=":${jobSimulation[$k]}"
#done

#echo "analyse job : "
#Command="sbatch -p common,dedicated --qos=fast --dependency=afterany:$jR --job-name=analyze ./other/interventionBash.sh normal $fileNameAnalyze $addName $nbOfScenario"
#Submit_Output="$($Command 2>&1)"
#JobIdAnalyze=`echo $Submit_Output | grep 'Submitted batch job' | awk '{print $4}'`
#echo $JobIdAnalyze

#echo "analyse job2 : "
#Command="sbatch -p common,dedicated --qos=fast --dependency=afterany:$jR --job-name=analyze ./other/simulationBash.sh $fileNameAnalyze $addName"
#Submit_Output="$($Command 2>&1)"
#JobIdAnalyze2=`echo $Submit_Output | grep 'Submitted batch job' | awk '{print $4}'`
#echo $JobIdAnalyze2

# sbatch -p common,dedicated --qos=fast --job-name=intervention ./other/simulationBash.sh byCat29 byCat29

# sbatch -p common,dedicated --qos=fast --job-name=intervention ./other/interventionBash.sh byCat29 byCat29 31


