#!/usr/bin/env bash
#SBATCH -n 6
#SBATCH -o slurm_simulation/test/test-%j.out # STDOUT
#SBATCH -e slurm_simulation/error/err-%j.err # STDERR
#SBATCH --time=00:05:00

# on s'assure que la commande module est definie
# source /local/gensoft2/adm/etc/profile.d/modules.sh

# on s'assure d'avoir un environnement propre
# module purge

# on charge les module necessaire au le script
module load gcc/9.2.0
module load R/4.2.2

Rscript createSCinterventions_20.R
Rscript createSCinterventions_60.R
Rscript createSCinterventions_100.R
