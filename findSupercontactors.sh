#!/usr/bin/env bash
#SBATCH -n 6
#SBATCH -o slurm_simulation/test/test-%j.out # STDOUT
#SBATCH -e slurm_simulation/error/err-%j.err # STDERR
#SBATCH --time=00:05:00

# on s'assure que la commande module est definie
# source /local/gensoft2/adm/etc/profile.d/modules.sh

# on s'assure d'avoir un environnement propre
# module purge

# on charge les module necessaire au le script
module load gcc/9.2.0
module load openmpi/4.0.5
module load netcdf/4.7.3
module load boost/1.72.0
module load repast_hpc/2.3.0

#module load R/4.2.2

nbOfSim=500

#sct: supercontactor type; duration, contact
#scg: supercontactor group; PA, PE

declare -a addNameVector
addNameVector=("byCatSCPEtc" "byCatSCPEtd" "byCatSCPAtc" "byCatSCPAtd")
addName=""

declare -a groupVector
groupVector=("PE" "PA")

declare -a typeVector
typeVector=("contact" "duration")


compteur=0
for groupi in ${groupVector[*]};do
  for typei in ${typeVector[*]};do
    addName=${addNameVector[compteur]}
    echo $addName
    Command="sbatch -p common,dedicated --qos=fast --job-name=findsupers ./modelCluster.sh -d build -w supercontractor -r $addName -a $groupi -e $typei"
    Submit_Output="$($Command 2>&1)"
		JobId1=`echo $Submit_Output | grep 'Submitted batch job' | awk '{print $4}'`
		echo "le job simulation est "
		echo $JobId1
    ((compteur=compteur+1))
  done
done


declare -a randomVector
randomVector=("RandomAll" "RandomCare" "RandomPatients")
for randi in ${randomVector[*]};do
  for i in `seq 1 $nbOfSim`;do
    Command="sbatch -p common,dedicated --qos=fast --job-name=findsupers ./modelCluster.sh -d build -w supercontractor -r $randi -a $randi -i $i"
    Submit_Output="$($Command 2>&1)"
		JobId2=`echo $Submit_Output | grep 'Submitted batch job' | awk '{print $4}'`
		echo "le job simulation est "
		echo $JobId2
  done
done

sbatch -q fast -p common,dedicated --job-name=findsupers --dependency=singleton buildSupercontactors.sh
