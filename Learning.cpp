/* LEARNING CLASS
* this file gather all organism from the project
*
*	
*	Learning.cpp
*
*	Created on: mai 22, 2018
*	Author: Audrey
*
*/
#include "Learning.h"

using namespace std;
using namespace repast;

Learning::Learning(){

}
 
Learning::~Learning(){
	if(!previousCtc.empty()){
		for(multimap<std::string,shared_ptr<Contact>>::iterator it = previousCtc.begin(); it!=previousCtc.end(); it++){
			previousCtc.erase(it);
		}
	}
	
	if(!allIndividuals.empty()){
		for(auto const& elem:allIndividuals){
			delete elem;
		}
	}
}

Learning::Learning(repast::Properties* mainProps, string beginDate_, string endDate_): beginDate(beginDate_), endDate(endDate_){
	eng = Random::instance()->engine();
	cout << "learning light " << endl;
}

Learning::Learning(repast::Properties* mainProps, string admin_,  string beginDate_, string endDate_, string nbOfIndInitDate_, string groupFile_):admin(admin_), beginDate(beginDate_), endDate(endDate_),nbOfIndInitDate(nbOfIndInitDate_), groupFile(groupFile_){
	try{
		if(FileExists(admin)==0){
			throw std::runtime_error(" fail to find admin file");
		}
	}catch(std::exception const& exc){
		cerr << exc.what() << "\n";
		exit(1);
	}
	cout<< "Files: " << endl;
	cout << admin << endl;
	cout << "Loading admission " << endl;
	tabAdmin = buildTabAdmin(admin);
	buildMeIndividuals();
	mapTabAdmin = updateTabAdmin(tabAdmin);
	initializeRandom(*mainProps);
	eng = Random::instance()->engine();
	groups = buildGroup(groupFile);
}

Learning::Learning(repast::Properties* mainProps, vector<string> listOfAllPath_, string admin_, string ctc_, string prlvt_, string beginDate_, string endDate_, int nbOfAcquisitionWeek_, bool firstAcqOnly_, bool probaByType_, bool durationByType_, int seuil_, int nbCtcInfMin_, bool optionDuration_, bool withLogPAPA_, bool withLogPAPE_,bool withLogPEPA_, bool withLogPEPE_, string groupFile_): listOfAllPath(listOfAllPath_) ,admin(admin_), ctc(ctc_), prlvt(prlvt_), beginDate(beginDate_), endDate(endDate_), nbOfAcquisitionWeek(nbOfAcquisitionWeek_), firstAcqOnly(firstAcqOnly_), probaByType(probaByType_),durationByType(durationByType_), seuil(seuil_), nbCtcInfMin(nbCtcInfMin_), optionDuration(optionDuration_), withLogPAPA(withLogPAPA_), withLogPAPE(withLogPAPE_), withLogPEPA(withLogPAPA_), withLogPEPE(withLogPEPE_), groupFile(groupFile_){
	if(nbOfAcquisitionWeek!=0){
		try{
			if(FileExists(admin)==0){
				throw std::runtime_error(" fail to find admin file");
			}else if(FileExists(ctc)==0){
				throw std::runtime_error(" fail to find contact file");
			}else if(FileExists(prlvt)==0){
				throw std::runtime_error(" fail to find swab file");
			}
		}catch(std::exception const& exc){
			cerr << exc.what() << "\n";
			exit(1);
		}
		cout<< "Files: " << endl;
		cout << admin << endl;
		cout << ctc << endl;
		cout << prlvt << endl;
		cout << "Loading admission " << endl;
		tabAdmin = buildTabAdmin(admin);
		buildMeIndividuals();
		mapTabAdmin = updateTabAdmin(tabAdmin);		
		cout << "Loading swabs " << endl;
		previousPrlvt = buildPreviousPrlvt(prlvt);
		pair<std::map<string, std::vector<Individual*>>, std::map<Individual*, std::map<time_t, std::vector<std::string>>>> mapsPrlvt2 = updateMap2(listOfAllPath);
		pathogensCarriers = mapsPrlvt2.first;
		allCarriers = mapsPrlvt2.second;
		
		if(!probaByType){
			groups = buildGroup(groupFile);
			mapGroup = findMapGroup();
		}
		mapCat = findMapCat();
		mapType = findMapType();
		cout << "size " << pathogensCarriers.size() << " and " << allCarriers.size() << " and " << previousPrlvt.size() << " and " << mapGroup.size() << endl;
		initializeRandom(*mainProps);
		eng = Random::instance()->engine();
		// try{
			// if((withLogPAPE || withLogPAPA || withLogPEPE || withLogPEPA) && !probaByType){
				// throw string("you can't use logPAPE or logPAPA or logPEPE or logPEPA with probaByType at false");
			// }
		// }catch(string const& chaine){
			// cerr << chaine << endl;
			// exit(1);
		// }
	}else{
		cout << " the minimal number of acquisition is 1 not 0 " << endl;
	}
}

vector<string> Learning::buildMevectorOfDates(time_t date1, time_t date2){
	//---- Fonction qui retourne un vecteur de date sous forme de string à partir de deux dates en time_t. Ne prend pas la dernière date date2 dans le vecteur ----//
	vector<string> res;
	for(time_t i(date1); i<date2; i = i + time_t(86400)){
		res.push_back(dateToString(i).substr(0,10));
	}
	return(res);
}

map<string, string> Learning::buildGroup(string file_){
	//---- Fonction qui construit les groupes dans lesquel sont répertoriés les catégories d'individu PE à partir d'un fichier donné par l'utilisateur. Pour l'analyse de sensibilité. ----//
	map<string, string> res;
	string line;
	ifstream ifs (file_);
	getline(ifs,line);
	while(getline(ifs, line)){
		vector<string> stock_tempo;
		boost::split(stock_tempo, line, boost::is_any_of(";"));
		res[stock_tempo[0]]=stock_tempo[1];
	}
	ifs.close();
	return res;
}


void Learning::buildPathogenProbabilities(){
	//---- Fonction générique qui rempli la map pathogenProbabilityOfTransmission ----//
	cout << "Loading contacts " << endl;
	previousCtc = buildPreviousCtc(ctc);
	cout << "Compute probabilities " << endl;
	// std::map< std::string ,std::map <std::pair<std::string, std::string>, std::vector <double>>> pathogenProbabilityOfTransmissionParameters = computeProbabilityOfTransmission(probaByType);
	std::map< std::string ,std::map <std::pair<std::string, std::string>, std::map<double, std::vector <double>>>> pathogenProbabilityOfTransmissionParameters2 = computeProbabilityOfTransmission2(probaByType);
	// std::map< std::string ,std::map <std::pair<std::string, std::string>, std::map<double, std::vector <double>>>> pathogenProbabilityOfTransmissionParameters2 = computeProbabilityOfTransmission3(probaByType);
	cout << "Probability of transmission in progress ... " << endl;
	int nbOfWeek = (stringToDate(endDate) - stringToDate(beginDateCtc))/604800;
	// pathogenProbabilityOfTransmission = meanMap(pathogenProbabilityOfTransmissionParameters, seuil, true, ((nbOfWeek-nbOfAcquisitionWeek)));
	pathogenProbabilityOfTransmission2 = meanMap(pathogenProbabilityOfTransmissionParameters2, seuil, true, ((nbOfWeek-nbOfAcquisitionWeek)));
	// pathogenProbabilityOfTransmission = meanMapPathogen(pathogenProbabilityOfTransmissionParameters, seuil, true, ((nbOfWeek-nbOfAcquisitionWeek)-1), ((nbOfWeek-nbOfAcquisitionWeek)-1), probaByType);
}

std::map<std::string, map<std::pair<std::string, std::string>, double>> Learning::meanMapPathogen(std::map<std::string ,std::map <std::pair<std::string, std::string>, std::vector<double>>> inputMap, int seuil, bool week, int nbOfWeekPA, int nbOfWeekPE, bool probaByType){
	map<string, map<pair<string, string>, double>> resFinal;
	for(auto const& [key,val]:inputMap){
		map<pair<string, string>, vector<double>> mapDuration = val;
		double sum;
		double mean;
		map<pair<string, string>, double> res;
		for(auto const& [cat, allNumbers]:mapDuration){
			vector<int> testSeuilVector;
			transform(allNumbers.begin(), allNumbers.end(), back_inserter(testSeuilVector),[](double n){ return(n > 0 ? 1 : 0); });
			int testSeuil = accumulate(testSeuilVector.begin(), testSeuilVector.end(),0);
			if(testSeuil >= seuil){
				double sum = accumulate(allNumbers.begin(), allNumbers.end(),0.0);
				int dividedNumber;
				if(week){
					if(probaByType){
						if(cat.second=="PE"){
							dividedNumber = nbOfWeekPE;
						}else{
							dividedNumber = nbOfWeekPA;
						}
					}else{
						auto itF = find(keyCat.begin(), keyCat.end(), cat.second);
						if(itF!=keyCat.end() && cat.second!="Patient"){
							dividedNumber = nbOfWeekPE;
						}else{
							dividedNumber = nbOfWeekPA;
						}
					}
					// dividedNumber = nbOfWeek;
				}else{
					dividedNumber = allNumbers.size();
				}
				if(sum!=0){
					cout << " sum " << sum << " / " << dividedNumber << " mais avant allNumbers.size() " << allNumbers.size() << endl;
					double mean = sum / dividedNumber;
					res.insert(make_pair(cat, mean));
				}
			}
		}
		resFinal.insert(make_pair(key, res));
	}
	return resFinal;
}

std::map<std::string, std::map<std::pair<std::string, std::string>, double>> Learning::getPathogenProbabilityOfTransmission(){
	return pathogenProbabilityOfTransmission;
}

map<string, map<string, pair<double, double>>> Learning::getDurationOfColonization(){
	cout << "Duration of colonization in progress ... " << endl;
	map<string, map<string, pair<double, double>>> meanVarCol = durationParameters2();
	return meanVarCol;
}

std::map<std::string, std::map<std::string, std::vector<double>>> Learning::getDistributionDuration(){
	map<string, map<string, pair<double, double>>> meanVarCol = durationParameters2();
	return distributionDuration;
}

map<string, map<string, pair<double, double>>> Learning::getDaysOfWeekParameters(){
	cout << "Days of week parameters in progress ... " << endl;
	map<string, map<string, pair<double, double>>> daysOfWeekParams = findMeanAndSdDayOfWeek();
	return daysOfWeekParams;
}

void Learning::buildAdmissionDischarge(){
	//---- Fonction qui calcule les différents paramètres pour le fichier de paramètre d'admission ----//
	cout << "Admission and discharge parameters in progress ... " << endl;
	tuple<map<string, map<string, double>>, map<string, map<string, double>>, map<string, double>, map<string, double>, map<string, map<string, double>>> parametersAC = admissionDischargeProbabilities(admissionDischargeStock);
	admissionDistributionParam = get<0>(parametersAC);
	dischargeDistributionParam = get<1>(parametersAC);
	admissionProba = get<2>(parametersAC);
	dischargeProba = get<3>(parametersAC);
	colonizedAtAdmissionParam = get<4>(parametersAC);
	try{
		if(admissionProba.empty()){
			throw std::runtime_error(" fail to compute probabilities ");
		}
	}catch(std::exception const& exc){
		cerr << exc.what() << "\n";
		exit(1);
	}
}

map<string, map<string, double>> Learning::getAdmissionDistributionParam(){
	return admissionDistributionParam;
}

map<string, map<string, double>> Learning::getDischargeDistributionParam(){
	return dischargeDistributionParam;
}

map<string, map<string, double>> Learning::getColonizedAtAdmissionParam(){
	return colonizedAtAdmissionParam;
}

map<string, double> Learning::getAdmissionProba(){
	return admissionProba;
}

map<string, double> Learning::getDischargeProba(){
	return dischargeProba;
}


void Learning::buildMeIndividuals(){
	//---- Fonction qui construit des objets Individual à partir du fichier d'admission entre beginDate et endDate ----//
	vector<string> allDatesPeriod = returnVectorOfDay(beginDate, endDate);
	sort(allDatesPeriod.begin(), allDatesPeriod.end());
	for(auto const& elem:tabAdmin){
		string date1 = elem[2];
		string date2 = elem[3];
		string name = elem[0];
		if(date2=="")date2=endDate.substr(0,10);
		vector<string> allDatesInd = returnVectorOfDay(date1, date2);
		sort(allDatesInd.begin(), allDatesInd.end());
		vector<string> common;
		set_intersection(allDatesPeriod.begin(), allDatesPeriod.end(), allDatesInd.begin(), allDatesInd.end(),  back_inserter(common));
		if(!common.empty()){
			vector<Individual*>::iterator itIndividual = find_if(allIndividuals.begin(), allIndividuals.end(),[name](Individual* i)->bool {return(i->getCalc_ident()==name);});
			if(itIndividual==allIndividuals.end()){
				if(elem[1]=="PA"){
					Patient* patient = new Patient(elem);
					allIndividuals.push_back(patient);
					allIndDates[patient] = common;
					admissionDischargeStock[patient].push_back(make_pair(patient->getDt_in(), patient->getDt_out()));
				}else if(elem[1]=="PE"){
					Staff* staff = new Staff(elem);
					allIndividuals.push_back(staff);
					string cat = staff->getCat();
					vector<string>::iterator itC = find(keyCat.begin(), keyCat.end(), cat);
					if(itC==keyCat.end()){
						keyCat.push_back(cat);
					}
					allIndDates[staff] = common;
					admissionDischargeStock[staff].push_back(make_pair(staff->getDt_in(), staff->getDt_out()));
				}else{
					cout << " ooooops je ne sais pas ce que c'est je ne peux pas initialiser ce truc! " << endl;
				}	
			}else{
				//il est deja dedans on rajoute des dates
				allIndDates[(*itIndividual)].insert(allIndDates[(*itIndividual)].end(), common.begin(), common.end());
				//il est deja dedans donc c'est une nouvelle période on va rajouter ces dates dans les admissions/discharges
				admissionDischargeStock[(*itIndividual)].push_back(make_pair(date1, date2));
			}	
		}
	}
	keyCat.push_back("Patient");
}

map<string, int> Learning::nbOfIndividualStart(){
	//---- Fonction qui détermine le nombre d'individus à la date nbOfIndInitDate. Retourne une map avec le type (type_motif/cat_service) et le nombre d'individus correspondant ----//
	map<string, int> res;
	for(auto const& [ind, days]:allIndDates){
		vector<string> daysV = days;
		string type = ind->getStatus();
		string cat;
		if(type=="PA"){
			Patient* p = dynamic_cast<Patient*>(ind);
			cat = p->getStatus() + "_" + p->getHosp_flag() + "_" + p->getWard();
		}else{
			Staff* s = dynamic_cast<Staff*>(ind);
			cat = s->getStatus() + "_" + s->getCat() + "_" + s->getWard();
		}
		auto it = find(daysV.begin(), daysV.end(), nbOfIndInitDate);
		if(it!=daysV.end()){
			res[cat] = res[cat] + 1;
		}
	}
	return res;
}

vector<Individual*> Learning::individualAtBeginDate(string date1){
	//---- Fonction qui retourne un vecteur d'individus Individual présent à la date date1 ----//
	vector<Individual*> res;
	for(auto const& [ind, days]:allIndDates){
		vector<string> daysV = days;
		auto it = find(daysV.begin(), daysV.end(), date1);
		if(it!=daysV.end()){
			res.push_back(ind);
		}
	}
	return res;
}

vector<string> Learning::returnVectorOfDay(string bday, string eday){
	//---- Fonction qui retourne un vecteur de date sous forme de string à partir de deux dates en string. Prend en compte la dernière date eday dans le vecteur ----//
	vector<string> allDatesPeriod;
	for(time_t i(stringToDate(bday.substr(0,10))); i<=stringToDate(eday.substr(0,10)); i=i+time_t(86400)){
		allDatesPeriod.push_back(dateToString(i).substr(0,10));
	}
	return(allDatesPeriod);
}

vector<vector<string>> Learning::buildTabAdmin(string admin){
	//---- Fonction qui construit un vecteur de vecteur à partir des données du fichier d'admission ----//
	Filetreatment *f = new Filetreatment(admin);
	const char *vinit[] = {"id", "status","firstDate","lastDate","ward","sex","hospitalization","cat"}; //vecteur avec les caractéristiques que l'on souhaite récupérer
	vector<string> nameC(vinit, end(vinit));
	vector<vector<string>> tabAdmin_=f->tabInfo(nameC); //on applique à f la fonction pour stocker les caractéristiques que l'on souhaite récupérés
	delete f;
	return tabAdmin_;
}

map<string, vector<shared_ptr<Contact>>> Learning::updateCtc(multimap<string,shared_ptr<Contact>> previousCtc){
	//---- Fonction qui prend en argument la multimap des contacts des individus et retourne une map avec en clé un string correspondant au nom de l'individus et en clé ses contacts sous la forme de shared_ptr<Contact> ----//
	map<string, vector<shared_ptr<Contact>>> mapCtc;
	multimap<string,shared_ptr<Contact>>::iterator it;
	for(it = previousCtc.begin(); it != previousCtc.end(); it++){
		string nameC = (*it).first;
		shared_ptr<Contact> prlvtTMP = (*it).second;
		map<string, vector<shared_ptr<Contact>>>::iterator itPctc = find_if(mapCtc.begin(),mapCtc.end(),[nameC](pair<string, vector<shared_ptr<Contact>>> v) -> bool {return v.first==nameC;} );
		if(itPctc!=mapCtc.end()){
			(*itPctc).second.push_back(prlvtTMP);
		}else{
			vector<shared_ptr<Contact>> currentVect;
			currentVect.push_back(prlvtTMP);
			mapCtc.insert(pair<string, vector<shared_ptr<Contact>>>(nameC,currentVect));
		}
	}
	return mapCtc;
}

std::multimap<std::string,shared_ptr<Contact>> Learning::buildPreviousCtc(string ctc){
	//---- Fonction qui prend en argument le fichier des contacts et retourne une multimap avec en clé le nom de l'individus sous forme de string associé à ses contacts sous forme de shared_ptr. Comme l'individus a plusieurs contact on crée une multimap pour que la clé unique soit l'association entre nom/contact ----//
	std::multimap<std::string,shared_ptr<Contact>> previousCtcRes;
	Filetreatment *f_ctc = new Filetreatment(ctc);
	string line_ctc;
	std::ifstream ifs_ctc;
	ifs_ctc =  ifstream(f_ctc->getFile());
	delete f_ctc;
	vector<string> tempo_ctc;
	getline(ifs_ctc,line_ctc); 
	getline(ifs_ctc, line_ctc); 
	boost::trim_if(line_ctc, boost::is_any_of("\r "));
	boost::split(tempo_ctc, line_ctc, boost::is_any_of(";"));
	struct tm tm_ctc = {0};
	const char *tempo_ctcChar = tempo_ctc[2].c_str();
	strptime(tempo_ctcChar, "%Y-%m-%d%t%T", &tm_ctc);
	time_t timCtc = mktime(&tm_ctc);
	time_t timStart = stringToDate(endDate);
	time_t timStartFirst = stringToDate(beginDate);	
	/* ensuite il faut avancer dans le fichier */
	if(timCtc < timStartFirst){
		while(timCtc < timStartFirst && !ifs_ctc.eof()){
			getline(ifs_ctc, line_ctc); 
			boost::trim_if(line_ctc, boost::is_any_of("\r "));
			boost::split(tempo_ctc, line_ctc, boost::is_any_of(";"));
			tempo_ctcChar = tempo_ctc[2].c_str();
			strptime(tempo_ctcChar, "%Y-%m-%d%t%T", &tm_ctc);
			timCtc = mktime(&tm_ctc);
		}
	}
	int c = 0;
	if(timCtc < timStart){
		while(timCtc < timStart && !ifs_ctc.eof()){
			c= c+1;
			if(c==1){
				beginDateCtc = tempo_ctc[2].substr(0,10);
			}
			shared_ptr<Contact> ctc = make_shared<Contact>(tempo_ctc);
			shared_ptr<Contact> ctc2 (ctc);
			allCtc[tempo_ctc[0]].push_back(ctc);
			allCtc[tempo_ctc[1]].push_back(ctc);
			previousCtcRes.insert(pair<string,shared_ptr<Contact>>(tempo_ctc[0],ctc));
			previousCtcRes.insert(pair<string,shared_ptr<Contact>>(tempo_ctc[1],ctc2));
			getline(ifs_ctc, line_ctc); 
			boost::trim_if(line_ctc, boost::is_any_of("\r "));
			boost::split(tempo_ctc, line_ctc, boost::is_any_of(";"));
			tempo_ctcChar = tempo_ctc[2].c_str();
			strptime(tempo_ctcChar, "%Y-%m-%d%t%T", &tm_ctc);
			timCtc = mktime(&tm_ctc);
		}
	}
	cout << "flag8" << endl;
	if(ifs_ctc.is_open()){
		cout << "flag9" << endl;
		ifs_ctc.close();
	}
	cout << "flag10" << endl;
	return previousCtcRes;
}

std::multimap<std::string,std::vector<std::string>> Learning::buildPreviousPrlvt(string prlvt){
	//---- Fonction qui prend en argument le fichier des prlvt et qui retourne une multimap contenant en clé l'association individus/prlvt (prlvt sous forme de vector de string ) ----//
	std::multimap<std::string,std::vector<std::string>> previousPrlvtRes;
	Filetreatment *f_prlvt = new Filetreatment(prlvt);
	string line_prlvt;
	std::ifstream ifs_prlvt;
	ifs_prlvt =  ifstream(f_prlvt->getFile());
	delete f_prlvt;
	vector<string> tempo_prlvt;
	getline(ifs_prlvt,line_prlvt); /* header */
	getline(ifs_prlvt, line_prlvt); /* première ligne */
	boost::trim_if(line_prlvt, boost::is_any_of("\r "));
	boost::split(tempo_prlvt, line_prlvt, boost::is_any_of(";"));
	struct tm tm_prlvt = {0};
	string tempo_prlvtString = tempo_prlvt[1] + " 00:00:00";
	const char *tempo_prlvtChar = tempo_prlvtString.c_str();
	strptime(tempo_prlvtChar, "%Y-%m-%d%t%T", &tm_prlvt);
	time_t timPrlvt = mktime(&tm_prlvt);
	time_t timStart = stringToDate(endDate);
	time_t timStartFirst = stringToDate(beginDate);
	/* ensuite il faut avancer dans le fichier */
	if(timPrlvt < timStartFirst){
		while(timPrlvt < timStartFirst && !ifs_prlvt.eof()){
			getline(ifs_prlvt, line_prlvt);
			boost::trim_if(line_prlvt, boost::is_any_of("\r "));
			boost::split(tempo_prlvt, line_prlvt, boost::is_any_of(";"));
			timPrlvt = stringToDateWithHour(tempo_prlvt[1]);
		}
	}
	/* ici timPrlvt est sup ou egale à timstartFirst*/
	if(timPrlvt < timStart){
		while(timPrlvt < timStart && !ifs_prlvt.eof()){
			previousPrlvtRes.insert(pair<string,vector<string>>(tempo_prlvt[0],tempo_prlvt));
			allPrlvt[tempo_prlvt[0]][tempo_prlvt[1]].push_back(tempo_prlvt);
			getline(ifs_prlvt, line_prlvt); /* première ligne */
			boost::trim_if(line_prlvt, boost::is_any_of("\r "));
			boost::split(tempo_prlvt, line_prlvt, boost::is_any_of(";"));
			timPrlvt = stringToDateWithHour(tempo_prlvt[1]);
		}
	}
	if(ifs_prlvt.is_open()){
		ifs_prlvt.close();
	}
	return previousPrlvtRes;
}

map<string, map<string, pair<double, double>>> Learning::findMeanAndSdDayOfWeek(){
	//---- Fonction qui détermine la moyenne et la variance de prélèvement pour chaque jour de la semaine. Retourne une map avec en clé le type (PA/PE) et en valeur une autre map avec le jour de la semaine en clé et une pair de paramètre (moyenne et variance des prélèvements) ----//
	map<string, map<string, pair<double, double>>> res;
	//1) réorganiser allPrel
	map<string, vector<string>> allPreldates;
	for(auto const& [key, val]:allPrel){
		vector<string> dates = val;
		for(auto const& d:dates){
			allPreldates[d].push_back(key);
		}
	}
	// 0 = dimanche; 1 = lundi; 2 = mardi; 3 = mercredi; 4 = jeudi; 5 = vendredi; 6 = samedi
	map<int, string> daysOfW;
	daysOfW[0] = "sunday";
	daysOfW[1] = "monday";
	daysOfW[2] = "tuesday";
	daysOfW[3] = "wednesday";
	daysOfW[4] = "thursday";
	daysOfW[5] = "friday";
	daysOfW[6] = "saturday";
	vector<string> nonWD = findMeNonWorkingDays(beginDate, endDate, true);
	map<string, map<string, vector<double>>> resInterm;
	for(auto const& [key, val]:allPreldates){
		auto itF = find(nonWD.begin(), nonWD.end(), key);
		if(itF==nonWD.end()){
			vector<string> valST = val;
			map<string, vector<string>> byType = catOfTheVectorST(valST, true);
			time_t stDate = stringToDate(key);
			tm* time_st = localtime(&stDate);
			int wday = time_st->tm_wday;
			for(auto const&[t, ind]:byType){
				resInterm[daysOfW[wday]][t].push_back(ind.size());
			}
		}
	}
	//2)rendre le mean et le sd
	for(auto const& [key, val]:resInterm){
		map<string, vector<double>> mapResultats = val;
		for(auto const& [t, ind]:mapResultats){
			double meanVal = computeMean(ind);
			double varVal = computeSampleVariance(meanVal, ind);
			res[key][t] = make_pair(meanVal, varVal);
		}
	}
	return res;
}

std::map<std::string, std::vector<Individual*>> Learning::findMapCat(){
	//---- Fonction qui pour chaque catégorie de tabAdmin renvoie une map avec comme clé la catégorie et comme valeur un vector d'individu appartenant à cette catégorie ----//
	map<string, vector<Individual*>> results;
	for(auto const& ind:allIndividuals){
		if(ind->getStatus()=="PA"){
			results["Patient"].push_back(ind);
		}else{
			Staff* s = dynamic_cast<Staff*>(ind);
			results[s->getCat()].push_back(ind);
		}
	}
	return results;
}

std::map<std::string, std::vector<Individual*>> Learning::findMapGroup(){
	//---- Fonction qui pour chaque catégorie de tabAdmin renvoie une map avec comme clé la catégorie et comme valeur un vector d'individu appartenant à un groupe ----//
	map<string, vector<Individual*>> results;
	for(auto const& ind:allIndividuals){
		if(ind->getStatus()=="PA"){
			results["Patient"].push_back(ind);
		}else{
			Staff* s = dynamic_cast<Staff*>(ind);			
			results[groups[s->getCat()]].push_back(ind);
		}
	}
	return results;
}

std::map<std::string, std::vector<Individual*>> Learning::findMapType(){
	//---- Fonction qui pour chaque catégorie de tabAdmin renvoie une map avec comme clé la catégorie et comme valeur un vector d'individu appartenant à ce type ----//
	map<string, vector<Individual*>> results;
	for(auto const& ind:allIndividuals){
		results[ind->getStatus()].push_back(ind);
	}
	return results;
}

pair<std::map<std::string, std::vector<Individual*>>, std::map<Individual*, std::map<time_t, std::vector<std::string>>>> Learning::updateMap2(vector<string> listOfAllPath){
	//---- Fonction qui retourne sous la forme d'une pair, deux map. La première map c'est pathogensCarriersRes qui prend comme clé le nom du pathogène et comme valeur un vecteur contenant les noms des individus qui ont été colonisés par ce pathogène. La seconde map contient en clé le nom de l'ind et en valeurs un vector de pair (la pair comprend la date de prlvt ainsi q'un vecteur de string avec le nom des pathogènes portés par l'individus à cette même date). Pour cela, la fonction prend en argument un vector de string contenant le nom des pathogènes dont on veut déterminer les paramètres ----//
	std::map<std::string, std::vector<Individual*>> pathogensCarriersRes;
	std::map<Individual*, std::map<time_t, std::vector<std::string>>> allCarriersRes;
	for(auto const& [ind, mapDate]:allPrlvt){
		map<string,vector<vector<string>>> mDate = mapDate;
		auto itInd = find_if(allIndividuals.begin(), allIndividuals.end(), [ind](Individual* i)->bool {return i->getCalc_ident()==ind;});
		if(itInd!=allIndividuals.end()){
			for(auto const& [date, prlvtVector]:mDate){
				for(auto const& prlvt:prlvtVector){
					//on va remplir la premiere map
					time_t dateTM = stringToDateWithHour(date);
					string pathogenPrlvt = string("Bacteria")+"_"+prlvt[3]+"_"+prlvt[4]+"_"+prlvt[5]+"_"+prlvt[6]+"_"+prlvt[7]+"_"+prlvt[8];
					vector<string>::iterator itAllPath = find_if(listOfAllPath.begin(), listOfAllPath.end(),[pathogenPrlvt](string pathAllPath)->bool {return pathogenPrlvt==pathAllPath;});
					if(itAllPath!=listOfAllPath.end()){
						//le pathogen est dans la liste des pathogens recherchés
						auto itP = find(pathogensCarriersRes[pathogenPrlvt].begin(), pathogensCarriersRes[pathogenPrlvt].end(), (*itInd));
						if(itP==pathogensCarriersRes[pathogenPrlvt].end()){
							pathogensCarriersRes[pathogenPrlvt].push_back((*itInd));
						}
					}
					//on va remplir la seconde map
					allCarriersRes[(*itInd)][dateTM].push_back(pathogenPrlvt);			
					auto itF = find(allPrel[(*itInd)->getCalc_ident()].begin(), allPrel[(*itInd)->getCalc_ident()].end(), date);
					if(itF==allPrel[(*itInd)->getCalc_ident()].end()){
						allPrel[(*itInd)->getCalc_ident()].push_back(date);
					}
				}
			}
		}else{
			cout << " cet individu n'est pas dans la liste des individus attention! updateMap " << endl;
		}	
	}
	return(make_pair(pathogensCarriersRes, allCarriersRes));
}




string Learning::getOtherCtc(string first, shared_ptr<Contact> ctc){
	//---- fonction qui prend en argument un individus from et son contact sous la forme d'un shared_ptr et retourne son partenaire to ----//
	string other;
	if(ctc->getFrom()!=first){
		other = ctc->getFrom();
	}else if (ctc->getTo()!=first){
		other = ctc->getTo();
	}else{
		throw(" il un contact qui n'est pas correct là dedans ");
	}
	return other;
}


map<string, int> Learning::findOverlap(vector<string> indAcquisitions, map<string,vector<string>> mapColonized, time_t date1, time_t date2){
	map<string, int> res;
	for(auto const& elem:indAcquisitions){
		int c = 0;
		for(auto const& [key, val]:mapColonized){
			pair<double, vector<string>> pairCC = durationOfContactWithInd2(elem, val, date1, date2);
			if(pairCC.first!=0.0){
				c = c + 1;
			}
		}
		res[elem] = c;
	}
	return res;
}

map<string ,map <pair<string, string>, vector <double>>> Learning::computeProbabilityOfTransmission(bool probaByType){
	//---- Fonction qui retourne une map vec en clé le pathogène, et en valeurs une autre map. Cette dernière contenant en clé une pair de catégorie et un vecteur double contenant les probabilités à la semaine que les deux catégories se transmettent le pathogène lors d'un contact ----//
	
	map<string ,map <pair<string, string>, vector <double>>> res;
	map <pair<string, string>, vector <double>> mapPathogen;
	map<time_t, vector<Individual*>> allAcquisition;
	map<time_t, vector<Individual*>> indPerDay = computeIndPerDay();
	vector<string> colonizedInd;
	for(map<string, vector<Individual*>>::iterator itPC = pathogensCarriers.begin(); itPC != pathogensCarriers.end(); itPC++){
		map <pair<string, string>, vector <double>> mapPathogen;
		string currentPathogen = (*itPC).first;
		vector<Individual*> currentCarriersOfPathogen = (*itPC).second;
		allAcquisition = findAllAcquisition(currentCarriersOfPathogen, currentPathogen, nbOfAcquisitionWeek);
		for(auto const& [key,val]:allAcquisition){
			cout << dateToString(key) << " : ";
			for(auto const& v:val){
				cout << v->getCalc_ident() << " ";
			}
			cout<<endl;
		}
		int c = 0;
		time_t secondD = stringToDate(beginDate) + time_t(604800)*nbOfAcquisitionWeek;
		
		for(time_t i = secondD; i< stringToDate(endDate); i = i +time_t(604800)){
			time_t firstLimit = (i-(time_t(604800)*nbOfAcquisitionWeek));
			time_t lastLimit = i;
			cout << " date " << dateToString(i) << endl;
			//recup les ind qui ont acquis cette semaine
			vector<string> indThisWeekAcquisition = indPerWeek(i, (i+604800), allAcquisition);
			cout << " acquisition du " << dateToString(i) << " au " << dateToString(i+604800) << " donc " << indThisWeekAcquisition.size() <<endl;
			cout << " colonisé du " << dateToString(firstLimit) << " au " << dateToString(lastLimit-86400) << endl;
			if(indThisWeekAcquisition.size()!=0){
				//recup tout les ind présent la semaine d'avant
				vector<string> indLastWeek = indPerWeek(firstLimit, lastLimit, indPerDay);
				vector<string> indTWeek = indPerWeek(i, (i+604800), indPerDay);
				map<string, vector<string>> coloStatus = findColonizedStatusByDate(firstLimit, lastLimit-86400, indLastWeek, indTWeek, currentPathogen);
				colonizedInd = coloStatus["colo"];
				vector<string> nonColonizedInd;
				nonColonizedInd = coloStatus["noColo"];
				cout << " colonizedInd " << colonizedInd.size() << " nonColonizedInd " << nonColonizedInd.size() << endl;
				if(colonizedInd.size()!=0){
					//on cherche les catégories des ind qui ont acquis
					map<string,vector<string>> mapAc;
					map<string,vector<string>> mapColonized;
					if(probaByType){
						mapAc = catOfTheVectorST(indThisWeekAcquisition, true);
						mapColonized = catOfTheVectorST(colonizedInd, true);
					}else{
						mapAc = catOfTheVectorST(indThisWeekAcquisition, false);
						mapColonized = catOfTheVectorST(colonizedInd, false);
					}
					cout << " mapAc " << mapAc.size() << " et mapColonized " << mapColonized.size() << endl;
					for(auto const& [cat, acquisition]:mapAc){
						double nbOfAc = acquisition.size();
						//on va chercher les ind non-colo qui sont de la même cat que le cas d'acquisition
						vector<Individual*> sameCatObject;
						if(probaByType){
							sameCatObject = mapType[cat];
						}else{
							sameCatObject = mapGroup[cat];
						}
						vector<string> sameCat = returnNameInString(sameCatObject);						
						sort(nonColonizedInd.begin(), nonColonizedInd.end());
						sort(sameCat.begin(), sameCat.end());
						vector<string> nonColonizedIndCatInterm;
						set_intersection(nonColonizedInd.begin(), nonColonizedInd.end(),sameCat.begin(), sameCat.end(), back_inserter(nonColonizedIndCatInterm));
						//tous les individus non colonisé de la même catégorie que les acqusitions = tous les susceptibles durant les deux semaines précédentes
						vector<string> nonColonizedIndCat = nonColonizedIndCatInterm;
						string paString;
						if(probaByType){
							paString = "PA";
						}else{
							paString = "Patient";
						}
						for(auto const& [catCol, allColonized]:mapColonized){
							pair<string, string> firstPair = make_pair(catCol, cat);
							double durationCat = 0;
							vector<string> acqInCtc;
							for(auto const& colonized:allColonized){
								pair<double, vector<string>> pairCC = durationOfContactWithInd2(colonized, nonColonizedIndCat, firstLimit, lastLimit);
								durationCat = durationCat + pairCC.first;
								acqInCtc.insert(acqInCtc.end(), pairCC.second.begin(), pairCC.second.end());
							}
							//ici on récupère les individus qui ont acquis et qui ont eu des contacts avec des colonisés durant les deux semaines précédentes
							sort( acqInCtc.begin(), acqInCtc.end() );
							acqInCtc.erase( unique( acqInCtc.begin(), acqInCtc.end() ), acqInCtc.end() );
							
							vector<string> acqReal = acquisition;
							sort( acqReal.begin(), acqReal.end() );
							vector<string> acqCommon;
							
							set_intersection(acqReal.begin(), acqReal.end(),acqInCtc.begin(), acqInCtc.end(), back_inserter(acqCommon));
							
							// map<string, int> acquisitionsOverlap = findOverlap(acqCommon, mapColonized, firstLimit, lastLimit);
							// double nbOfAc = 0.0;
							// for(auto const& [acI, vInt]:acquisitionsOverlap){
								// cout << " nbOfAc " << nbOfAc << " acI " << acI << " vInt " << vInt << endl;
								// nbOfAc = nbOfAc + (1.0/vInt);
							// }
							double nbOfAc = acqCommon.size();
							cout << " final nbOfAc " << nbOfAc << endl;
							if(durationCat==0)cout << " durationCAt = 0 " << endl;
							if(durationCat!=0 && (durationCat/30)>=nbCtcInfMin && nbOfAc!=0){
								string couple = catCol + "-" + cat;
								double res;
								if((withLogPAPA && couple=="PA-PA") || (withLogPAPE && couple=="PA-PE") || (withLogPEPA && couple=="PE-PA") || (withLogPEPE && couple=="PE-PE")){
									res = nbOfAc/(1+log10(durationCat/30));
								}else{
									res = nbOfAc/(durationCat/30);
									cout << dateToString(i) << " ----> " << nbOfAc << " / " << durationCat << " : " << couple << endl;
								}
								mapPathogen[firstPair].push_back(res);
							}
						}
					}
				}
			}
		}
		if(!mapPathogen.empty()){
			res.insert(make_pair(currentPathogen, mapPathogen));
		}
	}
	return res;
}

map<string ,map <pair<string, string>, map<double, vector <double>>>> Learning::computeProbabilityOfTransmission2(bool probaByType){
	//---- Fonction qui retourne une map vec en clé le pathogène, et en valeurs une autre map. Cette dernière contenant en clé une pair de catégorie et un vecteur double contenant les probabilités à la semaine que les deux catégories se transmettent le pathogène lors d'un contact ----//
	
	map<string ,map <pair<string, string>, map<double, vector <double>>>> res;
	// map <pair<string, string>, map<double, vector <double>>> mapPathogen;
	map<time_t, vector<Individual*>> allAcquisition;
	map<time_t, vector<Individual*>> indPerDay = computeIndPerDay();
	vector<string> colonizedInd;
	for(map<string, vector<Individual*>>::iterator itPC = pathogensCarriers.begin(); itPC != pathogensCarriers.end(); itPC++){
		map <pair<string, string>, map<double, vector <double>>> mapPathogen;
		string currentPathogen = (*itPC).first;
		vector<Individual*> currentCarriersOfPathogen = (*itPC).second;
		allAcquisition = findAllAcquisition(currentCarriersOfPathogen, currentPathogen, nbOfAcquisitionWeek);
		for(auto const& [key,val]:allAcquisition){
			cout << dateToString(key) << " : ";
			for(auto const& v:val){
				cout << v->getCalc_ident() << " ";
			}
			cout<<endl;
		}
		int c = 0;
		time_t secondD = stringToDate(beginDate) + time_t(604800)*nbOfAcquisitionWeek;
		
		for(time_t i = secondD; i< stringToDate(endDate); i = i +time_t(604800)){
			time_t firstLimit = (i-(time_t(604800)*nbOfAcquisitionWeek));
			time_t lastLimit = i;
			cout << " date " << dateToString(i) << endl;
			//recup les ind qui ont acquis cette semaine
			vector<string> indThisWeekAcquisition = indPerWeek(i, (i+604800), allAcquisition);
			cout << " acquisition du " << dateToString(i) << " au " << dateToString(i+604800) << " donc " << indThisWeekAcquisition.size() <<endl;
			cout << " colonisé du " << dateToString(firstLimit) << " au " << dateToString(lastLimit-86400) << endl;
			if(indThisWeekAcquisition.size()!=0){
				//recup tout les ind présent la semaine d'avant
				vector<string> indLastWeek = indPerWeek(firstLimit, lastLimit, indPerDay);
				vector<string> indTWeek = indPerWeek(i, (i+604800), indPerDay);
				map<string, vector<string>> coloStatus = findColonizedStatusByDate(firstLimit, lastLimit-86400, indLastWeek, indTWeek, currentPathogen);
				colonizedInd = coloStatus["colo"];
				vector<string> nonColonizedInd;
				nonColonizedInd = coloStatus["noColo"];
				vector<string> nonColonizedIndPlus;
				nonColonizedIndPlus = coloStatus["noColoPlus"];
				vector<string> nonPrlvtInd;
				nonPrlvtInd = coloStatus["non prlvt"];
				cout << " colonizedInd " << colonizedInd.size() << " nonColonizedInd " << nonColonizedInd.size() << " nonPrlvtInd " << nonPrlvtInd.size() << endl;
				if(colonizedInd.size()!=0){
					//on cherche les catégories des ind qui ont acquis
					map<string,vector<string>> mapAc;
					map<string,vector<string>> mapColonized;
					map<string,vector<string>> mapNonPrlvt;
					if(probaByType){
						mapAc = catOfTheVectorST(indThisWeekAcquisition, true);
						mapColonized = catOfTheVectorST(colonizedInd, true);
						mapNonPrlvt = catOfTheVectorST(nonPrlvtInd, true);
					}else{
						mapAc = catOfTheVectorST(indThisWeekAcquisition, false);
						mapColonized = catOfTheVectorST(colonizedInd, false);
						mapNonPrlvt = catOfTheVectorST(nonPrlvtInd, false);
					}
					cout << " mapAc " << mapAc.size() << " et mapColonized " << mapColonized.size() << endl;
					for(auto const& [cat, acquisition]:mapAc){
						double nbOfAc = acquisition.size();
						//on va chercher les ind non-colo qui sont de la même cat que le cas d'acquisition
						vector<Individual*> sameCatObject;
						if(probaByType){
							sameCatObject = mapType[cat];
						}else{
							sameCatObject = mapGroup[cat];
						}
						vector<string> sameCat = returnNameInString(sameCatObject);						
						sort(nonColonizedInd.begin(), nonColonizedInd.end());
						sort(sameCat.begin(), sameCat.end());
						vector<string> nonColonizedIndCatInterm;
						set_intersection(nonColonizedInd.begin(), nonColonizedInd.end(),sameCat.begin(), sameCat.end(), back_inserter(nonColonizedIndCatInterm));
						//tous les individus non colonisé de la même catégorie que les acqusitions = tous les susceptibles durant les deux semaines précédentes
						vector<string> nonColonizedIndCat = nonColonizedIndCatInterm;
						
						sort(nonColonizedIndPlus.begin(), nonColonizedIndPlus.end());
						vector<string> nonColonizedIndCatIntermPlus;
						set_intersection(nonColonizedIndPlus.begin(), nonColonizedIndPlus.end(),sameCat.begin(), sameCat.end(), back_inserter(nonColonizedIndCatIntermPlus));
						vector<string> nonColonizedIndCatPlus = nonColonizedIndCatIntermPlus;
						
						vector<string> nonColonizedIndCatBoth = nonColonizedIndCat;
						nonColonizedIndCatBoth.insert(nonColonizedIndCatBoth.end(), nonColonizedIndCatPlus.begin(), nonColonizedIndCatPlus.end());
						
						string paString;
						if(probaByType){
							paString = "PA";
						}else{
							paString = "Patient";
						}
						for(auto const& [catCol, allColonized]:mapColonized){
							pair<string, string> firstPair = make_pair(catCol, cat);
							double durationCat = 0.0;
							vector<string> acqInCtc;
							for(auto const& colonized:allColonized){
								pair<double, vector<string>> pairCC = durationOfContactWithInd2(colonized, nonColonizedIndCat, firstLimit, lastLimit);
								// pair<double, vector<string>> pairCC = durationOfContactWithInd2(colonized, nonColonizedIndCatBoth, firstLimit, lastLimit);
								durationCat = durationCat + pairCC.first;
								acqInCtc.insert(acqInCtc.end(), pairCC.second.begin(), pairCC.second.end());
							}
							//ici on récupère les individus qui ont acquis et qui ont eu des contacts avec des colonisés durant les deux semaines précédentes
							sort( acqInCtc.begin(), acqInCtc.end() );
							acqInCtc.erase( unique( acqInCtc.begin(), acqInCtc.end() ), acqInCtc.end() );
							
							vector<string> acqReal = acquisition;
							sort( acqReal.begin(), acqReal.end() );
							vector<string> acqCommon;
							
							set_intersection(acqReal.begin(), acqReal.end(),acqInCtc.begin(), acqInCtc.end(), back_inserter(acqCommon));
							
							vector<string> nonprlvt = mapNonPrlvt[catCol];
							vector<string> acqInCtcNP;
							double durationCatNP = 0.0;
							vector<string> allColonizedBoth = allColonized;
							allColonizedBoth.insert(allColonizedBoth.end(), nonprlvt.begin(), nonprlvt.end());
							for(auto const& np:allColonizedBoth){
								pair<double, vector<string>> pairCCNP = durationOfContactWithInd2(np, nonColonizedIndCat, firstLimit, lastLimit);
								durationCatNP = durationCatNP + pairCCNP.first;
								acqInCtcNP.insert(acqInCtcNP.end(), pairCCNP.second.begin(), pairCCNP.second.end());
							}
							sort( acqInCtcNP.begin(), acqInCtcNP.end() );
							acqInCtcNP.erase( unique( acqInCtcNP.begin(), acqInCtcNP.end() ), acqInCtcNP.end() );
							vector<string> acqCommonNP;
							
							set_intersection(acqReal.begin(), acqReal.end(),acqInCtcNP.begin(), acqInCtcNP.end(), back_inserter(acqCommonNP));
							vector<string> acqCommon2;
							if(!acqCommonNP.empty()){
								set_union(acqCommon.begin(), acqCommon.end(),acqCommonNP.begin(), acqCommonNP.end(), back_inserter(acqCommon2));
							}
							
							double nbOfAc = acqCommon.size();
							double nbOfAcNP = acqCommon2.size();
							
							// cout << " final nbOfAc " << nbOfAc << endl;
							if(durationCat==0)cout << " durationCAt = 0 " << endl;
							if(durationCat!=0 && (durationCat/30)>=nbCtcInfMin){
								string couple = catCol + "-" + cat;
								// for(auto const& [k,v]:nbOfAc){
									// if(v!=0.0){
										double resInf;
										double resSup;
										if((withLogPAPA && couple=="PA-PA") || (withLogPAPE && couple=="PA-PE") || (withLogPEPA && couple=="PE-PA") || (withLogPEPE && couple=="PE-PE")){
											// resInf = nbOfAcNP/(1+log10((durationCatNP + durationCat)/30));
											resInf = nbOfAcNP/(1+log10((durationCatNP )/30));
											resSup = nbOfAc/(1+log10(durationCat/30));
										}else{
											// resInf = nbOfAcNP/((durationCatNP + durationCat)/30);
											resInf = nbOfAcNP/((durationCatNP )/30);
											resSup = nbOfAc/(durationCat/30);
											
											cout << dateToString(i) << " ----> " << nbOfAc << " / " << durationCat << " = " << resSup << " : " << couple << endl;
											cout << dateToString(i) << " ----> " << nbOfAcNP << " / " << durationCatNP << " = " << resInf << " : " << couple << endl;
										}
										// mapPathogen[firstPair][0.0].push_back(resInf);
										mapPathogen[firstPair][1.0].push_back(resSup);
									// }else{
										// mapPathogen[firstPair][k].push_back(0.0);
									// }
									
								// }
								
							}
						}
					}
				}
			}
		}
		if(!mapPathogen.empty()){
			res.insert(make_pair(currentPathogen, mapPathogen));
		}
	}
	return res;
}

map<string ,map <pair<string, string>, map<double, vector <double>>>> Learning::computeProbabilityOfTransmission3(bool probaByType){
	//---- Fonction qui retourne une map vec en clé le pathogène, et en valeurs une autre map. Cette dernière contenant en clé une pair de catégorie et un vecteur double contenant les probabilités à la semaine que les deux catégories se transmettent le pathogène lors d'un contact ----//
	
	map<string ,map <pair<string, string>, map<double, vector <double>>>> res;
	// map<string ,map <pair<string, string>, vector <double>>> res;
	map<time_t, vector<Individual*>> allAcquisition;
	map<time_t, vector<Individual*>> indPerDay = computeIndPerDay();
	vector<string> colonizedInd;
	for(map<string, vector<Individual*>>::iterator itPC = pathogensCarriers.begin(); itPC != pathogensCarriers.end(); itPC++){
		map <pair<string, string>, map<double, vector <double>>> mapPathogen;
		// map <pair<string, string>, vector <double>> mapPathogen;
		string currentPathogen = (*itPC).first;
		vector<Individual*> currentCarriersOfPathogen = (*itPC).second;
		allAcquisition = findAllAcquisition(currentCarriersOfPathogen, currentPathogen, nbOfAcquisitionWeek);
		for(auto const& [key,val]:allAcquisition){
			cout << dateToString(key) << " : ";
			for(auto const& v:val){
				cout << v->getCalc_ident() << " ";
			}
			cout<<endl;
		}
		int c = 0;
		time_t secondD = stringToDate(beginDate) + time_t(604800)*nbOfAcquisitionWeek;
		
		for(time_t i = secondD; i< stringToDate(endDate); i = i +time_t(604800)){
			time_t firstLimit = (i-(time_t(604800)*nbOfAcquisitionWeek));
			time_t lastLimit = i;
			cout << " date " << dateToString(i) << endl;
			//recup les ind qui ont acquis cette semaine
			vector<string> indThisWeekAcquisition = indPerWeek(i, (i+604800), allAcquisition);
			cout << " acquisition du " << dateToString(i) << " au " << dateToString(i+604800) << " donc " << indThisWeekAcquisition.size() <<endl;
			cout << " colonisé du " << dateToString(firstLimit) << " au " << dateToString(lastLimit-86400) << endl;
			int Nacq = 0;
			if(indThisWeekAcquisition.size()!=0){
				map<string,vector<string>> mapAc;
				if(probaByType){
					mapAc = catOfTheVectorST(indThisWeekAcquisition, true);
				}else{
					mapAc = catOfTheVectorST(indThisWeekAcquisition, false);
				}
				//recup tout les ind présent la semaine d'avant
				map<pair<string, string>, map<double, double>> mapSumAll;
				for(auto const& [catIndACQ, individualsACQ]:mapAc){
					int Nacq = 0;
					map<pair<string, string>, map<double, double>> mapSum;
					for(auto const& indACQ:individualsACQ){
						Nacq = Nacq + 1;
						//on va chercher les individus avec qui il a eu des contacts 
						map<string, map<string, double>> mapInCtcStat = findIndInContactByDate(firstLimit, lastLimit-86400, indACQ, currentPathogen, probaByType);
						for(auto const& [cat, mapInCtcStatMini]:mapInCtcStat){
							map<string, double> mmap = mapInCtcStatMini;
							pair<string, string> couple = make_pair(cat, catIndACQ);
							double value2;
							double value1;
							double denom1 = (mmap["colo"] + mmap["non prlvt"]);
							double denom2 = mmap["colo"];
							
							if(denom1!=0.0){
								value1 = 1 / ((mmap["colo"] + mmap["non prlvt"])/30);
							}else{
								value1 = 0.0;
							}
							if(denom2!=0.0){
								value2 = 1 / (mmap["colo"]/30);
							}else{
								value2 = 0.0;
							}

							auto itF = mapSum.find(couple);
							if(itF!=mapSum.end()){
								mapSum[couple][0.0] = mapSum[couple][0.0] + value1;
								mapSum[couple][1.0] = mapSum[couple][1.0] + value2;
							}else{
								mapSum[couple][0.0] = value1;
								mapSum[couple][1.0] = value2;
							}
						}
					}
					for(auto const& [couple, mapsumValue]:mapSum){
						for(auto const& [borne, val]:mapsumValue){
							cout << couple.first << "_" << couple.second << " borne: " << borne << " --> " << val << " / " << individualsACQ.size() << endl;
							mapSumAll[couple][borne] = val / individualsACQ.size();
						}
						
					}
				}
				for(auto const& [couple, values]:mapSumAll){
					for(auto const& [bornes, valAll]:values){
						mapPathogen[couple][bornes].push_back(valAll);
					}
				}
			}
		}
		if(!mapPathogen.empty()){
			res.insert(make_pair(currentPathogen, mapPathogen));
		}
	}
	return res;
}

double Learning::durationOfContactWithInd(string colonized, vector<string> nonColonizedIndCat, time_t date1, time_t date2){
	//---- Fonction qui prend en attributs un string colonized correspondant à l'individus colonisé, un vecteur de string avc les individus non colonisé, deux dates sous forme de time_t. Renvoie un double correspondant à la durée moyenne de contact entre l'individus colonisé et son vecteur d'individus non colonisé entre les deux dates ----//
	double nbOfCtcCat = 0;
	pair<multimap<string,shared_ptr<Contact>>::iterator, multimap<string,shared_ptr<Contact>>::iterator> itPM;
	multimap<string,shared_ptr<Contact>>::iterator itN;
	itPM = previousCtc.equal_range(colonized);
	vector<shared_ptr<Contact>> vectCtcSecond;
	vector<shared_ptr<Contact>> vectCtc;
	itN = itPM.first;
	while(itN != itPM.second && stringToDate(itN->second->getDay())<date2){
		if(stringToDate(itN->second->getDay())>=date1){
			vectCtc.push_back(itN->second);
			string other = getOtherCtc(colonized, itN->second);
			vector<string>::iterator itIOC = find(nonColonizedIndCat.begin(), nonColonizedIndCat.end(), other);
			if(itIOC!=nonColonizedIndCat.end()){
				nbOfCtcCat = nbOfCtcCat + stoi(itN->second->getLength());
			}
		}
		itN++;
	}
	return nbOfCtcCat;
}

pair<double, vector<string>> Learning::durationOfContactWithInd2(string colonized, vector<string> nonColonizedIndCat, time_t date1, time_t date2){
	//---- Fonction qui prend en argument un individu colonized et un vecteur d'individus nonColonizedIndCat et détermine la durée de contact de contact entre l'individus colonized et tous les autres individus de nonColonizedIndCat. Retourne une map avec en clé la durée de contact et en valeur un vecteur avec les individus qui ont été en contact avec l'individu colonised entre date1 et date2 ----//
	double nbOfCtcCat = 0.0;
	double defnbOfCtcCat = 0.0;
	vector<string> resInd;
	pair<double, vector<string>> result;
	vector<shared_ptr<Contact>> contacts = allCtc[colonized];
	for(auto const& ctc:contacts){
		time_t date = stringToDate(ctc->getDay());
		if(date<date2 && date>=date1){
			string other = getOtherCtc(colonized, ctc);
			vector<string>::iterator itIOC = find_if(nonColonizedIndCat.begin(), nonColonizedIndCat.end(), [other](string i)->bool {return i==other;});
			if(itIOC!=nonColonizedIndCat.end()){
				int ctc_length = stoi(ctc->getLength());
				defnbOfCtcCat = defnbOfCtcCat + ctc_length;
				if(ctc_length > 3600){
					//cout << "Saturated contact, previous dur = " << ctc_length << endl;
					ctc_length = 3600;
				}
				nbOfCtcCat = nbOfCtcCat + ctc_length;
				resInd.push_back(other);
			}
		}
	}
	//cout << "Total duration w/o saturation: " << defnbOfCtcCat << " vs " << nbOfCtcCat << "; rel = " << nbOfCtcCat/defnbOfCtcCat << endl;
	result = make_pair(nbOfCtcCat, resInd);
	return result;
}



map<string, vector<Individual*>> Learning::catOfTheVector(vector<Individual*> indThisWeekAcquisition, bool type){
	//---- Fonction qui prend en attribut un vecteur de string contenant les individus qui ont acquis à une certaine semaine Ainsi qu'un booléen pour savoir si les individus sont pris par type ou par catégorie. Renvoie une map avec en clé la même clé que mapCat et en valeurs les individus présent dans indThisWeekAcquisition. Renvoie une sousMap de la mapCat pour le vecteur indThisWeekAcquisition ----//
	map<string,vector<Individual*>> mapCatAc;
	for(auto const& ind:indThisWeekAcquisition){
		if(type){
			mapCatAc[ind->getStatus()].push_back(ind);
		}else{
			mapCatAc[findCat(ind)].push_back(ind);
		}
	}	
	return mapCatAc;
}

map<string, vector<string>> Learning::catOfTheVectorST(vector<string> indThisWeekAcquisition, bool type){
	//---- Fonction qui prend en attribut un vecteur de string contenant les individus qui ont acquis à une certaine semaine ainsi qu'un booléen pour savoir si la catégorie est par type ou par groupe. Renvoie une map avec en clé la même clé que mapCat et en valeurs les individus présent dans indThisWeekAcquisition. Renvoie une sousMap de la mapCat pour le vecteur indThisWeekAcquisition ----//
	map<string,vector<string>> mapCatAc;
	for(auto const& i:indThisWeekAcquisition){
		auto it = find_if(allIndividuals.begin(), allIndividuals.end(), [i](Individual* indObject)->bool {return indObject->getCalc_ident()==i;});
		if(type){
			mapCatAc[(*it)->getStatus()].push_back(i);
		}else{
			string cc = findGroup((*it));
			mapCatAc[cc].push_back(i);
		}
	}
	return mapCatAc;
}

string Learning::catOfTheIndST(string ind, bool type){
	//---- Fonction qui prend en attribut un vecteur de string contenant les individus qui ont acquis à une certaine semaine ainsi qu'un booléen pour savoir si la catégorie est par type ou par groupe. Renvoie une map avec en clé la même clé que mapCat et en valeurs les individus présent dans indThisWeekAcquisition. Renvoie une sousMap de la mapCat pour le vecteur indThisWeekAcquisition ----//
	string CatAc;
	auto it = find_if(allIndividuals.begin(), allIndividuals.end(), [ind](Individual* indObject)->bool {return indObject->getCalc_ident()==ind;});
	if(type){
		CatAc = (*it)->getStatus();
	}else{
		CatAc = findGroup((*it));
	}
	return CatAc;
}

vector<string> Learning::returnNameInString(vector<Individual*> input_vector){
	//---- Fonction qui retourne un vecteur de string calc_ident à partir d'un vecteur d'individus Individual ----//
	vector<string> res;
	for(auto const& ind:input_vector){
		res.push_back(ind->getCalc_ident());
	}
	return res;
}

vector<Individual*> Learning::returnObjectWithName(vector<string> input_vector){
	//---- Fonction qui retourne un vecteur d'individus Individual à partir d'un vecteur de calc_ident ----//
	vector<Individual*> res;
	for(auto const& ind:input_vector){
		auto it = find_if(allIndividuals.begin(), allIndividuals.end(), [ind](Individual* i)->bool {return i->getCalc_ident()==ind;});
		if(it!=allIndividuals.end()){
			res.push_back((*it));
		}
	}
	return res;
}

vector<string> Learning::indPerWeek(time_t firstLimit, time_t lastLimit, map<time_t, vector<Individual*>> indPerDay){
	//---- Fonction qui prend en argument deux dates et une map avec en clé une date et en valeurs un vecteur de string d'individus. Renvoie un vecteur de string d'individus présent entre les deux dates (sans garder la date limit dans le décompte) ----//
	vector<string> indThisWeek;
	for(time_t itDOTW = firstLimit; itDOTW < lastLimit; itDOTW = itDOTW+86400){
		vector<Individual*> indThisDayObject = indPerDay[itDOTW];
		vector <string> indThisDay = returnNameInString(indThisDayObject);
		sort(indThisDay.begin(), indThisDay.end());
		sort(indThisWeek.begin(), indThisWeek.end());
		vector<string> common;
		set_union(indThisDay.begin(), indThisDay.end(), indThisWeek.begin(), indThisWeek.end(), back_inserter(common));
		indThisWeek = common;
	}
	return indThisWeek;
}



map<string, int> Learning::findPositiveSwab(map<time_t, vector<string>> prlvtVector, string pathogen){
	//---- Fonction qui prend en argument une map avec les résultats des prélèvements par jour. La fonction retourne une map avec en clé les jours et en valeur 1 si le pathogen a été trouvé ce jour là et 0 sinon ----//
	map<string, int> res;
	for(auto const& [d, prlvt]:prlvtVector){
		string date = dateToString(d).substr(0,10);
		vector<string> resPrlvt = prlvt;
		auto it = find(resPrlvt.begin(), resPrlvt.end(), pathogen);
		if(it!=resPrlvt.end()){
			res[date] = 1;
		}else{
			res[date] = 0;
		}
	}
	return(res);
}

bool Learning::indPrelThisWeek(string ind, time_t date1, time_t date2){
	vector<string> datesOfTheInd = allPrel[ind];
	vector<string> allDatesToFind = buildMevectorOfDates(date1, date2); 
	sort(datesOfTheInd.begin(), datesOfTheInd.end());
	vector<string> commonDates;
	set_intersection(allDatesToFind.begin(), allDatesToFind.end(),datesOfTheInd.begin(), datesOfTheInd.end(), back_inserter(commonDates));
	if(commonDates.empty()){
		return false;
	}else{
		return true;
	}
}

bool Learning::checkPreviousColonization(string ind, time_t date1, string pathogen){
	auto itCol = find_if(pathogensCarriers[pathogen].begin(),pathogensCarriers[pathogen].end(), [ind](Individual* i)->bool {return i->getCalc_ident()==ind;});
	if(itCol!=pathogensCarriers[pathogen].end()){
		//il a porté on va vérif s'il a porté avant date1
		auto itCarrier = find_if(allCarriers.begin(),allCarriers.end(), [ind](pair<Individual*, map<time_t, vector<string>>> i)->bool {return i.first->getCalc_ident()==ind;});
		map<time_t, vector<string>> prlvt = itCarrier->second;
		bool explore = false;
		// for(auto const& [date_time, prlvt_list]:prlvt){
			// vector<string> pathogens = prlvt_list;
			// if(date_time<date1){
				// ancien prlvt on verif si le patho est dedans
				// auto it = find(pathogens.begin(), pathogens.end(), pathogen);
				// if(it!=pathogens.end()){
					// explore = true;
				// }
			// }
		// }
		auto it = prlvt.rbegin();
		while(it!=prlvt.rend() && !explore){
			if((*it).first>=date1){
				++it;
			}else{
				auto itFind = find((*it).second.begin(), (*it).second.end(), pathogen);
				if(itFind!=(*it).second.end()){
					explore = true;
				}else{
					++it;
				}
			}
		}
		// cout << " hey l'ind " << ind << " explore " << explore << endl;
		return explore;
	}else{
		//jamais porté
		// cout << " hey l'ind " << ind << " jamais porté  " << endl;
		return false;
	}
}

map<string, vector<string>> Learning::findColonizedStatusByDate(time_t date1, time_t date2, vector<string> indWeek, vector<string> indThisWeek, string pathogen){
	//---- Fonction une map avec en clé le statut "colo" et "noncolo" et en valeur un vecteur de calc_ident d'individus colonisé ou non colonisé entre date1 et date2 ----//
	map<string, vector<string>> res;	
	vector<string> allDatesToFind = buildMevectorOfDates(date1, date2); 
	sort(allDatesToFind.begin(), allDatesToFind.end());
	for(auto const& ind:indWeek){
		vector<string> datesOfTheInd = allPrel[ind];
		sort(datesOfTheInd.begin(), datesOfTheInd.end());
		vector<string> commonDates;
		set_intersection(allDatesToFind.begin(), allDatesToFind.end(),datesOfTheInd.begin(), datesOfTheInd.end(), back_inserter(commonDates));
		if(!commonDates.empty()){
			//si il y a des dates de prélèvements pour cet individus on va vérifier s'il a été colo ou non			
			auto itCol = find_if(pathogensCarriers[pathogen].begin(),pathogensCarriers[pathogen].end(), [ind](Individual* i)->bool {return i->getCalc_ident()==ind;});
			if(itCol!=pathogensCarriers[pathogen].end()){
				//il a porté le pathogen on verifie qu'il est colo pendant la période
				auto itCarrier = find_if(allCarriers.begin(),allCarriers.end(), [ind](pair<Individual*, map<time_t, vector<string>>> i)->bool {return i.first->getCalc_ident()==ind;});
				map<time_t, vector<string>> prlvt = itCarrier->second;
				map<string, int> statusPathogen = findPositiveSwab(prlvt, pathogen);
				bool findBool = false;
				auto itFind = allDatesToFind.begin();
				while(itFind!=allDatesToFind.end() && !findBool){
					string d = (*itFind);
					if(statusPathogen[d]==1){
						findBool = true;
					}
					++itFind;
				}
				if(findBool){
					res["colo"].push_back(ind);
				}else{
					//non colo donc on va vérif qu'il est toujours présent la semaine d'après
					// vector<string>::iterator itV = find(indThisWeek.begin(), indThisWeek.end(), ind);
					// if(itV!=indThisWeek.end()){
					bool here = indPrelThisWeek(ind, date2+86400, date2+86400+604800);
					bool checkPreviousC = checkPreviousColonization(ind, date1, pathogen);
					if((here && firstAcqOnly && !checkPreviousC) || (here && !firstAcqOnly)){
						res["noColo"].push_back(ind);
					}
				}
			}else{
				//il a des prlvt et jamais eu le patho donc il va dans les non colonisés
				// res["noColo"].push_back(ind);
				// vector<string>::iterator itV = find(indThisWeek.begin(), indThisWeek.end(), ind);
				// if(itV!=indThisWeek.end()){
				bool here = indPrelThisWeek(ind, date2+86400, date2+86400+604800);
				bool checkPreviousC = checkPreviousColonization(ind, date1, pathogen);
				if((here && firstAcqOnly && !checkPreviousC) || (here && !firstAcqOnly)){
					res["noColo"].push_back(ind);
				}
			}
		}else{
			// res["non prlvt"].push_back(ind);
			
			// bool here = indPrelThisWeek(ind, date2+86400, date2+86400+604800);
			// bool checkPreviousC = checkPreviousColonization(ind, date1, pathogen);
			// if((here && firstAcqOnly && !checkPreviousC) || (here && !firstAcqOnly)){
				// res["noColoPlus"].push_back(ind);
			// }
			
			string checkCol = checkLastPrlvt(ind, date1, pathogen, 4);
			if(checkCol=="non colo"){
				bool here = indPrelThisWeek(ind, date2+86400, date2+86400+604800);
				bool checkPreviousC = checkPreviousColonization(ind, date1, pathogen);
				if((here && firstAcqOnly && !checkPreviousC) || (here && !firstAcqOnly)){
					res["noColo"].push_back(ind);
				}
			}else if(checkCol=="colo"){
			
				res["colo"].push_back(ind);
			}else if(checkCol=="non prlvt"){
				res["non prlvt"].push_back(ind);
				// bool here = indPrelThisWeek(ind, date2+86400, date2+86400+604800);
				// bool checkPreviousC = checkPreviousColonization(ind, date1, pathogen);
				// if((here && firstAcqOnly && !checkPreviousC) || (here && !firstAcqOnly)){
					// res["noColoPlus"].push_back(ind);
				// }
			}
		}
	}
	return(res);
}

string Learning::checkLastPrlvt(string ind, time_t dateSeuil, string pathogen, int week){
	string res = "";
	time_t dateLimit = dateSeuil - (604800*week);
	auto itCarrier = find_if(allCarriers.begin(),allCarriers.end(), [ind](pair<Individual*, map<time_t, vector<string>>> i)->bool {return i.first->getCalc_ident()==ind;});
	if(itCarrier!=allCarriers.end()){
		map<time_t, vector<string>> prlvt = itCarrier->second;
		vector<time_t> allDatesPrlvt;
		
		for(auto const& [dateT, val]:prlvt){
			allDatesPrlvt.push_back(dateT);
		}
		sort(allDatesPrlvt.begin(), allDatesPrlvt.end());
		auto itF = allDatesPrlvt.rbegin();
		bool findBool = false;
		if(((*itF)<=dateSeuil && week==0) || ((*itF)<=dateSeuil && (*itF)>=dateLimit)){
			findBool = true;
		}else{
			while((*itF)>dateSeuil && itF!=allDatesPrlvt.rend()){
				++itF;
			}
			if((itF!=allDatesPrlvt.rend() && week==0) || (itF!=allDatesPrlvt.rend() && (*itF)>=dateLimit)){
				findBool = true;
			}else{
				res = "non prlvt";
			}
		}
		if(findBool){
			vector<string> resPrlvt = prlvt[(*itF)];
			auto it = find(resPrlvt.begin(), resPrlvt.end(), pathogen);
			if(it!=resPrlvt.end()){
				res = "colo";
			}else{
				res = "non colo";
			}
		}
	}else{
		// cout << "hey! je ne trouve pas " << ind << " dans allCarriers " << endl;
		res = "non prlvt";
	}
	return res;
}

string Learning::colonizationStatusByDatesInd(time_t date1, time_t date2, string ind, string pathogen){
	string res;
	vector<string> allDatesToFind = buildMevectorOfDates(date1, date2); 
	sort(allDatesToFind.begin(), allDatesToFind.end());
	vector<string> datesOfTheInd = allPrel[ind];
	sort(datesOfTheInd.begin(), datesOfTheInd.end());
	vector<string> commonDates;
	set_intersection(allDatesToFind.begin(), allDatesToFind.end(),datesOfTheInd.begin(), datesOfTheInd.end(), back_inserter(commonDates));
	if(!commonDates.empty()){
		//si il y a des dates de prélèvements pour cet individus on va vérifier s'il a été colo ou non			
		auto itCol = find_if(pathogensCarriers[pathogen].begin(),pathogensCarriers[pathogen].end(), [ind](Individual* i)->bool {return i->getCalc_ident()==ind;});
		if(itCol!=pathogensCarriers[pathogen].end()){
			//il a porté le pathogen on verifie qu'il est colo pendant la période
			auto itCarrier = find_if(allCarriers.begin(),allCarriers.end(), [ind](pair<Individual*, map<time_t, vector<string>>> i)->bool {return i.first->getCalc_ident()==ind;});
			map<time_t, vector<string>> prlvt = itCarrier->second;
			map<string, int> statusPathogen = findPositiveSwab(prlvt, pathogen);
			bool findBool = false;
			auto itFind = allDatesToFind.begin();
			while(itFind!=allDatesToFind.end() && !findBool){
				string d = (*itFind);
				if(statusPathogen[d]==1){
					findBool = true;
				}
				itFind ++;
			}
			if(findBool){
				res = "colo";
			}else{
				//non colo donc on va vérif qu'il est toujours présent la semaine d'après
				bool here = indPrelThisWeek(ind, date2+86400, date2+86400+604800);
				bool checkPreviousC = checkPreviousColonization(ind, date1, pathogen);
				if((here && firstAcqOnly && !checkPreviousC) || (here && !firstAcqOnly)){
					res = "non colo";
				}
			}
		}else{
			//il a des prlvt et jamais eu le patho donc il va dans les non colonisés
			bool here = indPrelThisWeek(ind, date2+86400, date2+86400+604800);
			bool checkPreviousC = checkPreviousColonization(ind, date1, pathogen);
			if((here && firstAcqOnly && !checkPreviousC) || (here && !firstAcqOnly)){
				res = "non colo";
			}
		}
	}else{
		res = "non prlvt";
		// string checkRes = checkLastPrlvt(ind, date1, pathogen, 0);
		// if(checkRes!="")res = checkRes;
	}
	return res;
}


map<string, map<string, double>> Learning::findIndInContactByDate(time_t date1, time_t date2, string ind, string pathogen, bool probaByType){
	//---- Fonction une map avec en clé le statut "colo" et "noncolo" et en valeur un vecteur de calc_ident d'individus colonisé ou non colonisé entre date1 et date2 ----//
	map<string, map<string, double>> res;	
	vector<string> typeT;
	if(probaByType){
		typeT = {"PA", "PE"};
	}else{
		for(auto const& [key, val]:mapGroup){
			typeT.push_back(key);
		}
	}
	for(auto const& elem:typeT){
		res[elem]["colo"] = 0.0;
		res[elem]["non colo"] = 0.0;
		res[elem]["non prlvt"] = 0.0;
	}
	vector<string> resInd;
	pair<double, vector<string>> result;
	vector<shared_ptr<Contact>> contacts = allCtc[ind];
	for(auto const& ctc:contacts){
		time_t date = stringToDate(ctc->getDay());
		if(date<date2 && date>=date1){
			string other = getOtherCtc(ind, ctc);
			double duration = stod(ctc->getLength());
			string stat = colonizationStatusByDatesInd(date1, date2, other, pathogen);
			string typeCat = catOfTheIndST(other, probaByType);
			if(ind == "PE-141-BEC" && stat=="colo"){
				cout << "les dates de recherche: " << dateToString(date1) << " - " << dateToString(date2) << " la date contact " << dateToString(date) << ind << " en contact avec " << other << " pendant " << duration << " en plus il est " << stat << endl;
			}
			auto it = res[typeCat].find(stat);
			if(it!=res[typeCat].end()){
				res[typeCat][stat] = res[typeCat][stat] + duration;
			}else{
				res[typeCat][stat] = duration;
			}
		}
	}
	
	return(res);
}


string Learning::findCat(Individual* ind){
	//---- Fonction qui à partir de la mapCat retourne la catégorie de l'individu ind ----//
	string res;
	if(ind->getStatus()=="PA"){
		res = "Patient";
	}else{
		Staff* s = dynamic_cast<Staff*>(ind);
		res = s->getCat();
	}
	return res;
}

string Learning::findGroup(Individual* ind){
	//---- Fonction qui retrouve le groupe auquel appartient l'individu (déterminer à partir d'un fichier de groupe rentré par l'utilisateur). Analyse de sensibilité ----//
	string res;
	if(ind->getStatus()=="PA"){
		res = "Patient";
	}else{
		Staff* s = dynamic_cast<Staff*>(ind);
		string cat = s->getCat();
		res = groups[cat];
	}
	return res;
}

string Learning::findCatAndHospitalization(Individual* ind){
	//---- Fonction qui à partir de la mapCat retourne la catégorie de l'ind pour PE et le motif d'hospitalisation pour PA ----//
	string res;
	if(ind->getStatus()=="PA"){
		Patient* s = dynamic_cast<Patient*>(ind);
		res = s->getHosp_flag();
	}else{
		Staff* s = dynamic_cast<Staff*>(ind);
		res = s->getCat();
	}
	return res;
}

time_t Learning::stringToDate(string st){
	/* Fonction qui transforme un string en une date time_t. Si le string n'a pas d'heure la fonction rajoute 00:00:00 */
	struct tm tmSt = {0};
	if(st.size()<11){
		st = st + string(" 00:00:00");
	}
	const char *stDateChar = st.c_str();
	strptime(stDateChar, "%Y-%m-%d%t%T", &tmSt);
	time_t timSt = mktime(&tmSt);
	return(timSt);
}

string Learning::dateToString(time_t tm){
	/* Fonction qui redonne un string à partir d'un date time_t*/
	char buffer [22];
	strftime(buffer,22,"%Y-%m-%d %T",localtime(&tm));
	string str_buffer = buffer;
	return (str_buffer);
}

time_t Learning::stringToDateWithHour(string st){
	//---- Fonction qui transforme un string sans heure en une date time_t. Si le string n'a pas d'heure la fonction rajoute 00:00:00 ----//

	struct tm tm_prlvt = {0};
	string tempo_prlvtString = st + " 00:00:00";
	const char *tempo_prlvtChar = tempo_prlvtString.c_str();
	strptime(tempo_prlvtChar, "%Y-%m-%d%t%T", &tm_prlvt);
	time_t timPrlvt = mktime(&tm_prlvt);
	return(timPrlvt);
}

map<time_t, string> Learning::traject(map<time_t, vector<string>> input_map, string pathogen){
	//---- Fonction qui détermine la trajectoir d'un individu pour un pathogène. Pour chaque date de prélèvement de l'individus (input_map), la fonction regarde si le pathogène est bien retrouvé dans les prélèvement. Si oui la fonction attribut 1 à la date et 0 sinon ----//
	map<time_t, string> res;
	for(auto const& [d, prlvtVector]:input_map){
		time_t date = d;
		vector<string> prlvt = prlvtVector;
		vector<string>::iterator itFP = find(prlvt.begin(), prlvt.end(), pathogen);
		if(itFP!=prlvt.end()){
			res[date] = "1";
		}else{
			res[date] = "0";
		}
	}
	return res;
}

map<time_t, string> Learning::lissage(map<time_t, string> input_map, int sep){
	//---- Fonction qui lisse les trajectoires. Elle prend en argument le résultat d'une trajectoir de pathogène (1 si le pathogène est retrouver à la date et 0 sinon) et le degré de separation sep. sep permet de savoir si à quel point on va lisser. Si sep=2 alors on va lisser les +-+ en +++. ----//
	map<time_t, string> res;
	time_t firstD = input_map.begin()->first;
	int compt = 0;
	vector<time_t> dateToReplace;
	bool previous1 = false;
	for(auto const& [key, val]:input_map){
		if(val=="0" && key!=firstD && previous1){
			compt++;
			dateToReplace.push_back(key);
			res[key]="0";
		}else if(val=="0" && (key==firstD || !previous1)){
			res[key]="0";
		}
		if(val=="1" && compt<sep && compt!=0){
			compt=0;
			res[key]="1";
			for(auto const& elem:dateToReplace){
				res[elem]="1";
			}
			previous1=true;
			dateToReplace.clear();
		}
		if(val=="1" && (compt>=sep || compt==0)){
			res[key]="1";
			previous1=true;
			compt=0;
			if(!dateToReplace.empty()){
				dateToReplace.clear();
			}
		}
	}
	return res;
}

map<string, vector<int>> Learning::findDurationByIndividual(map<time_t, string> input_map, int nbOf, bool optionDuration){
	//---- Fonction qui renvoie pour un individu, les durées minimal et maximal de chaque épisode de colonisation ----//
	map<string, vector<int>> res;
	map<time_t, string>::iterator itMap;
	time_t firstDatePrlvt = input_map.begin()->first;
	vector<time_t> allDates = extract_keys(input_map);
	time_t firstDateMin;
	time_t firstDateMax;
	time_t lastDateMin;
	time_t lastDateMax;
	bool boolColonization = false;
	if(input_map.begin()->second=="1" && optionDuration){
		boolColonization = true;
		firstDateMin = firstDatePrlvt;
		firstDateMax = firstDatePrlvt;
	}
	for(itMap = next(input_map.begin(), nbOf); itMap!=input_map.end(); ++itMap){
		if(itMap->second=="1"){
			//il a le pathogen deux options : soit on est déjà dans un épisode et dans ce cas on continue, soit on est pas dans un episode et on check si s'en est un
			if(!boolColonization){
				//on est pas dans un épisode
				int nbCompt = nbOf;
				map<time_t, string>::iterator pos = itMap;
				--pos;
				bool ok = true;
				while(nbCompt>0 && ok){
					if(pos->second=="1"){
						ok = false;
					}else{
						ok = true;
					}
					nbCompt--;
					--pos;
				}
				if(ok){
					//c'est un episode! on stock la date d'acquisition 
					boolColonization = true;
					firstDateMin = itMap->first;
					firstDateMax = prev(itMap, 1)->first + (time_t)86400;
				}
			}else{
				if(itMap->first==allDates[allDates.size()-1] && optionDuration){
					boolColonization = false;
					lastDateMin = itMap->first;
					lastDateMax = itMap->first;
					int minimum = difftime(lastDateMin, firstDateMin);
					if(minimum==0){
						minimum = 86400; //car ça n'a durée qu'un jour au minimum
					}
					/* dans la durée maxi on utilise secondDate comme date de fin et lastNegdate comme date de début */
					int maximum = difftime(lastDateMax, firstDateMax);
					res["min"].push_back(minimum/86400);
					res["max"].push_back(maximum/86400);
					
				}
			}
		}else{
			//il a pas le pathogène donc deux solutions: soit on etait dans un episode qui donc se termine, soit c'est pas une vraie fin on verifie donc que c'est un vraie fin
			if(boolColonization){
				// on était dans un episode donc on va voir si on stop ou pas 
				if(nbOf-1!=0 && itMap->first!=allDates[allDates.size()-1]){
					// on verif les dates d'après
					if(prev(itMap, nbOf)->first==firstDatePrlvt){
						//dans le cas où on commence à 0 mais que l'individu arrive colonisé
						
						
					}else{
						map<time_t, string>::iterator pos = itMap;
						int compt = 1;
						++pos;
						bool ok = true;
						while(compt<nbOf && ok){
							if(pos->second == "0"){
								ok = true;
							}else{
								ok = false;
							}
							compt++;
							pos++;
						}
						if(ok){
							//on stop l'épisode
							boolColonization = false;
							lastDateMin = prev(itMap, 1)->first;
							lastDateMax = itMap->first - (time_t)86400;
							int minimum = difftime(lastDateMin, firstDateMin);
							if(minimum==0){
								minimum = 86400; //car ça n'a durée qu'un jour au minimum
							}
							/* dans la durée maxi on utilise secondDate comme date de fin et lastNegdate comme date de début */
							int maximum = difftime(lastDateMax, firstDateMax);
							res["min"].push_back(minimum/86400);
							res["max"].push_back(maximum/86400);
						}
					}
					
				}else{
					//soit on est dans le cas +- et on stop, soit on est en avant dernier donc on peut quand même stoper l'épisode mais on ne stock pas
					boolColonization = false;
					if(nbOf-1==0 || optionDuration){
						lastDateMin = prev(itMap, 1)->first;
						lastDateMax = itMap->first - (time_t)86400;
						int minimum = difftime(lastDateMin, firstDateMin);
						if(minimum==0){
							minimum = 86400; //car ça n'a durée qu'un jour au minimum
						}
						/* dans la durée maxi on utilise secondDate comme date de fin et lastNegdate comme date de début */
						int maximum = difftime(lastDateMax, firstDateMax);
						res["min"].push_back(minimum/86400);
						res["max"].push_back(maximum/86400);
					}
				}
			}
		}
	}
	return res;
}

bool Learning::checkAbsence(Individual* ind, string date1, string date2){
	//---- Fonction qui détermine si un individu ind à été prélevé entre date1 et date2. Retourne false si pas prélevé et true sinon. ----//
	bool absence;
	vector<string> dateToCheck = buildMevectorOfDates(stringToDate(date1), stringToDate(date2));
	vector<string> datePrelInd = allIndDates[ind];
	sort(dateToCheck.begin(), dateToCheck.end());
	sort(datePrelInd.begin(), datePrelInd.end());
	vector<string> common;
	set_intersection(dateToCheck.begin(), dateToCheck.end(),datePrelInd.begin(), datePrelInd.end(), back_inserter(common));
	if(common.size()!=dateToCheck.size()){
		absence = true;
	}else{
		absence = false;
	}
	return(absence);
}

vector<time_t> Learning::findAcquisitionByIndividual(Individual* ind, map<time_t, string> input_map, int nbOfAcquisition){
	//---- Fonction qui détermine les dates d'acquisition de l'individus ind en fonction de la map de trajectoir input_map et du nombre de prélèvement négatif précédent nbOfAcquisition ----//
	vector<time_t> res;
	map<time_t, string>::iterator itMap;
	map<time_t, string>::iterator itMapB;
	time_t firstDatePrlvt = input_map.begin()->first;
	vector<time_t> allDates = extract_keys(input_map);
	bool boolColonization = false;
	int nbOfEpisode = 0;
	bool isColonized = false;
	for(itMapB = input_map.begin(); itMapB!=next(input_map.begin(), nbOfAcquisition); ++itMapB){
		if(itMapB->second=="1"){
			isColonized = true;
		}
	}
	for(itMap = next(input_map.begin(), nbOfAcquisition); itMap!=input_map.end(); ++itMap){
		if(itMap->second=="1"){
			//il a le pathogen deux options : soit on est déjà dans un épisode et dans ce cas on continue, soit on est pas dans un episode et on check si s'en est un
			if(!boolColonization){
				//on est pas dans un épisode
				int nbCompt = nbOfAcquisitionWeek;
				map<time_t, string>::iterator pos = itMap;
				--pos;
				bool ok = true;
				string previousP;
				while(nbCompt>0 && ok){
					previousP = dateToString(pos->first).substr(0,10);
					if(pos->second=="1"){
						ok = false;
					}else{
						ok = true;
					}
					nbCompt--;
					--pos;
				}
				if(ok){
					//c'est un episode! on stock la date d'acquisition 
					boolColonization = true;
					nbOfEpisode++;
					if(!firstAcqOnly ||(firstAcqOnly && nbOfEpisode==1 && !isColonized && !checkAbsence(ind, previousP, dateToString(itMap->first).substr(0,10)))){
						res.push_back(itMap->first);
					}
				}else{
					//c'est pas un episode mais c'est une colonisation donc on va faire un boolean pour les firstAcqOnly. C'est les cas où il arrive colo et a un episode mais on ne veut que les gens qui n'arrive pas colo avec FirstAcqOnly
					isColonized = true;
				}
			}
		}else{
			//il a pas le pathogène donc deux solutions: soit on etait dans un episode qui donc se termine, soit c'est pas une vraie fin on verifie donc que c'est un vraie fin
			if(boolColonization){
				if(nbOfAcquisitionWeek-1!=0 && itMap->first!=allDates[allDates.size()-1]){
					// on verif les dates d'après
					map<time_t, string>::iterator pos = itMap;
					int compt = 1;
					++pos;
					bool ok = true;
					while(compt<nbOfAcquisitionWeek && ok){
						if(pos->second == "0"){
							ok = true;
						}else{
							ok = false;
						}
						compt++;
						pos++;
					}
					if(ok){
						//on stop l'épisode
						boolColonization = false;
					}
				}else{
					//soit on est dans le cas +- et on stop, soit on est en avant dernier donc on peut quand même stoper l'épisode
					boolColonization = false;
				}
			}
		}
	}
	return res;
}

map<time_t, vector<Individual*>> Learning::findAllAcquisition(vector<Individual*> currentCarriersOfPathogen, string currentPathogen, int nbOfAcquisitionWeek){
	//---- Fonction qui prend en argument un vecteur de string d'individus colonisés par le currentPathogen. Retourne une map avec comme clé la date et comme valeurs un vecteur de string contenant les individus qui ont eu une acquisition du currentPathogène à la date de la clé ----//
	map<time_t, vector<Individual*>> results;
	for(auto const& carrier:currentCarriersOfPathogen){
		map<time_t, vector<string>> currentVectPrlvt = allCarriers[carrier];
		map<time_t, string> mapTrajectory =traject(currentVectPrlvt, currentPathogen);
		vector<time_t> acquisitionDates = findAcquisitionByIndividual(carrier, mapTrajectory, nbOfAcquisitionWeek);
		for(auto const& elem:acquisitionDates){
			results[elem].push_back(carrier);
			// allAcquisitionInd.push_back(carrier);
		}
	}
	return results;
}	


map<string, map<string, vector<int>>> Learning::findAllDuration(vector<Individual*> currentCarriersOfPathogen, string currentPathogen, int nbOfAcquisitionWeek){
	//---- Fonction qui prend en argument un vecteur de string d'individus colonisés par le currentPathogen. Retourne une map avec comme clé la date et comme valeurs un vecteur de string contenant les individus qui ont eu une acquisition du currentPathogène à la date de la clé ----//
	int sep;
	if(nbOfAcquisitionWeek==0){
		sep = nbOfAcquisitionWeek + 1;
	}else{
		sep = nbOfAcquisitionWeek;
	}
	map<string, map<string, vector<int>>> results;
	for(vector<Individual*>::iterator itCCOP = currentCarriersOfPathogen.begin(); itCCOP != currentCarriersOfPathogen.end(); itCCOP++){
		Individual* currentName = (*itCCOP);
		map<time_t, vector<string>> currentVectPrlvt = allCarriers[currentName];
		map<Individual*, map<time_t, vector<string>>>::iterator itAC = find_if(allCarriers.begin(), allCarriers.end(), [currentName](std::pair<Individual*, map<time_t, vector<string>>> v) -> bool {return v.first->getCalc_ident()==currentName->getCalc_ident();} );
		if(!currentVectPrlvt.empty()){
			string cat;
			if(durationByType){
				cat = currentName->getStatus();
			}else{
				cat = findCat(currentName);
			}
			map<time_t, string> mapTrajectory =traject(currentVectPrlvt, currentPathogen);
			if(sep>1){
				mapTrajectory = lissage(mapTrajectory, sep);
			}
			map<string, vector<int>> durationsIndividual;
			if(mapTrajectory.size()>sep){
				durationsIndividual = findDurationByIndividual(mapTrajectory, 1, optionDuration);
			}
			if(!durationsIndividual.empty()){
				results[cat]["min"].insert(results[cat]["min"].end(), durationsIndividual["min"].begin(), durationsIndividual["min"].end());
				results[cat]["max"].insert(results[cat]["max"].end(), durationsIndividual["max"].begin(), durationsIndividual["max"].end());
			}
		}
	}
	return results;
}	



std::map<std::pair<std::string, std::string>, std::pair <std::string, std::string>> Learning::updateTabAdmin(std::vector<std::vector<std::string>> tabAdmin){
	/* Fonction qui retourne une map avec deux pairs, la premiere contient le nom de l'individus et sa date d'entrée, la seconde, la date d'entrée et la date de sortie */
	
	std::map<std::pair<std::string, std::string>, std::pair <std::string, std::string>> results;
	for(int i(0); i<tabAdmin.size(); i++){
		vector<string> currentPrlvt = tabAdmin[i];
		string currentName = currentPrlvt[0];
		pair<string, string> pairInterm = make_pair(currentName, currentPrlvt[2]);
		map<pair<string, string>, pair <string, string>>:: iterator itRes = find_if(results.begin(), results.end(),[pairInterm](pair<pair<string, string>, pair <string, string>> v)->bool {return (v.first.first==pairInterm.first && v.first.second==pairInterm.second);});
		if(itRes!=results.end()){
			//deja dedans
			cout << "PB DATE ADMISSION : oooooooops il a deja eu cette date d'arrivee!" << currentName << endl;
		}else{
			results.insert(make_pair(make_pair(currentName, currentPrlvt[2]), make_pair(currentPrlvt[2], currentPrlvt[3])));
			
		}
	}
	
	map<pair<string, string>, pair <string, string>>::iterator itT;

	return results;
}

string Learning::findRandomDate(std::map<std::string, double> LOSred, string date1){
	//---- Fonction qui retourne une date aléatoire de discharge aléatoire à partir de la durée de séjour. Cette durée de séjour est déterminer par une loi logNormal à partir du mean et variance de la map LOSred. ----//
	double duration;
	double LOSmean = LOSred["mean"];
	double LOSvar = LOSred["variance"];
	if(isnan(LOSvar) || LOSvar==0.0){
		duration = LOSmean;
	}else{
		LogNormalGenerator contenerNormal = Random::instance()->createLogNormalGenerator(LOSmean, sqrt(LOSvar));
		duration = contenerNormal.next();
	}
	string dt_out = dateToString(stringToDate(date1) + (int)duration*86400).substr(0,10);
	return dt_out;
}

string Learning::generateRandomLetters(int nb){
	//---- Fonction qui génère une lettre aléatoire ----//
	vector<string> alphabet = {"A", "B", "C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
	IntUniformGenerator chooseRandom = Random::instance()->createUniIntGenerator(0,25);
	string res;
	for(int i(0); i<nb; i++){
		int rnd = chooseRandom.next();
		res = res + alphabet[rnd];
	}
	return res;
}

int Learning::addToNewIndividuals(int nb, string type, string date1, int id, std::map<std::string, std::map<std::string, double>> LOS){
	//---- Fonction qui rajoute nb nouvel individus à newIndividuals. La fonction attribut un calc_ident au hasard ainsi qu'une durée de séjour pour chaque nouvel individus ainsi qu'un genre au hasard (pour le moment) ----//
	IntUniformGenerator chooseRandom = Random::instance()->createUniIntGenerator(0,1);
	vector<string> tempo_type;
	string dt_in = date1;
	boost::split(tempo_type, type, boost::is_any_of("_"));
	string status = tempo_type[0];
	string ward = tempo_type[2];
	for(int i(0); i<nb; i++){
		id++;
		int genderInt = chooseRandom.next();
		string gender = genderInt == 1 ? "Feminin":"Masculin";
		string agentId;
		if(id<10){
			agentId = to_string(0)+to_string(0)+to_string(id);
		}else if(id>=10 && id<100){
			agentId = to_string(0)+to_string(id);
		}else{
			agentId = to_string(id);
		}
		string dt_out = findRandomDate(LOS[type], date1);
		if(status == "PA"){
			string calc_ident = status + "-" + agentId + "-" + generateRandomLetters(3);
			string hosp = tempo_type[1];
			Patient* p = new Patient(calc_ident, status, dt_in, dt_out, ward, gender);
			p->setHosp_flag(hosp);
			newIndividuals.push_back(p);
		}else{
			string calc_ident = status + "-" + agentId + "-" + generateRandomLetters(3);
			string cat = tempo_type[1];
			Staff* s = new Staff(calc_ident, status, dt_in, dt_out, ward, gender);
			s->setCat(cat);
			newIndividuals.push_back(s);
		}
	}
	return id;
}

int Learning::addToTheCSVFile(vector<Individual*> input_vector, std::map<Individual*, std::vector<std::pair<std::string, std::string>>> admissionDischargeStock, string date1, string date2, ofstream& myCSV){
	//---- Fonction qui ajoute les individus de input_vector au fichier de csv pour les admissions simulées ----//
	int nb = 0;
	for(auto const& ind:input_vector){
		vector<pair<string, string>> ADdates = admissionDischargeStock[ind];
		for(auto const& pairDate:ADdates){
			string datePeriod1 = pairDate.first;
			string datePeriod2 = pairDate.second;
			if(datePeriod2=="")datePeriod2 = date2;
			vector<string> period = returnVectorOfDay(datePeriod1, datePeriod2);
			auto it = find(period.begin(), period.end(), date1);
			if(it!=period.end()){
				//on prend cette période là
				vector<string> tempo_ID;
				string idCalc = ind->getCalc_ident();
				boost::split(tempo_ID, idCalc, boost::is_any_of("-"));
				int idNB = stoi(tempo_ID[1]);
				if(idNB>nb)nb = idNB;
				if(ind->getStatus()=="PA"){
					Patient* p = dynamic_cast<Patient*>(ind);
					myCSV << ind->getCalc_ident() + ";" + ind->getStatus() + ";" + datePeriod1 + ";" + datePeriod2 + ";" + ind->getWard() + ";" + ind->getGender() + ";" + p->getHosp_flag() + ";" << endl;
				}else{
					Staff* s = dynamic_cast<Staff*>(ind);
					myCSV << ind->getCalc_ident() + ";" + ind->getStatus() + ";" + datePeriod1 + ";" + datePeriod2 + ";" + ind->getWard() + ";" + ind->getGender() + ";;"+ s->getCat() << endl;
				}
			}
		}
	}
	return nb;
}

void Learning::buildADSimulation(string filename, string date1, string date2, string filenameAdmission, bool useRealStart){
	//---- Fonction qui construit un fichier d'admission simulé à partir d'un fichier de paramètres filename. ----//
	Filetreatment *file = new Filetreatment(filename);
	Parameters* parameters = file->findADParameters();
	map<string, map<string, double>> admissionDistribution = parameters->getMapAdmissionDistribution();
	map<string, double> admission = parameters->getMapAdmission();
	std::map<std::string, std::map<std::string, double>> LOS = parameters->getMapLengthOfStay();
	vector<string> allDates = buildMevectorOfDates(stringToDate(date1), stringToDate(date2));
	int id = 0;
	ofstream myCSV;
	myCSV.open(filenameAdmission);
	myCSV << "id;status;firstDate;lastDate;ward;sex;hospitalization;cat" << endl;
	DoubleUniformGenerator chooseRandomDouble = Random::instance()->createUniDoubleGenerator(0,1);
	// 1) on commence par les personnes qui sont déjà présente puisque leur date d'admission est la date de départ
	string dt_in = date1;
	try{
		if(useRealStart){
			vector<Individual*> atbegin = individualAtBeginDate(date1);
			if(atbegin.empty()){
				throw runtime_error(" no individuals are in the hospital at the first date of the simulation try again ");
			}else{
				id = addToTheCSVFile(atbegin, admissionDischargeStock, date1, date2, myCSV);
			}
		}else{
			map<string, double> nbOfInd = parameters->getMapNbOfInd();
			for(auto const& [type, nb]:nbOfInd){
				id = addToNewIndividuals(nb, type, date1, id, LOS);
			}
		}	
	}catch(std::exception const& exc){
		cerr << exc.what() << "\n";
		exit(1);
	}
	//2) on poursuit avec les nouvelles admissions
	auto it = allDates.begin();
	for(auto const& date:allDates){
		if(date!=(*it)){
			//pour chaque type dans la map
			for(auto const& [type, value]:admission){
				map<string, double> distribParam = admissionDistribution[type];
				double AdmMean = distribParam["mean"];
				double AdmVar = distribParam["variance"];
				int nbOf;
				nbOf = randomPoisson(value, eng);
				id = addToNewIndividuals(nbOf, type, date, id, LOS);
			}
		}
	}	
	//3) et maintenant on ajoute dans un fichier
	for(auto const& ind:newIndividuals){
		if(ind->getStatus()=="PA"){
			Patient* p = dynamic_cast<Patient*>(ind);
			myCSV << ind->getCalc_ident() + ";" + ind->getStatus() + ";" + ind->getDt_in() + ";" + ind->getDt_out() + ";" + ind->getWard() + ";" + ind->getGender() + ";" + p->getHosp_flag() + ";" << endl;
		}else{
			Staff* s = dynamic_cast<Staff*>(ind);
			myCSV << ind->getCalc_ident() + ";" + ind->getStatus() + ";" + ind->getDt_in() + ";" + ind->getDt_out() + ";" + ind->getWard() + ";" + ind->getGender() + ";;"+ s->getCat() << endl;
		}
	}
	myCSV.close();
	delete parameters;
}

map<string, map<string, double>> Learning::lengthOfStay(std::map<Individual*, std::vector<std::pair<std::string, std::string>>> admissionDischargeStock){
	//---- Fonction qui détermine les paramètres nécessaire pour calculer la durée de séjour. Retourne une map avec pour chaque type (type_motif/cat_service) donne le mean et la variance des durées de séjours ----//
	map<string, vector<double>> resDistribution;
	map<string, map<string, double>> res;
	for(auto const& [ind, pairOfAD]:admissionDischargeStock){
		string type = ind->getStatus() + "_" + findCatAndHospitalization(ind) + "_" + ind->getWard();
		vector<pair<string, string>> allAD = pairOfAD;
		for(auto const& AD:allAD){
			pair<string, string> ADres = AD;
			string firstD = ADres.first;
			string lastD = ADres.second;
			if(stringToDate(firstD) < stringToDate(beginDate)){
				firstD = beginDate;
			}
			if(lastD == ""){
				lastD = endDate;
			}
			vector<string> vectorOfDates = buildMevectorOfDates(stringToDate(firstD), stringToDate(lastD));
			resDistribution[type].push_back(vectorOfDates.size());
		}
	}
	for(auto const& [type, distribution]:resDistribution){
		double meanD = computeMean(distribution);
		double varD = computeSampleVariance(meanD, distribution);
		res[type]["mean"] = meanD;
		res[type]["variance"] = varD;
	}
	return res;
}

tuple<map<string, map<string, double>>, map<string, map<string, double>>, map<string, double>, map<string, double>, map<string, map<string, double>>> Learning::admissionDischargeProbabilities(std::map<Individual*, std::vector<std::pair<std::string, std::string>>> admissionDischargeStock){
	//---- Fonction qui calcul plusieurs parametres calculer à partir du fichier d'admission. AdmissionDischargeStock est une map qui pour chaque individus, stock les dates d'admission et de discharge. Les paramètres de sortie sont:
	//admissionRes = map avec pour chaque type (type_motif/cat_service), le mean et la variance du nombre d'admission par jour
	//dischargeRes = map avec pour chaque type (type_motif/cat_service), le mean et la variance du nombre d'admission par jour
	//admissionProba = map avec pour chaque type (type_motif/cat_service) le taux d'admission par jour
	//dischargeProba = map avec pour chaque type (type_motif/cat_service) le taux de discharge par jour
	//colonizedRes = map avec pour chaque pathogène et chaque type (PE/PA) la probabilité de colonisation à l'admission
	//----//
	
	//type, date, vector d'individus
	map<string, map<string, vector<string>>> admissionResInterm;
	map<string, map<string, vector<string>>> dischargeResInterm;
	map<string, map<string, double>> admissionRes;
	map<string, map<string, double>> dischargeRes;
	map<string, double> admissionProba;
	map<string, double> dischargeProba;
	//patho, type, proba
	map<string, map<string, double>> colonizedRes;
	map<string, double> allAdmitted;
	vector<string> allDates = buildMevectorOfDates(stringToDate(beginDate), stringToDate(endDate));
	for(auto const& [ind, datesAD]:admissionDischargeStock){
		string type;
		string colonizedType = ind->getStatus();
		string adminDisType = ind->getStatus() + "_" + findCatAndHospitalization(ind) + "_" + ind->getWard();
		for(auto const& d:datesAD){
			if(stringToDate(d.first)>=stringToDate(beginDate)){
				admissionResInterm[adminDisType][d.first].push_back(ind->getCalc_ident());
				if(withCol){
					if(checkIfSwabbedAtAdmission(ind, d.first)){
						allAdmitted[colonizedType] = allAdmitted[colonizedType] + 1.0; 
						allAdmitted["all"] = allAdmitted["all"] + 1.0; 
						for(auto const& pathogen:listOfAllPath){
							if(checkIfColonizedAtAdmission(ind, d.first, pathogen)){
								auto itF = colonizedRes[pathogen].find(colonizedType);
								if(itF!=colonizedRes[pathogen].end()){
									colonizedRes[pathogen][colonizedType] = colonizedRes[pathogen][colonizedType] + 1.0;
								}else{
									colonizedRes[pathogen][colonizedType] = 1.0;
								}
								auto itF2 = colonizedRes[pathogen].find("all");
								if(itF2!=colonizedRes[pathogen].end()){
									colonizedRes[pathogen]["all"] = colonizedRes[pathogen]["all"] + 1.0;
								}else{
									colonizedRes[pathogen]["all"] = 1.0;
								}
							}
						}
					}
				}
			}
			if(d.second!="" && stringToDate(d.second)<=stringToDate(endDate)){
				dischargeResInterm[adminDisType][d.second].push_back(ind->getCalc_ident());
			}
		}
	}
	//admission
	for(auto const& [t, mapDatesInd]:admissionResInterm){
		map<string, vector<string>> mapDatesIndVal = mapDatesInd;
		int nbOfDays = 0;
		int nbOfDays2 = 0;
		string type;
		if(t!="Patient"){
			type = "PE";
		}else{
			type = "PA";
		}
		vector<double> distributionAdmission;
		for(auto const& [d,vectInd]:mapDatesIndVal){
			nbOfDays = nbOfDays + 1;
			nbOfDays2 = nbOfDays2 + vectInd.size();
			distributionAdmission.push_back(vectInd.size());
		}
		double meanAdmission = computeMean(distributionAdmission);
		admissionRes[t]["mean"] = meanAdmission;
		admissionRes[t]["variance"] = computeSampleVariance(meanAdmission, distributionAdmission);
		admissionProba[t] = (double)nbOfDays2/allDates.size();
	}
	//discharge
	for(auto const& [t, mapDatesInd]:dischargeResInterm){
		map<string, vector<string>> mapDatesIndVal = mapDatesInd;
		int nbOfDays = 0;
		vector<double> distributionDischarge;
		for(auto const& [d,vectInd]:mapDatesIndVal){
			nbOfDays = nbOfDays + 1;
			distributionDischarge.push_back(vectInd.size());
		}
		double meanDischarge = computeMean(distributionDischarge);
		dischargeRes[t]["mean"] = meanDischarge;
		dischargeRes[t]["variance"] = computeSampleVariance(meanDischarge, distributionDischarge);
		dischargeProba[t] = (double)nbOfDays/allDates.size();
	}
	//colonization
	if(withCol){
		for(auto const& [path, mapVal]:colonizedRes){
			map<string, double> mapValRes = mapVal;
			for(auto const& [t, val]:mapValRes){
				colonizedRes[path][t] = colonizedRes[path][t] / allAdmitted[t];
			}
		}
	}
	return make_tuple(admissionRes, dischargeRes, admissionProba, dischargeProba, colonizedRes);
}

bool Learning::checkIfColonizedAtAdmission(Individual* ind, string firstDate, string pathogen){
	//---- Fonction qui vérifie si l'individu ind est colonisé à l'admission. On regarde dans les 48h suivant son arrivée s'il a été prélevé et s'il est positif au pathogène pathogen. ----//
	bool res = false;
	map<time_t, vector<string>> prlvt = allCarriers[ind];
	time_t period1 = stringToDate(firstDate);
	time_t period2 = stringToDate(firstDate) + 2*86400;
	for(auto const& [date, pathogens]:prlvt){
		if(date>=period1 && date<=period2){
			auto itP = find(pathogens.begin(), pathogens.end(), pathogen);
			if(itP!=pathogens.end()){
				res = true;
			}
		}
	}
	return res;
}

bool Learning::checkIfSwabbedAtAdmission(Individual* ind, string firstDate){
	//---- Fonction qui regarde si l'individu ind a été prélevé dans les 48h suivant son admission. ----//
	bool res = false;
	map<time_t, vector<string>> prlvt = allCarriers[ind];
	time_t period1 = stringToDate(firstDate);
	time_t period2 = stringToDate(firstDate) + 2*86400; //on prend une période de deux jours
	auto it = find_if(prlvt.begin(), prlvt.end(), [period1, period2](pair<time_t, vector<string>> n )->bool {return(n.first>=period1 && n.first<=period2);});
	if(it!=prlvt.end()){
		res = true;
	}
	return res;
}

map<time_t, vector<Individual*>> Learning::computeIndPerDay(){
	//---- La fonction donne une map avec en clé la date en time_t et le vecteur d'individus présent ----//
	map<time_t,vector<Individual*>> results;
	for(auto const& [ind, dates]:allIndDates){
		for(auto const& d:dates){
			results[stringToDate(d)].push_back(ind);
		}
	}
	return results;
}

map<string, map<string, pair<double, double>>>Learning::durationParameters2(){
	//---- Fonction qui renvoie une map avec en clé le pathogen et en valeur une autre map dont la clé est le type (PA/PE) et la valeur est une pair avec la moyenne et la variance de la durée de colonisation du pathogen chez les individus ----//
	map<string, map<string, pair<double, double>>> mapRes;
	map<string, pair<double, double>> mapResInterm;
	for(map<string, vector<Individual*>>::iterator itPC = pathogensCarriers.begin(); itPC != pathogensCarriers.end(); itPC++){
		string currentPathogen = (*itPC).first;
		vector<Individual*> currentCarriersOfPathogen = (*itPC).second;
		map<string, map<string, vector<int>>> mapD = findAllDuration(currentCarriersOfPathogen, currentPathogen, nbOfAcquisitionWeek);		
		for(auto const& [key, val]:mapD){
			string cat = key;
			map<string, vector<int>> mapParamPosition = val;
			vector<int> min = mapParamPosition["min"];
			vector<int> max = mapParamPosition["max"];
			vector<double> meanMinMax;
			vector<double> newMax;
			for(int i = 0; i<min.size(); i++){
				double resMean;
				resMean = (((double)max[i] + min[i])/2);
				newMax.push_back((double)max[i]);
				meanMinMax.push_back(resMean);
			}
			if(meanMinMax.size()>1){
				double meanVal = computeMean(meanMinMax);
				double varVal = computeSampleVariance(meanVal, meanMinMax);
				mapResInterm[cat] = make_pair(meanVal, varVal);
			}
			distributionDuration[currentPathogen][key] = meanMinMax;
		}
		mapRes.insert(make_pair(currentPathogen, mapResInterm));
	}
	return mapRes;
}

double Learning::computeMean(const std::vector<double>& numbers){
	//---- Calcule la moyenne du vecteur de double numbers ----//
    if (numbers.empty())
        return std::numeric_limits<double>::quiet_NaN();
	
    return std::accumulate(numbers.begin(), numbers.end(), 0.0) / numbers.size();
}

double Learning::computeSampleVariance(const double mean, const std::vector<double>& numbers){
	//---- Fonction qui retourne la variance du vecteur de double numbers à l'aide de la moyenne mean ----//
    if (numbers.size() <= 1u){
		return std::numeric_limits<double>::quiet_NaN();
	}
	int n = numbers.size();
    double sum = 0.0;
    for (int i = 0; i < n; i++)
    {
        sum += (numbers[i] - mean) * (numbers[i] - mean);
    }
    return sum / (n-1);
}


void Learning::sensibilityON(string typeOfModification_, string natureOfModification_, int numberOfModification_){
	typeOfModification = typeOfModification_;
	natureOfModification = natureOfModification_;
	numberOfModification = numberOfModification_;
}

void Learning::resultParameters2(bool write, string fileNameResult, vector<string> parameters){
	//---- Fonction qui écrit dans un fichier csv fileNameResult tout les parametres du vecteur parameters ----//
	ofstream myCSV;
	if(write){
		string FileName = fileNameResult;
		myCSV.open(FileName);
	}
	auto it = find(parameters.begin(), parameters.end(),"colonization at admission");
	if(it!=parameters.end()){
		withCol = true;
	}else{
		withCol = false;
	}
	for(auto const& paramToFind:parameters){
		if(paramToFind == "probability of transmission"){
			cout << "ok " << endl;
			buildPathogenProbabilities();
			if(write){
				// for(auto const& [bacteria, valMap]: pathogenProbabilityOfTransmission){
					// for(auto const& [pairCat, proba] : valMap){
						// string allPairCat = pairCat.first + "-" + pairCat.second;
						// myCSV << "probability of transmission;" + bacteria + ";";
						// myCSV << allPairCat + ";" + to_string(proba) + ";";
						// myCSV << "\r" << endl;
					// }
				// }
				
				for(auto const& [bacteria, valMap]: pathogenProbabilityOfTransmission2){
					for(auto const& [pairCat, probas] : valMap){
						string allPairCat = pairCat.first + "-" + pairCat.second;
						myCSV << "probability of transmission;" + bacteria + ";";
						myCSV << allPairCat + ";";
						for(auto const& p:probas){
							double pp = p;
							cout << " sensibilityStatus " << sensibilityStatus << " typeOfModification " << typeOfModification << endl;
							if(sensibilityStatus && typeOfModification=="probability"){
								if(natureOfModification=="division"){
									pp = pp/(double)numberOfModification;
								}else{
									pp = pp * (double)numberOfModification;
								}
								cout << " ajout de la proba qui passe de " << p << " à " << pp << endl;
							}
							myCSV << to_string(pp) + ";";
						}
						myCSV << "\r" << endl;
					}
				}
			}
		}else if(paramToFind == "colonization at admission" || paramToFind == "admission" || paramToFind == "admission distribution" || paramToFind == "discharge" || paramToFind == "discharge distribution"){
			if(admissionProba.empty())buildAdmissionDischarge();
			if(paramToFind == "colonization at admission" && write){
				addMapInTheFile(colonizedAtAdmissionParam, myCSV, string("colonization at admission"));
			}else if(paramToFind == "admission" && write){
				addMapInTheFile(admissionProba, myCSV, string("admission"));
			}else if(paramToFind == "discharge" && write){
				addMapInTheFile(dischargeProba, myCSV, string("discharge"));
			}else if(paramToFind == "admission distribution" && write){
				addMapInTheFile(admissionDistributionParam, myCSV, string("admission distribution"));
			}else if(paramToFind == "discharge distribution" && write){
				addMapInTheFile(dischargeDistributionParam, myCSV, string("discharge distribution"));
			}
		}else if(paramToFind == "duration of colonization" || paramToFind == "distribution of duration"){
			if(durationParameters.empty())durationParameters = getDurationOfColonization();
			if(write && paramToFind == "duration of colonization"){
				for(auto const& [key, val] : durationParameters){
					map<string, pair<double, double>> valRes = val;
					for(auto const& [type, valMeanVar]:valRes){
						pair<double, double> meanVar = valMeanVar;
						double meanDuration = meanVar.first;
						double varDuration = meanVar.second;
						if(sensibilityStatus && typeOfModification=="duration"){
							if(natureOfModification=="division"){
								meanDuration = meanDuration/(double)numberOfModification;
								varDuration = varDuration/(double)numberOfModification;
							}else{
								meanDuration = meanDuration * (double)numberOfModification;
								varDuration = varDuration * (double)numberOfModification;
							}
						}
						myCSV << "duration of colonization;" + key + ";" + type + ";" + "mean;" +  to_string(meanDuration) + ";\r"<<endl;
						myCSV << "duration of colonization;" + key + ";" + type + ";" + "variance;" +  to_string(varDuration) + ";\r" << endl;
					}
				}
			}else if(write && paramToFind == "distribution of duration"){
				for(auto const& [pathogen, mapDistributionDuration]: distributionDuration){
					for(auto const& [type, vectorOfDuration]:mapDistributionDuration){
						myCSV << "distribution of duration;" + pathogen + ";" + type + ";";
						for(auto const& duree:vectorOfDuration){
							myCSV << to_string(duree) + ";";
						}
						myCSV << "\r" << endl;
					}
				}
			}
		}else if(paramToFind == "days of week"){
			daysOfWeekParams = getDaysOfWeekParameters();
			for(auto const& [key, val]: daysOfWeekParams){
				for(auto const& [t, pairVal]:val){
					myCSV << "days of week;" + key + ";" + t + ";";
					pair<double, double> meanDOW = pairVal;
					double meanDays = meanDOW.first;
					double varianceDays = meanDOW.second;
					myCSV << "mean;" +  to_string(meanDays) + ";";
					myCSV << "\r" << endl;
					myCSV << "days of week;" + key + ";" + t + ";";
					myCSV << "variance;" +  to_string(varianceDays) + ";";
					myCSV << "\r" << endl;
				}			
			}
		}else if(paramToFind == "number of individual"){
			indAtStart = nbOfIndividualStart();
			if(write){
				for(auto const& [key, val]:indAtStart){
					myCSV << "number of individual;" + key + ";" + to_string(val) + ";";
					myCSV << "\r" << endl;
				}
			}
		}else if(paramToFind == "length of stay"){
			LOS = lengthOfStay(admissionDischargeStock);
			if(write){
				addMapInTheFile(LOS, myCSV, string("length of stay"));
			}
		}
	}
	if(write)myCSV.close();
}

void Learning::addMapInTheFile(map<string, map<time_t,double>> input_map, ofstream& myCSV, string type){
	//---- Fonction qui ajoute une map de la forme map<string, map<time_t,double>> dans le csv de résultats ----//
	for(auto const& [key, val]:input_map){
		myCSV << type + ";" + key + ";";
		for(auto const& [k,v]:val){
			myCSV<<  to_string(v) + ";" ;
		}
		myCSV << "\r" << endl;
	}
}

void Learning::addMapInTheFile(map<string, double> input_map, ofstream& myCSV, string type){
	//---- Fonction qui ajoute une map de la forme map<string, double> dans le csv de résultats ----//
	for(auto const& [key, val]:input_map){
		myCSV << type + ";" + key + ";" + to_string(val) + ";";
		myCSV << "\r" << endl;
	}
}

void Learning::addMapInTheFile(map<string, map<string, double>> input_map, ofstream& myCSV, string type){
	//---- Fonction qui ajoute une map de la forme map<string, double> dans le csv de résultats ----//
	for(auto const& [key2, val2] : input_map){
		for(auto const& [k, v] : val2){
			myCSV << type+ ";" + key2 + ";";
			myCSV << k + ";" + to_string(v) + ";";
			myCSV << "\r" << endl;
		}
	}
}

string Learning::dateOfEaster(int year){
	//---- Fonction qui détermine les dates fériés pour l'année year ----//
    int Y = year;
    int a = Y % 19;
    int b = Y / 100;
    int c = Y % 100;
    int d = b / 4;
    int e = b % 4;
    int f = (b + 8) / 25;
    int g = (b - f + 1) / 3;
    int h = (19 * a + b - d - g + 15) % 30;
    int i = c / 4;
    int k = c % 4;
    int L = (32 + 2 * e + 2 * i - h - k) % 7;
    int m = (a + 11 * h + 22 * L) / 451;
	int month = (h + L - 7 * m + 114) / 31;
    int day = ((h + L - 7 * m + 114) % 31) + 1;
	string res = to_string(Y) + "-" + to_string(month) + "-" + to_string(day);
	return(res);
}

vector<string> Learning::findMeNonWorkingDays(string startD, string stopD, bool bridge){
	//---- Fonction qui détermine les dates non ouvré en france ----//
	vector<string> res;
	string firstD = startD.substr(0,10);
	string secondD = stopD.substr(0,10);
	int firstYear;
	int secondYear;
	vector<string> tempo_firstD;
	boost::split(tempo_firstD, firstD, boost::is_any_of("-"));
	vector<string> tempo_secondD;
	boost::split(tempo_secondD, secondD, boost::is_any_of("-"));
	firstYear = stoi(tempo_firstD[0]);
	secondYear = stoi(tempo_secondD[0]);
	vector<int> years;
	if(firstYear!=secondYear){
		for(int y(firstYear); y<=secondYear; y++){
			years.push_back(y);
		}
	}else{
		years.push_back(firstYear);
	}
	for(auto const& elem:years){
		string easterSunday = dateOfEaster(elem);
		string easterMonday = dateToString(stringToDate(easterSunday) + 1*86400).substr(0,10);
		string ascension = dateToString(stringToDate(easterSunday) + 39*86400).substr(0,10);
		string pentecost = dateToString(stringToDate(easterSunday) + 50*86400).substr(0,10);
		string christmas = to_string(elem) + "-12-25";
		string nationalDay = to_string(elem) + "-07-14";
		string madoneDesMotards = to_string(elem) + "-08-15";
		string labourDay = to_string(elem) + "-05-01";
		string victoire = to_string(elem) + "-05-08";
		string armistice = to_string(elem) + "-11-11";
		string toussaint = to_string(elem) + "-11-01";
		string beginningOfTheYear = to_string(elem) + "-01-01";
		res.push_back(easterMonday);
		res.push_back(ascension);
		res.push_back(pentecost);
		res.push_back(christmas);
		res.push_back(nationalDay);
		res.push_back(madoneDesMotards);
		res.push_back(labourDay);
		res.push_back(victoire);
		res.push_back(armistice);
		res.push_back(toussaint);
		res.push_back(beginningOfTheYear);
	}
	vector<string> addDatesBridge = frenchBridge(res);
	if(!addDatesBridge.empty() && bridge){
		res.insert(res.end(), addDatesBridge.begin(), addDatesBridge.end());
	}
	return(res);
}


vector<string> Learning::frenchBridge(vector<string> input_vector){
	//---- Fonction qui détermine s'il y a un pont ou non en fonction des jours fériés de input_vector ----//
	vector<string> resDates;
	for(auto const& date:input_vector){
		time_t stDate = stringToDate(date);
		tm* time_st = localtime(&stDate);
		int wday = time_st->tm_wday;
		if(wday==2){
			//si c'est un mardi ou un jeudi on suppose que les gens font le pont
			resDates.push_back(dateToString(stringToDate(date) - 1*86400).substr(0,10));
		}else if(wday==4){
			resDates.push_back(dateToString(stringToDate(date) + 1*86400).substr(0,10));
		}
	}
	return(resDates);
}

double Learning::randomPoisson(double mean, boost::mt19937& rng){
	//---- Fonction qui retourne un double tiré au sort selon une loi de poisson de paramètre mean ----//
    boost::random::poisson_distribution<> exp( mean );
    boost::variate_generator<boost::mt19937&,boost::random::poisson_distribution<> > var_expo( rng, exp);
    return var_expo();
}

bool Learning::FileExists(const std::string& name) {
  struct stat buffer;
  return (stat (name.c_str(), &buffer) == 0);
}

