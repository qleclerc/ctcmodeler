#!/usr/bin/env bash

#SBATCH --ntasks=3
#SBATCH -o slurm_simulation/test/test-%j.out # STDOUT
#SBATCH -e slurm_simulation/error/err-%j.err # STDERR


# on s'assure d'avoir un environnement propre
module purge

# on charge les module necessaire au le script
module load gcc/9.2.0
module load openmpi/4.0.5
module load netcdf/4.7.3
module load boost/1.72.0
module load repast_hpc/2.3.0


nbOfSim=300
nbSupers=60
nbOfScenario=3

sensib="probability_multiplication_2"

fileNameAnalyze="byCatSC"
xtra="complianceSC_double_transmission/"

sbatch -p common,dedicated --qos=fast --job-name=analysis ./modelInterventionsCP.sh $nbOfScenario $nbOfSim $fileNameAnalyze $xtra $nbSupers $sensib

fileNameAnalyze2="byCatSCVC"
xtra2="vaccinationSC_double_transmission/"

sbatch -p common,dedicated --qos=fast --job-name=analysis ./modelInterventionsVCSC.sh $nbOfScenario $nbOfSim $fileNameAnalyze2 $xtra2 $nbSupers $sensib


sensib="probability_division_2"

fileNameAnalyze="byCatSC"
xtra="complianceSC_half_transmission/"

sbatch -p common,dedicated --qos=fast --job-name=analysis ./modelInterventionsCP.sh $nbOfScenario $nbOfSim $fileNameAnalyze $xtra $nbSupers $sensib

fileNameAnalyze2="byCatSCVC"
xtra2="vaccinationSC_half_transmission/"

sbatch -p common,dedicated --qos=fast --job-name=analysis ./modelInterventionsVCSC.sh $nbOfScenario $nbOfSim $fileNameAnalyze2 $xtra2 $nbSupers $sensib



sensib="duration_multiplication_2"

fileNameAnalyze="byCatSC"
xtra="complianceSC_double_duration/"

sbatch -p common,dedicated --qos=fast --job-name=analysis ./modelInterventionsCP.sh $nbOfScenario $nbOfSim $fileNameAnalyze $xtra $nbSupers $sensib

fileNameAnalyze2="byCatSCVC"
xtra2="vaccinationSC_double_duration/"

sbatch -p common,dedicated --qos=fast --job-name=analysis ./modelInterventionsVCSC.sh $nbOfScenario $nbOfSim $fileNameAnalyze2 $xtra2 $nbSupers $sensib


sensib="duration_division_2"

fileNameAnalyze="byCatSC"
xtra="complianceSC_half_duration/"

sbatch -p common,dedicated --qos=fast --job-name=analysis ./modelInterventionsCP.sh $nbOfScenario $nbOfSim $fileNameAnalyze $xtra $nbSupers $sensib

fileNameAnalyze2="byCatSCVC"
xtra2="vaccinationSC_half_duration/"

sbatch -p common,dedicated --qos=fast --job-name=analysis ./modelInterventionsVCSC.sh $nbOfScenario $nbOfSim $fileNameAnalyze2 $xtra2 $nbSupers $sensib



