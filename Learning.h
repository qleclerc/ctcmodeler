/* LEARNING CLASS
* this file gather all organism from the project
*
*	
*	Learning.h
*
*	Created on: mai 22, 2018
*	Author: Audrey
*
*/

#ifndef LEARNING_H_
#define LEARNING_H_

#include <ctime>
#include <map>
#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <boost/algorithm/string.hpp>
#include <numeric>
#include <sys/stat.h>

#include <boost/random.hpp>
#include <boost/random/random_device.hpp>
#include <boost/math/distributions.hpp>
#include <boost/math/distributions/chi_squared.hpp>
#include <boost/random/gamma_distribution.hpp>

#include "repast_hpc/Utilities.h"
#include "repast_hpc/initialize_random.h"

#include <stdio.h>
#include <time.h>
#include <vector>
#include <regex>
#include "Individual.h"
#include "Filetreatment.h"

class Learning{
	protected:
	bool sensibilityStatus = false;
	std::string typeOfModification;
	std::string natureOfModification;
	int numberOfModification;
	boost::random::mt19937 eng; //paramètre pour avoir des nombres aléatoire
	std::vector<Individual*> allIndividuals; //tout les individus Individual entre beginDate et endDate
	std::vector<Individual*> newIndividuals; //tout les nouveaux individus pour l'admission simulée
	std::map<Individual*, vector<string>> allIndDates; //toutes les dates de présence de l'individus entre son admission et sa discharge
	std::map<std::string, std::vector<std::string>> allPrel; //toutes les dates de prélèvements pour chaque individu
	std::map<std::string, std::map<std::string, std::vector<std::vector<std::string>>>> allPrlvt; //ensemble des prélèvements du fichier de prelevement. calc_ident - date - vecteur de résultats
	std::map<std::string, std::vector<std::shared_ptr<Contact>>> allCtc; //ensemble des contacts du fichier ctc. calc_ident - vecteur de contact
	std::map<std::string, std::string> groups; //map avec les catégories professionnelles des PE et leur groupe correspondant. cat - group
	std::map<Individual*, std::vector<std::pair<std::string, std::string>>> admissionDischargeStock; //map des admission et discharge de chaque individus. individual - vecteur de pair d'admissione et discharge
	std::map<std::string, std::map<std::string, double>> admissionDistributionParam; //map des admissions (type_motif/cat_service), le mean et la variance du nombre d'admission par jour
	std::map<std::string, std::map<std::string, double>> dischargeDistributionParam; //map des discharges (type_motif/cat_service), le mean et la variance du nombre d'admission par jour
	std::map<std::string, double> admissionProba; //map des proba d'admission (type_motif/cat_service) - proba
	std::map<std::string, double> dischargeProba; //map des proba de discharge (type_motif/cat_service) - proba
	std::map<std::string, std::map<std::string, double>> LOS; //map des durées de séjours (type_motif/cat_service)
	std::map<std::string, int> indAtStart; //map des individus à l'initialisation pour les admissions simulées (type_motif/cat_service) - nombre d'individu
	std::map<std::string, std::map<std::string, double>> colonizedAtAdmissionParam; //map pour les paramètres de colonisation à l'admission pathogen - PA/PE - probabilité
	// std::vector<Individual*> allAcquisitionInd; //vecteur des individus qui ont eu une acquisition
	std::map<std::string, std::map<std::pair<std::string, std::string>, double>> pathogenProbabilityOfTransmission; //map des probabilités de transmission pour chaque pathogène et chaque coupe PE/PE
	std::map<std::string, std::map<std::pair<std::string, std::string>, std::vector<double>>> pathogenProbabilityOfTransmission2; //map des probabilités de transmission pour chaque pathogène et chaque coupe PE/PE
	std::map<std::string, std::map<std::string, std::pair<double, double>>> durationParameters; //map des durées de colonisations pour chaque pathogène, chaque type (PA/PE), pair de mean et variance
	std::map<std::string, std::map<std::string, std::vector<double>>> distributionDuration; //map de la distribution empirique des durées de colonisation pour chaque pathogène - type (PA/PE) - vecteur de durée de colonisation
	std::map<std::string, std::map<std::string, std::pair<double, double>>> daysOfWeekParams; //map avec les parametres pour la distribution des individus prélevé par jour de la semaine. Jour de la semaine - type (PA/PE) - mean et variance
	std::string groupFile=""; //fichier utilisation pour déterminer le groupe pour les PE dans le cadre de l'analyse de sensibilité
	std::string nbOfIndInitDate=""; //jour pour déterminer le nombre d'individu présent dans l'hôpital pour le fichier d'admission simulé
	int seuil; //nombre minimum de semaine à prendre en compte dans le calcul des proba de transmission (OBSOLETTE)
	bool withCol = true; //booleen pour savoir si prise en compte de la colonisation à l'admission dans la construction du fichier
	bool firstAcqOnly; //booleen pour ne prendre en compte que la première acquisition dans le calcule des probabilités de transmission
	int nbOfAcquisitionWeek; //nombre minimal de semaine de prélèvement négatif précédent un prélèvement positif pour le calcul des probabilités de transmission
	bool probaByType; //booleen pour savoir si on prend en compte le type (PA/PE) ou le group sinon dans les proba de transmission
	bool durationByType; //boolean pour savoir si le calcule des durées de colonisation se fait par type (PA/PE) sinon c'est pas catégorie professionnelle des PE (OBSOLETTE ne marche que pour le type PA/PE)
	bool optionDuration; //booleen pour savoir si on prend les durées longues ou courtes dans le calcule des durées de colonisation
	bool withLogPAPA; //booleen pour savoir si on utilise le log dans le calcul des proba de transmission
	bool withLogPEPA;
	bool withLogPAPE;
	bool withLogPEPE;
	int nbCtcInfMin = 10; //nombre de contact minimum qu'il faut pour prendre en compte la ou les semaines dans le calcule des proba de transmission
	std::vector<std::string> listOfAllPath; //un vecteur avec la liste de tous les pathogènes à étudier
	std::map<std::string, std::vector<Individual*>> pathogensCarriers; // (clé: pathogène; val: vecteur d'individus porteurs du pathogène clé)
	std::map<Individual*, std::map<time_t, std::vector<std::string>>>allCarriers; //(clé: pathogène; val: vecteur de pair(first: date; second: vecteur d'individus porteur du pathogène clé à la date first))
	std::string admin=""; //fichier des admissions
	std::string ctc=""; //fichier des contacts
	std::string prlvt; // fichier des prlvt
	std::vector<std::vector<std::string>> tabAdmin; //vecteur de vecteur contenant les données du fichier des admissions
	std::multimap<std::string,std::vector<std::string>> previousPrlvt; // multimap avec les données du fichier prlvt (clé: nom de l'individus; val: vector de string avec les données de sa ligne de prlvt)
	std::multimap<std::string,std::shared_ptr<Contact>> previousCtc; // multimap avec les données du fichier contact (clé: nom de l'individus; val: vector de shared_ptr contenant le contact)
	std::string beginDate; //date de début de l'étude
	std::string endDate; //date de fin de l'étude
	std::string beginDateCtc; //date de début des ctc 
	std::vector<std::string> keyCat; //vecteur avec toutes les catégories professionnelles des PE + Patient
	std::map<std::string, std::vector<Individual*>> mapCat; //map des catégories (clé: catégorie; val: vecteur d'individus)
	std::map<std::string, std::vector<Individual*>> mapType; //map des type (clé: type PA ou PE; val: vecteur d'individus)
	std::map<std::string, std::vector<Individual*>> mapGroup; //map des type (clé: type PA ou PE; val: vecteur d'individus)
	std::map<std::pair<std::string, std::string>, std::pair <std::string, std::string>> mapTabAdmin; // map des admissions (clé: pair(first: nom de l'individus; second: date d'entrée); val: pair(first: date d'entrée, second: date de sortie))
	
	public:
	//constructeur par défaut
	Learning();
	//1er constructeur
	Learning(repast::Properties* mainProps, std::string beginDate_, std::string endDate_);
	//2nd constructeur
	Learning(repast::Properties* mainProps, std::string admin_,  std::string beginDate_, std::string endDate_,std::string nbOfIndInitDate_, std::string groupFile_="");
	//3eme constructeur
	Learning(repast::Properties* mainProps, std::vector<std::string> listOfAllPath_, std::string admin_, std::string ctc_, std::string prlvt, std::string beginDate_, std::string endDate_, int nbOfAcquisitionWeek_, bool firstAcqOnly_, bool probaByType_, bool durationByType_, int seuil_, int nbCtcInfMin_, bool optionDuration_, bool withLogPAPA_, bool withLogPAPE_, bool withLogPEPA_, bool withLogPEPE_, std::string groupFile_="");
	//destructeur
	~Learning();
	//dates
	static time_t stringToDate(std::string st); 
	static std::string dateToString(time_t tm); 
	static time_t stringToDateWithHour(std::string st);
	std::vector<std::string> buildMevectorOfDates(time_t date1, time_t date2);
	std::vector<std::string> returnVectorOfDay(std::string bday, std::string eday);
	std::string findRandomDate(std::map<std::string, double> LOSred, std::string date1);
	static std::string dateOfEaster(int year);
	static std::vector<std::string> findMeNonWorkingDays(std::string startD, std::string stopD, bool bridge);
	static std::vector<std::string> frenchBridge(std::vector<std::string> input_vector);
	bool checkAbsence(Individual* ind, std::string date1, std::string date2);
	//relatif à l'analyse de sensibilité
	std::map<std::string, std::string> buildGroup(std::string file_);
	std::map<std::string, std::vector<Individual*>> findMapGroup();
	std::string findGroup(Individual* ind);
	//relatif aux catégories
	std::map<std::string, std::vector<Individual*>> findMapCat();
	static std::string findCat(Individual* ind);
	std::map<std::string, std::vector<std::string>> catOfTheVectorST(std::vector<std::string> indThisWeekAcquisition, bool type);
	std::map<std::string, std::vector<Individual*>> catOfTheVector(std::vector<Individual*> indThisWeekAcquisition, bool type);
	std::string findCatAndHospitalization(Individual* ind);
	//relatif aux types
	std::map<std::string, std::vector<Individual*>> findMapType();
	//admission
	static std::vector<std::vector<std::string>> buildTabAdmin(std::string admin);
	static std::map<std::pair<std::string, std::string>, std::pair <std::string, std::string>> updateTabAdmin(std::vector<std::vector<std::string>> tabAdmin);
	std::tuple<std::map<std::string, std::map<std::string, double>>, std::map<std::string, std::map<std::string, double>>, std::map<std::string, double>, std::map<std::string, double>, std::map<std::string, std::map<std::string, double>>> admissionDischargeProbabilities(std::map<Individual*, std::vector<std::pair<std::string, std::string>>> admissionDischargeStock);
	bool checkIfColonizedAtAdmission(Individual* ind, std::string firstDate, std::string pathogen);
	bool checkIfSwabbedAtAdmission(Individual* ind, std::string firstDate);
	void buildADSimulation(std::string filename, std::string date1, std::string date2, std::string filenameAdmission, bool useRealStart);
	std::map<std::string, std::map<std::string, double>> lengthOfStay(std::map<Individual*, std::vector<std::pair<std::string, std::string>>> admissionDischargeStock);
	void buildAdmissionDischarge();
	//contact
	std::multimap<std::string,std::shared_ptr<Contact>> buildPreviousCtc(std::string ctc);
	static std::map<std::string, std::vector<shared_ptr<Contact>>> updateCtc(std::multimap<std::string,shared_ptr<Contact>> previousCtc);//fonction devenue obsolette
	static std::string getOtherCtc(std::string first, shared_ptr<Contact> ctc);
	double durationOfContactWithInd(std::string colonized, std::vector<std::string> nonColonizedIndCat, time_t date1, time_t date2); //fonction devenue obsolette
	std::pair<double, std::vector<std::string>> durationOfContactWithInd2(std::string colonized, std::vector<std::string> nonColonizedIndCat, time_t date1, time_t date2);
	//prlvt
	std::multimap<std::string,std::vector<std::string>> buildPreviousPrlvt(std::string prlvt);
	std::map<std::string, int> findPositiveSwab(std::map<time_t, std::vector<std::string>> prlvtVector, string pathogen);
	//transmission probabilities
	std::map< std::string ,std::map <std::pair<std::string, std::string>, std::vector <double>>> computeProbabilityOfTransmission(bool probaByType);
	std::map< std::string ,std::map <std::pair<std::string, std::string>, std::map <double, std::vector <double>>>> computeProbabilityOfTransmission2(bool probaByType);
	std::map<time_t, std::string> traject(std::map<time_t, std::vector<std::string>> input_map, std::string pathogen);
	std::map<time_t, std::string> lissage(std::map<time_t, std::string> input_map, int sep);
	std::vector<time_t> findAcquisitionByIndividual(Individual* ind, std::map<time_t, std::string> input_map, int nbOfAcquisitionWeek);
	std::map<time_t, std::vector<Individual*>> findAllAcquisition(std::vector<Individual*> currentCarriersOfPathogen, std::string currentPathogen, int nbOfAcquisitionWeek);
	void buildPathogenProbabilities();
	//relatif aux individus
	std::vector<string> indPerWeek(time_t firstLimit, time_t lastLimit, std::map<time_t, std::vector<Individual*>> indPerDay);
	std::map<time_t, std::vector<Individual*>> computeIndPerDay();
	void buildMeIndividuals();
	std::vector<std::string> returnNameInString(std::vector<Individual*> input_vector);
	std::vector<Individual*> returnObjectWithName(std::vector<std::string> input_vector);
	std::map<std::string, int> nbOfIndividualStart();
	std::vector<Individual*> individualAtBeginDate(std::string date1);
	int addToNewIndividuals(int nb, std::string type, std::string date1, int id, std::map<std::string, std::map<std::string, double>> LOS);
	//statistics
	double randomPoisson(double mean, boost::mt19937& rng);
	static double computeMean(const std::vector<double>& numbers);
	static double computeSampleVariance(const double mean, const std::vector<double>& numbers);
	std::map<std::string, std::map< std::string, std::pair<double, double>>> findMeanAndSdDayOfWeek();
	//other
	std::pair<std::map<std::string, std::vector<Individual*>>, std::map<Individual*, std::map<time_t, std::vector<std::string>>>> updateMap2(std::vector<std::string>listOfAllPath);	
	void resultParameters2(bool write, std::string fileNameResult, std::vector<std::string> parameters);
	std::map<std::string, std::map<std::string, std::pair<double, double>>> durationParameters2();
	std::map<std::string, std::vector<int>> findDurationByIndividual(std::map<time_t, std::string> input_map, int nbOf, bool optionDuration);
	std::map<std::string, std::map<std::string, std::vector<int>>> findAllDuration(std::vector<Individual*> currentCarriersOfPathogen, std::string currentPathogen, int nbOfAcquisitionWeek);
	std::map<std::string, std::vector<std::string>> findColonizedStatusByDate(time_t date1, time_t date2, std::vector<std::string> indWeek,std::vector<std::string> indThisWeek, std::string pathogen);
	std::string generateRandomLetters(int nb);
	void addMapInTheFile(std::map<std::string, std::map<std::string, double>> input_map, ofstream& myCSV, std::string type);
	void addMapInTheFile(std::map<std::string, std::map<time_t,double>> input_map, std::ofstream& myCSV, std::string type);
	void addMapInTheFile(std::map<std::string, double> input_map, std::ofstream& myCSV, std::string type);
	int addToTheCSVFile(std::vector<Individual*> input_vector, std::map<Individual*, std::vector<std::pair<std::string, std::string>>> admissionDischargeStock, std::string date1, std::string date2, ofstream& myCSV);
	bool FileExists(const std::string& name);
	void sensibilityON(std::string typeOfModification_, std::string natureOfModification_, int numberOfModification_);
	
	/* GETTER */
	std::map<std::string, std::map<std::string, std::pair<double, double>>> getDaysOfWeekParameters();
	std::map<std::string, std::map<std::string, std::vector<double>>> getDistributionDuration();
	std::map<std::string, std::map<std::string, std::pair<double, double>>> getDurationOfColonization();
	std::map<std::string, std::map<std::pair<std::string, std::string>, double>> getPathogenProbabilityOfTransmission();
	std::map<std::string, double> getDischargeProba();
	std::map<std::string, double> getAdmissionProba();
	std::map<std::string, std::map<std::string, double>> getColonizedAtAdmissionParam();
	std::map<std::string, std::map<std::string, double>> getDischargeDistributionParam(); 
	std::map<std::string, std::map<std::string, double>> getAdmissionDistributionParam();
	bool getSensibilityStatus(){return sensibilityStatus;}
	/* SETTER */
	bool setSensibilityStatus(bool sensibBool){sensibilityStatus = sensibBool;}
	std::map<std::string, map<std::pair<std::string, std::string>, double>> meanMapPathogen(std::map<std::string ,std::map <std::pair<std::string, std::string>, std::vector<double>>> inputMap, int seuil, bool week, int nbOfWeekPA, int nbOfWeekPE, bool probaByType);
	bool indPrelThisWeek(std::string ind, time_t date1, time_t date2);
	
	std::map<std::string, int> findOverlap(std::vector<std::string> indAcquisitions, std::map<std::string,std::vector<std::string>> mapColonized, time_t date1, time_t date2);
	
	std::string catOfTheIndST(std::string ind, bool type);
	std::string colonizationStatusByDatesInd(time_t date1, time_t date2, std::string ind, std::string pathogen);
	std::map<std::string, std::map<std::string, double>> findIndInContactByDate(time_t date1, time_t date2, std::string ind, std::string pathogen, bool probaByType);
	std::map<std::string ,std::map <std::pair<std::string, std::string>, std::map <double, std::vector <double>>>> computeProbabilityOfTransmission3(bool probaByType);
	std::string checkLastPrlvt(std::string ind, time_t dateSeuil, std::string pathogen, int weel);
	
	bool checkPreviousColonization(std::string ind, time_t date1, std::string pathogen);
	
	
};

template<typename TK, typename TV>
std::vector<TK> extract_keys(std::map<TK, TV> const& input_map) {
  std::vector<TK> retval;
  retval.reserve(input_map.size());
  for (auto const& element : input_map) {
    retval.push_back(element.first);
  }
  return retval;
}

template<typename T>
std::vector<T> uniqueVector(std::vector<T> input_vector){
	std::set<T> s;
	unsigned size = input_vector.size();
	for( unsigned i = 0; i < size; ++i ) s.insert( input_vector[i] );
	input_vector.assign( s.begin(), s.end() );
	return input_vector;
}

template<typename TK, typename TV, typename TY>
std::map<TK, map<TV, TY>> meanMap(std::map<TK ,std::map <TV, std::vector<TY>>> inputMap, int seuil, bool week = false, int nbOfWeek = 0){
	map<TK, map<TV, TY>> resFinal;
	for(auto const& [key,val]:inputMap){
		map<TV, vector<TY>> mapDuration = val;
		TY sum;
		TY mean;
		map<TV, TY> res;
		for(auto const& [cat, allNumbers]:mapDuration){
			vector<int> testSeuilVector;
			transform(allNumbers.begin(), allNumbers.end(), back_inserter(testSeuilVector),[](TY n){ return(n > 0 ? 1 : 0); });
			int testSeuil = accumulate(testSeuilVector.begin(), testSeuilVector.end(),0);
			if(testSeuil >= seuil){
				TY sum = accumulate(allNumbers.begin(), allNumbers.end(),0.0);
				int dividedNumber;
				if(week){
					dividedNumber = nbOfWeek;
				}else{
					dividedNumber = allNumbers.size();
				}
				if(sum!=0){
					cout << " sum " << sum << " / " << dividedNumber << " mais avant allNumbers.size() " << allNumbers.size() << endl;
					TY mean = sum / dividedNumber;
					res.insert(make_pair(cat, mean));
				}
			}
		}
		resFinal.insert(make_pair(key, res));
	}
	return resFinal;
}

template<typename TK, typename TV, typename TY>
std::map<TK, map<TV, std::vector<TY>>> meanMap(std::map<TK ,std::map <TV, std::map <TY, std::vector<TY>>>> inputMap, int seuil, bool week = false, int nbOfWeek = 0){
	map<TK, map<TV, std::vector<TY>>> resFinal;
	for(auto const& [key,val]:inputMap){
		map<TV, map<TY, vector<TY>>> mapDuration = val;
		TY sum;
		TY mean;
		map<TV, std::vector<TY>> res;
		for(auto const& [cat, mapAllNumbers]:mapDuration){
			map<TY, vector<TY>> resMapAllNumbers = mapAllNumbers;
			for(auto const& [range, allNumbers]:resMapAllNumbers){
				vector<int> testSeuilVector;
				transform(allNumbers.begin(), allNumbers.end(), back_inserter(testSeuilVector),[](TY n){ return(n > 0 ? 1 : 0); });
				int testSeuil = accumulate(testSeuilVector.begin(), testSeuilVector.end(),0);
				if(testSeuil >= seuil){
					TY sum = accumulate(allNumbers.begin(), allNumbers.end(),0.0);
					int dividedNumber;
					if(week){
						dividedNumber = nbOfWeek;
					}else{
						dividedNumber = allNumbers.size();
					}
					if(sum!=0){
						cout << " sum " << sum << " / " << dividedNumber << " mais avant allNumbers.size() " << allNumbers.size() << endl;
						TY mean = sum / dividedNumber;
						// res.insert(make_pair(cat, mean));
						res[cat].push_back(mean);
					}else{
						res[cat].push_back(0.0);
					}
				}
			}
		}
		resFinal.insert(make_pair(key, res));
	}
	return resFinal;
}
#endif 