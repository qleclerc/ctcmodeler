#!/usr/bin/env bash
#SBATCH -n 6
#SBATCH --ntasks-per-node=6
#SBATCH -N 1
#SBATCH --mem-per-cpu=400
#SBATCH -o slurm/test/test_%A-%a.out # STDOUT
#SBATCH -e slurm/error/err_%A-%a.err # STDERR
#SBATCH --time=00:45:00


# on s'assure d'avoir un environnement propre
module purge

# on charge les module necessaire au le script
module load gcc/9.2.0
module load openmpi/4.0.5
module load netcdf/4.7.3
module load boost/1.72.0
module load repast_hpc/2.3.0

OS=""
BL=""
FA=""
NA=""
MODE=""
BT=""
LPAPA=""
LPAPE=""
LPEPA=""
LPEPE=""
NCI=""
nb_iteration=""
NCTC=""
SEED=""
SCG=""
SCT=""
directoryPlus=""
NBSUPERS=""

while getopts o:b:f:n:t:a:e:i:m:z:d:h:s:g:l:r:u:v:w:q:j:k:x: option
do
case "${option}"
in
a) SCG=${OPTARG};;
b) BL=${OPTARG};;
#c
d) NCTC=${OPTARG};;
e) SCT=${OPTARG};;
f) FA=${OPTARG};;
g) FCTC=${OPTARG};;
h) SC=${OPTARG};;
i) nb_iteration=${OPTARG};;
j) AN=${OPTARG};;
k) SENSIB=${OPTARG};;
l) LF=${OPTARG};;
m) MODE=${OPTARG};;
n) NA=${OPTARG};;
o) OS=${OPTARG};;
#p
q) NBSUPERS=${OPTARG};;
r) RF=${OPTARG};;
s) SEED=${OPTARG};;
t) BT=${OPTARG};;
u) SN=${OPTARG};;
v) NSN=${OPTARG};;
x) directoryPlus=${OPTARG};;
w) SCO=${OPTARG};;
z) NCI=${OPTARG};; 

esac
done

if [[ -n $SLURM_ARRAY_TASK_ID ]]
then 
	nb_iteration=$SLURM_ARRAY_TASK_ID
	# SEED=$SLURM_JOB_ID
fi

echo $FCTC

SEED=$SLURM_JOB_ID

mpirun ./main.exe config.props model.props --nb_iteration $nb_iteration --optionSim $OS --mode $MODE --buildLearning $BL --firstAcq $FA --nbOfAcquisition $NA --byType $BT --nbCtcInf $NCI --directoryPlus $directoryPlus --newCtc $NCTC --scenario $SC --seed $SEED --fileCtc $FCTC --learningFile $LF --resultFile $RF --groupFile $SN --nbGroupFile $NSN --interventionType $SCO --nbSupers $NBSUPERS --sensibSeq $SENSIB --acquisition_name $AN --SCG $SCG --SCT $SCT



