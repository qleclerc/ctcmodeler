/* INTERVENTIONS CLASS
* this file gather all organism from the project
*
*	
*	Interventions.cpp
*
*	Created on: april 22, 2019
*	Author: Audrey
*
*/
#include "repast_hpc/initialize_random.h"

#include "Interventions.h"

using namespace std;
using namespace repast;

Interventions::Interventions(){
	
}

Interventions::Interventions(string filenameInterventions){
	readInterventionsFromCSV(filenameInterventions);
}

Interventions::~Interventions(){
	
}

void Interventions::cohortingIntervention(FindContact* fContact, string fromStart, string filenameContact){
	cout << " c'est partie pour le cohorting " << endl;
	// std::map<std::pair<std::string, std::string>, int> coupleToCohort = findCoupleToCohortFromCSV(filenameCohort);
	if(!cohortingCouple.empty()){
		fContact->setCoupleToCohort(cohortingCouple);
		cout << "les couples sont: " << endl;
		for(auto const& [key, val]:cohortingCouple){
			cout << key.first << " - " << key.second << endl;
		}
		fContact->createMeNewContact(fromStart, filenameContact, false, true);
	}else{
		cout<< "il y a un pb dans le fichier d'intervention " << endl;
	}
	
}

std::map<std::pair<std::string, std::string>, int> Interventions::findCoupleToCohortFromCSV(string filename){
	std::map<std::pair<std::string, std::string>, int> res;
	
	Filetreatment *f_cohort = new Filetreatment(filename);
	string line;
	std::ifstream ifs;
	ifs =  ifstream(f_cohort->getFile());
	delete f_cohort;
	if(!ifs.good()){
		cout<<"enable to open file "<< filename <<endl;
	}
	vector<string> tempo;
	getline(ifs,line); 
	while(getline(ifs, line)){
		boost::trim_if(line, boost::is_any_of("\r "));
		boost::split(tempo, line, boost::is_any_of(";"));
		if(tempo[0]!="cohorting"){
			pair <string, string> pairCouple = make_pair(tempo[1], tempo[2]);
			res[pairCouple] = stoi(tempo[3]);
		}
	}
	ifs.close();
	return(res);
}

void Interventions::readInterventionsFromCSV(string filename){
	std::map<std::pair<std::string, std::string>, std::map<std::string, int>> res;
	std::map<std::pair<std::string, std::string>, int> res2;
	std::map<std::string, int> res3;
	Filetreatment *f_cohort = new Filetreatment(filename);
	string line;
	std::ifstream ifs;
	ifs =  ifstream(f_cohort->getFile());
	delete f_cohort;
	if(!ifs.good()){
		cout<<"enable to open file " << filename <<endl;
	}
	vector<string> tempo;
	getline(ifs,line); 
	while(getline(ifs, line)){
		boost::trim_if(line, boost::is_any_of("\r "));
		boost::split(tempo, line, boost::is_any_of(";"));
		if(tempo[0]=="cohorting"){
			pair <string, string> pairCouple = make_pair(tempo[1], tempo[2]);
			res[pairCouple][tempo[3]] = stoi(tempo[4]);
		}
		if(tempo[0]=="compliance"){
			pair <string, string> pairCouple = make_pair(tempo[1], tempo[2]);
			res2[pairCouple] = stoi(tempo[3]);
		}
		if(tempo[0]=="vaccination"){
			pair <string, string> pairCouple = make_pair(tempo[1], tempo[2]);
			res2[pairCouple] = stoi(tempo[3]);
		}
		if(tempo[0]=="superContractor"){
			res3[tempo[1]] = stoi(tempo[2]);
		}
	}
	ifs.close();
	cohortingCouple = res;
	complianceCouple = res2;
	superContractors = res3;
}

void Interventions::modifyContact(bool homogeneous, FindContact* fContact, string fromStart, string filenameContact){
	// fContact->buildMapContact();
	// fContact->findMeContactProbabilities();
	if(homogeneous)fContact->setResProba(changeResProba(fContact->getResProba()));
	fContact->createMeNewContact(fromStart, filenameContact);
}


std::map<std::string, std::map<std::string, std::map<std::string, double>>> Interventions::changeResProba(std::map<std::string, std::map<std::string, std::map<std::string, double>>> input_map){
	std::map<std::string, std::map<std::string, std::map<std::string, double>>> res;
	//hour_week - condition - cat - nb
	//std::map<std::string, std::map<std::string, std::map<std::string, double>>>
	//on veut lisser les proba PE-PA et PA-PE de 6h à 19h pour avoir la même proba de contact avec les patients
	//1) on récupe toutes les proba entre 6h et 19h dans deux vecteurs
	map<string, map<string, vector<double>>> allPEPA;
	map<string, map<string, vector<double>>> allPAPE;
	for(auto const& [HW, map1]:input_map){
		string HWkey = HW;
		vector<string> hour_week;
		boost::split(hour_week, HWkey, boost::is_any_of("_"));
		if(stoi(hour_week[0])>=6 && stoi(hour_week[0])<=18){
			map<string, map<string, double>> resMap1 = map1;
			for(auto const& [cdt, map2]:resMap1){
				map<string, double> resMap2 = map2;
				for(auto const& [cat, prob]:resMap2){
					if(cat=="PE_PA"){
						allPEPA[hour_week[1]][cdt].push_back(prob);
					}
					if(cat=="PA_PE"){
						allPAPE[hour_week[1]][cdt].push_back(prob);
					}
				}
			}
		}	
	}
	
	//2) on va faire la mediane
	map<string, map<string, map<string, double>>> mediansToReplace;
	mediansToReplace["PE_PA"] = findMedian(allPEPA);
	mediansToReplace["PA_PE"] = findMedian(allPAPE);
	
	//3) on va remplacer dans findContact
	for(auto const& [HW, map1]:input_map){
		string HWkey = HW;
		vector<string> hour_week;
		boost::split(hour_week, HWkey, boost::is_any_of("_"));
		map<string, map<string, double>> resMap1 = map1;
		for(auto const& [cdt, map2]:resMap1){
			map<string, double> resMap2 = map2;
			for(auto const& [cat, prob]:resMap2){
				if((stoi(hour_week[0])>=6 && stoi(hour_week[0])<=18) && (cat=="PA_PE" || cat=="PE_PA")){
					res[HW][cdt][cat] = mediansToReplace[cat][hour_week[1]][cdt];
				}else{
					res[HW][cdt][cat] = input_map[HW][cdt][cat];
				}
			}
		}
		
	}
	
	return res;
	
}

map<string, map<string, double>> Interventions::findMedian(map<string, map<string, vector<double>>> input_map){
	map<string, map<string, double>> res;
	for(auto const& [WS, map1]:input_map){
		map<string, vector<double>> resMap1 = map1;
		for(auto const& [cdt, numbers]:map1){
			vector<double> resNB = numbers;
			double med = FindContact::computeMedian(resNB);
			res[WS][cdt] = med;
		}
	}
	return res;
}

vector<string> Interventions::findSuperContractors(FindContact* fContact, string option, string optSC){
	std::map<std::string,vector<shared_ptr<Contact>>> mapCtcI = fContact->getMapCtc();
	std::map<std::string, std::string> mapCatI = fContact->getMapCat();
	vector <string> super_contractor;
	try{
		if(!mapCtcI.empty()){
			map<string, string> mapOption;
			if(option=="type"){
				mapOption = fillMapTypeGeneralString(fContact);
			}else if(option=="cat"){
				mapOption = fillMapCatGeneralString(fContact, false);
			}else if(option=="catAll"){
				mapOption = fillMapCatGeneralString(fContact, true);
			}
			
			//from - day - to - duration
			map<string, map<string, map<string, int>>> mapInterm;
			for(auto const& [from, contacts]:mapCtcI){
				for(auto const& contact:contacts){
					string day = contact->getDay().substr(0,10);
					string to = contact->getTo();
					string other;
					if(to==from){
						other = contact->getFrom();
					}else{
						other = contact->getTo();
					}
					string length = contact->getLength();
					auto itTo = mapInterm[from][day].find(other);
					if(itTo!=mapInterm[from][day].end()){
						mapInterm[from][day][other] = mapInterm[from][day][other] + stod(length);
					}else{
						mapInterm[from][day][other] = stod(length);
					}
					auto itFrom = mapInterm[other][day].find(from);
					if(itFrom!=mapInterm[other][day].end()){
						mapInterm[other][day][from] = mapInterm[other][day][from] + stod(length);
					}else{
						mapInterm[other][day][from] = stod(length);
					}
				}
			}
			// cout << "mapInterm " << mapInterm.size() << endl;
			map<string, map<string, pair<double, double>>> mapIndividuals;
			map<string, vector<double>> allMeanNumbers;
			map<string, vector<double>> allMeanDurations;
			for(auto const& [from, mmap]:mapInterm){
				map<string, map<string, int>> mapInd = mmap;
				int nbOfDays = mapInd.size();
				double cumulativeNumber = 0.0;
				double cumulativeDuration = 0.0;
				for(auto const& [day, indDuration]:mapInd){
					map<string, int> mapIndDuration = indDuration;
					cumulativeNumber = cumulativeNumber + mapIndDuration.size();
					double durations = 0.0;
					for(auto const& [to, duration]:mapIndDuration){
						durations = durations +(double)duration;
					}
					double allDuration = durations/mapIndDuration.size();
					cumulativeDuration = cumulativeDuration + allDuration;
				}
				double meanNumber = cumulativeNumber / nbOfDays;
				double meanDuration = cumulativeDuration / nbOfDays;
				
				mapIndividuals[mapOption[from]][from] = make_pair(meanNumber, meanDuration);
				allMeanNumbers[mapOption[from]].push_back(meanNumber);
				allMeanDurations[mapOption[from]].push_back(meanDuration);
			}
			for(auto const& [opt, vct]:allMeanNumbers){
				vector<double> allMeanNumbers_vector = allMeanNumbers[opt];
				vector<double> allMeanDurations_vector = allMeanDurations[opt];
				map<string, pair<double, double>> mapIndividuals_map = mapIndividuals[opt];
				int tau = 90;
				int tauNB = 43;
				if(opt=="PA" && optSC=="duration"){
					//tauNB = 43;
					tauNB = 100;
				}else if(opt=="PA" && optSC=="contact") {
					//tauNB = 31;
					tauNB = 100;
				}else{
					//tauNB = 30;
					tauNB = 100;
				}
				//double seuil_nb = percentile(allMeanNumbers_vector, tau);
				//double seuil_duree = percentile(allMeanDurations_vector, tau);
				vector<string> bigIndNb;
				vector<string> bigIndLength;
				vector <string> super_contractor_interm;
				if(optSC=="contact"){
					// bigIndNb = findBigIndividuals(mapIndividuals_map, seuil_nb, true);
					// super_contractor_interm = bigIndNb;
					super_contractor_interm = bestByNumber(mapIndividuals_map, tauNB, true);
					
				}else if(optSC=="duration"){
					// bigIndLength = findBigIndividuals(mapIndividuals_map, seuil_duree, false);
					// super_contractor_interm = bigIndLength;
					super_contractor_interm = bestByNumber(mapIndividuals_map, tauNB, false);
					
				}else if (optSC=="both"){
					// bigIndNb = findBigIndividuals(mapIndividuals_map, seuil_nb, true);
					// bigIndLength = findBigIndividuals(mapIndividuals_map, seuil_duree, false);
					int tauNBc = 30;
					int tauNBd = 30;
					// if(opt=="PA"){
					// 	tauNBc = 32;
					// 	tauNBd = 43;
					// }
					bigIndNb = bestByNumber(mapIndividuals_map, tauNBc, true);
					bigIndLength = bestByNumber(mapIndividuals_map, tauNBd, false);
					sort(bigIndNb.begin(), bigIndNb.end());
					sort(bigIndLength.begin(), bigIndLength.end());
					set_union(bigIndNb.begin(), bigIndNb.end(), bigIndLength.begin(), bigIndLength.end(),  back_inserter(super_contractor_interm));
					
				}else if(optSC=="bothMin"){
					// map<double, string> bigIndNbMap = findBigIndividualsMap(mapIndividuals_map, seuil_nb, true);
					// map<double, string> bigIndLengthMap = findBigIndividualsMap(mapIndividuals_map, seuil_duree, false);
					// int lim1 = (int)((double)bigIndNbMap.size()/2);
					// int lim2 = (int)((double)bigIndLengthMap.size()/2);
					int tauNBc = 30;
					int tauNBd = 30;
					// if(opt=="PA"){
					// 	tauNBc = 32;
					// 	tauNBd = 54;
					// }
					
					map<double, string> bigIndNbMap = findIndividualsMap(mapIndividuals_map, true);
					map<double, string> bigIndLengthMap = findIndividualsMap(mapIndividuals_map, false);
					int lim1 = (int)((double)tauNBc/2);
					int lim2 = (int)((double)tauNBd/2);
					
					cout << "lim1 " << lim1 << " lim2 " << lim2 <<endl;
					map<double, string>::reverse_iterator itLim1 = bigIndNbMap.rbegin();
					map<double, string>::reverse_iterator itLim2 = bigIndLengthMap.rbegin();
					
					int parcours1 = 0;
					while(itLim1!=bigIndNbMap.rend() && parcours1< lim1){
						bigIndNb.push_back((*itLim1).second);
						parcours1 = parcours1 + 1;
						++itLim1;
					}
					int parcours2 = 0;
					while(itLim2!=bigIndLengthMap.rend() && parcours2< lim2){
						bigIndLength.push_back((*itLim2).second);
						parcours2 = parcours2 + 1;
						++itLim2;
					}
					sort(bigIndNb.begin(), bigIndNb.end());
					sort(bigIndLength.begin(), bigIndLength.end());
					set_union(bigIndNb.begin(), bigIndNb.end(), bigIndLength.begin(), bigIndLength.end(),  back_inserter(super_contractor_interm));
				}
				super_contractor.insert(super_contractor.end(), super_contractor_interm.begin(), super_contractor_interm.end());
				
			}
			
			return(super_contractor);
		}else{
			throw runtime_error(" mapCtc is empty ");
		}
	}catch(std::exception const& exc){
		cerr << exc.what() << "\n";
		exit(1);
	}
}

vector<string> Interventions::bestByNumber(map<string, pair<double, double>> mapIndividuals_map, int tauNB, bool BB){
	vector<string> res;
	map<double, string> bigIndLengthMap = findIndividualsMap(mapIndividuals_map, BB);
	map<double, string>::reverse_iterator itLim1 = bigIndLengthMap.rbegin();
	int parcours1 = 0;
	vector<string> EVC = {"PA-001-LAM", "PA-003-MEG", "PA-004-MAA", "PA-006-DEC", "PA-007-SIM", "PA-008-SLP", "PA-009-DEJ", "PA-011-REA", "PA-012-LEN", "PA-025-AIA", "PA-026-SEA", "PA-046-DIS", "PA-048-BLH", "PA-057-IBN", "PA-085-MET", "PA-090-JBA"};
	while(itLim1!=bigIndLengthMap.rend() && parcours1< tauNB){
		if(std::find(EVC.begin(), EVC.end(), (*itLim1).second) == EVC.end()){
			res.push_back((*itLim1).second);
			parcours1 = parcours1 + 1;
		}
		++itLim1;
	}
	return res;
}

vector<string> Interventions::findBigIndividuals(map<string, pair<double, double>> input_map, double seuil, bool nbBool){
	vector<string> res;
	for(auto const& [from, pairNB]:input_map){
		if(nbBool && pairNB.first>seuil){
			res.push_back(from);
		}else if(!nbBool && pairNB.second>seuil){
			res.push_back(from);
		}
	}
	return(res);
}

map<double, string> Interventions::findBigIndividualsMap(map<string, pair<double, double>> input_map, double seuil, bool nbBool){
	map<double, string> res;
	for(auto const& [from, pairNB]:input_map){
		if(nbBool && pairNB.first>seuil){
			res[pairNB.first] = from;
		}else if(!nbBool && pairNB.second>seuil){
			res[pairNB.second] = from;
		}
	}
	return(res);
}

map<double, string> Interventions::findIndividualsMap(map<string, pair<double, double>> input_map, bool nbBool){
	map<double, string> res;
	for(auto const& [from, pairNB]:input_map){
		if(nbBool){
			res[pairNB.first] = from;
		}else if(!nbBool){
			res[pairNB.second] = from;
		}
	}
	return(res);
}

double Interventions::percentile(std::vector<double> &vectorIn, double percent)
{
    sort(vectorIn.begin(), vectorIn.end());
	auto nth = vectorIn.begin() + (percent*vectorIn.size())/100;
    std::nth_element(vectorIn.begin(), nth, vectorIn.end());
    return *nth;
}

std::map<std::string, std::string> Interventions::fillMapCatGeneralString(FindContact* fContact, bool withHosp){
	vector<Individual*> input_vector = fContact->getAllIndividuals();
	map<string, string> mapCatGeneral;
	for(auto const& elem:input_vector){
		string cat = fContact->findMeCatGeneral(elem, withHosp);
		mapCatGeneral[elem->getCalc_ident()] = cat;
	}
	return(mapCatGeneral);
}

std::map<std::string, std::string> Interventions::fillMapTypeGeneralString(FindContact* fContact){
	map<string, string> mapCatGeneral;
	vector<Individual*> input_vector = fContact->getAllIndividuals();
	for(auto const& elem:input_vector){
		if(elem->getStatus()=="PE"){
			mapCatGeneral[elem->getCalc_ident()] = "PE";
		}else{
			mapCatGeneral[elem->getCalc_ident()] = "PA";
		}
	}
	return(mapCatGeneral);
}


