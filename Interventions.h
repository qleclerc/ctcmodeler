/* INTERVENTIONS CLASS
* this file gather all organism from the project
*
*	
*	Interventions.h
*
*	Created on: april 22, 2019
*	Author: Audrey
*
*/

#ifndef INTERVENTIONS_H_
#define INTERVENTIONS_H_

#include "FindContact.h"
#include "Individual.h"

#include <ctime>
#include <map>
#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <functional>
#include <numeric>
#include "repast_hpc/initialize_random.h"
#include <random>

#include <boost/algorithm/string.hpp>
#include <boost/random.hpp>
#include <boost/random/random_device.hpp>
#include <boost/math/distributions.hpp>
#include <boost/math/distributions/chi_squared.hpp>
#include <boost/random/gamma_distribution.hpp>

#include <stdio.h>
#include <time.h>
#include <vector>
#include <regex>

class Interventions{
	protected:
	std::map<std::pair<std::string, std::string>, std::map<std::string, int>> cohortingCouple;
	std::map<std::pair<std::string, std::string>, int> complianceCouple;
	std::map<std::string, int> superContractors;
	
	
	public:
	Interventions();
	Interventions(std::string filenameInterventions);
	~Interventions();
	void modifyContact(bool homogeneous, FindContact* fContact, std::string fromStart, std::string filenameContact);
	std::map<std::string, std::map<std::string, std::map<std::string, double>>> changeResProba(std::map<std::string, std::map<std::string, std::map<std::string, double>>> input_map);
	std::map<std::string, std::map<std::string, double>> findMedian(std::map<std::string, std::map<std::string, std::vector<double>>> input_map);
	std::map<std::pair<std::string, std::string>, int> findCoupleToCohortFromCSV(std::string filename);
	void cohortingIntervention(FindContact* fContact, std::string fromStart, std::string filenameContact);
	void readInterventionsFromCSV(std::string filename);
	
	std::map<std::pair<std::string, std::string>, std::map<std::string, int>> getCohortingCouple(){return cohortingCouple;}
	std::map<std::pair<std::string, std::string>, int> getComplianceCouple(){return complianceCouple;}
	std::map<std::string, int> getSuperContractors(){return superContractors;}
	double percentile(std::vector<double> &vectorIn, double percent);
	std::vector<std::string> findSuperContractors(FindContact* fContact, std::string option, std::string optSC);
	std::vector<std::string> findBigIndividuals(std::map<std::string, std::pair<double, double>> input_map, double seuil, bool nbBool);
	std::map<double, std::string> findIndividualsMap(std::map<std::string, std::pair<double, double>> input_map, bool nbBool);
	std::map<double, std::string> findBigIndividualsMap(std::map<std::string, std::pair<double, double>> input_map, double seuil, bool nbBool);
	std::map<std::string, std::string> fillMapCatGeneralString(FindContact* fContact, bool withHosp);
	std::map<std::string, std::string> fillMapTypeGeneralString(FindContact* fContact);
	std::vector<std::string> bestByNumber(std::map<std::string, std::pair<double, double>> mapIndividuals_map, int tauNB, bool BB);

};



#endif