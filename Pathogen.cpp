/* Pathogen
* 
*
*	
*	Pathogen.cpp
*
*	Created on: Feb 22, 2018
*	Author: Audrey
*
*/
#include "Pathogen.h"

#include "repast_hpc/io.h"
#include "repast_hpc/logger.h"
#include "repast_hpc/initialize_random.h"
#include "repast_hpc/Utilities.h"
#include "repast_hpc/NetworkBuilder.h"
#include "repast_hpc/SVDataSetBuilder.h"

#include <boost/lexical_cast.hpp>
#include <boost/mpi.hpp>
#include <boost/mpi/collectives.hpp>

#include <boost/algorithm/string.hpp>
#include <iostream>  
#include <iterator>
#include <sstream>
#include <cstdlib>
#include <algorithm>
#include <stdexcept>
#include <math.h>
#include <vector>
#include <iomanip>

BOOST_CLASS_EXPORT(Bacteria)

using namespace std;
using namespace repast;

/* Phenotype */
Phenotype::Phenotype(){
	
}

Phenotype::~Phenotype(){
	
}

Phenotype::Phenotype(std::string liste, std::string atb){
	atbString = atb; 
	for(int i(0); i < atb.size(); i++){
		std::string s;
		s.push_back(atb[i]);
		listPhenot.push_back(s);
	}	
	boost::split(listATB,liste , boost::is_any_of("-"),boost::token_compress_on);
}

Phenotype::Phenotype(vector<string>listATB_, vector<string>listPhenot_): listATB{move(listATB_)}, listPhenot{move(listPhenot_)}{
	for(int i(0); i<listPhenot.size();i++){
		atbString = atbString + listPhenot[i];
	}
}

Phenotype::Phenotype(Phenotype const& phenotypeCopy):listATB{move(phenotypeCopy.listATB)}, listPhenot{move(phenotypeCopy.listPhenot)}, atbString(phenotypeCopy.atbString){

}

/* Pathogen */

Pathogen::Pathogen(){
	
}

Pathogen::Pathogen(repast::AgentId id):Organism(id){
	
}

Pathogen::Pathogen(repast::AgentId id, std::string typePath_):Organism(id), typePath(typePath_){
	
}

Pathogen::Pathogen(repast::AgentId id, std::string typePath_, std::map <std::string, std::string> positiveIndividuals_, std::vector <std::string> actualPositiveIndividuals_, std::map <std::pair<std::string, std::string>, std::vector <double> > probabilityOfTransmission_):Organism(id), typePath(typePath_),positiveIndividuals(positiveIndividuals_),actualPositiveIndividuals(actualPositiveIndividuals_), probabilityOfTransmission(probabilityOfTransmission_) {
	
}

Pathogen::Pathogen(std::string typePath_):typePath(typePath_){
	
}

Pathogen::Pathogen(Pathogen const& pathoCopy):Organism(pathoCopy.id_), typePath(pathoCopy.typePath),positiveIndividuals(pathoCopy.positiveIndividuals),actualPositiveIndividuals(pathoCopy.actualPositiveIndividuals), probabilityOfTransmission(pathoCopy.probabilityOfTransmission){
	
}

void Pathogen::affiche() const {
	
	cout<< "I'm alive! and I'm a Pathogen "  <<endl;
}

void Pathogen::removeIndFromActualPositiveIndividuals(std::string i){
	// cout << "hey d'abord il y a " << actualPositiveIndividuals.size() << " gens là dedans!"  << i << endl;
	// cout << " on est chez " << id_ << " ";
	// for(vector<std::string>::iterator itT = actualPositiveIndividuals.begin(); itT!=actualPositiveIndividuals.end(); itT++){
		// cout << (*itT) << " ";
	// }
	// cout<<endl;
	
	if(!actualPositiveIndividuals.empty()){
		if(actualPositiveIndividuals.size()==1){
			vector<std::string> vectToy;
			this->actualPositiveIndividuals = vectToy;
		}else{
			// this.actualPositiveIndividuals.erase(std::remove_if(this.actualPositiveIndividuals.begin(), this.actualPositiveIndividuals.end(),[i](std::string j) { return i==j; }),this.actualPositiveIndividuals.end() );
			vector<string>::iterator it = remove_if(this->actualPositiveIndividuals.begin(), this->actualPositiveIndividuals.end(), [i](std::string j) { return i==j; });
			this->actualPositiveIndividuals.erase(it, this->actualPositiveIndividuals.end());
		}
	}
	
}

bool  Pathogen::operator==(const Pathogen &b){
	if(this->typePath=="Bacteria" && b.typePath=="Bacteria"){
		Bacteria* b1 = dynamic_cast<Bacteria*>(this);
		const Bacteria &b2 = dynamic_cast<const Bacteria &>(b);
		if(b1->getFamily()== b2.getFamily() && b1->getGenus() == b2.getGenus() && b1->getSpecies() == b2.getSpecies() && b1->getPhenotypeClass().getAtbString() == b2.getPhenotypeClass().getAtbString()){
			return true;
		}else{
			return false;
		}
	}else{
		cout<< "les deux objets ne sont pas les mêmes, impossible de faire une comparaison"<<endl;
	}
}

void Pathogen::set(int _currentRank,std::string _typePath, std::map <std::string, std::string> _positiveIndividuals, std::vector <std::string> _actualPositiveIndividuals, std::map <std::pair<std::string, std::string>, std::vector <double> > _probabilityOfTransmission){
	id_.currentRank(_currentRank);
	typePath =_typePath;
	positiveIndividuals = _positiveIndividuals;
	actualPositiveIndividuals = _actualPositiveIndividuals;
	probabilityOfTransmission = _probabilityOfTransmission;

}

void Pathogen::addIndividual(std::string individual, std::string dateAdd){
	map<string,string>::iterator itMapB;
	itMapB = find_if(positiveIndividuals.begin(), positiveIndividuals.end(),[individual](std::pair<string, string> v) -> bool { return v.first==individual;} );
	if(itMapB!=positiveIndividuals.end()){
		// l'individu est dans la map de la bacterie on met à jour la date de dernière rencontre //
		(*itMapB).second = dateAdd;
		vector <std::string>::iterator it_actualInd = find_if(actualPositiveIndividuals.begin(), actualPositiveIndividuals.end(),[individual](std::string v) -> bool { return v ==individual;} );
		if(it_actualInd==actualPositiveIndividuals.end()){
			//il n'est pas dans actualPos
			this->addActualPositiveIndividuals(individual);
		}
		
	}else{
		// sinon c'est une acquisition donc on l'ajoute dans la map et dans le vecteur acquisition //
		this->addPositiveIndividuals(individual, dateAdd);
		this->addActualPositiveIndividuals(individual);
		/*map<string,string>::iterator itMapD = find_if(positiveIndividuals.begin(), positiveIndividuals.end(),[date](std::pair<string, string> v) -> bool { return v.second==date;} );
		if(itMapD!=positiveIndividuals.end()){
			// si la date est dedans c'est qu'il y a eu des acquisitions ce jour là donc on ajoute juste au vect
			bacteria->addActualPositiveIndividuals(ind);
		}else{
			vector<std::string> vPrlvt;
			vPrlvt.push_back(ind);
			bacteria -> setActualPositiveIndividuals(vPrlvt);
		}*/				
	}
}

void Pathogen::addActualPositiveIndividuals(std::string p3){
	vector<string>::iterator it = find(actualPositiveIndividuals.begin(), actualPositiveIndividuals.end(), p3);
	if(it==actualPositiveIndividuals.end()){
		actualPositiveIndividuals.push_back(p3);
	}
}

PathogenPackage::PathogenPackage(){
	
}

PathogenPackage::PathogenPackage(int _id, int _rank, int _type, int _currentRank, std::string _typePath, std::map <std::string, std::string> _positiveIndividuals, std::vector <std::string> _actualPositiveIndividuals, std::map <std::pair<std::string, std::string>, std::vector <double> > _probabilityOfTransmission): 
OrganismPackage(_id, _rank, _type, _currentRank), typePath(_typePath), positiveIndividuals(_positiveIndividuals), actualPositiveIndividuals(_actualPositiveIndividuals), probabilityOfTransmission(_probabilityOfTransmission){ 

}

Pathogen::~Pathogen(){
	
}

/* Bacteria */

Bacteria::Bacteria(){
	
}

Bacteria::~Bacteria(){
	
}

Bacteria::Bacteria(std::string typePath_, std::string family_, std::string genus_, std::string species_, std::string spatype_, std::string BMR_): Pathogen(typePath_),family(family_), genus(genus_),species(species_), spatype(spatype_), BMR(BMR_){
	stringId = typePath + "_" + family + "_" + genus + "_" + species + "_" + spatype + "_" + BMR;
}

Bacteria::Bacteria(std::string typePath_,std::string family_, std::string genus_, std::string species_, std::string spatype_, std::string BMR_,Phenotype sequenceATB_): Pathogen(typePath_), family(family_), genus(genus_),species(species_),spatype(spatype_), BMR(BMR_), sequenceATB(sequenceATB_){
	stringId = typePath + "_" + family + "_" + genus + "_" + species + "_" + spatype + "_" + BMR + "_" + sequenceATB.getAtbString();
}

Bacteria::Bacteria(repast::AgentId id, std::string typePath_,std::string family_, std::string genus_, std::string species_, std::string spatype_, std::string BMR_, Phenotype sequenceATB_): Pathogen(id, typePath_), family(family_), genus(genus_),species(species_), spatype(spatype_), BMR(BMR_),sequenceATB(sequenceATB_){
	stringId = typePath + "_" + family + "_" + genus + "_" + species + "_" + spatype + "_" + BMR + "_" + sequenceATB.getAtbString();
}

Bacteria::Bacteria(repast::AgentId id, std::vector<std::string> v, std::string listATB): Pathogen(id, v[0]), family(v[1]), genus(v[2]),species(v[3]), spatype(v[4]), BMR(v[5]){
	Phenotype p = Phenotype(listATB, v[6]);
	sequenceATB = p;
	stringId = typePath + "_" + family + "_" + genus + "_" + species + "_" + spatype + "_" + BMR + "_" + sequenceATB.getAtbString();
}

Bacteria::Bacteria(repast::AgentId id, std::string typePath_, std::map <std::string, std::string> positiveIndividuals_, std::vector <std::string> actualPositiveIndividuals_, std::map <std::pair<std::string, std::string>, std::vector <double> > probabilityOfTransmission_, std::string family_, std::string genus_, std::string species_, std::string spatype_, std::string BMR_ ,Phenotype sequenceATB_): Pathogen(id, typePath_, positiveIndividuals_,actualPositiveIndividuals_, probabilityOfTransmission_), family(family_), genus(genus_),species(species_), spatype(spatype_), BMR(BMR_),sequenceATB(sequenceATB_){
	stringId = typePath + "_" + family + "_" + genus + "_" + species + "_" + spatype + "_" + BMR + "_" + sequenceATB.getAtbString();
}

Bacteria::Bacteria(Bacteria const& bactCopy):Pathogen(bactCopy.id_, bactCopy.typePath, bactCopy.positiveIndividuals, bactCopy.actualPositiveIndividuals, bactCopy.probabilityOfTransmission), family(bactCopy.family), genus(bactCopy.genus),species(bactCopy.species), spatype(bactCopy.spatype), BMR(bactCopy.BMR),sequenceATB(bactCopy.sequenceATB){
	stringId = typePath + "_" + family + "_" + genus + "_" + species + "_" + spatype + "_" + BMR + "_" + sequenceATB.getAtbString();
}

// bool Bacteria::isEqual(const Bacteria* b) const{
	// if(family== b->family && genus == b->genus && species == b->species && sequenceATB.getListPhenot() == b->sequenceATB.getListPhenot()){
		// return true;
	// }else{
		// return false;
	// }
// }

bool Bacteria::isEqual(const Bacteria &b) const{

	if(family== b.family && genus == b.genus && species == b.species && spatype == b.spatype && BMR == b.BMR && sequenceATB.getAtbString() == b.sequenceATB.getAtbString()){
		return true;
	}else{
		return false;
	}
}

bool Pathogen::isEqual(vector<string> tmp){
	Bacteria* m1 = dynamic_cast<Bacteria*>(this);
	if(m1->getFamily() == tmp[0] && m1->getGenus() == tmp[1] && m1->getSpecies() == tmp[2] && m1->getSpatype() == tmp[3] && m1->getBMR() == tmp[4] && m1->getPhenotype() == tmp[5]){
		return true;
	}else{
		return false;
	}
}

// bool  Bacteria::operator==(const Bacteria &b){
	// return this->isEqual(b);
// }

void Bacteria::affiche() const {
	
	cout<< "I'm alive! and I'm a Bacteria "<< family <<" "<< genus << " "<< species << " " << spatype << " " << sequenceATB.getAtbString()<<endl;
}

void Bacteria::affiche2() const {
	
	cout<< family <<" "<< genus << " "<< species << " "<< BMR << " " << spatype << " " << sequenceATB.getAtbString()<<endl;
}

void Bacteria::set(int _currentRank,std::string _typePath, std::map <std::string, std::string> _positiveIndividuals, std::vector <std::string> _actualPositiveIndividuals, std::map <std::pair<std::string, std::string>, std::vector <double> > _probabilityOfTransmission, std::string _family, std::string _genus, std::string _species, std::string _spatype, std::string _BMR, Phenotype _sequenceATB){
	id_.currentRank(_currentRank);
	typePath =_typePath;
	positiveIndividuals = _positiveIndividuals;
	actualPositiveIndividuals = _actualPositiveIndividuals;
	probabilityOfTransmission = _probabilityOfTransmission;
	family =_family;
	genus = _genus;
	species = _species;
	spatype = _spatype;
	BMR = _BMR;
	sequenceATB = _sequenceATB;
	stringId = _typePath + "_" + _family + "_" + _genus + "_" + _species + "_" + _spatype + "_" + _BMR + "_" + _sequenceATB.getAtbString();

}

BacteriaPackage::BacteriaPackage(){
	
}

BacteriaPackage::BacteriaPackage(int _id, int _rank, int _type, int _currentRank, std::string _typePath, std::map <std::string, std::string> _positiveIndividuals, std::vector <std::string> _actualPositiveIndividuals, std::map <std::pair<std::string, std::string>, std::vector <double> > _probabilityOfTransmission, std::string _family, std::string _genus, std::string _species, std::string _spatype, std::string _BMR,Phenotype _sequenceATB): 
PathogenPackage(_id, _rank, _type, _currentRank, _typePath, _positiveIndividuals, _actualPositiveIndividuals, _probabilityOfTransmission), family(_family), genus(_genus), species(_species), spatype(_spatype), BMR(_BMR),sequenceATB(_sequenceATB){ 

}

