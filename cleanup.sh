#!/usr/bin/env bash
#SBATCH -N 1
#SBATCH --mem=100
#SBATCH -o slurm_simulation/test/test_%A-%a.out # STDOUT
#SBATCH -e slurm_simulation/error/err_%A-%a.err # STDERR
#SBATCH --time=01:30:00

# on s'assure d'avoir un environnement propre
module purge

rm -r output/acquisition/complianceSC_100
rm -r output/acquisition/complianceSC_20
rm -r output/acquisition/vaccinationSC_100
rm -r output/acquisition/vaccinationSC_20
rm -r output/acquisition/complianceSC_PVS
rm -r output/acquisition/vaccinationSC_PVS
rm -r output/acquisition/complianceSC_double_duration
rm -r output/acquisition/complianceSC_half_duration
rm -r output/acquisition/vaccinationSC_double_duration
rm -r output/acquisition/vaccinationSC_half_duration
rm -r output/acquisition/complianceSC_double_transmission
rm -r output/acquisition/complianceSC_half_transmission
rm -r output/acquisition/vaccinationSC_double_transmission
rm -r output/acquisition/vaccinationSC_half_transmission

#rm -r slurm
#rm -r slurm_simulation
#rm -r output/interventions/random

mkdir output/acquisition/complianceSC_100
mkdir output/acquisition/complianceSC_20
mkdir output/acquisition/vaccinationSC_100
mkdir output/acquisition/vaccinationSC_20
mkdir output/acquisition/complianceSC_PVS
mkdir output/acquisition/vaccinationSC_PVS
mkdir output/acquisition/complianceSC_double_duration
mkdir output/acquisition/complianceSC_half_duration
mkdir output/acquisition/vaccinationSC_double_duration
mkdir output/acquisition/vaccinationSC_half_duration
mkdir output/acquisition/complianceSC_double_transmission
mkdir output/acquisition/complianceSC_half_transmission
mkdir output/acquisition/vaccinationSC_double_transmission
mkdir output/acquisition/vaccinationSC_half_transmission


#mkdir slurm
#mkdir slurm/test
#mkdir slurm/error
#mkdir slurm_simulation
#mkdir slurm_simulation/test
#mkdir slurm_simulation/error
#mkdir output/interventions/random