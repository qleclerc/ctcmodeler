<h1>Using contact network dynamics to implement efficient interventions against pathogen spread in hospital settings</h1>
<h2>Ctcmodeler code</h2>

**Quentin J Leclerc<sup>1,2,3</sup>, Audrey Duval<sup>1,2,3</sup>, Didier Guillemot<sup>1,2,4</sup>, Lulla Opatowski<sup>1,2</sup>, Laura Temime<sup>3,5</sup>**

1. Institut Pasteur, Université Paris Cité, Epidemiology and Modelling of Bacterial Escape to Antimicrobials (EMEA), 75015 Paris, France
1. INSERM, Université Paris-Saclay, Université de Versailles St-Quentin-en-Yvelines, Team Echappement aux Anti-infectieux et Pharmacoépidémiologie U1018, CESP, 78000 Versailles, France
1. Laboratoire Modélisation, Epidémiologie et Surveillance des Risques Sanitaires, Conservatoire National des Arts et Métiers, 75003 Paris, France
1. AP-HP, Paris Saclay, Department of Public Health, Medical Information, Clinical research, F-92380, Garches
1. Institut Pasteur, Conservatoire National des Arts et Métiers, Unité PACRI,  75015 Paris, France



Please refer to the [preprint](https://doi.org/10.1101/2023.12.08.23299666) for more information about this model and its use to simulate MRSA transmission across a detailed inter-individual contact network in a long-term care facility.


_**Please note that this project makes use of data previously collected as part of the i-Bird study. Full details are available in "Detailed Contact Data and the Dissemination of Staphylococcus aureus in Hospitals" (Obadia et al, PLOS Computational Biology 2015).**_


<h1>HOW TO</h1>

All necessary options are stored in **model.props** but some of these options are directly available for bash script purpose. All files needed and built must be csv file separated by ";". All parts of the program run indepedently. You can run them with bash script in a cluster.

To run it on the Institut Pasteur cluster, you first need to load the modules<br/>
<code>
module load gcc/7.2.0<br/>
module load openmpi/2.0.4<br/>
module load netcdf/4.2.1.1<br/>
module load boost/1_62_0<br/>
module load repast_hpc/2.2.0<br/></code>

and use the make file as<br/>
<code>make -j 8</code>


Minimum line code to run the ABM model
======================================
n is the number of processes you need (for example, in the i-Bird study we need 6 because we have 5 real wards and 1 artificial ward for mobile staff). Two files are necessary the **config.props** and **model.props**. The last one contains all the options to run the program.
<br/>
<code>mpirun -n 6 ./main.exe config.props model.props</code>

Running the agent-based model (ABM)
==================

Basic run
---------
To run one ABM simulation you need the command line option **--mode sim**, or **simuGameON=yes** inside the **model.props** file. You can provide a seed in option **--seed**. You can also run multiple simulations with bash and define the number of iterations as **--nb_iteration**. These last two options are not found in **model.props** and must be defined in command line in the bash script.
To run the ABM you can define multiple options inside **model.props**. All the following options are not necessary to run the ABM component.
<ul>
<li><b>pathogen.file</b> path of the pathogen file in which all pathogens of interest are listed</li>
<li><b>filenameAcquisition</b> path to the acquisition file </li>
<li><b>filenamePrevalence</b> path to the prevalence file </li>
<li><b>filenameStatusByDay</b> path to the status by day file </li>
<li><b>filenameSecondayCases</b> path to the status by day file </li>
<li><b>filenamePEreplacement</b> path to the status by day file </li>
<li><b>filenameManuportageTransmission</b> path to the status by day file </li>
<li><b>interventionsFile</b> path to the intervention file (this file is different according to the intervention type)</li>
<li><b>simuGameDates</b> first and last dates to run the simulation </li>
<li><b>tick.path</b> time step for contacts </li>
<li><b>units</b> time units for tick.path </li>
<li><b>ward.name</b> list of all wards used separated by "-"</li>
<li><b>usePreviousPrlvt</b> <i>yes</i> or <i>no</i> to use previous swab results as first pathogen inside individual at the beginning of each simulation</li>
<li><b>nbOfInitializedPatient</b> number of patients who are initially colonized or infected </li>
<li><b>nbOfInitializedStaff</b> number of staff who are initially colonized or infected </li>
<li><b>nbOfInitializedIndividual</b> number of patients and staff who are initially colonized or infected </li>
<li><b>nbOfInitializedIndividualEquiprob</b> must be <i>yes</i> or <i>no</i>. Determines if individuals should be initialised uniformally. If nbOfInitializedIndividual!=0 & Equiprob = yes then choose patients or staff, else if Equiprob = no choose from a Uniform distribution</li>
<li><b>cat.start</b> must be a sequence of hospital staff professions who will be colonised or infected at admission</li>
<li><b>hosp.start</b> same as cat.start but for patients and their reason for hospitalization</li>
<li><b>InitializedPatientAtAdmission</b> if <i>yes</i>, patients can have the pathogen at admission</li>
<li><b>InitializedStaffAtAdmission</b> if <i>yes</i>, staff can have the pathogen at admission</li>
<li><b>fixedColonization</b> must be <i>yes</i>, or <i>no</i> to add spontaneous colonization. If yes then patients can be infected or colonized at admission, and staff spontaneously each week</li>
<li><b>fixedColonizationNb</b> number of individuals who will be spontaneously colonised in a week</li>
<li><b>fixedColonizationStatus</b> must be <i>PA</i>, <i>PE</i>, <i>both</i> or <i>either</i></li>
<li><b>firstAcqOnly</b> must be <i>yes</i> or <i>no</i>, to take into account only the first acquisition when calculating incidence</li>
<li><b>nbOfAcquisitionWeek</b> number of previous weeks to take into account in the incidence definition</li>
<li><b>response</b> must be <i>month</i>, <i>week</i> or <i>day</i>. Time step for acquisition or prevalence</li>
<li><b>manualDuration</b> is taken into account if not equal to 0 in days</li>
<li><b>distributionProb</b> defines the type of distribution for the colonization, can be <i>Gamma</i>, <i>Exponential</i>, <i>Empiric distribution</i>, <i>LogNormal</i> or <i>Normal</i></li>
<li><b>probaByType</b> must be <i>yes</i> or <i>no</i> if probabilities are provided by type (PA or PE) or by hospital staff profession </li>
<li><b>durationByType</b> same as probaByType but for duration of colonization</li>
<li><b>diseaseStatus</b> must be <i>colonization</i> to only take into account the colonization (useful for bacteria) or <i>infection</i> if you take into account both colonization and infection. In this last case you need to define the natural history of the disease as well as all parameters needed (e.g. the incubation period) </li>
<li><b>incubationPeriod</b> lower and upper limits for the uniform distribution in which to pick up the incubation period, separated by "-" </li>
<li><b>incubationPeriodWithContagion</b> same as incubationPeriod </li>
<li><b>infectiousPeriod</b> same as incubationPeriod </li>
<li><b>immunization</b> must be <i>yes</i> or <i>no</i> to take into account an immunization period</li>
<li><b>immunizationDuration</b> mean duration of immunization, or infinite immunization if at
 "Inf"</li>
<li><b>immunizationVAR</b> variance parameter for immunization</li>
<li><b>asymptomatic</b> proportion of asymptomatic individuals, between 0 and 1</li>
<li><b>severityPA</b> proportion of severely ill patients, between 0 and 1</li>
<li><b>severityPE</b> proportion of severely ill staff, between 0 and 1</li>
<li><b>aleaLevel</b> if <i>yes</i> choose amongst incubation, incubationWithContagion, symptomatic, or asymptomatic as the first level of infection. If <i>no</i> choose incubation as the first level of infection</li>
<li><b>probaAdmissionIncubation</b> probability that an individual is in incubation period at admission </li>
<li><b>probaAdmissionIncubationWithContagionAsympto</b> probability that an individual is in incubation contagious asymptomatic period at admission </li>
<li><b>probaAdmissionIncubationWithContagionSympto</b> probability that an individual is in incubation contagious symptomatic period at admission </li>
<li><b>probaAdmissionAsymptomatic</b> probability that an individual is in asymptomatic period at admission </li>
<li><b>probaAdmissionSymptomatic</b> probability that an individual is in symptomatic period at admission </li>
<li><b>probaThreshold</b> to add a threshold in the probability of transmission. Probability of transmission during a contact depends on the duration of contact, and we can define a threshold for this duration (must be in seconds, or same units as the model) </li> 
<li><b>percentageFirstImmunizedPA</b> percentage of patiens who are immunized at the beginning of the simulation</li>
<li><b>percentageFirstImmunizedPE</b> same as percentageFirstImmunizedPA for hospital staff</li>
<li><b>admissionImmun</b> must be <i>yes</i> or <i>no</i> if individuals at admission are immunized or not </li>
<li><b>probaOfReplacement</b> probability of a staff to be replaced when symptomatic </li>
<li><b>probaOfIsolationPA</b> probability of a patient to be isolated when symptomatic </li>
<li><b>probaOfDeathPE</b> probability of death when infection is severe </li>
<li><b>probaOfDeathPA</b> probability of death when infection is severe </li>
<li><b>periodReanimation</b> parameters for a uniform distribution for reanimation period </li>
<li><b>periodDeath</b> time before death after beginning of severe disease </li>
<li><b>useParameters</b> must be <i>yes</i> or <i>no</i> to use the parameters file</li>
<li><b>swabPeople</b> must be <i>yes</i> or <i>no</i> to take into account the probability of swab date</li>
<li><b>returnPrevalence</b> must be <i>yes</i> or <i>no</i> to build a prevalence file </li>
<li><b>returnAcquisition</b> must be <i>yes</i> or <i>no</i> to build an acquisition file </li>
<li><b>returnSwab</b> must be <i>yes</i> or <i>no</i> </li>
<li><b>prevalenceByWard</b> <i>yes</i> or <i>no</i> provide prevalence by ward </li>
<li><b>returnStatusByDay</b> must be <i>yes</i> or <i>no</i> to build a status by day file </li>
<li><b>returnSecondaryCases</b> must be <i>yes</i> or <i>no</i> to build a secondary cases file </li>
<li><b>returnPEreplacement</b> must be <i>yes</i> or <i>no</i> to build a staff replacement file. Lists all staff that leave the simulation and are replaced </li>
<li><b>returnManuportageTransmission</b> must be <i>yes</i> or <i>no</i> to build a manuportage file that describe the manuportage history </li>
<li><b>secondaryIndexOnly</b> must be <i>yes</i> or <i>no</i> to build only the index cases in the secondary cases file </li>
<li><b>interventions</b> must be <i>yes</i> or <i>no</i> to use an implemented intervention</li>
<li><b>cohorting</b> must be <i>yes</i> or <i>no</i> to add a cohorting strategy</li>
<li><b>contactPrecaution</b> must be <i>yes</i> or <i>no</i> to add a contact precaution strategy</li>
<li><b>superContractorON</b> must be <i>yes</i> or <i>no</i> to target supercontactors</li>
<li><b>units</b> must be <i>sec</i>, <i>min</i> or <i>hour</i>. Unit time of the ABM</li>
</ul>
<code>mpirun -n 6 ./main.exe config.props model.props --mode sim --seed seed --contactFile pathToContactFile.csv --nb_iteration nbOfIteration --learningFile pathToParametersFile.csv --filenameALL nomFichier </code>


Interventions
------------
Three interventions are available in CTCmodeler: hospital staff cohorting, contact precautions, and vaccination. You can switch from one option to another with **--interventionType** command line option or with the **interventions=yes** as well as **cohorting=yes** or **contactPrecaution=yes**. The options **superContractorON=yes** or **findSuperContractor=yes** options in **model.props**<br/> allow you to control whether you are targeting supercontactors with the intervention.

1. **Cohorting**<br/>
>Cohorting strategy is based on a modification of the contact network, so you need to build a simulated contact network that takes into account the specificity of the cohorting (e.g. the number of patient that a group of hospital staff profession cares for) stored in an intervention file as below.<br/>

>| intervention | first           | second  | ward    | number |
>|--------------|:---------------:|:-------:|:-------:|:------:|
>| cohorting    | nurse           | Patient |  Ward 1 | 6      |
>| cohorting    | hospital porter | Patient |  Ward 1 | 18     |
>| cohorting    | Physician       | Patient |  Ward 1 | 8      |

<code>mpirun ./main.exe config.props model.props --mode sim --interventionType cohorting --seed seed --contactFile pathToContactFile.csv --filenameALL nomFichier --nb_iteration nbOfIteration --learningFile pathToParametersFile.csv --interventionsFile pathToInterventionFile.csv</code><br/>

2. **Contact precautions of hospital staff profession group**<br/>
>Contact precautions lead to a reduction of the transmission probabilities. It needs an Intervention file and runs directly within the ABM. The intervention file must include the fold-reduction in the probabilities of transmission. Below an exemple of a 2-fold reduction among three hospital staff professions.

>| intervention | first           | second  | number |
>|--------------|:---------------:|:-------:|:------:|
>| compliance   | nurse           | Patient |2       |
>| compliance   | hospital porter | Patient |2       |
>| compliance   | Physician       | Patient |2       |


<code>mpirun ./main.exe config.props model.props --mode sim --interventionType compliance --interventionsFile pathToInterventionFile.csv --contactFile pathToContactFile.csv --filenameALL nomFichier --nb_iteration nbOfIteration --seed seed --learningFile pathToParametersFile.csv</code>

3. **Vaccination of hospital staff profession group**<br/>
>Vaccination only reduces acquisition risk, but from all sources. It needs an Intervention file and run directly within the ABM. The intervention file must include the fold-reduction in the probabilities of transmission. Below an exemple of a 2-fold reduction among three hospital staff professions.

>| intervention | first           | second  | number |
>|--------------|:---------------:|:-------:|:------:|
>| vaccination  | nurse           | Patient |2       |
>| vaccination  | hospital porter | Patient |2       |
>| vaccination  | Physician       | Patient |2       |


<code>mpirun ./main.exe config.props model.props --mode sim --interventionType vaccination --interventionsFile pathToInterventionFile.csv --contactFile pathToContactFile.csv --filenameALL nomFichier --nb_iteration nbOfIteration --seed seed --learningFile pathToParametersFile.csv</code>

4. **Targeting supercontactors**<br/>
>Two types of supercontactors (SC) are available. Frequency-based SC represents people who have many contacts and Duration-based SC represents people who have long duration of contact. 
>>The first step is to identify them. To do so, you need to change the option **--interventionType findSupercontractor** and the program will find SC in the contact network. Because the simulated contact network does not take into account for SC, you need to search for SC inside the raw contact network. Options are available in the model.props file for SC.
>>+ **superContractorOption1** must be type, cat or catAll (to consider also the reason for hospitalization) 
>>+ **superContractorOption2** must be contact, duration or both. Returns Frequency-based SC or Duration-based SC or both
>>+ **superContractorOption3** must be both, PA or PE. Returns PA SC or PE SC or both
>>+ **nbOfSC** is the number of SC you want

>>All SC are stored in the intervention file with the transmission probabilities reduction associated as below

>>| superContractor   | name            |  number |
>>|-------------------|:---------------:|:-------:|
>>| superContractor   | ID-1            |2        |
>>| superContractor   | ID-2            |2        |
>>| superContractor   | ID-3            |2        |

><code>mpirun ./main.exe config.props model.props --mode contact --interventionType findSupercontractor --interventionsFile pathToInterventionFile.csv --contactFile pathToContactFile.csv --learningFile pathToParametersFile.csv</code>

>>The second step is the use the new intervention file inside the ABM<br/>

><code>./main.exe config.props model.props --mode sim --interventionType compliance --interventionsFile pathToInterventionFile.csv --contactFile pathToRawContact.csv  --filenameALL nomFichier --nb_iteration nbOfIteration --seed seed --learningFile pathToParametersFile.csv</code>
<br/>

Basic input
============
All input files must be in .csv with ";" as separation character, and must be in the following format

The admission file
------------------
|id  |status|firstDate|lastDate|ward|sex|hospitalization|cat
|:------:|:------:|:------:|:------:|:------:|:------:|:------:|:------:|
|PA-001-AAA |PA|2008-09-29|2010-02-11|ward 1|Masculin|EVC||
|PA-002-BBB |PA|2008-04-08|2009-11-10|ward 1|Masculin|Geriatrie||
|PE-001-AAA |PE|2009-04-01|2009-10-31|ward 2|Feminin| |IDE|

The contact file
-----------------
|from|to|date_posix|length|
|:----:|:----:|:----:|:----:|
|PA-001-AAA|PE-001-AAA|2009-07-01 00:07:00|450|
|PA-002-BBB|PA-001-AAA|2009-07-01 00:15:30|210|
|PE-001-AAA|PE-001-ABB|2009-07-01 00:20:00|270|

The pathogen.file
------------------

|type|family|genus|species|spatype|BMR|antibiotics|
|:----:|:----:|:----:|:----:|:----:|:----:|:----:|
|Bacteria|Coronaviridae|Alphaletovirus|Covid19|||

The swab file
-------------
This file is used to determine epidemiological parameters and to build the learning file.

|calc_ident|date_prl|origin|family|genus|species|spatype|BMR|antibiotics|
|:----:|:----:|:----:|:----:|:----:|:----:|:----:|:----:|:----:|
|PA-001-AAA|2009-05-04|Nasal|Staphylococcaceae|Staphylococcus|aureus||SARM||
|PA-001-AAA|2009-05-04|Tracheotomie|Staphylococcaceae|Staphylococcus|aureus||SARM||
|PA-001-AAB|2009-05-04|Gastrostomie|Staphylococcaceae|Staphylococcus|aureus||SARM||

The staff schedule file
------------------------
This file is used to determine contact parameters and to build the contact file.

|id|status|cat|ward|firstDate|lastDate|
|:----:|:----:|:----:|:----:|:----:|:----:|
|PE-001-AAA|PE|IDE|ward 1|2009-07-01 08:00:00|2009-07-01 15:00:00|
|PE-001-AAA|PE|IDE|ward 1|2009-07-02 08:00:00|2009-07-02 16:40:00|
|PE-001-AAB|PE|IDE|ward 1|2009-07-03 08:00:00|2009-07-03 14:00:00|


List of command line options
============================
<ul>
<li><b>--config</b> path to the repast configuration file</li>
<li><b>--props</b> path to the model properties file</li>
<li><b>--admissionFile</b> admission file path (optional)</li>
<li><b>--admissionParametersFile</b> admission parameters file path for admission simulation (optional)</li>
<li><b>--byType</b> select transmission probabilities by type if yes by cat if no (optional if enter in model.props)</li>
<li><b>--contactFile</b> contact file path (optional)</li>
<li><b>--contactParametersFile</b> contact parameters file path for contact simulation (optional)</li>
<li><b>--fileCtc</b> networks id if multiple networks, for random graph or graph with sensors bias (optional)</li>
<li><b>--filenameALL</b> file id for file (optional)</li>
<li><b>--fixedColonization</b> must be yes or no (optional)</li>
<li><b>--firstAcq</b> use the first acquisition if yes, use all acquisition if no(optional)</li>
<li><b>--groupFile</b> group file if byType is false </li>
<li><b>--iIND</b> specify the number of patient or staff that are infected at start. Must be PA, PE or both. If only PA, PE or both is entered, the number of initialized individual follows nbOfInitializedPatient, nbOfInitializedStaff or nbOfInitializedIndividual in model.props. Then if a number is given after '_' this number will be used to initialized individuals. For example, PA_5 will initialized only 5 patients but both_2_3 will initialized with 2 patients and 3 staffs (optional)</li>
<li><b>--iPA</b> specify the percentage of patients that are immunized at start (optional)</li>
<li><b>--iPE</b> specify the percentage of hospital staff that are immunized at start (optional)</li>
<li><b>--interventionsFile</b> specify file path of intervention characteristics (optional)</li>
<li><b>--interventionType</b> option cohorting compliance or supercontractor (optional)</li>
<li><b>--learningFile</b> specify learningfile in command line, but you can add it into model.props (optional)</li>
<li><b>--mode</b> <i>sim</i> for testing simulation only, <i>real</i> for testing real only, <i>sim&real</i> for both, <i>learning</i> for learning part only, <i>contact</i> for contact part, <i>contactWithoutProba</i> for contact part without computing probabilities (need a contactParametersFile), <i>admission</i> for admission part and <i>admissionWithoutProba</i> for admission without computing probabilities (need an admissionParametersFile) or <i>none</i> (optional)</li>
<li><b>--nbGroupFile</b> file that gather hospital staff profession into groups for sensibility analysis (optional)</li>
<li><b>--nbOfAcquisition</b> number of acquisition interval (optional)</li>
<li><b>--nb_iteration</b> number of iteration (optional)</li>
<li><b>--sensibSeq</b> string sequence with characteristics for sennsibility analysis, example 'duration_multiplication_2' means multiplication of duration of colonization by 2 (optional)</li>
<li><b>--seed</b> seed fot multiple execution (optional)</li>
</ul>

Parameters used and stored in the model.props file
--------------------------------------------------
- <b>pathogen.file</b> is needed and list all pathogen to investigate but currently working only with one pathogen for this version of CTCmodeler
- <b>nbOfInitializedPatient</b> or <b>nbOfInitializedStaff</b> to initialize all simulation with one patient or one staff infected
- <b>diseaseStatus</b> must be in infection state
- <b>incubationPeriod</b> parameters for the uniform distribution must be separated by "-"
- <b>incubationPeriodWithContagion</b> parameters for the uniform distribution must be separated by "-"
- <b>infectiousPeriod</b> parameters for the uniform distribution must be separated by "-"
- <b>immunization</b> must be yes
- <b>immunizationDuration</b> must be Inf or parameters for log-normal distribution
- <b>immunizationVAR</b> variance of the log-normal distribution (not really necessary if immunization at Inf)
- <b>asymptomatic</b> probability 
- <b>severityPA</b> probability
- <b>severityPE</b> probability
- <b>aleaLevel</b> if yes select the first state of admitted patient (i.e., incubation, incubation with contagion +- symptomatique, symptomatic or asymptomatic)
- <b>probaAdmissionIncubation</b> probability at admission if alea = yes
- <b>probaAdmissionIncubationWithContagionAsympto</b> probability at admission if alea = yes
- <b>probaAdmissionIncubationWithContagionSympto</b> probability at admission if alea = yes
- <b>probaAdmissionAsymptomatic</b> probability at admission if alea = yes
- <b>probaAdmissionSymptomatic</b> probability at admission if alea = yes
- <b>percentageFirstImmunizedPA</b> percentage of immunized patient at start
- <b>percentageFirstImmunizedPE</b> percentage of immunized staff at start
- <b>admissionImmun</b> must be yes to initialize with immunized individual
- <b>probaOfReplacement</b> probability of staff replacement, if a staff is replaced another staff come but with the same calc_ident and a new possibility of having SARS-CoV2 according to the admission way
- <b>probaOfIsolationPA</b> probability of a patient to be isolated
- <b>probaOfDeathPE</b> probability of death if severity
- <b>probaOfDeathPA</b> probability of death if severity
- <b>periodReanimation</b> parameters for uniform distribution for reanimation duration for patients
- <b>periodDeath</b> parameters for duration until death if severity
- <b>returnSwab</b> must be no for coronavirus. Must be yes if you want acquisition and prevalence, but they depend on swab 
- <b>returnPrevalence</b> must be no for coronavirus
- <b>returnAcquisition</b> must be no for coronavirus
- <b>returnStatusByDay</b> yes if you want state of each individual as "-" if the individual is not currently present, "0" if susceptible, "1" if incubation, "2I" if incubation with contagion asymptomatic, "2II" same with symptomatic, "3I" if asymptomatic, "3II" if symptomatic, "4" if immunized. 
- <b>returnSecondaryCases</b> yes if you want the list of secondary cases. For each individual A, give the date and all individuals that receive the virus from A as well as the pathway (classical pathway from transmission probabilities or manuporting).
- <b>returnPEreplacement</b> yes if you want the list of replacement for staff but also the isolation of patients with their corresponding date and reason.
- <b>returnManuportageTransmission</b> yes if you want the list of manuporting event. For each individual A, give the list of all individuals that became in a manuporting event because of A.

Return the parameters file
==========================
The **--mode** option can switch to contact, learning or ABM part of the program. To activate the learning part with just the **model.props** file you just have to modify **buildLearning**=no in **buildLearning**=yes. The other options are also found in the **model.props** file in **contactFile** and **admissionFile**. It also need the **pathogen.file** in which all pathogen of interest are listed.
Multiple options define in model.props are necessary
<ul>
<li><b>learningDates</b> with first and last date to find parameters seperated by "_"</li>
<li><b>nbOfIndInitDate</b> first date to find parameters</li>
<li><b>nbOfAcquisitionWeekLearning</b> number of previous weeks to take into account in the definition of acquisition</li>
<li><b>firstAcqOnly</b> must be yes or no to take into account only the first acquisition of an individual in the equation of transmission probability</li>
<li><b>probaByType</b> must be yes or no to take into account type (patient or staff) or category (hospital staff profession)</li>
<li><b>durationByType</b> same as previous for duration of colonization parameters</li>
<li><b>optionDuration</b> must be long or short</li>
<li><b>groupFile</b> (optional) to gather profession into groups</li>
</ul>
<br/>
<code>mpirun -n 6 ./main.exe config.props model.props --mode learning --learningFile pathToParametersFile.csv --contactFile pathToContactFile.csv --admissionFile pathToAdmissionFile.csv</code>

Return a simulated contact file
===============================
You can have a simulated contact file with two options.
First, you don't have the contact parameters file (in which all contact rates needed to build a simulated contact file are stored). The program will build a contact parameters file with all parameters needed to build a simulated contact file with the option **contactParametersFile** as path for parameters file and **contactFile** option as path for the simulated contact file. It also need an admission file stored in **admissionFile**, the hospital staff schedule stored in **schedule** and the raw contact file stored in **contact**.
The **--mode contact** option is found in the **model.props** as **buildContact=yes** and **findContactParameters=yes**.<br/>
<code>mpirun -n 6 ./main.exe config.props model.props --mode contact --contactFile pathToContactFile.csv --contactParametersFile pathToContactParametersFile.csv</code>


Second, you have the contact parameters file. Only **--mode** change and then **--contactParametersFile** is the path to the file that will be used to read the contact rate. The **--mode contactWithoutProba** option is found in the **model.props** as **buildContact**=yes and **findContactParameters**=no.<br/>
<code>mpirun -n 6 ./main.exe config.props model.props --mode contactWithoutProba --contactFile pathToContactFile.csv --contactParametersFile pathToContactParametersFile.csv</code>


Return a admission file
=======================
You can have a simulated admission file with two options.
First, you don't have the admission parameters file (in which all admission rates needed to build a simulated admission file are stored). The program will build a admission parameters file with all parameters needed to build a simulated admission file with the option **admissionParametersFile** as path for parameters file and **admissionFile** option as path for the simulated admission file. It also need an contact file stored in **contactFile**, the hospital staff schedule stored in **schedule** and the raw admission file stored in **admission**.
The **--mode contact** option is found in the **model.props** as **buildAdmission=yes** and **buildADParameters=yes**.<br/>
<code>mpirun -n 6 ./main.exe config.props model.props --mode admission --admissionFile pathToAdmissionFile.csv --admissionParametersFile pathToAdmissionParametersFile.csv</code>

Second, you have the admission parameters file. Only **--mode** change and then **--admissionParametersFile**is the path to the file that will be used to read the admission rate. The **--mode admissionWithoutProba** option is found in the **model.props** as **buildAdmission=yes** and **buildADParameters=none**.<br/>
<code>mpirun -n 6 ./main.exe config.props model.props --mode admissionWithoutProba --admissionFile pathToAdmissionFile.csv --admissionParametersFile pathToAdmissionParametersFile.csv</code>

Run real scenario inside a hospital
=================================
The real option runs the program with real swab data and does not take into account probabilities and rates estimated from the data, but only the input files and hence directly recreates the real hospital data. The option **realGameON=yes** inside the **model.props** file or the command line option **--mode real** can provide this.
Options **filenameAcquisition**, **filenamePrevalence**, **filenameStatusByDay** are the paths in which the program returns the acquisition, prevalence and statusByDay results. The options to control whether or not to return these results are in **model.props**: **returnPrevalence** (yes or no), **returnAcquisition** (yes or no), **returnSwab** (yes or no). Be careful - the name of the contact, admission and swab files to be used with this option are stored in **contact**, **admission** and **prlvt** options in **model.props**. <br/>
<code>mpirun -n 6 ./main.exe config.props model.props --mode real --filenameALL nomFichier</code>

<br/><br/><br/>
