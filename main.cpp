/* MAIN
* main file of the project
*
*	
*	main.cpp
*
*	Created on: Nov 17, 2017
*	Last update: Jan 3, 2018
*	Author: Audrey
*
*/


#include "repast_hpc/RepastProcess.h"
#include "repast_hpc/logger.h"
#include "repast_hpc/Edge.h"
#include "repast_hpc/Utilities.h"

#include <boost/mpi.hpp>
#include <boost/serialization/export.hpp>
#include <boost/lexical_cast.hpp>
#include <vector>
#include <exception>
#include <regex>

#include <getopt.h>

#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <ctime>
#include <time.h>
#include <map>
#include "repast_hpc/Properties.h"

#include "Individual.h"
#include "Filetreatment.h"
#include "Model.h"
#include "Learning.h"
#include "FindContact.h"
#include "Interventions.h"

namespace mpi = boost::mpi;
using namespace repast;

using namespace std;

void usage() {
	std::cerr << "usage: X config properties" << std::endl;
	std::cerr << "command line options, you can find all this options into model.props" << std::endl;
	std::cerr << "  --config = the path to the repast configuration file"<< std::endl;
	std::cerr << "  --props = the path to the model properties file" << std::endl;
	std::cerr << "  --mode = sim for testing simulation only, real for testing real only or both for testing sim and real (optional)" << std::endl;
	std::cerr << "  --optionSim = yes or no for testing simulation (optional)" << std::endl;
	std::cerr << "  --buildLearning = yes or no to build learning file (optional)" << std::endl;
	std::cerr << "  --firstAcq = use the first acquisition if yes, use all acquisition if no(optional) (optional)" << std::endl;
	std::cerr << "  --nbOfAcquisition = number of acquisition interval (optional)" << std::endl;
	std::cerr << "  --byType = select transmission probabilities by type if yes by cat if no (optional)" << std::endl;
	std::cerr << "  --nbCtcInf = minimal number of infectious contact for probabilities determination (optional)" << std::endl;
	// std::cerr << "  --logPAPA = seuil for probabilities (optional)" << std::endl;
	// std::cerr << "  --logPAPE = seuil for probabilities (optional)" << std::endl;
	// std::cerr << "  --logPEPA = seuil for probabilities (optional)" << std::endl;
	// std::cerr << "  --logPEPE = seuil for probabilities (optional)" << std::endl;
	std::cerr << "  --seed = seed fot multiple execution" << std::endl;
	std::cerr << "  --nb_iteration = number of iteration (optional)" << std::endl;
	std::cerr << "  --scenario = scenario number for intervention if interventions is multiple (optional)" << std::endl;
	std::cerr << "  --newCtc = four options: build -> build contact; use -> use new contact; both -> build and use new contact; none -> for nothing (optional)" << std::endl;
	std::cerr << "  --groupFile = group file if byType is false " << std::endl;
	std::cerr << "  --interventionType = option cohorting compliance supercontractor vaccination or SCvaccination" << std::endl;
	std::cerr << "  --fileCtc = networks id if multiple networks (optional)" << std::endl;
	std::cerr << "  --learningFile = specify learningfile in command line, but you can add it into model.props (optional)" << std::endl;
	
}

bool checkAdmissionLength(string admin, string dateSim1, string dateSim2, string filenameAdmission, bool realIndStart){
	bool res = false;
	
	Learning* learn = new Learning();
	
	vector<vector<string>> tabAdmin = learn->buildTabAdmin(admin);
	time_t limitTime = Learning::stringToDate(dateSim2);
	time_t maxDate = Learning::stringToDate(dateSim1); //on met la date max au début de la simu
	for(auto const& line:tabAdmin){
		time_t date = Learning::stringToDate(line[2]);
		if(date > maxDate){
			maxDate = date;
		}
	}
	// cout << Learning::dateToString(maxDate) << endl;
	if(maxDate == Learning::stringToDate(dateSim1)){
		//pas du tout d'admission dispo pendant la période de simulation
		try{
			string response1;
			string response2;
			cout << " no admission data available during the simulation \n" << endl;
			cout << " do you want to simulate it? " << endl;
			cin >> response1;
			if(response1 == "yes"){
				cout << " do you have a parameters file? " << endl;
				cin >> response2;
				if(response2 == "yes"){
					string fileAD;
					cout << " Enter your file " << endl;
					cin >> fileAD;
					learn->buildADSimulation(fileAD, dateSim1, dateSim2, filenameAdmission, realIndStart);
				}else if(response2 == "no"){
					string response3;
					cout << " do you want me to build a new one? " << endl;
					cin >> response3;
					if(response3 == "yes"){
						res = true;
					}else if(response3 == "no"){
						// cout << " ok, see you soon ;) " << endl;
						throw string("ok, see you soon ;)");
						
					}else{
						// cout << " sorry I didn't understand " << endl;
						throw string("sorry I didn't understand");
					}
				}else{
					// cout << " sorry I didn't understand " << endl;
					throw string("sorry I didn't understand");
					
				}
			}else{
				// cout << " sorry I need admission data " << endl;
				throw string("sorry I need admission data");
				
			}
		}catch(string const& chaine){
			cerr << chaine << endl;
			exit(1);
		}
		
	}else{
		//il y a des admission dispo pendant la période de simulation 
	}
	delete learn;
	return res;
}


int main(int argc, char **argv) {
	mpi::environment env(argc, argv);
	mpi::communicator world;
	if(world.rank()==0){
		cout<<"................ WELCOME TO THE GAME ................"<<endl;
		cout<<"....................................................."<<endl;
		cout<<"..................MMMMMMMMMMMMMMMMM.................."<<endl;
		cout<<".................MMMMMM       MMMMMM................."<<endl;
		cout<<".................MMM     MMM     MMM................."<<endl;
		cout<<".................MMM   MMMMMMM   MMM................."<<endl;
		cout<<"..MNMMMMMMMMMMMM.MMM   MMMMMMM   MMM.MMMMMMMMMMMMMM.."<<endl;
		cout<<"..MMMMMMMMMMMMMM.MMM     MMM     MMM.MMMMMMMMMMMMMM.."<<endl;
		cout<<"..MMMMMMMMMMMMMM.MMMMMM       MMMMMM.MMMMMMMMMMMMMM.."<<endl;
		cout<<"..MMM   MM   MMM.MMMMMMMMMMMMMMMMMMM.MMM   MM   MMM.."<<endl;
		cout<<"..MMMMMMMMMMMMMM.MMMMMMMMMMMMMMMMMMM.MMMMMMMMMMMMMM.."<<endl;
		cout<<"..MMM   MM   MMM.MMMMMMMMMMMMMMMMMMM.MMM   MM   MMM.."<<endl;
		cout<<"..MMMMMMMMMMMMMM.MMMMMMMMMMMMMMMMMMM.MMMMMMMMMMMMMM.."<<endl;
		cout<<"..MMM   MM   MMM.MMMMMM       MMMMMM.MMM   MM   MMM.."<<endl;
		cout<<"..MMMMMMMMMMMMMM.MMMMMM       MMMMMM.MMMMMMMMMMMMMM.."<<endl;
		cout<<"..MMMMMMMMMMMMMM.MMMMMM       MMMMMM.MMMMMMMMMMMMMM.."<<endl;
		cout<<"..MMMMMMMMMMMMMM.MMMMMM       MMMMMM.MMMMMMMMMMMMMM.."<<endl;
		cout<<"..MMMMMMMMMMMMMM.MMMMMM       MMMMMM.MMMMMMMMMMMMMM.."<<endl;
		cout<<"MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM"<<endl;
		cout<<"MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMBYDREY"<<endl;
		cout<<"....................................................."<<endl;
		cout << "\n" << endl;
	}
	
	//-------------------------------------------- INITIALISATION -------------------------------------------//
	setenv("TZ", "", 1);
	tzset();
	std::string admin; //pour stocker le fichier d'entrée caractéristiques
	std::string ctc;
	std::string prlvt;
	std::string mode = "";
	std::string optimSimu = "";
	std::string buildLearning = "";
	std::string firstAcq = "";
	std::string nbOfAcquisition = "";
	std::string byType = "";
	std::string nbCtcInf = "";
	std::string iPA = "";
	std::string iPE = "";
	// std::string iIND = "";
	// std::string logPAPA = "";
	// std::string logPEPA = "";
	// std::string logPAPE = "";
	// std::string logPEPE = "";
	std::string nb_iteration = ""; 
	std::string newContact = "";
	std::string scenarioOption = "";
	std::string seed="";
	std::string fileCtc="";
	std::string learningFile="";
	std::string resultFile="";
	std::string groupFile="";
	std::string nbGroupFile="";
	std::string interventionType="";
	std::string nbSupers="";
	std::string sensibSeq="";
	std::string acquisition_name="";
	std::string SCG="";
	std::string SCT="";
	std::string directoryPlus="";
	string configFile = argv[1];
	string propsFile = argv[2];
	// std::string configFile = "-";
	// std::string propsFile = "-";
	
	static struct option long_options[] = {
		{"config",required_argument, 0,'c'},
		{"optionSim", optional_argument, 0, 'o'},
		{"props", required_argument, 0, 'p'},
		{"mode", optional_argument, 0, 'm'},
		{"buildLearning", optional_argument, 0, 'b'},
		{"firstAcq", optional_argument, 0, 'f'},
		{"nbOfAcquisition", optional_argument, 0, 'n'},
		{"byType", optional_argument, 0, 't'},
		{"nbCtcInf", optional_argument, 0, 'z'},
		// {"iPA", optional_argument, 0, 'a'},
		// {"iPE", optional_argument, 0, 'e'},
		{"directoryPlus", optional_argument, 0, 'x'},
		{"nb_iteration", optional_argument, 0, 'i'},
		{"newCtc", optional_argument, 0, 'd'},
		{"fileCtc", optional_argument, 0, 'g'},
		{"seed", optional_argument, 0, 's'},
		{"scenario", optional_argument, 0, 'h'},
		{"learningFile", optional_argument, 0, 'l'},
		{"resultFile", optional_argument, 0, 'r'},
		{"groupFile", optional_argument, 0, 'u'},
		{"nbSupers", optional_argument, 0, 'q'},
		{"nbGroupFile", optional_argument, 0, 'v'},
		{"interventionType", optional_argument, 0, 'w'},
		{"sensibSeq", optional_argument, 0, 'k'},
		{"acquisition_name", optional_argument, 0, 'j'},
		{"SCG", optional_argument, 0, 'a'},
		{"SCT", optional_argument, 0, 'e'},
		{0,0,0,0}
	};
	int opt = 0;
	int long_index =0;
    while ((opt = getopt_long_only(argc, argv,"", 
                   long_options, &long_index )) != -1) {
        switch (opt) {
            case 'c' : configFile = optarg;
				break;
			case 'p' : propsFile = optarg;
				break;
			case 'o' : 
				if(optarg==NULL && argv[optind]!=NULL && argv[optind][0]!='-'){
					if(world.rank()==0)printf("optimSimu: %s\n", argv[optind]);
					optimSimu = argv[optind];
					++optind;
				}else{
					if(world.rank()==0)printf("optimSimu: %s\n", optarg);
					if(optarg!=NULL)optimSimu = optind;
				}
				break;
			case 'm' : 
				if(optarg==NULL && argv[optind]!=NULL && argv[optind][0]!='-'){
					if(world.rank()==0)printf("mode: %s\n", argv[optind]);
					mode = argv[optind];
					++optind;
				}else{
					if(world.rank()==0)printf("mode: %s\n", optarg);
					if(optarg!=NULL)mode = optind;
				}
				break;
			case 'b' : 
				if(optarg==NULL && argv[optind]!=NULL && argv[optind][0]!='-'){
					if(world.rank()==0)printf("buildLearning: %s\n", argv[optind]);
					buildLearning = argv[optind];
					++optind;
				}else{
					if(world.rank()==0)printf("buildLearning: %s\n", optarg);
					if(optarg!=NULL)buildLearning = optind;
				}
				break;
			case 'f' : 
				if(optarg==NULL && argv[optind]!=NULL && argv[optind][0]!='-'){
					if(world.rank()==0)printf("firstAcq: %s\n", argv[optind]);
					firstAcq = argv[optind];
					++optind;
				}else{
					if(world.rank()==0)printf("firstAcq: %s\n", optarg);
					if(optarg!=NULL)firstAcq = optind;
				}
				break;
			case 'n' : 
				if(optarg==NULL && argv[optind]!=NULL && argv[optind][0]!='-'){
					if(world.rank()==0)printf("nbOfAcquisition: %s\n", argv[optind]);
					nbOfAcquisition = argv[optind];
					++optind;
				}else{
					if(world.rank()==0)printf("nbOfAcquisition: %s\n", optarg);
					if(optarg!=NULL)nbOfAcquisition = optind;
				}
				break;
			case 't' : 
				if(optarg==NULL && argv[optind]!=NULL && argv[optind][0]!='-'){
					if(world.rank()==0)printf("byType: %s\n", argv[optind]);
					byType = argv[optind];
					++optind;
				}else{
					if(world.rank()==0)printf("byType: %s\n", optarg);
					if(optarg!=NULL)byType = optind;
				}
				break;
			case 'z' : 
				if(optarg==NULL && argv[optind]!=NULL && argv[optind][0]!='-'){
					if(world.rank()==0)printf("nbCtcInf: %s\n", argv[optind]);
					nbCtcInf = argv[optind];
					++optind;
				}else{
					if(world.rank()==0)printf("nbCtcInf: %s\n", optarg);
					if(optarg!=NULL)nbCtcInf = optind;
				}
				break;
			case 'a' : 
				if(optarg==NULL && argv[optind]!=NULL && argv[optind][0]!='-'){
					//if(world.rank()==0)printf("iPA: %s\n", argv[optind]);
					//iPA = argv[optind];
					if(world.rank()==0)printf("SCG: %s\n", argv[optind]);
					SCG = argv[optind];
					++optind;
				}else{
					//if(world.rank()==0)printf("iPA: %s\n", optarg);
					//if(optarg!=NULL)iPA = optind;
					if(world.rank()==0)printf("SCG: %s\n", optarg);
					if(optarg!=NULL)SCG=optind;
				}
				break;
			case 'e' : 
				if(optarg==NULL && argv[optind]!=NULL && argv[optind][0]!='-'){
					//if(world.rank()==0)printf("iPE: %s\n", argv[optind]);
					//iPE = argv[optind];
					if(world.rank()==0)printf("SCT: %s\n", argv[optind]);
					SCT = argv[optind];
					++optind;
				}else{
					//if(world.rank()==0)printf("iPE: %s\n", optarg);
					//if(optarg!=NULL)iPE = optind;
					if(world.rank()==0)printf("SCT: %s\n", optarg);
					if(optarg!=NULL)SCT=optind;
				}
				break;
			case 'x' : 
				if(optarg==NULL && argv[optind]!=NULL && argv[optind][0]!='-'){
					if(world.rank()==0)printf("directoryPlus: %s\n", argv[optind]);
					directoryPlus = argv[optind];
					++optind;
				}else{
					if(world.rank()==0)printf("directoryPlus: %s\n", optarg);
					if(optarg!=NULL)directoryPlus = optind;
				}
				break;
			case 'i' : 
				if(optarg==NULL && argv[optind]!=NULL && argv[optind][0]!='-'){
					if(world.rank()==0)printf("nb_iteration: %s\n", argv[optind]);
					nb_iteration = argv[optind];
					++optind;
				}else{
					if(world.rank()==0)printf("nb_iteration: %s\n", optarg);
					if(optarg!=NULL)nb_iteration = optind;
				}
				break;
			case 'd' : 
				if(optarg==NULL && argv[optind]!=NULL && argv[optind][0]!='-'){
					if(world.rank()==0)printf("newContact: %s\n", argv[optind]);
					newContact = argv[optind];
					++optind;
				}else{
					if(world.rank()==0)printf("newContact: %s\n", optarg);
					if(optarg!=NULL)newContact = optind;
				}
				break;
			case 's' : 
				if(optarg==NULL && argv[optind]!=NULL && argv[optind][0]!='-'){
					if(world.rank()==0)printf("seed: %s\n", argv[optind]);
					seed = argv[optind];
					++optind;
				}else{
					if(world.rank()==0)printf("seed: %s\n", optarg);
					if(optarg!=NULL)seed = optind;
				}
				break;
			case 'g' : 
				if(optarg==NULL && argv[optind]!=NULL && argv[optind][0]!='-'){
					if(world.rank()==0)printf("fileCtc: %s\n", argv[optind]);
					fileCtc = argv[optind];
					++optind;
				}else{
					if(world.rank()==0)printf("fileCtc: %s\n", optarg);
					if(optarg!=NULL)fileCtc = optind;
				}
				break;
			case 'h' : 
				if(optarg==NULL && argv[optind]!=NULL && argv[optind][0]!='-'){
					if(world.rank()==0)printf("scenario: %s\n", argv[optind]);
					scenarioOption = argv[optind];
					++optind;
				}else{
					if(world.rank()==0)printf("scenario: %s\n", optarg);
					if(optarg!=NULL)scenarioOption = optind;
				}
				break;
			case 'l' : 
				if(optarg==NULL && argv[optind]!=NULL && argv[optind][0]!='-'){
					if(world.rank()==0)printf("learningFile: %s\n", argv[optind]);
					learningFile = argv[optind];
					++optind;
				}else{
					if(world.rank()==0)printf("learningFile: %s\n", optarg);
					if(optarg!=NULL)learningFile = optind;
				}
				break;
			case 'u' : 
				if(optarg==NULL && argv[optind]!=NULL && argv[optind][0]!='-'){
					if(world.rank()==0)printf("groupFile: %s\n", argv[optind]);
					groupFile = argv[optind];
					++optind;
				}else{
					if(world.rank()==0)printf("groupFile: %s\n", optarg);
					if(optarg!=NULL)groupFile = optind;
				}
				break;
			case 'v' : 
				if(optarg==NULL && argv[optind]!=NULL && argv[optind][0]!='-'){
					if(world.rank()==0)printf("nbGroupFile: %s\n", argv[optind]);
					nbGroupFile = argv[optind];
					++optind;
				}else{
					if(world.rank()==0)printf("nbGroupFile: %s\n", optarg);
					if(optarg!=NULL)nbGroupFile = optind;
				}
				break;
			case 'w' : 
				if(optarg==NULL && argv[optind]!=NULL && argv[optind][0]!='-'){
					if(world.rank()==0)printf("interventionType: %s\n", argv[optind]);
					interventionType = argv[optind];
					++optind;
				}else{
					if(world.rank()==0)printf("interventionType: %s\n", optarg);
					if(optarg!=NULL)interventionType = optind;
				}
				break;
			case 'q' : 
				if(optarg==NULL && argv[optind]!=NULL && argv[optind][0]!='-'){
					if(world.rank()==0)printf("nbSupers: %s\n", argv[optind]);
					nbSupers = argv[optind];
					++optind;
				}else{
					if(world.rank()==0)printf("nbSupers: %s\n", optarg);
					if(optarg!=NULL)nbSupers = optind;
				}
				break;
			case 'r' : 
				if(optarg==NULL && argv[optind]!=NULL && argv[optind][0]!='-'){
					if(world.rank()==0)printf("resultFile: %s\n", argv[optind]);
					resultFile = argv[optind];
					++optind;
				}else{
					if(world.rank()==0)printf("resultFile: %s\n", optarg);
					if(optarg!=NULL)resultFile = optind;
				}
				break;
			case 'j' : 
				if(optarg==NULL && argv[optind]!=NULL && argv[optind][0]!='-'){
					if(world.rank()==0)printf("acquisition_name: %s\n", argv[optind]);
					acquisition_name = argv[optind];
					++optind;
				}else{
					if(world.rank()==0)printf("acquisition_name: %s\n", optarg);
					if(optarg!=NULL)acquisition_name = optind;
				}
				break;
			case 'k' : 
				if(optarg==NULL && argv[optind]!=NULL && argv[optind][0]!='-'){
					if(world.rank()==0)printf("sensibSeq: %s\n", argv[optind]);
					sensibSeq = argv[optind];
					++optind;
				}else{
					if(world.rank()==0)printf("sensibSeq: %s\n", optarg);
					if(optarg!=NULL)sensibSeq = optind;
				}
				break;
            default: usage(); 
                exit(EXIT_FAILURE);
        }
    }
	
	if(optimSimu=="yes"){
		if(argc < 4){
			usage();
			exit(EXIT_FAILURE);
		}
	}else if(optimSimu==""){
		optimSimu="no";
	}
	
	repast::Properties* mainProps = new repast::Properties(propsFile,argc,argv,&world);
	admin = mainProps->getProperty("admission"); //on récup le fichier d'entrée avec les caractéristiques des individus
	ctc = mainProps->getProperty("contact");	//on récup le fichier d'entrée avec les contacts
	prlvt = mainProps->getProperty("prlvt");	//on récup le fichier d'entrée avec les prlvt
	string schedulePE = mainProps->getProperty("schedule");

	if(SCG!="")mainProps->putProperty("superContractorOption3", SCG);
	if(SCT!="")mainProps->putProperty("superContractorOption2", SCT);

	string borne = mainProps->getProperty("borne");
	if(borne!="inf" && borne!="sup" && borne!=""){
		borne=="";
	}

	if(nbSupers!="")mainProps->putProperty("nbSupers", nbSupers);
	if(nbSupers==""){
		nbSupers=="60";
	}


	bool sensibilityStatus;
	if(sensibSeq!=""){
		sensibilityStatus = true;
	}else{
		sensibilityStatus = false;
	}
	
	cout << "sensibSeq " << sensibSeq << " et sensibilityStatus " << sensibilityStatus << endl;
	
	//modification des props si simuOptim
	// if(optimSimu=="yes"){
		if(buildLearning=="yes"){
			mainProps->putProperty("resultsFromFiles", "yes");
			mainProps->putProperty("parametersOnly", "yes");
		}else if(buildLearning=="no"){
			mainProps->putProperty("resultsFromFiles", "no");
			mainProps->putProperty("parametersOnly", "no");
		}
		if(nbOfAcquisition!=""){
			mainProps->putProperty("nbOfAcquisitionWeek", nbOfAcquisition);
			mainProps->putProperty("nbOfAcquisitionWeekLearning", nbOfAcquisition);
		}
		if(firstAcq!="")mainProps->putProperty("firstAcqOnly", firstAcq);
		if(byType!="")mainProps->putProperty("probaByType", byType);
		if(nbCtcInf!="")mainProps->putProperty("nbCtcInfMin", nbCtcInf);
		// if(logPAPA!="")mainProps->putProperty("withLogPAPA", logPAPA);
		// if(logPAPE!="")mainProps->putProperty("withLogPAPE", logPAPE);
		// if(logPEPA!="")mainProps->putProperty("withLogPEPA", logPEPA);
		// if(logPEPE!="")mainProps->putProperty("withLogPEPE", logPEPE);
		if(iPA!="")mainProps->putProperty("percentageFirstImmunizedPA", iPA);
		if(iPE!="")mainProps->putProperty("percentageFirstImmunizedPE", iPE);
		if(newContact!="")mainProps->putProperty("newContact", newContact);
		if(scenarioOption!="")mainProps->putProperty("scenario", scenarioOption);
		if(learningFile!="")mainProps->putProperty("filenameParameters", learningFile);
		// if(fileCtc!="")mainProps->putProperty("contactFile", fileCtc);
	// }
	
	std::string iIND = "";
	if(iIND!=""){
		if(iIND=="PA"){
			mainProps->putProperty("nbOfInitializedIndividual", "0");
			mainProps->putProperty("nbOfInitializedPatient", "1");
			mainProps->putProperty("nbOfInitializedStaff", "0");
		}else if(iIND=="PE"){
			mainProps->putProperty("nbOfInitializedIndividual", "0");
			mainProps->putProperty("nbOfInitializedPatient", "0");
			mainProps->putProperty("nbOfInitializedStaff", "1");
		}else if(iIND=="both"){
			mainProps->putProperty("nbOfInitializedIndividual", "1");
			mainProps->putProperty("nbOfInitializedPatient", "0");
			mainProps->putProperty("nbOfInitializedStaff", "0");
		}
		
	}
	
	if(mode=="sim"){
		mainProps->putProperty("realGameON", "no");
		mainProps->putProperty("simuGameON", "yes");
	}else if(mode=="real"){
		mainProps->putProperty("realGameON", "yes");
		mainProps->putProperty("simuGameON", "no");
	}else if(mode=="both"){
		mainProps->putProperty("realGameON", "yes");
		mainProps->putProperty("simuGameON", "yes");
	}else if(mode=="none"){
		mainProps->putProperty("realGameON", "no");
		mainProps->putProperty("simuGameON", "no");
	}
	
	if(seed!=""){
		// srand (stoi(nb_iteration));
		// string nn;
		// for(int i(0); i<10; i++){
			// int ch = rand() % 9 + 1;
			// nn = nn + to_string(ch);
		// }

		// int findRand = stol(nn);
		int findRand = stoi(seed);
		mainProps->putProperty("random.seed", findRand);
		// if(world.rank() == 0)cout << "\nSeed : " << mainProps->getProperty("random.seed") << endl;
	}
	
	try{
		if(byType=="no" && groupFile==""){
			string s = " we need a file if you don't want to use byType ";
			throw s;
		}
	}catch(string const& chaine){
		cerr<< chaine << endl;
		return -1;
	}
	
	if(world.rank()==0){
		//exception à gerer
		
		cout << "\n-------------- LISTE DES OPTIONS -------------- \n" <<endl;
		// cout << "Les arguments sont: \n"<< " configFile: " << configFile << "\n propsFile: " << propsFile << "\n optimSimu: " << optimSimu << "\n buildLearning: " << buildLearning << "\n firstAcq: " << firstAcq << "\n nbOfAcquisition: " << nbOfAcquisition << "\n byType: " << byType << "\n nbCtcInf: " << nbCtcInf << "\n logPAPA: " << logPAPA << "\n logPAPE: " << logPAPE << "\n logPEPA: " << logPEPA << "\n logPEPE: " << logPEPE << "\n nb_iteration: " << nb_iteration << endl;
		cout << "Les arguments sont: \n"<< " configFile: " << configFile << "\n propsFile: " << propsFile << "\n optimSimu: " << optimSimu << "\n buildLearning: " << buildLearning << "\n firstAcq: " << firstAcq << "\n nbOfAcquisition: " << nbOfAcquisition << "\n byType: " << byType << "\n nbCtcInf: " << nbCtcInf <<  "\n nb_iteration: " << nb_iteration << endl;
		cout<<"\n"<<endl;
		// cout << "Dans le fichier de props: \n" << " resultsFromFiles: " << mainProps->getProperty("resultsFromFiles") <<  "\n parametersOnly: " << mainProps->getProperty("parametersOnly") << "\n firstAcq:  " << mainProps->getProperty("firstAcqOnly") << "\n nbOfAcquisitionWeek:  " << mainProps->getProperty("nbOfAcquisitionWeek") <<  "\n nbOfAcquisitionWeekLearning:  " <<mainProps->getProperty("nbOfAcquisitionWeekLearning") <<  "\n probaByType: " <<mainProps->getProperty("probaByType") <<  "\n nbCtcInfMin " << mainProps->getProperty("nbCtcInfMin") <<  "\n withLogPAPA: " << mainProps->getProperty("withLogPAPA") <<  "\n withLogPAPE: " << mainProps->getProperty("withLogPAPE") << "\n withLogPEPA: " << mainProps->getProperty("withLogPEPA") <<  "\n withLogPEPE: " << mainProps->getProperty("withLogPEPE") <<  endl;
		cout << "Dans le fichier de props: \n" << " resultsFromFiles: " << mainProps->getProperty("resultsFromFiles") <<  "\n parametersOnly: " << mainProps->getProperty("parametersOnly") << "\n firstAcq:  " << mainProps->getProperty("firstAcqOnly") << "\n nbOfAcquisitionWeek:  " << mainProps->getProperty("nbOfAcquisitionWeek") <<  "\n nbOfAcquisitionWeekLearning:  " <<mainProps->getProperty("nbOfAcquisitionWeekLearning") <<  "\n probaByType: " <<mainProps->getProperty("probaByType") <<  "\n nbCtcInfMin " << mainProps->getProperty("nbCtcInfMin")  <<  endl;
		cout << "Seed : " << mainProps->getProperty("random.seed") << endl;
		cout << "\n----------------------------------------------- " <<endl;
	}
	
	
	string checkFile = mainProps->getProperty("resultsFromFiles");	
	string wardsString = mainProps->getProperty("ward.name");
	string parametersOnly = mainProps->getProperty("parametersOnly");
	vector<string> wards;
	boost::split(wards,wardsString , boost::is_any_of("-"),boost::token_compress_on);
	try{
		if(wards.size()!= world.size()){
			string s = " number of rank is not equal to the number of ward: " + wards.size();
			throw s;
		}
	}catch(string const& chaine){
		cerr<< chaine << endl;
		return -1;
	}
	
	string scenario = mainProps->getProperty("scenario");
	string seuilString = mainProps->getProperty("seuil");
	int seuil = strToInt(seuilString);
	
	int nbOfAcquisitionWeekLearning = strToInt(mainProps->getProperty("nbOfAcquisitionWeekLearning"));
	int nbCtcInfMinLearning = strToInt(mainProps->getProperty("nbCtcInfMin"));
	bool probaByType = mainProps->getProperty("probaByType") == "yes" ? true : false;
	bool durationByType = mainProps->getProperty("durationByType") == "yes" ? true : false;
	bool optionDuration = mainProps->getProperty("optionDuration") == "long" ? true : false;
	// bool withLogPAPA = mainProps->getProperty("withLogPAPA") == "yes" ? true : false;
	// bool withLogPAPE = mainProps->getProperty("withLogPAPE") == "yes" ? true : false;
	// bool withLogPEPA = mainProps->getProperty("withLogPEPA") == "yes" ? true : false;
	// bool withLogPEPE = mainProps->getProperty("withLogPEPE") == "yes" ? true : false;
	bool firstAcqOnly = mainProps->getProperty("firstAcqOnly") == "yes" ? true : false;
	bool startWithRealIndividualAtAdmissionSim = mainProps->getProperty("startWithRealIndividualAtAdmissionSim") == "yes" ? true : false;

	bool workOnContactOnly = mainProps->getProperty("workOnContactOnly") == "yes" ? true : false;
	string newContactStatus = mainProps->getProperty("newContact");
	bool useNewContact = false;
	bool newContactBool = false;
	if(newContactStatus=="build"){
		newContactBool = true;
		workOnContactOnly = true;
	}else if(newContactStatus=="use"){
		useNewContact = true;
	}else if(newContactStatus=="both"){
		newContactBool = true;
		useNewContact = true;
		workOnContactOnly = false;
	}else if(newContactStatus=="none"){
		newContactBool = false;
		useNewContact = false;
		workOnContactOnly = false;
	}
	// bool useNewContact = mainProps->getProperty("useNewContact") == "yes" ? true : false;
	// bool newContactBool = mainProps->getProperty("newContact") == "yes" ? true : false;
	
	
	bool useNewAdmission = mainProps->getProperty("useNewAdmission") == "yes" ? true : false;
	bool useNewAdmissionOnly = mainProps->getProperty("useNewAdmissionOnly") == "yes" ? true : false;
	string filenameAdmission = mainProps->getProperty("admissionFile");
	string resultParameters;
	
	
	// int scenarioNB = -1;
	
	// if(scenario!="none"){
		// scenarioNB = strToInt(mainProps->getProperty("scenario"));
		// interventions = true;
		// mainProps->putProperty("interventions", "yes");
	// }
	
	
	
	if(interventionType=="cohorting"){
		cout << " entré " << endl;
		mainProps->putProperty("cohorting", "yes");
		mainProps->putProperty("interventions", "yes");
	}else if (interventionType=="compliance"){
		mainProps->putProperty("contactPrecaution", "yes");
		mainProps->putProperty("interventions", "yes");
	}else if (interventionType=="supercontractor"){
		mainProps->putProperty("contactPrecaution", "yes");
		mainProps->putProperty("superContractorON", "yes");
		mainProps->putProperty("interventions", "yes");
	}else if (interventionType=="vaccination"){
		mainProps->putProperty("vaccination", "yes");
		mainProps->putProperty("interventions", "yes");
	}else if (interventionType=="SCvaccination"){
		mainProps->putProperty("vaccination", "yes");
		mainProps->putProperty("superContractorON", "yes");
		mainProps->putProperty("interventions", "yes");
	}

	bool interventions = mainProps->getProperty("interventions") == "yes" ? true : false;
	
	if(interventions) cout << "Intervention enabled: cohorting " << mainProps->getProperty("cohorting") << "; compliance " <<
	 mainProps->getProperty("contactPrecaution") << "; supercontactors: " << mainProps->getProperty("superContractorON") << endl;


	// if(mainProps->getProperty("cohorting")=="yes" && (scenario=="0")){
		// interventions = false;
		// mainProps->putProperty("interventions", "no");
	// }
	// if(mainProps->getProperty("cohorting")=="yes" && (scenario!="0")){
		// interventions = true;
		// mainProps->putProperty("interventions", "yes");
	// }
	
	try{
		if(optimSimu=="yes" && interventions){
			string s = " you can't start optimSimu and interventions at the same time ";
			throw s;
		}
		
	}catch(string const& chaine){
		cerr<< chaine << endl;
		return -1;
	}
	// cout << " on s'occupe des fichiers acquisition prevalence " << endl;
	
	//on va s'occuper du fichier resultParameters
	if(optimSimu=="yes"){
		// resultParameters = "parameters/resultParameters"+ fileCtc +"_" + mainProps->getProperty("firstAcqOnly") + "_" + mainProps->getProperty("nbOfAcquisitionWeekLearning") +"_" + mainProps->getProperty("probaByType") + "_" + mainProps->getProperty("nbCtcInfMin") + "_"+ mainProps->getProperty("withLogPAPA") + "_" + mainProps->getProperty("withLogPAPE") + "_"+ mainProps->getProperty("withLogPEPA") + "_" + mainProps->getProperty("withLogPEPE") + ".csv";
		resultParameters = "parameters/resultParameters"+ fileCtc +"_" + mainProps->getProperty("firstAcqOnly") + "_" + mainProps->getProperty("nbOfAcquisitionWeekLearning") +"_" + mainProps->getProperty("probaByType") + "_" + mainProps->getProperty("nbCtcInfMin") + ".csv";
	}else{
		resultParameters = mainProps->getProperty("filenameParameters");
	}
	// mainProps->putProperty("filenameParameters", resultParameters);
	
	string realGameON = mainProps->getProperty("realGameON");
	string simuGameON = mainProps->getProperty("simuGameON");
	string ctcSim;
	
	//on va s'occuper des fichiers acquisition et prevalence
	bool returnPrev = mainProps->getProperty("returnPrevalence") == "yes" ? true : false;
	bool returnAcq = mainProps->getProperty("returnAcquisition") == "yes" ? true : false;
	bool returnStatusByDay = mainProps->getProperty("returnStatusByDay") == "yes" ? true : false;
	string filenameAcquisitionR;
	string filenameAcquisitionS;
	string filenameStatusByDayR;
	string filenameStatusByDayS;
	string filenamePrevalenceR = mainProps->getProperty("output/prevalence/prevalenceREAL.csv");
	string filenamePrevalenceS = mainProps->getProperty("output/prevalence/prevalenceSIMU.csv");
	string suppString;
	if(resultFile=="" && fileCtc!=""){
		suppString = fileCtc;
	}else if(resultFile=="" && fileCtc==""){
		suppString = "";
	}else{
		suppString = resultFile;
	}
	
	suppString = fileCtc+""+resultFile;
	string suppAcq;
	if(acquisition_name!=""){
		mainProps->putProperty("addN", acquisition_name);
		suppAcq = mainProps->getProperty("addN");
	}else{
		suppAcq = mainProps->getProperty("addN");
	}
	// string suppAcq = mainProps->getProperty("addN");
	if(suppAcq=="none")suppAcq="";
	if(borne=="none")borne="";
	if(directoryPlus!="")mainProps->putProperty("directoryPlus", directoryPlus);
	
	if(optimSimu!="yes" && returnAcq){
		if(!interventions || scenario=="none"){
			// filenameAcquisitionR = mainProps->getProperty("filenameAcquisitionReal");
			filenameAcquisitionR = "output/acquisition/"+directoryPlus+"acquisition_real"+suppAcq+".csv";
			if(nb_iteration!=""){
				filenameAcquisitionS = "output/acquisition/"+directoryPlus+"acquisition" + nbGroupFile + suppString+""+suppAcq+""+borne +"_simu"+ nb_iteration + ".csv";
			}else{
				if(suppString==""){
					filenameAcquisitionS = mainProps->getProperty("filenameAcquisitionSimu");
				}else{
					filenameAcquisitionS = "output/acquisition/"+directoryPlus+"acquisition"+ nbGroupFile + suppString +""+suppAcq+""+borne +"_simu.csv";
				}
			}
		}else{
			// filenameAcquisitionR = mainProps->getProperty("filenameAcquisitionReal");
			filenameAcquisitionR = "output/acquisition/"+directoryPlus+"acquisition_real"+suppAcq+".csv";
			if(nb_iteration!=""){
				filenameAcquisitionS = "output/acquisition/"+directoryPlus+"acquisition" + nbGroupFile +suppString+""+suppAcq+""+borne+"_simu_scenario_" + scenario + "_" + nb_iteration + ".csv";
			}else{
				filenameAcquisitionS = "output/acquisition/"+directoryPlus+"acquisition" + nbGroupFile+ suppString+""+suppAcq+""+borne +"_simu_scenario_" + scenario + ".csv";
			}
		}
		
	}
	if(optimSimu=="yes" && returnAcq){
		filenameAcquisitionR = "output/acquisition/"+directoryPlus+"acquisition_real"+suppAcq+"_" + firstAcq + "_" + nbOfAcquisition + ".csv";
		if(nb_iteration!=""){
			// filenameAcquisitionS = "output/acquisition/"+directoryPlus+"acquisition"+ suppString+""+suppAcq+""+borne +"_simu_" + firstAcq + "_" + nbOfAcquisition +"_" + byType + "_" + nbCtcInf + "_"+ logPAPA + "_" + logPAPE + "_"+ logPEPA + "_"+ logPEPE + "_" + nb_iteration + ".csv";
			filenameAcquisitionS = "output/acquisition/"+directoryPlus+"acquisition"+ suppString+""+suppAcq+""+borne +"_simu_" + firstAcq + "_" + nbOfAcquisition +"_" + byType + "_" + nbCtcInf + "_"+ nb_iteration + ".csv";
		}else{
			// filenameAcquisitionS = "output/acquisition/"+directoryPlus+"acquisition"+ suppString+""+suppAcq+""+borne +"_simu_" + firstAcq + "_" + nbOfAcquisition +"_" + byType + "_" + nbCtcInf + "_"+ logPAPA + "_" + logPAPE + "_"+ logPEPA + "_"+ logPEPE + ".csv";
			filenameAcquisitionS = "output/acquisition/"+directoryPlus+"acquisition"+ suppString+""+suppAcq+""+borne +"_simu_" + firstAcq + "_" + nbOfAcquisition +"_" + byType + "_" + nbCtcInf + "_" + ".csv";
		}
	}
	
	if(optimSimu!="yes" && returnPrev){
		if(!interventions || scenario=="none"){
			// filenamePrevalenceR = mainProps->getProperty("filenamePrevalenceReal");
			filenamePrevalenceR = "output/prevalence/"+directoryPlus+"prevalenceREAL"+suppAcq+".csv";
			if(nb_iteration!=""){
				filenamePrevalenceS = "output/prevalence/"+directoryPlus+"prevalence"+ nbGroupFile+suppString+""+suppAcq+""+borne +"SIMU"+ nb_iteration + ".csv";
			}else{
				if(suppString==""){
					filenamePrevalenceS = mainProps->getProperty("filenamePrevalenceSimu");
				}else{
					filenamePrevalenceS = "output/prevalence/"+directoryPlus+"prevalence"+ nbGroupFile+suppString+""+suppAcq+""+borne +"SIMU.csv";
				}
			}
		}else{
			// filenamePrevalenceR = mainProps->getProperty("filenamePrevalenceReal");
			filenamePrevalenceR = "output/prevalence/"+directoryPlus+"prevalenceREAL"+suppAcq+".csv";
			if(nb_iteration!=""){
				filenamePrevalenceS = "output/prevalence/"+directoryPlus+"prevalence"+ nbGroupFile+ suppString+""+suppAcq+""+borne +"SIMU_scenario_"+ scenario + "_" + nb_iteration + ".csv";
			}else{
				filenamePrevalenceS = "output/prevalence/"+directoryPlus+"prevalence"+ nbGroupFile+ suppString+""+suppAcq+""+borne +"SIMU_scenario_"+ scenario + ".csv";
			}
		}
	}
	if(optimSimu=="yes" && returnPrev){
		filenamePrevalenceR = "output/prevalence/"+directoryPlus+"prevalenceREAL"+suppAcq+"_" + firstAcq + "_" + nbOfAcquisition + ".csv";
		if(nb_iteration!=""){
			// filenamePrevalenceS = "output/prevalence/"+directoryPlus+"prevalence"+ suppString+""+suppAcq+""+borne +"SIMU_" + firstAcq + "_" + nbOfAcquisition +"_" + byType + "_" + nbCtcInf + "_"+ logPAPA + "_" + logPAPE + "_"+ logPEPA + "_"+ logPEPE  + "_" + nb_iteration + ".csv";
			filenamePrevalenceS = "output/prevalence/"+directoryPlus+"prevalence"+ suppString+""+suppAcq+""+borne +"SIMU_" + firstAcq + "_" + nbOfAcquisition +"_" + byType + "_" + nbCtcInf + "_" + nb_iteration + ".csv";
		}else{
			// filenamePrevalenceS = "output/prevalence/"+directoryPlus+"prevalence"+ suppString+""+suppAcq+""+borne +"SIMU_" + firstAcq + "_" + nbOfAcquisition +"_" + byType + "_" + nbCtcInf + "_"+ logPAPA + "_" + logPAPE + "_"+ logPEPA + "_"+ logPEPE + ".csv";
			filenamePrevalenceS = "output/prevalence/"+directoryPlus+"prevalence"+ suppString+""+suppAcq+""+borne +"SIMU_" + firstAcq + "_" + nbOfAcquisition +"_" + byType + "_" + nbCtcInf  + ".csv";
		}
	}
	
	if(optimSimu!="yes" && returnStatusByDay){
		if(!interventions || scenario=="none"){
			filenameStatusByDayR = "output/statusByDay/"+directoryPlus+"statusByDayREAL"+suppAcq+".csv";
			if(nb_iteration!=""){
				filenameStatusByDayS = "output/statusByDay/"+directoryPlus+"statusByDay"+ nbGroupFile+suppString+""+suppAcq+""+borne +"SIMU"+ nb_iteration + ".csv";
			}else{
				if(suppString==""){
					filenameStatusByDayS = mainProps->getProperty("filenameStatusByDaySimu");
				}else{
					filenameStatusByDayS = "output/statusByDay/"+directoryPlus+"statusByDay"+ nbGroupFile+suppString+""+suppAcq+""+borne +"SIMU.csv";
				}
			}
		}else{
			filenameStatusByDayR = "output/statusByDay/"+directoryPlus+"statusByDayREAL"+suppAcq+".csv";
			if(nb_iteration!=""){
				filenameStatusByDayS = "output/statusByDay/"+directoryPlus+"statusByDay"+ nbGroupFile+ suppString+""+suppAcq+""+borne +"SIMU_scenario_"+ scenario + "_" + nb_iteration + ".csv";
			}else{
				filenameStatusByDayS = "output/statusByDay/"+directoryPlus+"statusByDay"+ nbGroupFile+ suppString+""+suppAcq+""+borne +"SIMU_scenario_"+ scenario + ".csv";
			}
		}
	}
	if(optimSimu=="yes" && returnStatusByDay){
		filenameStatusByDayR = "output/statusByDay/"+directoryPlus+"statusByDayREAL"+suppAcq+"_" + firstAcq + "_" + nbOfAcquisition + ".csv";
		if(nb_iteration!=""){
			// filenameStatusByDayS = "output/statusByDay/"+directoryPlus+"statusByDay"+ suppString+""+suppAcq+""+borne +"SIMU_" + firstAcq + "_" + nbOfAcquisition +"_" + byType + "_" + nbCtcInf + "_"+ logPAPA + "_" + logPAPE + "_"+ logPEPA + "_"+ logPEPE  + "_" + nb_iteration + ".csv";
			filenameStatusByDayS = "output/statusByDay/"+directoryPlus+"statusByDay"+ suppString+""+suppAcq+""+borne +"SIMU_" + firstAcq + "_" + nbOfAcquisition +"_" + byType + "_" + nbCtcInf + "_"+ nb_iteration + ".csv";
		}else{
			// filenameStatusByDayS = "output/statusByDay/"+directoryPlus+"statusByDay"+ suppString+""+suppAcq+""+borne +"SIMU_" + firstAcq + "_" + nbOfAcquisition +"_" + byType + "_" + nbCtcInf + "_"+ logPAPA + "_" + logPAPE + "_"+ logPEPA + "_"+ logPEPE + ".csv";
			filenameStatusByDayS = "output/statusByDay/"+directoryPlus+"statusByDay"+ suppString+""+suppAcq+""+borne +"SIMU_" + firstAcq + "_" + nbOfAcquisition +"_" + byType + "_" + nbCtcInf + ".csv";
		}
	}
	
	mainProps->putProperty("filenameAcquisitionReal", filenameAcquisitionR);
	mainProps->putProperty("filenameAcquisitionSimu", filenameAcquisitionS);
	mainProps->putProperty("filenamePrevalenceReal", filenamePrevalenceR);
	mainProps->putProperty("filenamePrevalenceSimu", filenamePrevalenceS);
	mainProps->putProperty("filenameStatusByDayReal", filenameStatusByDayR);
	mainProps->putProperty("filenameStatusByDaySimu", filenameStatusByDayS);
	
	string datesFromLearning =  mainProps->getProperty("learningDates");
	vector<string> datesFL;
	boost::split(datesFL,datesFromLearning , boost::is_any_of("_"),boost::token_compress_on);
	
	//interventions
	string cohorting = mainProps->getProperty("cohorting");
	string compliance = mainProps->getProperty("contactPrecaution");
	string superContractorON = mainProps->getProperty("superContractorON");
	string vaccination = mainProps->getProperty("vaccination");
	string filenameInterventions;
	if(interventions){
		if(scenario=="none"){
			filenameInterventions = mainProps->getProperty("interventionsFile");
		}else{
			if(cohorting=="yes"){
				filenameInterventions = "output/interventions/cohortingFile_scenario"+ scenario +".csv";
			}else if (compliance=="yes"){
				if(superContractorON=="yes"){
					if(SCG=="RandomAll" || SCG=="RandomCare" || SCG=="RandomPatients"){
						filenameInterventions = "output/interventions/random" + nbSupers + "/superContractorFile" + suppString +"_scenario"+ scenario +".csv";
					} else filenameInterventions = "output/interventions/superContractorFile" + suppString + "_"+ nbSupers + "_scenario"+ scenario +".csv";
				} else {
					filenameInterventions = "output/interventions/complianceFile_scenario"+ scenario +".csv";
				}
			}else if (vaccination=="yes"){
				if(superContractorON=="yes"){
					if(SCG=="RandomAll" || SCG=="RandomCare" || SCG=="RandomPatients"){
						filenameInterventions = "output/interventions/random" + nbSupers + "/superContractorFile"+ suppString +"_scenario"+ scenario +".csv";
					} else filenameInterventions = "output/interventions/superContractorFile" + suppString + "_"+ nbSupers + "_scenario"+ scenario +".csv";
				} else {
					filenameInterventions = "output/interventions/vaccinationFile_scenario"+ scenario +".csv";
				}
			}
		}
		mainProps->putProperty("interventionsFile", filenameInterventions);
	}
	cout << "Intervention file: " << filenameInterventions << endl;
	
	
	//------------------------------------------------------- Admission Simulation --------------------------//
	
	if(world.rank() == 0 && useNewAdmissionOnly){	
		
		cout << "\n ADMISSION SIMULATION... \n" << endl;
		if(nb_iteration!=""){
			filenameAdmission =  "output/admission/admissionSim_" + nb_iteration + ".csv";
		}
		string datesSimuForAdm =  mainProps->getProperty("simuGameDates");
		string nbOfIndInitDate = mainProps->getProperty("nbOfIndInitDate");
		string admissionParametersFile = mainProps->getProperty("admissionParametersFile");
		string buildADParameters = mainProps->getProperty("buildADParameters");
		vector<string> datesS;
		boost::split(datesS,datesSimuForAdm , boost::is_any_of("_"),boost::token_compress_on);
		//1) construction des parametres d'admission si besoin
		if(buildADParameters=="build" || buildADParameters=="both"){
			cout << " building parameters at admission file ... " << endl;
			Learning* learnParam = new Learning(mainProps, admin, datesFL[0], datesFL[1], nbOfIndInitDate);
			vector<string> paramsToFind = {"admission", "length of stay", "admission distribution", "number of individual"};
			learnParam-> resultParameters2(true, admissionParametersFile, paramsToFind);
			delete learnParam;
			cout << " done in " << admissionParametersFile << endl;
		}		
		//2) construction du fichier d'admission simulée
		if(buildADParameters!="build"){
			cout << " building simulated admission file ... " << endl;
			Learning* learn;
			if(startWithRealIndividualAtAdmissionSim){
				//besoin des admissions
				learn = new Learning(mainProps, admin, datesS[0], datesS[1], nbOfIndInitDate);
			}else{
				learn = new Learning(mainProps, datesS[0], datesS[1]);
			}
			learn-> buildADSimulation(admissionParametersFile, datesS[0], datesS[1], filenameAdmission, startWithRealIndividualAtAdmissionSim);
			delete learn;
			cout << " done in " << filenameAdmission << endl;
		}
		
		
	}
	//-------------------------------------------------------- CONTACTS ------------------------------------//
	string filenameContact = mainProps->getProperty("contactFile");
	string filenameContactParameters = mainProps->getProperty("contactParametersFile");
	bool findContactParameters = mainProps->getProperty("findContactParameters") == "true" ? true : false;
	if(optimSimu=="yes"){
		// filenameContact = "output/contact/matContactBuilt"+ suppString +"_" + mainProps->getProperty("firstAcqOnly") + "_" + mainProps->getProperty("nbOfAcquisitionWeekLearning") +"_" + mainProps->getProperty("probaByType") + "_" + mainProps->getProperty("nbCtcInfMin") + "_"+ mainProps->getProperty("withLogPAPA") + "_" + mainProps->getProperty("withLogPAPE") + "_"+ mainProps->getProperty("withLogPEPA") + "_" + mainProps->getProperty("withLogPEPE") + ".csv";
		filenameContact = "output/contact/matContactBuilt"+ suppString +"_" + mainProps->getProperty("firstAcqOnly") + "_" + mainProps->getProperty("nbOfAcquisitionWeekLearning") +"_" + mainProps->getProperty("probaByType") + "_" + mainProps->getProperty("nbCtcInfMin") +  ".csv";
		
		// filenameContactParameters = "parameters/contactParameters/parameters_matContactBuilt"+ suppString +"_" + mainProps->getProperty("firstAcqOnly") + "_" + mainProps->getProperty("nbOfAcquisitionWeekLearning") +"_" + mainProps->getProperty("probaByType") + "_" + mainProps->getProperty("nbCtcInfMin") + "_"+ mainProps->getProperty("withLogPAPA") + "_" + mainProps->getProperty("withLogPAPE") + "_"+ mainProps->getProperty("withLogPEPA") + "_" + mainProps->getProperty("withLogPEPE") + ".csv";
		filenameContactParameters = "parameters/contactParameters/parameters_matContactBuilt"+ suppString +"_" + mainProps->getProperty("firstAcqOnly") + "_" + mainProps->getProperty("nbOfAcquisitionWeekLearning") +"_" + mainProps->getProperty("probaByType") + "_" + mainProps->getProperty("nbCtcInfMin")  + ".csv";
	}else{
		if(interventionType=="cohorting"){
			if(newContactBool){ //if building, need to recover iter of network
				filenameContact =  "output/contact/matContactBuilt"+ suppString + "_iter_" + nb_iteration + "_scenario_" + scenario +".csv";
				filenameContactParameters =  "parameters/contactParameters/parameters_matContactBuilt"+ suppString + "_iter_" + nb_iteration + "_scenario_" + scenario +".csv";
			}else{ //if using, iter of network should be provided within parameter -g at command line
				filenameContact =  "output/contact/matContactBuilt" + suppString + "_scenario_" + scenario + ".csv";
				filenameContactParameters =  "parameters/contactParameters/parameters_matContactBuilt" + suppString + "_scenario_" + scenario + ".csv";
			}
		} else {
			if((newContactBool || useNewContact) && (scenario=="none") && suppString!=""){
				filenameContact =  "output/contact/matContactBuilt"+ suppString +".csv";
				filenameContactParameters =  "parameters/contactParameters/parameters_matContactBuilt"+ suppString +".csv";
			}else if((newContactBool || useNewContact) && scenario!="none"){
				filenameContact =  "output/contact/matContactBuilt" + suppString + "_scenario_" + scenario + ".csv";
				filenameContactParameters =  "parameters/contactParameters/parameters_matContactBuilt" + suppString + "_scenario_" + scenario + ".csv";
			}
		}
	}
	
	bool catB = mainProps->getProperty("catBoolCtc") == "true" ? true : false;
	string fromStart = mainProps->getProperty("firstDateCtc");
	bool hospB = mainProps->getProperty("withHospitalizationCtc") == "true" ? true : false;
	
	if(world.rank() == 0 && (newContactBool || workOnContactOnly) && !useNewAdmissionOnly){
		cout << "\n CONTACTS IN PROGRESS... \n" << endl;
		string filenameParameters = mainProps->getProperty("filenameParameters");
		string datesFromContact =  mainProps->getProperty("contactDates");
		vector<string> datesCTC;
		boost::split(datesCTC,datesFromContact , boost::is_any_of("_"),boost::token_compress_on);
		string firstDateCTC = datesCTC[0];
		string secondDateCTC = datesCTC[1];
		string firstDateLearn = datesFL[0];
		string secondDateLearn = datesFL[1];
		FindContact* fContactLearn;
		FindContact* fContact;
		string adminLearnCTC;
		string adminSimCTC;
		if(useNewAdmission){
			adminLearnCTC = admin;
			adminSimCTC = filenameAdmission;
		}else{
			adminLearnCTC = admin;
			adminSimCTC = admin;
		}
		cout << " fileCtc ------------->  " << fileCtc << endl;
		
		if(fileCtc=="testRecord" || fileCtc=="RandomGraph2"){
			fContactLearn = new FindContact(mainProps, ctc, adminLearnCTC, schedulePE,firstDateLearn, secondDateLearn, true, catB);
		}else{
			fContactLearn = new FindContact(mainProps, ctc, adminLearnCTC, schedulePE,firstDateLearn, secondDateLearn, false, catB);
		}
		fContactLearn->setWithHosp(hospB);
		cout << " interventions " << interventions << endl;
		cout << " cohorting " << cohorting  << " scenario " << scenario << endl;
		if(!interventions || (interventions && cohorting=="yes" && scenario=="0")){
			fContactLearn->buildMapContact();
			if(fileCtc=="testRecord"){
				cout << "let's go for sensors biase " << endl;
				if(findContactParameters){
					cout << " first find the contact probabilities " << endl;
					fContactLearn->findMeContactProbabilities(false);
					fContactLearn->writeContactParameters(filenameContactParameters, firstDateLearn, secondDateLearn , false);
				}
				cout << " second build contact " << endl;
				fContactLearn->getContactParameters(filenameContactParameters);
				fContactLearn->createMeNewContact(fromStart, filenameContact);
				cout << "new contacts stored in " << filenameContact << endl;
			}else if(fileCtc=="RandomGraph" || fileCtc=="RandomGraph2"){
				fContactLearn->findMeContactProbabilities(true);
				fContactLearn->writeContactParameters(filenameContactParameters, firstDateLearn, secondDateLearn , true);
				
				fContact = new FindContact(mainProps, ctc, adminSimCTC, schedulePE, firstDateCTC, secondDateCTC, false, catB);
				fContact->getContactParameters(filenameContactParameters);
				fContact->createMeNewContact(fromStart, filenameContact, true);
				cout << "new contacts stored in " << filenameContact << endl;
			}else if(fileCtc=="TestReseau1"){
				fContactLearn->findMeContactProbabilities(false, "test1");
				fContactLearn->writeContactParameters(filenameContactParameters, firstDateLearn, secondDateLearn , false);
				
				fContact = new FindContact(mainProps, ctc, adminSimCTC, schedulePE, firstDateCTC, secondDateCTC, false, catB);
				fContact->getContactParameters(filenameContactParameters);
				fContact->createMeNewContact(fromStart, filenameContact, false, false, "test1");
				cout << "new contacts stored in " << filenameContact << endl;
			}else if(fileCtc=="TestReseau2"){
				fContactLearn->findMeContactProbabilities(false, "test2");
				fContactLearn->writeContactParameters(filenameContactParameters, firstDateLearn, secondDateLearn , false);
				
				fContact = new FindContact(mainProps, ctc, adminSimCTC, schedulePE, firstDateCTC, secondDateCTC, false, catB);
				fContact->getContactParameters(filenameContactParameters);
				fContact->createMeNewContact(fromStart, filenameContact, false, false, "test2");
				cout << "new contacts stored in " << filenameContact << endl;
			}else{
				cout << "new contacts stored in " << filenameContact << endl;
				cout << " première phase " << endl;
				if(findContactParameters){
					cout << " recherche des proba activée " << endl;
					fContactLearn->findMeContactProbabilities(false);
					fContactLearn->writeContactParameters(filenameContactParameters, firstDateLearn, secondDateLearn , false);
				}else{
					cout << " recherche des proba désactivée " << endl;
					
				}
				cout << " le fichier de proba de conctact est : " << filenameContactParameters << endl;
				cout << " seconde phase " << endl;
				fContact = new FindContact(mainProps, ctc, adminSimCTC, schedulePE, firstDateCTC, secondDateCTC, false, catB);
				cout << "add parameters" << endl;
				fContact->getContactParameters(filenameContactParameters);
				cout << "creation" << endl;
				fContact->createMeNewContact(fromStart, filenameContact);
				cout << "new contacts stored in " << filenameContact << endl;
			}
			
		}else if((interventions && scenario!="0" && cohorting!="yes") || (interventions && cohorting=="yes" && scenario!="none" && scenario!="0" )){
			cout << " START INTERVENTIONS " << endl;
			string findSuperContractor = mainProps->getProperty("findSuperContractor");
			Interventions* interventionContact;
			if(cohorting=="yes"){
				interventionContact = new Interventions(filenameInterventions);
				cout << " debut du cohorting " << endl;
				cout<< "le fichier de parametre est: " << filenameContactParameters << endl;
				cout << "le fichier de stockage des contacts est: " << filenameContact << endl;
				fContactLearn->buildMapContact();
				fContactLearn->findMeContactProbabilities();
				fContactLearn->writeContactParameters(filenameContactParameters, firstDateLearn, secondDateLearn , false);
		
				fContact = new FindContact(mainProps, ctc, adminSimCTC, schedulePE, firstDateCTC, secondDateCTC, false, catB);
				fContact->getContactParameters(filenameContactParameters);			
				interventionContact->cohortingIntervention(fContact, fromStart, filenameContact);
				cout << "new contacts stored in " << filenameContact << endl;
			} else if(findSuperContractor=="yes"){
				
				interventionContact = new Interventions();
				cout << " debut du find super_contractor " << endl;
				fContact = new FindContact(mainProps, ctc, adminSimCTC, schedulePE, firstDateCTC, secondDateCTC, false, catB);
				fContact->buildMapContact();
				string superContractorOption1 = mainProps->getProperty("superContractorOption1");
				string superContractorOption2 = mainProps->getProperty("superContractorOption2");
				string superContractorOption3 = mainProps->getProperty("superContractorOption3");

				// RANDOM ALL ////////////////
				if(superContractorOption3 == "RandomAll"){
					vector<Individual*> allindiv = fContact->getAllIndividuals();
					vector<Individual*> allstaff;
					for(auto const& indiv:allindiv){
						if(indiv->getStatus() == "PE"){
							allstaff.push_back(indiv);
						}
					}
					vector<Individual*> chosenstaff20 = fContact->chooseCandidatesInsideVector(allstaff, 20);
					vector<Individual*> chosenstaff60 = fContact->chooseCandidatesInsideVector(allstaff, 60);
					vector<Individual*> chosenstaff100 = fContact->chooseCandidatesInsideVector(allstaff, 100);
					
					vector<string> superContractorInterm20;
					vector<string> superContractorInterm60;
					vector<string> superContractorInterm100;

					for(auto const& indiv:chosenstaff20){
						superContractorInterm20.push_back(indiv->getCalc_ident());
					}

					for(auto const& indiv:chosenstaff60){
						superContractorInterm60.push_back(indiv->getCalc_ident());
					}

					for(auto const& indiv:chosenstaff100){
						superContractorInterm100.push_back(indiv->getCalc_ident());
					}

					ofstream myCSV;
					int nb = 2;
					for(int i(1); i<=5; i++){

						string filenameSuperContractor20 = "output/interventions/random20/superContractorFileRandomAll"+ nb_iteration +"_scenario"+ to_string(i) +".csv";
						myCSV.open(filenameSuperContractor20);
						myCSV << "superContractor;name;number;"<<endl;
						for(auto const& elem:superContractorInterm20){
							myCSV << "superContractor;" + elem + ";" + to_string(nb) + ";" <<endl;
						}
						myCSV.close();

						string filenameSuperContractor60 = "output/interventions/random60/superContractorFileRandomAll"+ nb_iteration +"_scenario"+ to_string(i) +".csv";
						myCSV.open(filenameSuperContractor60);
						myCSV << "superContractor;name;number;"<<endl;
						for(auto const& elem:superContractorInterm60){
							myCSV << "superContractor;" + elem + ";" + to_string(nb) + ";" <<endl;
						}
						myCSV.close();

						string filenameSuperContractor100 = "output/interventions/random100/superContractorFileRandomAll"+ nb_iteration +"_scenario"+ to_string(i) +".csv";
						myCSV.open(filenameSuperContractor100);
						myCSV << "superContractor;name;number;"<<endl;
						for(auto const& elem:superContractorInterm100){
							myCSV << "superContractor;" + elem + ";" + to_string(nb) + ";" <<endl;
						}
						myCSV.close();
					
						nb = nb + 2;

					}

				// RANDOM NURSES ////////////////
				} else if(superContractorOption3 == "RandomCare"){
					vector<Individual*> allindiv = fContact->getAllIndividuals();
					vector<Individual*> allnurses;
					for(auto const& indiv:allindiv){
						if(indiv->getStatus() == "PE"){
							Staff* staff = dynamic_cast<Staff*>(indiv);
							string indiv_cat = staff->getCat();
							//if(indiv_cat == "IDE" || indiv_cat == "Cadre de santŽ" || indiv_cat == "Aide soignant" || indiv_cat == "Eleve/Stagiaire"){
							if(indiv_cat == "Aide soignant"){
									allnurses.push_back(indiv);
							}
						}
					}
					vector<Individual*> chosenstaff20 = fContact->chooseCandidatesInsideVector(allnurses, 20);
					vector<Individual*> chosenstaff60 = fContact->chooseCandidatesInsideVector(allnurses, 60);
					vector<Individual*> chosenstaff100 = fContact->chooseCandidatesInsideVector(allnurses, 100);

					vector<string> superContractorInterm20;
					vector<string> superContractorInterm60;
					vector<string> superContractorInterm100;

					for(auto const& indiv:chosenstaff20){
						superContractorInterm20.push_back(indiv->getCalc_ident());
					}

					for(auto const& indiv:chosenstaff60){
						superContractorInterm60.push_back(indiv->getCalc_ident());
					}

					for(auto const& indiv:chosenstaff100){
						superContractorInterm100.push_back(indiv->getCalc_ident());
					}

					ofstream myCSV;
					int nb = 2;
					for(int i(1); i<=5; i++){

						string filenameSuperContractor20 = "output/interventions/random20/superContractorFileRandomCare"+ nb_iteration +"_scenario"+ to_string(i) +".csv";
						myCSV.open(filenameSuperContractor20);
						myCSV << "superContractor;name;number;"<<endl;
						for(auto const& elem:superContractorInterm20){
							myCSV << "superContractor;" + elem + ";" + to_string(nb) + ";" <<endl;
						}
						myCSV.close();

						string filenameSuperContractor60 = "output/interventions/random60/superContractorFileRandomCare"+ nb_iteration +"_scenario"+ to_string(i) +".csv";
						myCSV.open(filenameSuperContractor60);
						myCSV << "superContractor;name;number;"<<endl;
						for(auto const& elem:superContractorInterm60){
							myCSV << "superContractor;" + elem + ";" + to_string(nb) + ";" <<endl;
						}
						myCSV.close();

						string filenameSuperContractor100 = "output/interventions/random100/superContractorFileRandomCare"+ nb_iteration +"_scenario"+ to_string(i) +".csv";
						myCSV.open(filenameSuperContractor100);
						myCSV << "superContractor;name;number;"<<endl;
						for(auto const& elem:superContractorInterm100){
							myCSV << "superContractor;" + elem + ";" + to_string(nb) + ";" <<endl;
						}
						myCSV.close();
					
						nb = nb + 2;

					} 

					// RANDOM PATIENTS ///////
				} else if(superContractorOption3 == "RandomPatients"){

					vector<Individual*> allindiv = fContact->getAllIndividuals();
					vector<Individual*> allstaff;
					vector<string> EVC = {"PA-001-LAM", "PA-003-MEG", "PA-004-MAA", "PA-006-DEC", "PA-007-SIM", "PA-008-SLP", "PA-009-DEJ", "PA-011-REA", "PA-012-LEN", "PA-025-AIA", "PA-026-SEA", "PA-046-DIS", "PA-048-BLH", "PA-057-IBN", "PA-085-MET", "PA-090-JBA"};
					for(auto const& indiv:allindiv){
						if(indiv->getStatus() == "PA"){
							if(std::find(EVC.begin(), EVC.end(), indiv->getCalc_ident()) == EVC.end()){
								allstaff.push_back(indiv);
							}
						}
					}

					vector<Individual*> chosenstaff20 = fContact->chooseCandidatesInsideVector(allstaff, 20);
					vector<Individual*> chosenstaff60 = fContact->chooseCandidatesInsideVector(allstaff, 60);
					vector<Individual*> chosenstaff100 = fContact->chooseCandidatesInsideVector(allstaff, 100);
					
					vector<string> superContractorInterm20;
					vector<string> superContractorInterm60;
					vector<string> superContractorInterm100;

					for(auto const& indiv:chosenstaff20){
						superContractorInterm20.push_back(indiv->getCalc_ident());
					}

					for(auto const& indiv:chosenstaff60){
						superContractorInterm60.push_back(indiv->getCalc_ident());
					}

					for(auto const& indiv:chosenstaff100){
						superContractorInterm100.push_back(indiv->getCalc_ident());
					}

					ofstream myCSV;
					int nb = 2;
					for(int i(1); i<=5; i++){

						string filenameSuperContractor20 = "output/interventions/random20/superContractorFileRandomPatients"+ nb_iteration +"_scenario"+ to_string(i) +".csv";
						myCSV.open(filenameSuperContractor20);
						myCSV << "superContractor;name;number;"<<endl;
						for(auto const& elem:superContractorInterm20){
							myCSV << "superContractor;" + elem + ";" + to_string(nb) + ";" <<endl;
						}
						myCSV.close();

						string filenameSuperContractor60 = "output/interventions/random60/superContractorFileRandomPatients"+ nb_iteration +"_scenario"+ to_string(i) +".csv";
						myCSV.open(filenameSuperContractor60);
						myCSV << "superContractor;name;number;"<<endl;
						for(auto const& elem:superContractorInterm60){
							myCSV << "superContractor;" + elem + ";" + to_string(nb) + ";" <<endl;
						}
						myCSV.close();

						string filenameSuperContractor100 = "output/interventions/random100/superContractorFileRandomPatients"+ nb_iteration +"_scenario"+ to_string(i) +".csv";
						myCSV.open(filenameSuperContractor100);
						myCSV << "superContractor;name;number;"<<endl;
						for(auto const& elem:superContractorInterm100){
							myCSV << "superContractor;" + elem + ";" + to_string(nb) + ";" <<endl;
						}
						myCSV.close();
					
						nb = nb + 2;

					}				

				// REAL SUPERCONTACTORS ////////////////
				} else {
					vector<string> superContractorInterm;
					superContractorInterm = interventionContact->findSuperContractors(fContact, superContractorOption1, superContractorOption2);
					cout << "Les super_contractor sont: \n";
					for(auto const& elem:superContractorInterm){
						cout << elem << " ";
					}
					cout <<endl;
					vector<string> superContractor;
					if(superContractorOption3=="PA" || superContractorOption3=="PE"){
						for(auto const& elem:superContractorInterm){
							if(elem.substr(0,2)==superContractorOption3){
								superContractor.push_back(elem);
							}
						}
					}else{
						superContractor = superContractorInterm;
					}
					//vector<string> EVC = {"PA-001-LAM", "PA-003-MEG", "PA-004-MAA", "PA-006-DEC", "PA-007-SIM", "PA-008-SLP", "PA-009-DEJ", "PA-011-REA", "PA-012-LEN", "PA-025-AIA", "PA-026-SEA", "PA-046-DIS", "PA-048-BLH", "PA-057-IBN", "PA-085-MET", "PA-090-JBA"};
					vector<string> superContractorF;
					//sort(EVC.begin(), EVC.end());
					//sort(superContractor.begin(), superContractor.end());
					//set_difference(superContractor.begin(), superContractor.end(), EVC.begin(), EVC.end(),  back_inserter(superContractorF));
					superContractorF = superContractor;
					cout << "Les super_contractor sont: \n";
					for(auto const& elem:superContractorF){
						cout << elem << " ";
					}
					cout<<endl;
					cout << "Il y a " << superContractorF.size() << " supercontracteurs " << endl;
					
					ofstream myCSV;
					int nb = 2;
					for(int i(1); i<=5; i++){
						string filenameSuperContractor = "output/interventions/superContractorFile"+ suppString +"_scenario"+ to_string(i) +".csv";
						myCSV.open(filenameSuperContractor);
						myCSV << "superContractor;name;number;"<<endl;
						for(auto const& elem:superContractorF){
							myCSV << "superContractor;" + elem + ";" + to_string(nb) + ";" <<endl;
						}
						myCSV.close();
					
						nb = nb + 2;

					}

				}
				
			}
			delete interventionContact;
		}

		delete fContactLearn;
		delete fContact;
		cout << "\n CONTACTS DONE \n" << endl;
	}
	if(useNewContact){
		ctcSim = filenameContact;
			
	}else{
		ctcSim = ctc;
	}
	
	//------------------- LEARNING ------------------------------------//
	bool learnAndCtc = false;
	if((checkFile=="yes" && newContactStatus=="build")){
		//on veut construire les ctc et faire le learning
		learnAndCtc = true;
		ctcSim = filenameContact;
	}
	
	if(world.rank()==0 && checkFile=="yes" && (!workOnContactOnly || learnAndCtc) && !useNewAdmissionOnly){ //checkfile = resultsFromFiles
		
		ifstream is(resultParameters);
		cout << "\n LEARNING IN PROGRESS... \n" << endl;
		string infoB = mainProps->getProperty("pathogen.file");
		Filetreatment *f = new Filetreatment(infoB);
		vector<vector<string>> Mt_path=f->tabInfoPathogen(); 
		vector<string> AllBacteria = f->buildMeAVectorOfPathogens(Mt_path);
		delete f;
		
		Learning* learn;
		if(byType=="no"){
			// learn = new Learning(mainProps, AllBacteria, admin, ctcSim, prlvt, datesFL[0], datesFL[1], nbOfAcquisitionWeekLearning, firstAcqOnly,probaByType, durationByType, seuil, nbCtcInfMinLearning, optionDuration, withLogPAPA, withLogPAPE,withLogPEPA, withLogPEPE,groupFile);
			learn = new Learning(mainProps, AllBacteria, admin, ctcSim, prlvt, datesFL[0], datesFL[1], nbOfAcquisitionWeekLearning, firstAcqOnly,probaByType, durationByType, seuil, nbCtcInfMinLearning, optionDuration, false, false,false, false,groupFile);
		}else{
			// learn = new Learning(mainProps, AllBacteria, admin, ctcSim, prlvt, datesFL[0], datesFL[1], nbOfAcquisitionWeekLearning, firstAcqOnly,probaByType, durationByType, seuil, nbCtcInfMinLearning, optionDuration, withLogPAPA, withLogPAPE,withLogPEPA, withLogPEPE);
			learn = new Learning(mainProps, AllBacteria, admin, ctcSim, prlvt, datesFL[0], datesFL[1], nbOfAcquisitionWeekLearning, firstAcqOnly,probaByType, durationByType, seuil, nbCtcInfMinLearning, optionDuration, false, false,false, false);
		}
		
		vector<string> paramsToFind = {"probability of transmission", "colonization at admission", "admission", "discharge", "admission distribution", "discharge distribution", "duration of colonization", "distribution of duration", "days of week", "length of stay", "number of individual"};
		
		if(sensibilityStatus){
			cout << sensibilityStatus << " and " << sensibSeq << endl;
			vector<string> tempo_sensibility;
			boost::split(tempo_sensibility, sensibSeq , boost::is_any_of("_"),boost::token_compress_on);
			learn->setSensibilityStatus(true);
			cout << learn->getSensibilityStatus() << endl;
			learn->sensibilityON(tempo_sensibility[0],tempo_sensibility[1],stoi(tempo_sensibility[2]));
		}
		
		learn-> resultParameters2(true, resultParameters, paramsToFind);
		
		
		// if(useNewAdmission){
			// cout << " new admission ON " << endl;
			// if(nb_iteration!=""){
				// filenameAdmission =  "output/admission/admissionSim_" + nb_iteration + ".csv";
			// }
			// string datesSimuForAdm =  mainProps->getProperty("simuGameDates");
			// vector<string> datesS;
			// boost::split(datesS,datesSimuForAdm , boost::is_any_of("_"),boost::token_compress_on);
			// learn-> buildADSimulation(resultParameters, datesS[0], datesS[1], filenameAdmission, startWithRealIndividualAtAdmissionSim);
		// }

		delete learn;
		cout << "\n LEARNING DONE \n" << endl;

		
	}
	
	//----------------------------------------- REAL -------------------------------------------------------------------------//
	
	if(realGameON=="yes" && parametersOnly=="no" && !workOnContactOnly && !useNewAdmissionOnly){
		if(world.rank()==0){
			cout << "\n DEBUT DU REAL GAME \n" << endl;
		}
		
		string realGameDates = mainProps->getProperty("realGameDates");
		vector<string> allDates;
		boost::split(allDates,realGameDates , boost::is_any_of("_"),boost::token_compress_on);
		repast::RepastProcess::init(configFile, &world);
		repast::ScheduleRunner& runner = repast::RepastProcess::instance()->getScheduleRunner();
		Model* realGame = new Model(mainProps,argc,argv,&world, allDates[0], allDates[1], "real");
		if(world.rank() == 0){
			cout << "Les fichiers utilisés dans le real sont: \n";
			cout << admin << " \n";
			cout << ctc << " \n";
			cout << prlvt << " \n";
			cout << resultParameters << " \n";
			cout << "Optim simulation: " << optimSimu << "\n";
			cout << endl;
			
		}
		realGame->init(admin,ctc,prlvt,resultParameters, optimSimu);
		realGame->initSchedule(runner);
		runner.run();
		delete realGame;
		repast::RepastProcess::instance()->done();
	}
	world.barrier();
	
	//----------------------------------------- SIMULATION -------------------------------------------------------------------------//
	if(simuGameON=="yes" && parametersOnly=="no" && !workOnContactOnly && !useNewAdmissionOnly){
		string simuGameDates = mainProps->getProperty("simuGameDates");
		vector<string> allDates;
		boost::split(allDates,simuGameDates , boost::is_any_of("_"),boost::token_compress_on);
		if(world.rank()==0){
			cout << "\n DEBUT DE LA SIMULATION \n" << endl;
			cout << " check admission availability " << endl;
			bool checkAD = checkAdmissionLength(admin, allDates[0], allDates[1], filenameAdmission, startWithRealIndividualAtAdmissionSim);
			if(checkAD){
				string adminCIN;
				cout << " give me your training file " << endl;
				cin>>adminCIN;
				string firstD;
				string lastD;
				cout << " give me the first training date " << endl;
				cin>>firstD;
				cout << " give me the last training date " << endl;
				cin>>lastD;
				Learning* learn;
				learn = new Learning(mainProps, adminCIN, firstD, lastD, lastD);
				// Learning* learn;
				// learn = new Learning(admin, allDates[0], allDates[1]);
				vector<string> paramsToFind = {"admission", "length of stay", "admission distribution", "number of individual"};
				learn-> resultParameters2(true, "parameters/admission/resultParametersAD.csv", paramsToFind);
				learn-> buildADSimulation("parameters/admission/resultParametersAD.csv", allDates[0], allDates[1], filenameAdmission, startWithRealIndividualAtAdmissionSim);
				useNewAdmission = true;
				delete learn;
			}
		}
		
		if(useNewAdmission){
			// if(nb_iteration==""){
				admin = mainProps->getProperty("admissionFile");
			// }else{
				// admin = "output/admission/admissionSim_" + nb_iteration + ".csv";
			// }
		}
		if(world.rank() == 0){
			cout << "Les fichiers utilisés dans la simulation sont: \n";
			cout << admin << " \n";
			cout << ctcSim << " \n";
			cout << prlvt << " \n";
			cout << resultParameters << " \n";
			cout << "Optim simulation: " << optimSimu << "\n";
			cout << "Nombre d'itération: " << nb_iteration;
			cout << endl;
			
		}
		
		repast::RepastProcess::init(configFile, &world);
		Model* simuGame = new Model(mainProps,argc,argv,&world, allDates[0], allDates[1], "simu");
		repast::ScheduleRunner& runner = repast::RepastProcess::instance()->getScheduleRunner();
		if(byType=="no"){
			simuGame->setGroupFile(groupFile);
		}
		simuGame->init(admin,ctcSim, prlvt, resultParameters, optimSimu, nb_iteration);
		simuGame->initSchedule(runner);
		runner.run();
		delete simuGame;
		repast::RepastProcess::instance()->done();

	}
	world.barrier();
	
}

