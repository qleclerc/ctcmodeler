/* Model
* this file allow the treatment of the different files
*
*	
*	Model.h
*
*	Created on: Jan 8, 2018
*	Author: Audrey
*
*/

#ifndef MODEL
#define MODEL
#include <boost/iostreams/device/mapped_file.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/type_traits/integral_constant.hpp>
#include <iostream>
#include <fstream>

#include <boost/mpi.hpp>
#include "repast_hpc/Schedule.h"
#include "repast_hpc/Properties.h"
#include "repast_hpc/SharedContext.h"
#include "repast_hpc/SharedNetwork.h"
#include "repast_hpc/AgentRequest.h"
#include "repast_hpc/Properties.h"

#include "Organism.h"
#include "Individual.h"
#include "Pathogen.h"
#include "Filetreatment.h"
#include "Interventions.h"

#include "repast_hpc/TDataSource.h"
#include "repast_hpc/SVDataSet.h"

/*Organism Package Provider*/
class OrganismPackageProvider{
	
	private:
	repast::SharedContext<Organism>* organisms;
	
	public:
	OrganismPackageProvider(repast::SharedContext<Organism>* organismPtr);
	
	void providePackage(Pathogen* pathogen, std::vector<PathogenPackage>& out);
	void provideContent(repast::AgentRequest req, std::vector<PathogenPackage>& out);
	
	void providePackage(Individual* individual, std::vector<IndividualPackage>& out);
	void provideContent(repast::AgentRequest req, std::vector<IndividualPackage>& out);
	
	void providePackage(Bacteria* bacteria, std::vector<BacteriaPackage>& out);
	void provideContent(repast::AgentRequest req, std::vector<BacteriaPackage>& out);
	
	void providePackage(Organism* organism, std::vector<OrganismPackage>& out);
	void provideContent(repast::AgentRequest req2, std::vector<OrganismPackage>& out);
};

/*Organism Package Receiver*/
class OrganismPackageReceiver{
	
	private:
	repast::SharedContext<Organism>* organisms;
	
	public:
	OrganismPackageReceiver(repast::SharedContext<Organism>* organismPtr);
	
	Pathogen * createAgent(PathogenPackage package);
	void updateAgent(PathogenPackage package);
	
	Individual * createAgent(IndividualPackage package);
	void updateAgent(IndividualPackage package);
	
	Bacteria * createAgent(BacteriaPackage package);
	void updateAgent(BacteriaPackage package);
	
	Organism * createAgent(OrganismPackage package);
	void updateAgent(OrganismPackage package);
};


class Model{
	friend class Filetreatment;
	
	private:
	std::map<std::string, std::map<std::string, std::map<std::string, std::vector<std::string>>>> index_second_cases;
	std::map<std::string, std::vector<std::string>> ImmunizedPeople;
	bool fixedColonization;
	int fixedColonizationNb;
	std::string fixedColonizationStatus;
	std::vector<std::string> chosenAdmittedPatient;
	std::map<std::string, std::vector<std::string>> chosenFixedColonizedStaff;
	std::vector<std::string> chosenFixedColonizedStaffThisWeek;
	bool swabPeople;
	std::string stringCatStart;
	std::string stringHospStart;
	std::vector<std::string> catStartV;
	std::vector<std::string> hospStartV;
	bool InitializedPatientAtAdmission;
	bool InitializedStaffAtAdmission;
	std::string probaThreshold;
	int weeklyColStaff = 0;
	bool aleaLevel;
	bool returnPrev;
	bool returnAcq;
	bool returnSwab;
	bool returnStatusByDay;
	std::string incubationPeriod;
	std::string incubationPeriodWithContagion;
	bool immunization;
	std::string immunizationDuration;
	std::string immunizationVAR;
	std::string asymptomatic;
	std::string severity;
	std::map<std::string, std::map<std::string, std::vector<std::pair<std::string, std::string>>>> statusMap;
	std::map<std::string, std::map<std::string, double>> mapProb;
	std::map<std::string, std::vector<std::string>> mapPreviousSwab;
	//analyse de sensibilité
	std::map<std::string, std::string> groups;
	std::string groupFile="";
	//interventions
	bool interventions = false;
	bool compliance = false;
	bool vaccination = false;
	bool superC = false;
	std::map<std::pair<std::string, std::string>, int> complianceCouple;
	std::map<std::string, int> superContractors;
	//prlvt
	//type - jour - mean - variance
	std::map<std::string, std::map<int, std::pair<double,double>>> mapDayRandom; //map des paramètres pour prélever les individus à la semaine
	//date - vecteur d'individus	
	std::map<std::string, std::vector<std::string>> presence; //map avec la liste des individus présent chaque semaine
	//pathogen - date - type - colonisé - prélevé
	std::map<std::string, std::map<std::string, std::map<std::string, std::pair<int, int>>>> mapPrevalenceWeek; //map avec la prevalence par semaine
	//pathogen - date - type - acquisition - negatif
	std::map<std::string, std::map<std::string, std::map<std::string, std::pair<int, int>>>> mapAcquisitionWeek;
	//pathogen - date - individuals
	std::map<std::string, std::map<std::string, std::vector<std::string>>> mapAcquisitionWeekInd;
	//date - vecteur d'individu en string
	std::map<std::string, std::vector<std::string>> newAdmission; //map avec les nouvelles admissions
	std::vector<std::string> nonWorkingDays; //vecteur des jours fériés et des ponts en France
	std::string fileNameResult; //nom du fichier learning avec les paramètres pour le model
	std::string optionSimu; //option pour multiple simu (un peu moins utiliser aujourd'hui)
	std::vector<std::string> allPeopleAcquisition; //liste des individus qui ont eu une acquisition
	std::vector<std::string> peopleAcquisition; //liste des individus qui ont eu une acquisition
	std::string distributionProb; //option pour la distribution des probabilités de durée de colonisation
	bool prevalenceByWard; //option pour savoir si la prévalence est rendu par service
	bool firstAcqOnly; //option qui ne prend en compte que la première acquisition d'un individu dans le décompte du nombre d'acquisition
	bool probaByType; //option pour savoir si les probabilités de transmission sont prises en compte par type (sinon c'est par groupe et c'est uniquement dans l'analyse de sensibilité actuellement)
	bool durationByType; //option pour savoir si la durée de colonisation est donnée par type (actuellement seulement donnée par type pas autre chose)
	bool logPAPA;//option pour savoir si dans le calcule des proba on utilise le seuil
	bool logPAPE;
	bool logPEPA;
	bool logPEPE;
	string numberOfSim; //numéro de la simulation actuel
	std::map<std::string, std::map<std::string, std::vector<double>>> mapPrevalence; //OBSOLETTE utiliser dans gnuplot mais plus d'actualité, je le laisse au cas où
	//calc_ident - Id repast
	std::map<std::string, repast::AgentId> mapId; //map avec les id des organismes
	std::string useOfPreviousPrlvt; //option pour savoir si on utilise le fichier de prlvt pour initialiser les individus et les bactéries
	bool debugMode; //option pour afficher les messages de debug 
	std::vector<std::string> allBacteriaToAnalyzed; //vecteur des bacteries a analyser
	std::string gameStatus; //statut simu ou real
	Parameters* parameters; //objet parameters
	repast::SharedContext<Organism> context; //context dans lequel sont stocker les objects organismes (c'est un contener avec les objets physique)
	vector < Individual* > movingPeople; //vecteur d'individu avec les individus qui sont sorti de l'hopital
	repast::SVDataSet* dataSet; //OBSOLETTE option pour dataset 
	vector<vector<string>> Mt; // vecteur de vecteur avec les résultats des admissions des individus
	repast::Properties* props; //fichier de propriété (obligatoire pour repast)
	vector<int> pathCounts; //decompte pour les id pathogen
	vector<int> indCounts; //decompte pour les id individus
	std::vector<std::string> wards; //vecteur avec les services de l'hopital
	std::multimap<std::string,std::vector<std::string>> previousPrlvt; //multimap avec les résultats de prélèvements
	std::map<std::string,std::vector<Contact*>>previousCtc; //map avec les anciens contacts
	int rank; //rank
	int countOfIndividuals;
	std::vector<std::vector<std::string>> Mt_path; //vecteur de vecteur avec les caractéristiques des bactéries à analyser
	long int stopAt; //time stop
	int tickDay; 
	int timeResp; 
	int tickPath;
	std::string startDate;
	std::string stopDate;
	time_t timStart;
	std::string enteroATB; //liste en string des ATB
	std::string staphATB; //liste en string des ATB
	std::ifstream ifs_ctc; 
	std::vector<std::string> tempo_ctc;
	std::ifstream ifs_prlvt;
	std::vector<std::string> tempo_prlvt;
	std::ofstream myCSVacquisition;
	std::ofstream myCSVprevalence;
	std::ofstream myCSVswab;
	//date - calc_ident - vecteur de date
	std::map<std::string, std::map<std::string, std::vector<std::string>>> mapAdmission; //map des admissions
	//date - calc_ident - vecteur de date
	std::map<std::string, std::map<std::string, std::vector<std::string>>> mapDischarge;
	Individual* previousSource = nullptr;
	Individual* previousTarget = nullptr;
	int nbOfAcquisitionWeek;
	//date - vecteur de calc_ident
	std::map<std::string, std::vector<std::string>> mapPrlvt; //map avec les prlvt renouvellée toutes les semaines
	//date - vecteur de calc_ident
	std::map<std::string, std::vector<std::string>> mapPrlvtALL; //map avec tous les prlvt de toutes la période
	boost::random::mt19937 eng;
	int manualDuration; //durée de colonisation manuelle
	bool boolManDuration = false; //booleen pour savoir si la durée de colo est manuelle
	//date - vecteur de calc_ident
	std::map<std::string, std::vector<std::string>> coloAtAdmi; //map avec les individus colonisé à l'admission
	//bacteria - vecteur de calc_ident
	std::map<std::string, std::vector<std::string>> colonizedIndividuals; //map avec les individus colonisé
	OrganismPackageProvider* provider; 
	OrganismPackageReceiver* receiver;
	repast::RepastEdgeContentManager <Organism> edgeContentManager;
	repast::SharedNetwork<Organism,repast::RepastEdge<Organism>,repast::RepastEdgeContent<Organism>,repast::RepastEdgeContentManager<Organism> >* organismNetwork;
	void initializeFirstImmunized(double percentagePA, double percentagePE);
	
	public:
		void buildSecondaryCases();
		//constructeur
		Model(repast::Properties* mainProps, int argc, char** argv, boost::mpi::communicator* comm, std::string startD, std::string stopD, std::string gameStat);
		//destructeur
		~Model();
		//initialisation
		void init(std::string admin, std::string ctc, std::string prlvt, std::string fileNameResult = "",std::string optionSimu = "no", std::string numberOfSim = "");
		int InitializeWithBacteriaToAnalyzed();
		int initializeIndividuals(std::string admin);
		void initializeFirstPathogens();
		void afficheIndividuals();
		void affichePathogens();
		//check
		void checkContact();
		void checkAdmission();
		void checkDischarge();
		void checkPrlvt();
		void checkInside(std::string name, std::string typeOrga);
		void checkClear();
		//repast
		void requestOrganism(std::vector<int> previousindCounts, int typeId, std::string setId);
		void initSchedule(repast::ScheduleRunner& runner);
		void cancelAgentRequests(); //Jamais utilisé mais peut être utile plus tard
		void synchInd();
		void synchPath();
		void synchronizeOrganism();
		void testLocalNonLocal(); //fonction de test, plus utiliser mais illustre les copies repast
		void afficheId(int typeP);
		//pathogen
		std::vector<std::string> findChosenOne(int nbOfPeople, std::string type, std::string bacteria);
		void chooseIndForSimu(int nbOfPeople);
		void chooseIndForSimu(int nbOfInd, int nbOfPatient, int nbOfStaff);
		int firstPathogen();
		void addFirstPathogen(Individual* ind, std::vector<std::string> tmp);
		void clearPathogenacquisition();		
		std::vector<Organism*> selectBacteria(std::vector<Organism*> listOfB);
		void testActualPatho();
		void removeIndFromPathogens(Individual* m1, std::vector<Organism*> PathogensContext);
		//contact
		void testNetwork();
		//Dates and time
		int findTickDay();
		std::string currentDate();
		static std::string dateToString(time_t tm);
		static time_t stringToDate(std::string st);
		time_t stringToDateWithHour(std::string st);
		std::vector<std::string> findAbscenceDay(std::string ind);
		std::string dateOfEaster(int year);
		std::vector<std::string> findMeNonWorkingDays(std::string startD, std::string stopD, bool bridge);
		std::vector<std::string> frenchBridge(std::vector<std::string> input_vector);
		std::string findDate(int const&nbOfDay);
		int findTimeResponse();
		std::vector<std::string> findDateBetweenTwoDays(std::string firstDay, std::string secondDay);
		bool absencePeriod(std::string ind, std::string previousDay, std::string actualDay);
		//cat 
		std::map<std::string, std::vector<std::string>> mapCat(std::vector<std::string> common);
		std::string returnCat(Individual* ind);
		std::vector<std::string> findIndividualFromThisCat(std::string nature, bool old);
		//acquisition
		void testAc();
		void showMeAcquisition(); //OBSOLETTE
		std::map<string, std::map<string, int>> getNumberOfAcquisition(); //OBSOLETTE
		void buildMeAcquisition();
		void getNumberOfAcquisitionWeeks();
		void getAcquisitionWeeks(std::string bacteria);
		std::pair<std::map<std::string, int>, std::vector<std::string>> findMeAcquisitionWeek(std::vector<Organism*> listOfI, std::string bacteria, std::vector<std::string> weekDates);
		std::map<std::string, std::vector<std::string>> findMePreviousNegIndividual(std::string date, std::string pathogen, int nbOfWeek);
		void EndOfAcquisition();
		bool acquisitionStatus(Individual* ind, std::vector<std::string> weekDates, string bacteria,bool accountPreviousSwab);
		//prevalence
		void drawMePrevalence();
		void buildMePrevalence();
		std::map<std::string, double> getPrevalenceByWeeks(std::string bacteria, std::string ward);
		std::map<std::string, double> returnMapPrevRes(std::vector<Organism*>colonizedIndividual, std::vector<Organism*> listOfI, std::string ward, std::string path);
		std::map<std::string, double> getPrevalenceNow(std::string bacteria, std::string ward);
		int nbOf(std::vector<Organism*> o, std::string cat, std::string ward);
		std::map<std::string, std::map<std::string, std::pair<int, int>>> changeMapPrevalence(std::map<std::string, std::pair<int, int>> input_map);
		//transmission
		std::vector<std::string> infectiousPeriod(std::string name, Bacteria* b); //plus utilisé
		void transmissionByContact(Individual* source, Individual* target, std::string length);
		double findProba(Individual* transmitter, Individual* receiver, std::map<std::string, double> mapProbBacteria);
		double findProba2(Individual* transmitter, Individual* receiver, std::map<std::string, std::vector<double>> mapProbBacteria);
		//admission
		int addAdmission(int previousidI);
		std::map<std::string, std::string> colonizedAtAdmission(Individual* indObj);
		void addNewCol(Individual* indObj, std::map<std::string, std::string> newB);
		void addNewCol2(Individual* indObj, Bacteria* bacteriaObject);
		void addColoAtAdmission(std::vector<std::string> newIndividual);
		std::map<int, std::map<std::string, int>> buildMeNbOfColoAtAdmission();
		//prlvt
		void findMapDayRandom();
		void mapPrlvtSimu();
		void checkMapPrlvt();
		std::pair<std::map<std::string, int>, std::map<std::string, std::map<int, int>>> findNbOfIndToSwab(std::map<string, std::map<int, std::pair<double,double>>> mapDayRandom);
		void buildMeACompleteSwabFile();
		bool zeroMap(std::map<int,int> input_map);
		void cleanIndividualSwab();
		//statistics
		int findQuantile(std::vector<int> inputVector, double q);
		double randomGamma(double mean, double variance, boost::mt19937& rng);
		double randomExpo(double mean, boost::mt19937& rng);
		double findRandomNb();
		//colonisation/decolonisation
		std::string findDurationOfColonization(std::string pathogen, std::string actualDate, std::string type);
		void decolonization();
		//Analyse de sensibilité - test de plusieurs groupes de PE pour la probabilité de transmission
		std::map<std::string, std::string> buildGroup(std::string file_);
		std::string getGroupFile(){return groupFile;}
		void setGroupFile(std::string groupFile_){groupFile=groupFile_;}
		//other
		std::vector<std::string> stringToVectorProps(std::string propsName, std::string delim);
		void copyToMovingPeople(Individual* m1);
		void compteur();
		void testGnuPlot(); //OBSOLETTE quand je voulais tester gnuplot (ne sert à rien actuellement)		
		void showMe();
		void showMeMovingPeople();
		void afficheColonized();
		void showMeOutputPrevAndAcq();
		bool FileExists(const std::string& name);
		void colonizationStatus();
		void buildMeStatusByDay();
		void changePathogensStatusFromIndividuals();
		void changeIndividualPathogenStatus(Individual* ind, std::string bacteria, std::string level, std::string date);
		std::string findStartLevel(bool aleaLevel=false);
		int returnUnif(std::string x);
		void newColonizedIndividual(int nbOfWeekCol);
		bool checkCdtAdm(Individual* ind);
		void checkFixedColonizedStaff();
		void choseFixedColonizedIndividual();
		std::vector<std::string> findRandomInAList(int nb, std::vector<std::string> input_vector);
		void addManuportage(Individual* ind);
};

enum Unit
{
    second = 86400, minute = 1440, hour = 24
};

enum TimeResponse
{
    month = 30, week = 7, day = 1
};

class DataSource_acquisition : public repast::TDataSource<int>{
	/* Vieille fonction test pour l'acquisition mais plus d'actualité. Je la laisse au cas où car c'est un cas type de repast */
	private:
		repast::SharedContext<Organism>* context;
		std::vector<std::string>* vP;
	public:
		DataSource_acquisition(repast::SharedContext<Organism>* c, std::vector<std::string>* v);
		int getData();
};

#endif